package com.example.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.models.ExtensionStatus;
import com.example.models.POStatus;
import com.example.models.PlantRepository;
import com.example.models.PurchaseOrder;
import com.example.models.PurchaseOrderExtension;
import com.example.models.PurchaseOrderExtensionRepository;
import com.example.models.PurchaseOrderRepository;

@RestController
@RequestMapping("/generated")
public class ActualController {
	@Autowired
	PlantRepository plantRepo;

	@Autowired
	PurchaseOrderRepository purchaseOrderRepo;
	
	@Autowired
	PurchaseOrderExtensionRepository purchaseOrderExtensionRepo;
	
	@RequestMapping(value="/pos",method=RequestMethod.POST)
	public ResponseEntity<PurchaseOrder> createPO(@RequestBody PurchaseOrder po) {
		po.setPoStatus(POStatus.PENDING);
		po.calculateCost();
		po = purchaseOrderRepo.save(po);
		po.add(new Link("/pos/" + po.get_id()));
		
		HttpHeaders headers = new HttpHeaders();
		headers.add("Location", po.getId().getHref());
		
		return new ResponseEntity<PurchaseOrder>(po, headers, HttpStatus.valueOf(201));
	}
	
	@RequestMapping(value="/pos/{id}/accept",method=RequestMethod.POST)
	public ResponseEntity<PurchaseOrder> acceptPO(@PathVariable Long id) throws Exception{
		PurchaseOrder po = purchaseOrderRepo.findOne(id);
		po.setPoStatus(POStatus.OPEN);
		po = purchaseOrderRepo.save(po);
		
		HttpHeaders headers = new HttpHeaders();
		
		return new ResponseEntity<PurchaseOrder>(po, headers, HttpStatus.valueOf(200));
	}
	
	@RequestMapping(value="/pos/{id}/accept",method=RequestMethod.DELETE)
	public ResponseEntity<PurchaseOrder> rejectPO(@PathVariable Long id) throws Exception{
		PurchaseOrder po = purchaseOrderRepo.findOne(id);
		po.setPoStatus(POStatus.REJECTED);
		po = purchaseOrderRepo.save(po);
		
		HttpHeaders headers = new HttpHeaders();
		
		return new ResponseEntity<PurchaseOrder>(po, headers, HttpStatus.valueOf(200));
	}
	
	@RequestMapping(value="/pos/{id}/cancel",method=RequestMethod.DELETE)
	public ResponseEntity<PurchaseOrder> cancelPO(@PathVariable Long id) throws Exception{
		PurchaseOrder po = purchaseOrderRepo.findOne(id);
		po.setPoStatus(POStatus.CANCELLED);
		po = purchaseOrderRepo.save(po);
		
		HttpHeaders headers = new HttpHeaders();
		
		return new ResponseEntity<PurchaseOrder>(po, headers, HttpStatus.valueOf(200));
	}
	
	@RequestMapping(value="/pos/{id}",method=RequestMethod.DELETE)
	public ResponseEntity<PurchaseOrder> closePO(@PathVariable Long id) throws Exception{
		PurchaseOrder po = purchaseOrderRepo.findOne(id);
		po.setPoStatus(POStatus.CLOSED);
		po = purchaseOrderRepo.save(po);
		
		HttpHeaders headers = new HttpHeaders();
		
		return new ResponseEntity<PurchaseOrder>(po, headers, HttpStatus.valueOf(200));
	}
	
	@RequestMapping(value="/pos/{id}",method=RequestMethod.PUT)
	public ResponseEntity<PurchaseOrder> updatePO(@RequestBody PurchaseOrder poNew, @PathVariable Long id) throws Exception{
		PurchaseOrder po = purchaseOrderRepo.findOne(id);
		po.setPlant(poNew.getPlant());
		po.setStartDate(poNew.getStartDate());
		po.setEndDate(poNew.getEndDate());
		po.calculateCost();
		po.setPoStatus(POStatus.PENDING);
		po = purchaseOrderRepo.save(po);
		
		HttpHeaders headers = new HttpHeaders();
		
		return new ResponseEntity<PurchaseOrder>(po, headers, HttpStatus.valueOf(200));
	}
	
	@RequestMapping(value="/pos/{_poid}/poext",method=RequestMethod.POST)
	public ResponseEntity<PurchaseOrderExtension> createPOExtension(@RequestBody PurchaseOrderExtension poex, @PathVariable Long _poid) throws Exception{
		poex.setExtStatus(ExtensionStatus.PENDING);
		poex = purchaseOrderExtensionRepo.save(poex);
		
		poex.add(new Link("/pos/"+_poid+"/poext/" + poex.get_id()));
		
		HttpHeaders headers = new HttpHeaders();
		headers.add("Location", poex.getId().getHref());
		return new ResponseEntity<PurchaseOrderExtension>(poex, headers, HttpStatus.valueOf(201));
	}
	
	@RequestMapping(value="/pos/{_id}/poext/{_extid}/accept",method=RequestMethod.POST)
	public ResponseEntity<PurchaseOrder> acceptPOExtension(@PathVariable Long _id, @PathVariable Long _extid) throws Exception{
		PurchaseOrderExtension poExtension = purchaseOrderExtensionRepo.findOne(_extid);
		poExtension.setExtStatus(ExtensionStatus.ACCEPTED);
		poExtension = purchaseOrderExtensionRepo.save(poExtension);
		
		PurchaseOrder po = purchaseOrderRepo.findOne(_id);
		po.setEndDate(poExtension.getEndDate());
		po = purchaseOrderRepo.save(po);
		
		HttpHeaders headers = new HttpHeaders();
		return new ResponseEntity<PurchaseOrder>(po, headers, HttpStatus.valueOf(200));
	}
	
	@RequestMapping(value="/pos/{_id}/poext/{_extid}/accept",method=RequestMethod.DELETE)
	public ResponseEntity<PurchaseOrder> rejectPOExtension(@PathVariable Long _id, @PathVariable Long _extid) throws Exception{
		PurchaseOrderExtension poExtension = purchaseOrderExtensionRepo.findOne(_extid);
		poExtension.setExtStatus(ExtensionStatus.REJECTED);
		poExtension = purchaseOrderExtensionRepo.save(poExtension);
		
		PurchaseOrder po = purchaseOrderRepo.findOne(_id);
	
		HttpHeaders headers = new HttpHeaders();
		return new ResponseEntity<PurchaseOrder>(po, headers, HttpStatus.valueOf(200));
	}
}
