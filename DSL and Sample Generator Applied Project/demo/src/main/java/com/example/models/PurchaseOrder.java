package com.example.models;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;

import org.springframework.hateoas.ResourceSupport;

import lombok.Getter;
import lombok.Setter;
import net.karneim.pojobuilder.GeneratePojoBuilder;

@Entity
@Getter @Setter
@GeneratePojoBuilder
public class PurchaseOrder extends ResourceSupport {
	@org.springframework.data.annotation.Id
	@Id @GeneratedValue
	Long _id;
	
	@OneToOne
	Plant plant;
	LocalDate startDate;
	LocalDate endDate;
	Double cost;
	@Enumerated(EnumType.STRING)
	POStatus poStatus;
	
	public Long get_id() {
		return _id;
	}
	public void set_id(Long _id) {
		this._id = _id;
	}
	public Plant getPlant() {
		return plant;
	}
	public void setPlant(Plant plant) {
		this.plant = plant;
	}
	public LocalDate getStartDate() {
		return startDate;
	}
	public void setStartDate(LocalDate startDate) {
		this.startDate = startDate;
	}
	public LocalDate getEndDate() {
		return endDate;
	}
	public void setEndDate(LocalDate endDate) {
		this.endDate = endDate;
	}
	public Double getCost() {
		return cost;
	}
	public void setCost(Double cost) {
		this.cost = cost;
	}
	public POStatus getPoStatus() {
		return poStatus;
	}
	public void setPoStatus(POStatus poStatus) {
		this.poStatus = poStatus;
	}
	@Override
	public String toString() {
		return "PurchaseOrder [_id=" + _id + ", plant=" + plant + ", startDate=" + startDate + ", endDate=" + endDate
				+ ", cost=" + cost + ", poStatus=" + poStatus + "]";
	}
	public void calculateCost() {
		setCost(ChronoUnit.DAYS.between(startDate,endDate) * plant.getPrice());
	}
	
}
