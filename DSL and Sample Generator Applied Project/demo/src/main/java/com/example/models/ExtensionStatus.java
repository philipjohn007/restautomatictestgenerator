package com.example.models;

public enum ExtensionStatus {
	PENDING, ACCEPTED, REJECTED
}
