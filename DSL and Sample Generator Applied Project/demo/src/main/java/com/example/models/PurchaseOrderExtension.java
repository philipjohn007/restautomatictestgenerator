package com.example.models;

import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import org.springframework.hateoas.ResourceSupport;

import lombok.Getter;
import lombok.Setter;
import net.karneim.pojobuilder.GeneratePojoBuilder;

@Entity
@Getter @Setter
@GeneratePojoBuilder
public class PurchaseOrderExtension extends ResourceSupport {
	@org.springframework.data.annotation.Id
	@Id @GeneratedValue
	Long _id;
	LocalDate endDate;
	ExtensionStatus extStatus;
	
	public Long get_id() {
		return _id;
	}
	public void set_id(Long _id) {
		this._id = _id;
	}
	public LocalDate getEndDate() {
		return endDate;
	}
	public void setEndDate(LocalDate endDate) {
		this.endDate = endDate;
	}
	public ExtensionStatus getExtStatus() {
		return extStatus;
	}
	public void setExtStatus(ExtensionStatus extStatus) {
		this.extStatus = extStatus;
	}
	@Override
	public String toString() {
		return "PurchaseOrderExtension [_id=" + _id + ", endDate=" + endDate + "]";
	}
}
