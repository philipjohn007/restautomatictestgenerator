package com.example.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import lombok.Data;
import net.karneim.pojobuilder.GeneratePojoBuilder;

@Entity
@Data
@GeneratePojoBuilder
public class Plant {
	@org.springframework.data.annotation.Id
	@Id @GeneratedValue
	Long _id;
	String name;
	String description;
	Double price;
	public Long get_id() {
		return _id;
	}
	public void set_id(Long _id) {
		this._id = _id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}
	@Override
	public String toString() {
		return "Plant [_id=" + _id + ", name=" + name + ", description=" + description + ", price=" + price + "]";
	}
}
