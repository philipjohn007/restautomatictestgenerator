package com.example.models;

import org.springframework.data.repository.CrudRepository;

public interface MockPurchaseOrderRepository extends CrudRepository<PurchaseOrder, Long>{

}
