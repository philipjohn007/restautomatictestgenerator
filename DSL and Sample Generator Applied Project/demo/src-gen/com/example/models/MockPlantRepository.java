package com.example.models;

import org.springframework.data.repository.CrudRepository;

public interface MockPlantRepository extends CrudRepository<Plant, Long>{

}
