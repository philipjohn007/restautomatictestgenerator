package com.example.models;

import org.springframework.data.repository.CrudRepository;

public interface MockPurchaseOrderExtensionRepository extends CrudRepository<PurchaseOrderExtension, Long>{

}
