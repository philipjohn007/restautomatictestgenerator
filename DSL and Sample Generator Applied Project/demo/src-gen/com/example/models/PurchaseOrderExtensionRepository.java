package com.example.models;

import org.springframework.data.jpa.repository.JpaRepository;

public interface PurchaseOrderExtensionRepository extends JpaRepository<PurchaseOrderExtension, Long>{

}
