package com.example;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.expression.*;
import org.springframework.expression.common.TemplateParserContext;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.expression.spel.support.StandardEvaluationContext;
import org.springframework.stereotype.Component;
import com.example.models.Plant;
import com.example.models.PlantRepository;
import com.example.models.PurchaseOrder;
import com.example.models.PurchaseOrderRepository;
import com.example.models.PurchaseOrderExtension;
import com.example.models.PurchaseOrderExtensionRepository;
import com.fasterxml.jackson.databind.*;
import com.github.fge.jsonpatch.JsonPatch;
import com.github.fge.jsonpatch.mergepatch.JsonMergePatch;

@Component
public class SkeletonHelper {
	@Autowired
	PlantRepository plantRepo;

	@Autowired
	PurchaseOrderRepository purchaseOrderRepo;

	@Autowired
	PurchaseOrderExtensionRepository purchaseOrderExtensionRepo;

	ObjectMapper mapper = new ObjectMapper().findAndRegisterModules();
	
	StandardEvaluationContext spelContext;
	ExpressionParser spelParser;
	
	class LocalSpelContext {
		public PlantRepository $Plants = plantRepo;
		public PurchaseOrderRepository $PurchaseOrders = purchaseOrderRepo;
		public PurchaseOrderExtensionRepository $PurchaseOrderExtensions = purchaseOrderExtensionRepo;
		public String $toJson(Object o) throws Exception {
			return mapper.writeValueAsString(o);
		}		
	}
	
	public JsonNode $mergePatch(JsonNode obj, String json) throws Exception {
		JsonMergePatch mp = mapper.readValue(json, JsonMergePatch.class);
		return mp.apply(obj);
	}

	public JsonNode $patch(JsonNode obj, String json) throws Exception {
		JsonPatch mp = mapper.readValue(json, JsonPatch.class);
		return mp.apply(obj);
	}
	
	String[] plantFixtures = {
		"{\"_id\":\"1\",\"name\":\"Mini excavator\",\"description\":\"Excavator 1.5 tons\",\"price\":\"100.00\"}",
		"{\"_id\":\"2\",\"name\":\"Mini excavator\",\"description\":\"Excavator 2.5 tons\",\"price\":\"120.00\"}",
		"{\"_id\":\"3\",\"name\":\"Midi excavator\",\"description\":\"Excavator 3.0 tons\",\"price\":\"150.00\"}",
		"{\"_id\":\"4\",\"name\":\"Maxi excavator\",\"description\":\"Excavator 5.0 tons\",\"price\":\"200.00\"}",
	};
	String[] purchaseOrderFixtures = {
		"{\"_id\":\"1\",\"plant\":#{$toJson($Plants.findOne(1l))},\"startDate\":\"2016-02-29\",\"endDate\":\"2016-03-18\",\"cost\":\"2400.00\",\"poStatus\":\"PENDING\"}",
		"{\"_id\":\"2\",\"plant\":#{$toJson($Plants.findOne(2l))},\"startDate\":\"2016-02-29\",\"endDate\":\"2016-03-18\",\"cost\":\"2400.00\",\"poStatus\":\"OPEN\"}",
	};
	String[] purchaseOrderExtensionFixtures = {
		"{\"_id\":\"1\",\"endDate\":\"2016-03-18\",\"extStatus\":\"PENDING\"}",
	};
//
	public void setupBackground() throws Exception {
		spelContext = new StandardEvaluationContext(new LocalSpelContext());
		spelParser = new SpelExpressionParser();
		spelContext.registerFunction("$toJson", LocalSpelContext.class.getDeclaredMethod("$toJson", new Class[] { Object.class }));
//
		for (String json: plantFixtures) {
			Expression expression = spelParser.parseExpression(json, new TemplateParserContext());
			String rewrittenJson = expression.getValue(spelContext, String.class);
			plantRepo.save(mapper.readValue(rewrittenJson, Plant.class));
		}
//
		for (String json: purchaseOrderFixtures) {
			Expression expression = spelParser.parseExpression(json, new TemplateParserContext());
			String rewrittenJson = expression.getValue(spelContext, String.class);
			purchaseOrderRepo.save(mapper.readValue(rewrittenJson, PurchaseOrder.class));
		}
//
		for (String json: purchaseOrderExtensionFixtures) {
			Expression expression = spelParser.parseExpression(json, new TemplateParserContext());
			String rewrittenJson = expression.getValue(spelContext, String.class);
			purchaseOrderExtensionRepo.save(mapper.readValue(rewrittenJson, PurchaseOrderExtension.class));
		}
//
	}
//
	public PurchaseOrder getCreatePOData() throws Exception{
		Expression expression = spelParser.parseExpression("{\"plant\": #{$toJson($Plants.findOne(1l))}, \"startDate\": \"2016-02-29\", \"endDate\": \"2016-03-04\"}", new TemplateParserContext());
		String expressionResult = expression.getValue(spelContext, String.class);
		PurchaseOrder _purchaseOrder = mapper.readValue(expressionResult, PurchaseOrder.class);
		return _purchaseOrder;
	}
	
//
	public PurchaseOrder getAcceptPOData() throws Exception{
		Expression expression = spelParser.parseExpression("#{$PurchaseOrders.findOne(1L)}", new TemplateParserContext());
		PurchaseOrder _purchaseOrder = expression.getValue(spelContext, PurchaseOrder.class);
		return _purchaseOrder;
	}
	
//
	public PurchaseOrder getRejectPOData() throws Exception{
		Expression expression = spelParser.parseExpression("#{$PurchaseOrders.findOne(1L)}", new TemplateParserContext());
		PurchaseOrder _purchaseOrder = expression.getValue(spelContext, PurchaseOrder.class);
		return _purchaseOrder;
	}
	
//
	public PurchaseOrder getCancelPOData() throws Exception{
		Expression expression = spelParser.parseExpression("#{$PurchaseOrders.findOne(1L)}", new TemplateParserContext());
		PurchaseOrder _purchaseOrder = expression.getValue(spelContext, PurchaseOrder.class);
		return _purchaseOrder;
	}
	
//
	public PurchaseOrder getClosePOData() throws Exception{
		Expression expression = spelParser.parseExpression("#{$PurchaseOrders.findOne(1L)}", new TemplateParserContext());
		PurchaseOrder _purchaseOrder = expression.getValue(spelContext, PurchaseOrder.class);
		return _purchaseOrder;
	}
	
//
	public PurchaseOrder getUpdatePOData() throws Exception{
		Expression expression = spelParser.parseExpression("{\"plant\": #{$toJson($Plants.findOne(1l))}, \"startDate\": \"2016-03-01\", \"endDate\": \"2016-03-04\"}", new TemplateParserContext());
		String expressionResult = expression.getValue(spelContext, String.class);
		PurchaseOrder _purchaseOrder = mapper.readValue(expressionResult, PurchaseOrder.class);
		return _purchaseOrder;
	}
	
	public PurchaseOrder getUpdatePOData2() throws Exception{
		Expression expression = spelParser.parseExpression("#{$PurchaseOrders.findOne(1L)}", new TemplateParserContext());
		PurchaseOrder _purchaseOrder = expression.getValue(spelContext, PurchaseOrder.class);
		return _purchaseOrder;
	}
//
	public PurchaseOrderExtension getCreatePOExtensionData() throws Exception{
		Expression expression = spelParser.parseExpression("{\"endDate\": \"2016-03-10\"}", new TemplateParserContext());
		String expressionResult = expression.getValue(spelContext, String.class);
		PurchaseOrderExtension _purchaseOrderExtension = mapper.readValue(expressionResult, PurchaseOrderExtension.class);
		return _purchaseOrderExtension;
	}
	
//
	public PurchaseOrder getAcceptPOExtensionData() throws Exception{
		Expression expression = spelParser.parseExpression("#{$PurchaseOrderExtensions.findOne(_extid)}", new TemplateParserContext());
		PurchaseOrder _purchaseOrder = expression.getValue(spelContext, PurchaseOrder.class);
		return _purchaseOrder;
	}
	
//
	public PurchaseOrder getRejectPOExtensionData() throws Exception{
		Expression expression = spelParser.parseExpression("#{$PurchaseOrderExtensions.findOne(_extid)}", new TemplateParserContext());
		PurchaseOrder _purchaseOrder = expression.getValue(spelContext, PurchaseOrder.class);
		return _purchaseOrder;
	}
	
}
