package com.example;

import org.junit.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.*;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.*;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.example.models.*;
import com.fasterxml.jackson.databind.ObjectMapper;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.hamcrest.CoreMatchers.equalTo;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = DemoApplication.class)
@WebAppConfiguration
@DirtiesContext
public class SkeletonTest {
	@Autowired
	SkeletonHelper helper;
				
	@Autowired
	private WebApplicationContext wac;

	@Autowired
	@Qualifier("_halObjectMapper")
	ObjectMapper mapper;

	private MockMvc mockMvc;

	@Before
	public void setup() throws Exception {
		this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
		helper.setupBackground();
		mockMvc.perform(post("/generated/initialize"));
	}


	@Test
	public void testCreateAcceptAndExtendPurchaseOrder() throws Exception {
		MvcResult result = null;
		PurchaseOrder _purchaseOrder = null;
		PurchaseOrder purchaseOrder = null;
//
		purchaseOrder = helper.getCreatePOData();
		result = mockMvc.perform(post("/generated/pos")
								.header("keepAlive", true)
								.content(mapper.writeValueAsString(purchaseOrder))
								.contentType(MediaType.APPLICATION_JSON))
								.andExpect(status().is(201))
								.andReturn();
		
		_purchaseOrder = mapper.readValue(result.getResponse().getContentAsString(), PurchaseOrder.class);
		Assert.assertThat(_purchaseOrder.getPoStatus().toString(), equalTo("PENDING"));
		
//
		result = mockMvc.perform(post("/generated/pos/{id}/accept",_purchaseOrder.get_id())
								.header("keepAlive", true))
								.andExpect(status().is(200))
								.andReturn();
		
		_purchaseOrder = mapper.readValue(result.getResponse().getContentAsString(), PurchaseOrder.class);
		Assert.assertThat(_purchaseOrder.getPoStatus().toString(), equalTo("OPEN"));
		
//		
		PurchaseOrderExtension _purchaseOrderExtension = null;
		PurchaseOrderExtension purchaseOrderExtension = null;
//
		purchaseOrderExtension = helper.getCreatePOExtensionData();
		result = mockMvc.perform(post("/generated/pos/{_poid}/poext",_purchaseOrder.get_id())
								.header("keepAlive", true)
								.content(mapper.writeValueAsString(purchaseOrderExtension))
								.contentType(MediaType.APPLICATION_JSON))
								.andExpect(status().is(201))
								.andReturn();
		
		_purchaseOrderExtension = mapper.readValue(result.getResponse().getContentAsString(), PurchaseOrderExtension.class);
		Assert.assertThat(_purchaseOrderExtension.getExtStatus().toString(), equalTo("PENDING"));
		
//
		result = mockMvc.perform(post("/generated/pos/{_id}/poext/{_extid}/accept",_purchaseOrder.get_id(),_purchaseOrderExtension.get_id())
								.header("keepAlive", true))
								.andExpect(status().is(200))
								.andReturn();
		
		_purchaseOrder = mapper.readValue(result.getResponse().getContentAsString(), PurchaseOrder.class);
		Assert.assertThat(_purchaseOrder.getPoStatus().toString(), equalTo("OPEN"));
		
//
    }
    

	@Test
	public void testCreateRejectAndCancelOfPurchaseOrder() throws Exception {
		MvcResult result = null;
		PurchaseOrder _purchaseOrder = null;
		PurchaseOrder purchaseOrder = null;
//
		purchaseOrder = helper.getCreatePOData();
		result = mockMvc.perform(post("/generated/pos")
								.header("keepAlive", true)
								.content(mapper.writeValueAsString(purchaseOrder))
								.contentType(MediaType.APPLICATION_JSON))
								.andExpect(status().is(201))
								.andReturn();
		
		_purchaseOrder = mapper.readValue(result.getResponse().getContentAsString(), PurchaseOrder.class);
		Assert.assertThat(_purchaseOrder.getPoStatus().toString(), equalTo("PENDING"));
		
//
		result = mockMvc.perform(post("/generated/pos/{id}/accept",_purchaseOrder.get_id())
								.header("keepAlive", true))
								.andExpect(status().is(200))
								.andReturn();
		
		_purchaseOrder = mapper.readValue(result.getResponse().getContentAsString(), PurchaseOrder.class);
		Assert.assertThat(_purchaseOrder.getPoStatus().toString(), equalTo("OPEN"));
		
//
		purchaseOrder = helper.getCreatePOData();
		result = mockMvc.perform(post("/generated/pos")
								.header("keepAlive", true)
								.content(mapper.writeValueAsString(purchaseOrder))
								.contentType(MediaType.APPLICATION_JSON))
								.andExpect(status().is(201))
								.andReturn();
		
		_purchaseOrder = mapper.readValue(result.getResponse().getContentAsString(), PurchaseOrder.class);
		Assert.assertThat(_purchaseOrder.getPoStatus().toString(), equalTo("PENDING"));
		
//
		result = mockMvc.perform(delete("/generated/pos/{id}/accept",_purchaseOrder.get_id())
								.header("keepAlive", true))
								.andExpect(status().is(200))
								.andReturn();
		
		_purchaseOrder = mapper.readValue(result.getResponse().getContentAsString(), PurchaseOrder.class);
		Assert.assertThat(_purchaseOrder.getPoStatus().toString(), equalTo("REJECTED"));
		
//
		result = mockMvc.perform(delete("/generated/pos/{id}/cancel",_purchaseOrder.get_id())
								.header("keepAlive", true))
								.andExpect(status().is(200))
								.andReturn();
		
		_purchaseOrder = mapper.readValue(result.getResponse().getContentAsString(), PurchaseOrder.class);
		Assert.assertThat(_purchaseOrder.getPoStatus().toString(), equalTo("CANCELLED"));
		
//
    }
    

	@Test
	public void testCreateAcceptAndCloseOfPurchaseOrder() throws Exception {
		MvcResult result = null;
		PurchaseOrder _purchaseOrder = null;
		PurchaseOrder purchaseOrder = null;
//
		purchaseOrder = helper.getCreatePOData();
		result = mockMvc.perform(post("/generated/pos")
								.header("keepAlive", true)
								.content(mapper.writeValueAsString(purchaseOrder))
								.contentType(MediaType.APPLICATION_JSON))
								.andExpect(status().is(201))
								.andReturn();
		
		_purchaseOrder = mapper.readValue(result.getResponse().getContentAsString(), PurchaseOrder.class);
		Assert.assertThat(_purchaseOrder.getPoStatus().toString(), equalTo("PENDING"));
		
//
		result = mockMvc.perform(post("/generated/pos/{id}/accept",_purchaseOrder.get_id())
								.header("keepAlive", true))
								.andExpect(status().is(200))
								.andReturn();
		
		_purchaseOrder = mapper.readValue(result.getResponse().getContentAsString(), PurchaseOrder.class);
		Assert.assertThat(_purchaseOrder.getPoStatus().toString(), equalTo("OPEN"));
		
//
		result = mockMvc.perform(delete("/generated/pos/{id}",_purchaseOrder.get_id())
								.header("keepAlive", true))
								.andExpect(status().is(200))
								.andReturn();
		
		_purchaseOrder = mapper.readValue(result.getResponse().getContentAsString(), PurchaseOrder.class);
		Assert.assertThat(_purchaseOrder.getPoStatus().toString(), equalTo("CLOSED"));
		
//
    }
    

	@Test
	public void testCreateAcceptAndCancelOfPurchaseOrder() throws Exception {
		MvcResult result = null;
		PurchaseOrder _purchaseOrder = null;
		PurchaseOrder purchaseOrder = null;
//
		purchaseOrder = helper.getCreatePOData();
		result = mockMvc.perform(post("/generated/pos")
								.header("keepAlive", true)
								.content(mapper.writeValueAsString(purchaseOrder))
								.contentType(MediaType.APPLICATION_JSON))
								.andExpect(status().is(201))
								.andReturn();
		
		_purchaseOrder = mapper.readValue(result.getResponse().getContentAsString(), PurchaseOrder.class);
		Assert.assertThat(_purchaseOrder.getPoStatus().toString(), equalTo("PENDING"));
		
//
		result = mockMvc.perform(post("/generated/pos/{id}/accept",_purchaseOrder.get_id())
								.header("keepAlive", true))
								.andExpect(status().is(200))
								.andReturn();
		
		_purchaseOrder = mapper.readValue(result.getResponse().getContentAsString(), PurchaseOrder.class);
		Assert.assertThat(_purchaseOrder.getPoStatus().toString(), equalTo("OPEN"));
		
//
		result = mockMvc.perform(delete("/generated/pos/{id}/cancel",_purchaseOrder.get_id())
								.header("keepAlive", true))
								.andExpect(status().is(200))
								.andReturn();
		
		_purchaseOrder = mapper.readValue(result.getResponse().getContentAsString(), PurchaseOrder.class);
		Assert.assertThat(_purchaseOrder.getPoStatus().toString(), equalTo("CANCELLED"));
		
//
    }
    

	@Test
	public void testCreateRejectAndUpdationOfPurchaseOrder() throws Exception {
		MvcResult result = null;
		PurchaseOrder _purchaseOrder = null;
		PurchaseOrder purchaseOrder = null;
//
		purchaseOrder = helper.getCreatePOData();
		result = mockMvc.perform(post("/generated/pos")
								.header("keepAlive", true)
								.content(mapper.writeValueAsString(purchaseOrder))
								.contentType(MediaType.APPLICATION_JSON))
								.andExpect(status().is(201))
								.andReturn();
		
		_purchaseOrder = mapper.readValue(result.getResponse().getContentAsString(), PurchaseOrder.class);
		Assert.assertThat(_purchaseOrder.getPoStatus().toString(), equalTo("PENDING"));
		
//
		result = mockMvc.perform(delete("/generated/pos/{id}/accept",_purchaseOrder.get_id())
								.header("keepAlive", true))
								.andExpect(status().is(200))
								.andReturn();
		
		_purchaseOrder = mapper.readValue(result.getResponse().getContentAsString(), PurchaseOrder.class);
		Assert.assertThat(_purchaseOrder.getPoStatus().toString(), equalTo("REJECTED"));
		
//
		purchaseOrder = helper.getUpdatePOData();
		result = mockMvc.perform(put("/generated/pos/{id}",_purchaseOrder.get_id())
								.header("keepAlive", true)
								.content(mapper.writeValueAsString(purchaseOrder))
								.contentType(MediaType.APPLICATION_JSON))
								.andExpect(status().is(200))
								.andReturn();
		
		_purchaseOrder = mapper.readValue(result.getResponse().getContentAsString(), PurchaseOrder.class);
		Assert.assertThat(_purchaseOrder.getPoStatus().toString(), equalTo("PENDING"));
		
//
    }
    

	@Test
	public void testCreateAcceptExtendAndRejectExtensionOfPurchaseOrder() throws Exception {
		MvcResult result = null;
		PurchaseOrder _purchaseOrder = null;
		PurchaseOrder purchaseOrder = null;
//
		purchaseOrder = helper.getCreatePOData();
		result = mockMvc.perform(post("/generated/pos")
								.header("keepAlive", true)
								.content(mapper.writeValueAsString(purchaseOrder))
								.contentType(MediaType.APPLICATION_JSON))
								.andExpect(status().is(201))
								.andReturn();
		
		_purchaseOrder = mapper.readValue(result.getResponse().getContentAsString(), PurchaseOrder.class);
		Assert.assertThat(_purchaseOrder.getPoStatus().toString(), equalTo("PENDING"));
		
//
		result = mockMvc.perform(post("/generated/pos/{id}/accept",_purchaseOrder.get_id())
								.header("keepAlive", true))
								.andExpect(status().is(200))
								.andReturn();
		
		_purchaseOrder = mapper.readValue(result.getResponse().getContentAsString(), PurchaseOrder.class);
		Assert.assertThat(_purchaseOrder.getPoStatus().toString(), equalTo("OPEN"));
		
//		
		PurchaseOrderExtension _purchaseOrderExtension = null;
		PurchaseOrderExtension purchaseOrderExtension = null;
//
		purchaseOrderExtension = helper.getCreatePOExtensionData();
		result = mockMvc.perform(post("/generated/pos/{_poid}/poext",_purchaseOrder.get_id())
								.header("keepAlive", true)
								.content(mapper.writeValueAsString(purchaseOrderExtension))
								.contentType(MediaType.APPLICATION_JSON))
								.andExpect(status().is(201))
								.andReturn();
		
		_purchaseOrderExtension = mapper.readValue(result.getResponse().getContentAsString(), PurchaseOrderExtension.class);
		Assert.assertThat(_purchaseOrderExtension.getExtStatus().toString(), equalTo("PENDING"));
		
//
		result = mockMvc.perform(delete("/generated/pos/{_id}/poext/{_extid}/accept",_purchaseOrder.get_id(),_purchaseOrderExtension.get_id())
								.header("keepAlive", true))
								.andExpect(status().is(200))
								.andReturn();
		
		_purchaseOrder = mapper.readValue(result.getResponse().getContentAsString(), PurchaseOrder.class);
		Assert.assertThat(_purchaseOrder.getPoStatus().toString(), equalTo("OPEN"));
		
//
    }
    

	@Test
	public void testCreatePurchaseOrder() throws Exception {
		MvcResult result = null;
		PurchaseOrder _purchaseOrder = null;
		PurchaseOrder purchaseOrder = null;
//
		purchaseOrder = helper.getCreatePOData();
		result = mockMvc.perform(post("/generated/pos",purchaseOrder.get_id())
								.header("keepAlive", false)
								.content(mapper.writeValueAsString(purchaseOrder))
								.contentType(MediaType.APPLICATION_JSON))
								.andExpect(status().is(201))
								.andReturn();
		
		_purchaseOrder = mapper.readValue(result.getResponse().getContentAsString(), PurchaseOrder.class);
		Assert.assertThat(_purchaseOrder.getPoStatus().toString(), equalTo("PENDING"));
		
//
    }
    

	@Test
	public void testAcceptPurchaseOrder() throws Exception {
		MvcResult result = null;
		PurchaseOrder _purchaseOrder = null;
		PurchaseOrder purchaseOrder = null;
//
		purchaseOrder = helper.getAcceptPOData();
		result = mockMvc.perform(post("/generated/pos/{id}/accept",purchaseOrder.get_id())
								.header("keepAlive", false))
								.andExpect(status().is(200))
								.andReturn();
		
		_purchaseOrder = mapper.readValue(result.getResponse().getContentAsString(), PurchaseOrder.class);
		Assert.assertThat(_purchaseOrder.getPoStatus().toString(), equalTo("OPEN"));
		
//
    }
    

	@Test
	public void testUpdatePurchaseOrder() throws Exception {
		MvcResult result = null;
		PurchaseOrder _purchaseOrder = null;
		PurchaseOrder purchaseOrder = null;
//
		purchaseOrder = helper.getUpdatePOData();
		_purchaseOrder = helper.getUpdatePOData2();
		result = mockMvc.perform(put("/generated/pos/{id}",_purchaseOrder.get_id())
								.header("keepAlive", false)
								.content(mapper.writeValueAsString(purchaseOrder))
								.contentType(MediaType.APPLICATION_JSON))
								.andExpect(status().is(200))
								.andReturn();
		
		_purchaseOrder = mapper.readValue(result.getResponse().getContentAsString(), PurchaseOrder.class);
		Assert.assertThat(_purchaseOrder.getPoStatus().toString(), equalTo("PENDING"));
		
//
    }
    
}
