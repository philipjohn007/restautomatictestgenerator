package com.example;

import java.util.Iterator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.expression.*;
import org.springframework.expression.common.TemplateParserContext;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.expression.spel.support.StandardEvaluationContext;
import org.springframework.hateoas.Link;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.stereotype.Component;
import com.example.models.*;
import com.fasterxml.jackson.databind.*;
import com.fasterxml.jackson.databind.node.NullNode;
import com.github.fge.jsonpatch.JsonPatch;
import com.github.fge.jsonpatch.mergepatch.JsonMergePatch;

@Component
public class Skeleton {
	@Autowired
	MockPlantRepository plantRepo;

	@Autowired
	MockPurchaseOrderRepository purchaseOrderRepo;

	@Autowired
	MockPurchaseOrderExtensionRepository purchaseOrderExtensionRepo;

	ObjectMapper mapper = new ObjectMapper().findAndRegisterModules();
	
	StandardEvaluationContext spelContext;
	ExpressionParser spelParser;
	
	class LocalSpelContext {
		public MockPlantRepository $Plants = plantRepo;
		public MockPurchaseOrderRepository $PurchaseOrders = purchaseOrderRepo;
		public MockPurchaseOrderExtensionRepository $PurchaseOrderExtensions = purchaseOrderExtensionRepo;
		public String $toJson(Object o) throws Exception {
			return mapper.writeValueAsString(o);
		}		
	}
	
	public JsonNode $mergePatch(JsonNode obj, String json) throws Exception {
		JsonMergePatch mp = mapper.readValue(json, JsonMergePatch.class);
		return mp.apply(obj);
	}

	public JsonNode $patch(JsonNode obj, String json) throws Exception {
		JsonPatch mp = mapper.readValue(json, JsonPatch.class);
		return mp.apply(obj);
	}
	
	String[] plantFixtures = {
		"{\"_id\":\"1\",\"name\":\"Mini excavator\",\"description\":\"Excavator 1.5 tons\",\"price\":\"100.00\"}",
		"{\"_id\":\"2\",\"name\":\"Mini excavator\",\"description\":\"Excavator 2.5 tons\",\"price\":\"120.00\"}",
		"{\"_id\":\"3\",\"name\":\"Midi excavator\",\"description\":\"Excavator 3.0 tons\",\"price\":\"150.00\"}",
		"{\"_id\":\"4\",\"name\":\"Maxi excavator\",\"description\":\"Excavator 5.0 tons\",\"price\":\"200.00\"}",
	};
	String[] purchaseOrderFixtures = {
		"{\"_id\":\"1\",\"plant\":#{$toJson($Plants.findOne(1l))},\"startDate\":\"2016-02-29\",\"endDate\":\"2016-03-18\",\"cost\":\"2400.00\",\"poStatus\":\"PENDING\"}",
		"{\"_id\":\"2\",\"plant\":#{$toJson($Plants.findOne(2l))},\"startDate\":\"2016-02-29\",\"endDate\":\"2016-03-18\",\"cost\":\"2400.00\",\"poStatus\":\"OPEN\"}",
	};
	String[] purchaseOrderExtensionFixtures = {
		"{\"_id\":\"1\",\"endDate\":\"2016-03-18\",\"extStatus\":\"PENDING\"}",
	};
//
	@RequestMapping("/initialize")
	public void setupBackground() throws Exception {		
		spelContext = new StandardEvaluationContext(new LocalSpelContext());
		spelParser = new SpelExpressionParser();
		spelContext.registerFunction("$toJson", LocalSpelContext.class.getDeclaredMethod("$toJson", new Class[] { Object.class }));
//
		for (String json: plantFixtures) {
			Expression expression = spelParser.parseExpression(json, new TemplateParserContext());
			String rewrittenJson = expression.getValue(spelContext, String.class);
			plantRepo.save(mapper.readValue(rewrittenJson, Plant.class));
		}
//
		for (String json: purchaseOrderFixtures) {
			Expression expression = spelParser.parseExpression(json, new TemplateParserContext());
			String rewrittenJson = expression.getValue(spelContext, String.class);
			purchaseOrderRepo.save(mapper.readValue(rewrittenJson, PurchaseOrder.class));
		}
//
		for (String json: purchaseOrderExtensionFixtures) {
			Expression expression = spelParser.parseExpression(json, new TemplateParserContext());
			String rewrittenJson = expression.getValue(spelContext, String.class);
			purchaseOrderExtensionRepo.save(mapper.readValue(rewrittenJson, PurchaseOrderExtension.class));
		}
//
	}
//
	@RequestMapping(value="/pos",method=RequestMethod.POST)
	public ResponseEntity<PurchaseOrder> createPO(@RequestHeader("keepAlive") Boolean keepDbAlive, @RequestBody String po) throws Exception{
		if (!keepDbAlive) {
			setupBackground();
		}
		
		JsonNode po1 = mapper.readTree(po);
		
		
		Expression expression = spelParser.parseExpression("{\"_id\": #{$PurchaseOrders.count()+1}, \"poStatus\": \"PENDING\", \"cost\": \"200\"}}", new TemplateParserContext());
		String expressionResult = expression.getValue(spelContext, String.class);
		JsonNode mergePatchResult = $mergePatch(po1, expressionResult);
		PurchaseOrder _purchaseOrder = mapper.treeToValue(mergePatchResult, PurchaseOrder.class);
		_purchaseOrder.add(new Link("/pos/" + _purchaseOrder.get_id()));
		
		purchaseOrderRepo.save(_purchaseOrder);
		
		HttpHeaders headers = new HttpHeaders();
		headers.add("Location", _purchaseOrder.getId().getHref());
		
		return new ResponseEntity<PurchaseOrder>(_purchaseOrder, headers, HttpStatus.valueOf(201));
	}
	
//
	@RequestMapping(value="/pos/{id}/accept",method=RequestMethod.POST)
	public ResponseEntity<PurchaseOrder> acceptPO(@RequestHeader("keepAlive") Boolean keepDbAlive, @PathVariable Long id) throws Exception{
		if (!keepDbAlive) {
			setupBackground();
		}
		
		Expression expression1 = null;
		if (!keepDbAlive) {
			expression1 = spelParser.parseExpression("#{$PurchaseOrders.findOne(1L)}", new TemplateParserContext());
		} else {
			expression1 = spelParser.parseExpression(String.format("#{$PurchaseOrders.findOne(new Long(%d))}", id), new TemplateParserContext());
		}
		PurchaseOrder _purchaseOrder1 = expression1.getValue(spelContext, PurchaseOrder.class);
		JsonNode purchaseOrderJsonNode = mapper.valueToTree(_purchaseOrder1);
		
		
		Expression expression = spelParser.parseExpression("[{\"op\": \"replace\", \"path\": \"/poStatus\", \"value\": \"OPEN\"}]", new TemplateParserContext());
		String expressionResult = expression.getValue(spelContext, String.class);
		JsonNode mergePatchResult = $patch(purchaseOrderJsonNode, expressionResult);
		PurchaseOrder _purchaseOrder = mapper.treeToValue(mergePatchResult, PurchaseOrder.class);
		
		purchaseOrderRepo.save(_purchaseOrder);
		
		HttpHeaders headers = new HttpHeaders();
		return new ResponseEntity<PurchaseOrder>(_purchaseOrder, headers, HttpStatus.valueOf(200));
	}
	
//
	@RequestMapping(value="/pos/{id}/accept",method=RequestMethod.DELETE)
	public ResponseEntity<PurchaseOrder> rejectPO(@RequestHeader("keepAlive") Boolean keepDbAlive, @PathVariable Long id) throws Exception{
		if (!keepDbAlive) {
			setupBackground();
		}
		
		Expression expression1 = null;
		if (!keepDbAlive) {
			expression1 = spelParser.parseExpression("#{$PurchaseOrders.findOne(1L)}", new TemplateParserContext());
		} else {
			expression1 = spelParser.parseExpression(String.format("#{$PurchaseOrders.findOne(new Long(%d))}", id), new TemplateParserContext());
		}
		PurchaseOrder _purchaseOrder1 = expression1.getValue(spelContext, PurchaseOrder.class);
		JsonNode purchaseOrderJsonNode = mapper.valueToTree(_purchaseOrder1);
		
		
		Expression expression = spelParser.parseExpression("[{\"op\": \"replace\", \"path\": \"/poStatus\", \"value\": \"REJECTED\"}]", new TemplateParserContext());
		String expressionResult = expression.getValue(spelContext, String.class);
		JsonNode mergePatchResult = $patch(purchaseOrderJsonNode, expressionResult);
		PurchaseOrder _purchaseOrder = mapper.treeToValue(mergePatchResult, PurchaseOrder.class);
		
		purchaseOrderRepo.save(_purchaseOrder);
		
		HttpHeaders headers = new HttpHeaders();
		return new ResponseEntity<PurchaseOrder>(_purchaseOrder, headers, HttpStatus.valueOf(200));
	}
	
//
	@RequestMapping(value="/pos/{id}/cancel",method=RequestMethod.DELETE)
	public ResponseEntity<PurchaseOrder> cancelPO(@RequestHeader("keepAlive") Boolean keepDbAlive, @PathVariable Long id) throws Exception{
		if (!keepDbAlive) {
			setupBackground();
		}
		
		Expression expression1 = null;
		if (!keepDbAlive) {
			expression1 = spelParser.parseExpression("#{$PurchaseOrders.findOne(1L)}", new TemplateParserContext());
		} else {
			expression1 = spelParser.parseExpression(String.format("#{$PurchaseOrders.findOne(new Long(%d))}", id), new TemplateParserContext());
		}
		PurchaseOrder _purchaseOrder1 = expression1.getValue(spelContext, PurchaseOrder.class);
		JsonNode purchaseOrderJsonNode = mapper.valueToTree(_purchaseOrder1);
		
		
		Expression expression = spelParser.parseExpression("[{\"op\": \"replace\", \"path\": \"/poStatus\", \"value\": \"CANCELLED\"}]", new TemplateParserContext());
		String expressionResult = expression.getValue(spelContext, String.class);
		JsonNode mergePatchResult = $patch(purchaseOrderJsonNode, expressionResult);
		PurchaseOrder _purchaseOrder = mapper.treeToValue(mergePatchResult, PurchaseOrder.class);
		
		purchaseOrderRepo.save(_purchaseOrder);
		
		HttpHeaders headers = new HttpHeaders();
		return new ResponseEntity<PurchaseOrder>(_purchaseOrder, headers, HttpStatus.valueOf(200));
	}
	
//
	@RequestMapping(value="/pos/{id}",method=RequestMethod.DELETE)
	public ResponseEntity<PurchaseOrder> closePO(@RequestHeader("keepAlive") Boolean keepDbAlive, @PathVariable Long id) throws Exception{
		if (!keepDbAlive) {
			setupBackground();
		}
		
		Expression expression1 = null;
		if (!keepDbAlive) {
			expression1 = spelParser.parseExpression("#{$PurchaseOrders.findOne(1L)}", new TemplateParserContext());
		} else {
			expression1 = spelParser.parseExpression(String.format("#{$PurchaseOrders.findOne(new Long(%d))}", id), new TemplateParserContext());
		}
		PurchaseOrder _purchaseOrder1 = expression1.getValue(spelContext, PurchaseOrder.class);
		JsonNode purchaseOrderJsonNode = mapper.valueToTree(_purchaseOrder1);
		
		
		Expression expression = spelParser.parseExpression("[{\"op\": \"replace\", \"path\": \"/poStatus\", \"value\": \"CLOSED\"}]", new TemplateParserContext());
		String expressionResult = expression.getValue(spelContext, String.class);
		JsonNode mergePatchResult = $patch(purchaseOrderJsonNode, expressionResult);
		PurchaseOrder _purchaseOrder = mapper.treeToValue(mergePatchResult, PurchaseOrder.class);
		
		purchaseOrderRepo.save(_purchaseOrder);
		
		HttpHeaders headers = new HttpHeaders();
		return new ResponseEntity<PurchaseOrder>(_purchaseOrder, headers, HttpStatus.valueOf(200));
	}
	
//
	@RequestMapping(value="/pos/{id}",method=RequestMethod.PUT)
	public ResponseEntity<PurchaseOrder> updatePO(@RequestHeader("keepAlive") Boolean keepDbAlive, @PathVariable Long id, @RequestBody String po) throws Exception{
		if (!keepDbAlive) {
			setupBackground();
		}
		
		JsonNode po1 = mapper.readTree(po);
		Expression expression1 = null;
		if (!keepDbAlive) {
			expression1 = spelParser.parseExpression("#{$PurchaseOrders.findOne(1L)}", new TemplateParserContext());
		} else {
			expression1 = spelParser.parseExpression(String.format("#{$PurchaseOrders.findOne(new Long(%d))}", id), new TemplateParserContext());
		}
		PurchaseOrder _purchaseOrder1 = expression1.getValue(spelContext, PurchaseOrder.class);
		JsonNode purchaseOrderJsonNode = mapper.valueToTree(_purchaseOrder1);
		
		Iterator<JsonNode> nodeIterator = po1.iterator();
		while(nodeIterator.hasNext()) {
			if(nodeIterator.next() instanceof NullNode) nodeIterator.remove();
			
		}
		
		purchaseOrderJsonNode = $mergePatch(purchaseOrderJsonNode, po1.toString());
		
		Expression expression = spelParser.parseExpression("[{\"op\": \"replace\", \"path\": \"/poStatus\", \"value\": \"PENDING\"}]", new TemplateParserContext());
		String expressionResult = expression.getValue(spelContext, String.class);
		JsonNode mergePatchResult = $patch(purchaseOrderJsonNode, expressionResult);
		PurchaseOrder _purchaseOrder = mapper.treeToValue(mergePatchResult, PurchaseOrder.class);
		
		purchaseOrderRepo.save(_purchaseOrder);
		
		HttpHeaders headers = new HttpHeaders();
		return new ResponseEntity<PurchaseOrder>(_purchaseOrder, headers, HttpStatus.valueOf(200));
	}
	
//
	@RequestMapping(value="/pos/{_poid}/poext",method=RequestMethod.POST)
	public ResponseEntity<PurchaseOrderExtension> createPOExtension(@RequestHeader("keepAlive") Boolean keepDbAlive, @PathVariable Long _poid, @RequestBody String poex) throws Exception{
		if (!keepDbAlive) {
			setupBackground();
		}
		
		JsonNode poex1 = mapper.readTree(poex);
		
		
		Expression expression = spelParser.parseExpression("{\"_id\": #{$PurchaseOrderExtensions.count()+1}, \"extStatus\": \"PENDING\"}}", new TemplateParserContext());
		String expressionResult = expression.getValue(spelContext, String.class);
		JsonNode mergePatchResult = $mergePatch(poex1, expressionResult);
		PurchaseOrderExtension _purchaseOrderExtension = mapper.treeToValue(mergePatchResult, PurchaseOrderExtension.class);
		_purchaseOrderExtension.add(new Link("/pos/"+ _poid + "/poext/" + _purchaseOrderExtension.get_id()));
		
		purchaseOrderExtensionRepo.save(_purchaseOrderExtension);
		
		HttpHeaders headers = new HttpHeaders();
		return new ResponseEntity<PurchaseOrderExtension>(_purchaseOrderExtension, headers, HttpStatus.valueOf(201));
	}
	
//
	@RequestMapping(value="/pos/{_id}/poext/{_extid}/accept",method=RequestMethod.POST)
	public ResponseEntity<PurchaseOrder> acceptPOExtension(@RequestHeader("keepAlive") Boolean keepDbAlive, @PathVariable Long _id, @PathVariable Long _extid) throws Exception{
		if (!keepDbAlive) {
			setupBackground();
		}
		
		Expression expression1 = null;
		if (!keepDbAlive) {
			expression1 = spelParser.parseExpression("#{$PurchaseOrderExtensions.findOne(1L)}", new TemplateParserContext());
		} else {
			expression1 = spelParser.parseExpression(String.format("#{$PurchaseOrderExtensions.findOne(new Long(%d))}", _extid), new TemplateParserContext());
		}
		PurchaseOrderExtension _purchaseOrderExtension1 = expression1.getValue(spelContext, PurchaseOrderExtension.class);
		JsonNode purchaseOrderExtensionJsonNode = mapper.valueToTree(_purchaseOrderExtension1);
		
		Expression expressionPurchaseOrderExtension = spelParser.parseExpression("[{\"op\": \"replace\", \"path\": \"/extStatus\", \"value\": \"ACCEPTED\"}]", new TemplateParserContext());
		String expressionResultPurchaseOrderExtension = expressionPurchaseOrderExtension.getValue(spelContext, String.class);
		JsonNode mergePatchResultPurchaseOrderExtension = $patch(purchaseOrderExtensionJsonNode, expressionResultPurchaseOrderExtension);
		PurchaseOrderExtension _purchaseOrderExtension = mapper.treeToValue(mergePatchResultPurchaseOrderExtension, PurchaseOrderExtension.class);
		
		purchaseOrderExtensionRepo.save(_purchaseOrderExtension);
		if (!keepDbAlive) {
			expression1 = spelParser.parseExpression("#{$PurchaseOrders.findOne(1L)}", new TemplateParserContext());
		} else {
			expression1 = spelParser.parseExpression(String.format("#{$PurchaseOrders.findOne(new Long(%d))}", _id), new TemplateParserContext());
		}
		PurchaseOrder _purchaseOrder1 = expression1.getValue(spelContext, PurchaseOrder.class);
		JsonNode purchaseOrderJsonNode = mapper.valueToTree(_purchaseOrder1);
		
		Expression expressionPurchaseOrder = spelParser.parseExpression(String.format("[{\"op\": \"replace\", \"path\": \"/endDate\", \"value\": \"#{$PurchaseOrderExtensions.findOne(new Long(%d)).getEndDate().toString()}\"}]",_extid), new TemplateParserContext());
		String expressionResultPurchaseOrder = expressionPurchaseOrder.getValue(spelContext, String.class);
		JsonNode mergePatchResultPurchaseOrder = $patch(purchaseOrderJsonNode, expressionResultPurchaseOrder);
		PurchaseOrder _purchaseOrder = mapper.treeToValue(mergePatchResultPurchaseOrder, PurchaseOrder.class);
		
		purchaseOrderRepo.save(_purchaseOrder);
		
		HttpHeaders headers = new HttpHeaders();
		return new ResponseEntity<PurchaseOrder>(_purchaseOrder, headers, HttpStatus.valueOf(200));
	}
	
//
	@RequestMapping(value="/pos/{_id}/poext/{_extid}/accept",method=RequestMethod.DELETE)
	public ResponseEntity<PurchaseOrder> rejectPOExtension(@RequestHeader("keepAlive") Boolean keepDbAlive, @PathVariable Long _id, @PathVariable Long _extid) throws Exception{
		if (!keepDbAlive) {
			setupBackground();
		}
		
		Expression expression1 = null;
		if (!keepDbAlive) {
			expression1 = spelParser.parseExpression("#{$PurchaseOrderExtensions.findOne(1L)}", new TemplateParserContext());
		} else {
			expression1 = spelParser.parseExpression(String.format("#{$PurchaseOrderExtensions.findOne(new Long(%d))}", _extid), new TemplateParserContext());
		}
		PurchaseOrderExtension _purchaseOrderExtension1 = expression1.getValue(spelContext, PurchaseOrderExtension.class);
		JsonNode purchaseOrderExtensionJsonNode = mapper.valueToTree(_purchaseOrderExtension1);
		
		Expression expressionPurchaseOrderExtension = spelParser.parseExpression("[{\"op\": \"replace\", \"path\": \"/extStatus\", \"value\": \"REJECTED\"}]", new TemplateParserContext());
		String expressionResultPurchaseOrderExtension = expressionPurchaseOrderExtension.getValue(spelContext, String.class);
		JsonNode mergePatchResultPurchaseOrderExtension = $patch(purchaseOrderExtensionJsonNode, expressionResultPurchaseOrderExtension);
		PurchaseOrderExtension _purchaseOrderExtension = mapper.treeToValue(mergePatchResultPurchaseOrderExtension, PurchaseOrderExtension.class);
		
		purchaseOrderExtensionRepo.save(_purchaseOrderExtension);
		if (!keepDbAlive) {
			expression1 = spelParser.parseExpression("#{$PurchaseOrders.findOne(1L)}", new TemplateParserContext());
		} else {
			expression1 = spelParser.parseExpression(String.format("#{$PurchaseOrders.findOne(new Long(%d))}", _id), new TemplateParserContext());
		}
		PurchaseOrder _purchaseOrder1 = expression1.getValue(spelContext, PurchaseOrder.class);
		JsonNode purchaseOrderJsonNode = mapper.valueToTree(_purchaseOrder1);
		
		Expression expressionPurchaseOrder = spelParser.parseExpression("[]", new TemplateParserContext());
		String expressionResultPurchaseOrder = expressionPurchaseOrder.getValue(spelContext, String.class);
		JsonNode mergePatchResultPurchaseOrder = $patch(purchaseOrderJsonNode, expressionResultPurchaseOrder);
		PurchaseOrder _purchaseOrder = mapper.treeToValue(mergePatchResultPurchaseOrder, PurchaseOrder.class);
		
		purchaseOrderRepo.save(_purchaseOrder);
		
		HttpHeaders headers = new HttpHeaders();
		return new ResponseEntity<PurchaseOrder>(_purchaseOrder, headers, HttpStatus.valueOf(200));
	}
	
}
