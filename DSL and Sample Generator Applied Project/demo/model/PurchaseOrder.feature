Feature: PurchaseOrder Feature
	As a customer
	In order to rent plant equipment
	I need to process a Purchase Order

Background: Initial plant catalog and purchase orders
	Given the following $Plants
		| _id | name  		   | description        | price  |
		|  1  | Mini excavator | Excavator 1.5 tons | 100.00 |
		|  2  | Mini excavator | Excavator 2.5 tons | 120.00 |
		|  3  | Midi excavator | Excavator 3.0 tons | 150.00 |
		|  4  | Maxi excavator | Excavator 5.0 tons | 200.00 |
	Given the following $PurchaseOrders
		| _id | plant 	                        | startDate  | endDate    | cost    | poStatus  |
		|  1  | #{$toJson($Plants.findOne(1l))} | 2016-02-29 | 2016-03-18 | 2400.00 | PENDING   |
		|  2  | #{$toJson($Plants.findOne(2l))} | 2016-02-29 | 2016-03-18 | 2400.00 | OPEN      |
	Given the following $PurchaseOrderExtensions
		| _id | endDate    | extStatus  |
		|  1  | 2016-03-18 | PENDING    |

Scenario Outline: Creation of PurchaseOrder
	When customer calls 'createPO' using 'POST' on <uri> with <po>
	Then $PurchaseOrders must contain $mergePatch(<po>,<po_patch_merge>)
	And status code must be <status>
	And location must have <location>
	And 'poStatus' must be <poStatus>
	Examples:
		| uri  | po                                                                                             | status | location      | po_patch_merge 																| poStatus	|
		| /pos | {"plant": #{$toJson($Plants.findOne(1l))}, "startDate": "2016-02-29", "endDate": "2016-03-04"} | 201    | /pos/<po._id> | {"_id": #{$PurchaseOrders.count()+1}, "poStatus": "PENDING", "cost": "200"}} | PENDING	|

Scenario Outline: Processing of Pending PurchaseOrder
	When clerk calls <function_name> using <verb> on <uri>
	Then <po> should be '#{$PurchaseOrders.findOne(id)}'
	And $PurchaseOrders must contain $patch(<po>,<po_patch>)
	And status code must be <status>
	And 'poStatus' must be <poStatus>
	Examples:
		| function_name | verb   | uri                       | status | po_patch                                                       | id | poStatus	|
		| acceptPO      | POST   | /pos/{id}/accept 		 | 200    | [{"op": "replace", "path": "/poStatus", "value": "OPEN"}]      | 1L | OPEN		|
		| rejectPO      | DELETE | /pos/{id}/accept 		 | 200    | [{"op": "replace", "path": "/poStatus", "value": "REJECTED"}]  | 1L | REJECTED	|
		| cancelPO      | DELETE | /pos/{id}/cancel 		 | 200    | [{"op": "replace", "path": "/poStatus", "value": "CANCELLED"}] | 1L | CANCELLED	|

Scenario Outline: Processing of Accepted PurchaseOrder
	When clerk calls <function_name> using <verb> on <uri>
	Then <po> should be '#{$PurchaseOrders.findOne(id)}'
	And $PurchaseOrders must contain $patch(<po>,<po_patch>)
	And status code must be <status>
	And 'poStatus' must be <poStatus>
	Examples:
		| function_name | verb   | uri                       | status | po_patch                                                       | id | poStatus	|
		| closePO       | DELETE | /pos/{id} 		         | 200    | [{"op": "replace", "path": "/poStatus", "value": "CLOSED"}]    | 1L | CLOSED	|
		| cancelPO      | DELETE | /pos/{id}/cancel 		 | 200    | [{"op": "replace", "path": "/poStatus", "value": "CANCELLED"}] | 1L | CANCELLED	|

Scenario Outline: Processing of Rejected PurchaseOrder
	When clerk calls <function_name> using <verb> on <uri> with <po>
	Then <po> should be '#{$PurchaseOrders.findOne(id)}'
	And $PurchaseOrders must contain $patch(<po>,<po_patch>)
	And status code must be <status>
	And 'poStatus' must be <poStatus>
	Examples:
		| function_name | verb   | uri                       | po                                                                                              | status | po_patch                                                       | id | poStatus	|
		| updatePO      | PUT    | /pos/{id}				 | {"plant": #{$toJson($Plants.findOne(1l))}, "startDate": "2016-03-01", "endDate": "2016-03-04"}  | 200    | [{"op": "replace", "path": "/poStatus", "value": "PENDING"}]   | 1L | PENDING		|
		| cancelPO      | DELETE | /pos/{id}/cancel 		 |																								   | 200    | [{"op": "replace", "path": "/poStatus", "value": "CANCELLED"}] | 1L | CANCELLED	|

Scenario Outline: Creation of PurchaseOrderExtension
	When customer calls 'createPOExtension' using 'POST' on <uri> with <poex>
	Then $PurchaseOrderExtensions must contain $mergePatch(<poex>,<poex_patch_merge>)
	And status code must be <status>
	And location must have <location>
	And 'extStatus' must be <extStatus>
	Examples:
		| uri				 | poex                        	| status | location      				| poex_patch_merge 															| _poid | extStatus	|
		| /pos/{_poid}/poext | {"endDate": "2016-03-10"} 	| 201    | /pos/{_poid}/poext/{_id} 	| {"_id": #{$PurchaseOrderExtensions.count()+1}, "extStatus": "PENDING"}} 	| 1L    | PENDING	|

Scenario Outline: Processing of extending PurchaseOrder
	When clerk calls <function_name> using <verb> on <uri>
	Then <po> should be '#{$PurchaseOrders.findOne(_id)}'
	Then <poex> should be '#{$PurchaseOrderExtensions.findOne(_extid)}'
	And $PurchaseOrders must contain $patch(<po>,<po_patch>)
	And $PurchaseOrderExtensions must contain $patch(<poex>,<poex_patch>)
	And status code must be <status>
	And 'poStatus' must be <poStatus>
	Examples:
		| function_name   	| verb   | uri                         				| status | poex_patch                                                       | po_patch 																													| _id	| _extid	| poStatus	|
		| acceptPOExtension | POST   | /pos/{_id}/poext/{_extid}/accept 		| 200    | [{"op": "replace", "path": "/extStatus", "value": "ACCEPTED"}] 	| [{"op": "replace", "path": "/endDate", "value": "#{$PurchaseOrderExtensions.findOne(_extid).getEndDate().toString()}"}] 	| 1L  	| 1L		| OPEN		|
		| rejectPOExtension | DELETE | /pos/{_id}/poext/{_extid}/accept 		| 200    | [{"op": "replace", "path": "/extStatus", "value": "REJECTED"}]	| []																														| 1L  	| 1L		| OPEN		|

Scenario: Create accept and extend PurchaseOrder
	When scenario "Creation of PurchaseOrder" with [1]
	And scenario "Processing of Pending PurchaseOrder" with [1]
	And scenario "Creation of PurchaseOrderExtension" with [1]
	And scenario "Processing of extending PurchaseOrder" with [1]
	
Scenario: Create reject and cancel of PurchaseOrder
	When scenario "Creation of PurchaseOrder" with [1]
	And scenario "Processing of Pending PurchaseOrder" with [1]
	And scenario "Creation of PurchaseOrder" with [1]
	And scenario "Processing of Pending PurchaseOrder" with [2]
	And scenario "Processing of Accepted PurchaseOrder" with [2]

Scenario: Create accept and close of PurchaseOrder
	When scenario "Creation of PurchaseOrder" with [1]
	And scenario "Processing of Pending PurchaseOrder" with [1]
	And scenario "Processing of Accepted PurchaseOrder" with [1]

Scenario: Create accept and cancel of PurchaseOrder
	When scenario "Creation of PurchaseOrder" with [1]
	And scenario "Processing of Pending PurchaseOrder" with [1]
	And scenario "Processing of Accepted PurchaseOrder" with [2]

Scenario: Create reject and updation of PurchaseOrder
	When scenario "Creation of PurchaseOrder" with [1]
	And scenario "Processing of Pending PurchaseOrder" with [2]
	And scenario "Processing of Rejected PurchaseOrder" with [1]

Scenario: Create accept extend and reject extension of PurchaseOrder
	When scenario "Creation of PurchaseOrder" with [1]
	And scenario "Processing of Pending PurchaseOrder" with [1]
	And scenario "Creation of PurchaseOrderExtension" with [1]
	And scenario "Processing of extending PurchaseOrder" with [2]

Scenario: Create PurchaseOrder
	When scenario "Creation of PurchaseOrder" with [1]
	
Scenario: Accept PurchaseOrder
	When scenario "Processing of Pending PurchaseOrder" with [1]
	
Scenario: Update PurchaseOrder
	When scenario "Processing of Rejected PurchaseOrder" with [1]