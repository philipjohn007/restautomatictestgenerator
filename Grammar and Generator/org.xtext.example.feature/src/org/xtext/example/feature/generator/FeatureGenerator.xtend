/*
 * generated by Xtext 2.9.1
 */
package org.xtext.example.feature.generator

import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.xtext.generator.AbstractGenerator
import org.eclipse.xtext.generator.IFileSystemAccess2
import org.eclipse.xtext.generator.IGeneratorContext
import java.util.LinkedHashMap
import java.util.regex.Pattern
import java.util.ArrayList
import java.util.StringTokenizer
import com.google.common.collect.LinkedListMultimap
import java.util.LinkedList
import java.util.HashMap
import java.util.Arrays
import java.util.List
import org.xtext.example.feature.feature.Feature
import org.xtext.example.feature.feature.ScenarioOutline
import org.xtext.example.feature.feature.Scenario

/**
 * Generates code from your model files on save.
 * 
 * See https://www.eclipse.org/Xtext/documentation/303_runtime_concepts.html#code-generation
 */
class FeatureGenerator extends AbstractGenerator {

	var methods = new HashMap<String, HashMap<String,ArrayList<String>>>()
	
	override void doGenerate(Resource resource, IFileSystemAccess2 fsa, IGeneratorContext context) {
		for (e: resource.allContents.toIterable.filter(Feature)) {
			generateRepositories(fsa,e)
			fsa.generateFile(
				"com/example/Skeleton.java", 
				e.generateMock)
			fsa.generateFile(
				"com/example/SkeletonHelper.java", 
				e.generateHelper)
			fsa.generateFile(
				"com/example/SkeletonTest.java", 
				e.generateTest)
		}
	}
	
	def void generateRepositories(IFileSystemAccess2 fsa, Feature feature) {
		for (step: feature.background.steps) {
			for (table : step.tables) {
				var type = step.description.substring(step.description.indexOf("following $") + "following $".length)
				type = type.substring(0,type.length-1)
				fsa.generateFile(
					"com/example/models/Mock"+type+"Repository.java", 
					type.generateMockRepository)
				fsa.generateFile(
					"com/example/models/"+type+"Repository.java", 
					type.generateJpaRepository)
			}
		}
	}
	
	def generateMockRepository(String type) {
		'''
		package com.example.models;
		
		import org.springframework.data.repository.CrudRepository;
		
		public interface Mock�type�Repository extends CrudRepository<�type�, Long>{
		
		}
		'''
	}
	
	def generateJpaRepository(String type) {
		'''
		package com.example.models;
		
		import org.springframework.data.jpa.repository.JpaRepository;
		
		public interface �type�Repository extends JpaRepository<�type�, Long>{
		
		}
		'''
	}
	
	def generateMock(Feature feature) {
		methods = new HashMap<String, HashMap<String,ArrayList<String>>>()
		var typeMap = new LinkedHashMap<String,Pair<String,String>>()
		var fixtures = new String
		for (step: feature.background.steps) {
			for (table : step.tables) {
				var type = step.description.substring(step.description.indexOf("following $") + "following $".length)
				type = type.substring(0,type.length-1)
				fixtures = fixtures + "\tString[] " + type.toFirstLower + "Fixtures = {\n"
				typeMap.put(type.toFirstLower+"Repo",new Pair(type.toFirstLower+"Fixtures",type.substring(0,type.length)))
				var titleRow = table.rows.get(0)
				var titleArray = titleRow.split("\\|", -1);
				titleArray = titleArray.subList(1,titleArray.size-1)
				for (valueRow : table.rows.subList(1, table.rows.size)) {
					var valueArray = valueRow.split("\\|", -1);
					valueArray = valueArray.subList(1,valueArray.size-1)
					var firstIteration = true
					fixtures = fixtures + "\t\t\"{"
					for (item : titleArray) {
						var value = valueArray.get(titleArray.indexOf(item)).trim
						var strFormat = "%s\"%s\\\":"+(if (value.startsWith("#{$")) "%s" else "\\\"%s\\\"")
						fixtures = fixtures + String.format(strFormat, (if (firstIteration) "\\" else ",\\"), 
							item.trim, value
						)
						firstIteration = false
					}
					fixtures = fixtures + "}\",\n"
				}
				fixtures = fixtures + "\t};\n"
			}
		}
		var Pattern tableHeaderPattern = Pattern.compile("(\\w+)")
		var Pattern whenStepPattern = Pattern.compile(".+?\\bcalls?\\s+(\\'\\w+\\'|<\\w+>)\\s+using\\s+(\\'\\w+\\'|<\\w+>)\\s+on\\s+(\\'\\w+\\'|<\\w+>)(?:\\s+with\\s+(\\'\\w+\\'|<\\w+>))?")
		var Pattern pathVariablePattern = Pattern.compile("/\\{(\\w+)\\}/?")
		var Pattern objectFromRepoVariablePattern = Pattern.compile("findOne\\((\\w+)\\)")
		var Pattern statusPattern = Pattern.compile("[.+]?status.+must\\sbe\\s(<\\w+>|'\\w+')")
		var Pattern locationPattern = Pattern.compile("[.+]?location.+should\\shave\\s(<\\w+>|'\\w+')")
		var Pattern patchPattern = Pattern.compile("[.+]?must\\scontain.+\\((<\\w+>|'\\w+'),[\\s]?(<\\w+>|'\\w+')\\)")
		var Pattern objectFromRepoPattern = Pattern.compile("[.+]?<\\w+>\\sshould\\sbe\\s('.*')")
		var Pattern testVariableCheckPattern = Pattern.compile("'(\\w+)'\\smust\\sbe\\s<(\\w+)>")
		
		var methodBody = ''''''
		for (scenario : feature.scenarios) {
			if (scenario instanceof ScenarioOutline) {
				
				var methodStructureMap = new LinkedHashMap<String, String>()
				var object = scenario.title.substring(scenario.title.lastIndexOf(" ")+1)
				var associationTable = LinkedListMultimap.create()
				var variables = new ArrayList<String>()
				var scOutline = scenario as ScenarioOutline
				
				var methodStrMap = new HashMap<String,ArrayList<String>>()
				var header = scOutline.examples.table.rows.get(0)
				var headerMatcher = tableHeaderPattern.matcher(header)
				
				while (headerMatcher.find)
					variables.add(headerMatcher.group(1))
								
				println(variables)
				var firstTime = true
				
				for (row : scOutline.examples.table.rows) {
					if (!firstTime) {
						var tokenizer = new StringTokenizer(row, "|")
						var pos = 0;
						while (tokenizer.hasMoreTokens) {
							if (pos < variables.size)
								associationTable.put(variables.get(pos++), tokenizer.nextToken.trim)
							else
								tokenizer.nextToken
						}
					}
					firstTime = false					
				}

				var stepMatchingVariables = new LinkedList<String>()
				while(stepMatchingVariables.size<12) {
					stepMatchingVariables.add(null)
				}
				for (step : scOutline.steps) {
					var exampleRowSIze = scOutline.examples.table.rows.size - 1
					var whenStepMatcher = whenStepPattern.matcher(step.description)
					var statusStepMatcher = statusPattern.matcher(step.description)
					var locationStepMatcher = locationPattern.matcher(step.description)
					var patchStepMatcher = patchPattern.matcher(step.description)
					var objectFromRepoMatcher = objectFromRepoPattern.matcher(step.description)
					var testVariableCheckMatcher = testVariableCheckPattern.matcher(step.description)
					
					while (whenStepMatcher.find) {
						if(whenStepMatcher.group(1) != null && whenStepMatcher.group(2) != null && whenStepMatcher.group(3) != null) {
							stepMatchingVariables.set(0,whenStepMatcher.group(1).substring(1,whenStepMatcher.group(1).length-1))
							stepMatchingVariables.set(1,whenStepMatcher.group(2).substring(1,whenStepMatcher.group(2).length-1))
							stepMatchingVariables.set(2,whenStepMatcher.group(3).substring(1,whenStepMatcher.group(3).length-1))
							if(whenStepMatcher.group(4) != null)
								stepMatchingVariables.set(3,whenStepMatcher.group(4).substring(1,whenStepMatcher.group(4).length-1))
							
							if (!whenStepMatcher.group(1).startsWith("<") && !whenStepMatcher.group(2).startsWith("<")) {
								methodStructureMap.put(
									whenStepMatcher.group(1).substring(1,whenStepMatcher.group(1).length-1),
									whenStepMatcher.group(2).substring(1,whenStepMatcher.group(2).length-1))
							} else if (whenStepMatcher.group(1).startsWith("<") && !whenStepMatcher.group(2).startsWith("<")) {
								for (var count=0; count < exampleRowSIze; count++) {
									methodStructureMap.put(
										associationTable.get(whenStepMatcher.group(1).substring(1,whenStepMatcher.group(1).length-1)).get(count),
										whenStepMatcher.group(2).substring(1,whenStepMatcher.group(2).length-1))
								}
							} else if (!whenStepMatcher.group(1).startsWith("<") && whenStepMatcher.group(2).startsWith("<")) {
								for (var count=0; count < exampleRowSIze; count++) {
									methodStructureMap.put(
										whenStepMatcher.group(1).substring(1,whenStepMatcher.group(1).length-1),
										associationTable.get(whenStepMatcher.group(2).substring(1,whenStepMatcher.group(2).length-1)).get(count))
								}
							} else if (whenStepMatcher.group(1).startsWith("<") && whenStepMatcher.group(2).startsWith("<")) {
								for (var count=0; count < exampleRowSIze; count++) {
									methodStructureMap.put(
										associationTable.get(whenStepMatcher.group(1).substring(1,whenStepMatcher.group(1).length-1)).get(count),
										associationTable.get(whenStepMatcher.group(2).substring(1,whenStepMatcher.group(2).length-1)).get(count))
								}
							}
						}
					}
					while (testVariableCheckMatcher.find) {
						if(testVariableCheckMatcher.group(1) != null && testVariableCheckMatcher.group(2) != null) {
							stepMatchingVariables.set(10, testVariableCheckMatcher.group(1))
							stepMatchingVariables.set(11, testVariableCheckMatcher.group(2))
						}
					}
					if(statusStepMatcher.find) {
						stepMatchingVariables.set(4,statusStepMatcher.group(1).substring(1,statusStepMatcher.group(1).length-1))
					}
					if(locationStepMatcher.find) {
						stepMatchingVariables.set(5,locationStepMatcher.group(1).substring(1,locationStepMatcher.group(1).length-1))
					}
					if(patchStepMatcher.find) {
						if(stepMatchingVariables.get(6)!=null)
							stepMatchingVariables.set(7,patchStepMatcher.group(2).substring(1,patchStepMatcher.group(2).length-1))
						else
							stepMatchingVariables.set(6,patchStepMatcher.group(2).substring(1,patchStepMatcher.group(2).length-1))
					}
					if(objectFromRepoMatcher.find) {
						if(stepMatchingVariables.get(8)!=null)
							stepMatchingVariables.set(9,objectFromRepoMatcher.group(1).substring(1,objectFromRepoMatcher.group(1).length-1))
						else	
							stepMatchingVariables.set(8,objectFromRepoMatcher.group(1).substring(1,objectFromRepoMatcher.group(1).length-1))
					}
				}
				var count = 0
				
				
				for (key : methodStructureMap.keySet) {
					var methodStrList = new ArrayList<String>()
					for (var listIndex = 0;listIndex<10;listIndex++) {
						methodStrList.add(null)
					}
					
					var isSingleObjectMethod = stepMatchingVariables.get(9)==null
					var uri = associationTable.get(stepMatchingVariables.get(2)).get(count)
					var pathVariableMatcher = pathVariablePattern.matcher(uri)
					var isPathVariableNotEMpty = pathVariableMatcher.find
					var requestBodyVariable = stepMatchingVariables.get(3)
					var patchJson1 = stepMatchingVariables.get(6)
					var patchJson2 = stepMatchingVariables.get(7)
					var isReqBodyNotEmpty = !requestBodyVariable.isNullOrEmpty
					
					var objects = new LinkedHashMap<String,ArrayList<String>>
					var initialObjectValue1 = stepMatchingVariables.get(8)
					var initialObjectValue2 = stepMatchingVariables.get(9)
					var isInitialObjectValueNotEmpty = !initialObjectValue1.isNullOrEmpty
					
					if(!initialObjectValue1.isNullOrEmpty && !initialObjectValue2.isNullOrEmpty) {
						val idName1 = initialObjectValue1.substring(initialObjectValue1.indexOf("findOne(")+8,initialObjectValue1.indexOf(")}"))
						val idName2 = initialObjectValue2.substring(initialObjectValue2.indexOf("findOne(")+8,initialObjectValue2.indexOf(")}"))
						
						objects.put(initialObjectValue1.substring(initialObjectValue1.indexOf("#{$")+3,initialObjectValue1.indexOf("s.findOne")), new ArrayList(Arrays.asList(initialObjectValue1,idName1, patchJson1)) )
						objects.put(initialObjectValue2.substring(initialObjectValue2.indexOf("#{$")+3,initialObjectValue2.indexOf("s.findOne")), new ArrayList(Arrays.asList(initialObjectValue2,idName2, patchJson2)) )
					}
					
					var String[] splitUri = #["",""]
					if (isPathVariableNotEMpty)
						splitUri = uri.split("\\{"+pathVariableMatcher.group(1)+"\\}",2)
					var status = associationTable.get(stepMatchingVariables.get(4)).get(count)
					var idVal = new String
					if(isPathVariableNotEMpty)
						idVal = associationTable.get(pathVariableMatcher.group(1)).get(count)
					
					methodStrList.set(0, key)
					methodStrList.set(1, object)
					methodStrList.set(2, uri)
					methodStrList.set(3, methodStructureMap.get(key))
					methodStrList.set(4, status)
					if(stepMatchingVariables.get(3) != null)
						methodStrList.set(5, associationTable.get(stepMatchingVariables.get(3)).get(count))
					methodStrList.set(6, stepMatchingVariables.get(10))
					methodStrList.set(7, associationTable.get(stepMatchingVariables.get(11)).get(count))
					methodStrMap.put((count+1).toString,methodStrList)
					
					if(!methodBody.contains("ResponseEntity<"+object+"> "+key+"(")) {
						if(isSingleObjectMethod) {
							methodBody = methodBody + '''
		//
			@RequestMapping(value="�uri�",method=RequestMethod.�methodStructureMap.get(key)�)
			public ResponseEntity<�object�> �key�(@RequestHeader("keepAlive") Boolean keepDbAlive�IF isPathVariableNotEMpty�, @PathVariable Long �pathVariableMatcher.group(1)��ENDIF��IF isReqBodyNotEmpty�, @RequestBody String �requestBodyVariable��ENDIF�) throws Exception{
				if (!keepDbAlive) {
					setupBackground();
				}
				
				�IF isReqBodyNotEmpty�
				JsonNode �requestBodyVariable�1 = mapper.readTree(�requestBodyVariable�);
				�ENDIF�
				�IF isPathVariableNotEMpty && isInitialObjectValueNotEmpty�
				Expression expression1 = null;
				if (!keepDbAlive) {
					expression1 = spelParser.parseExpression("�initialObjectValue1.replace("("+pathVariableMatcher.group(1)+")","("+idVal+")")�", new TemplateParserContext());
				} else {
					expression1 = spelParser.parseExpression(String.format("�initialObjectValue1.replace("("+pathVariableMatcher.group(1)+")","(new Long(%d))")�", �pathVariableMatcher.group(1)�), new TemplateParserContext());
				}
				�object� _�object.toFirstLower�1 = expression1.getValue(spelContext, �object�.class);
				JsonNode �object.toFirstLower�JsonNode = mapper.valueToTree(_�object.toFirstLower�1);
				�ENDIF�
				
				�IF isReqBodyNotEmpty && isInitialObjectValueNotEmpty�
				Iterator<JsonNode> nodeIterator = �requestBodyVariable�1.iterator();
				while(nodeIterator.hasNext()) {
					if(nodeIterator.next() instanceof NullNode) nodeIterator.remove();
					
				}
				
				�object.toFirstLower�JsonNode = $mergePatch(�object.toFirstLower�JsonNode, �requestBodyVariable�1.toString());
				�ENDIF�
				
				Expression expression = spelParser.parseExpression("�associationTable.get(patchJson1).get(count).replace("\"","\\\"")�", new TemplateParserContext());
				String expressionResult = expression.getValue(spelContext, String.class);
				�IF isReqBodyNotEmpty && !isInitialObjectValueNotEmpty�
				JsonNode mergePatchResult = $mergePatch(�requestBodyVariable�1, expressionResult);
				�object� _�object.toFirstLower� = mapper.treeToValue(mergePatchResult, �object�.class);
				�IF isPathVariableNotEMpty�
				_�object.toFirstLower�.add(new Link("�splitUri.get(0)�"+ �pathVariableMatcher.group(1)� + "�splitUri.get(1)�/" + _�object.toFirstLower�.get_id()));
				�ELSE�
				_�object.toFirstLower�.add(new Link("�uri�/" + _�object.toFirstLower�.get_id()));
				�ENDIF�
				�ELSEIF isPathVariableNotEMpty�
				JsonNode mergePatchResult = $patch(�object.toFirstLower�JsonNode, expressionResult);
				�object� _�object.toFirstLower� = mapper.treeToValue(mergePatchResult, �object�.class);
				�ENDIF�
				
				�object.toFirstLower�Repo.save(_�object.toFirstLower�);
				
				HttpHeaders headers = new HttpHeaders();
				�IF isReqBodyNotEmpty && !isPathVariableNotEMpty�
				headers.add("Location", _�object.toFirstLower�.getId().getHref());
				
				�ENDIF�
				return new ResponseEntity<�object�>(_�object.toFirstLower�, headers, HttpStatus.valueOf(�status�));
			}
			
		'''
						} else {
							var List<String> objList = new ArrayList<String>(objects.keySet)
							objList = objList.reverse
							methodBody = methodBody + '''
		//
			@RequestMapping(value="�uri�",method=RequestMethod.�methodStructureMap.get(key)�)
			public ResponseEntity<�object�> �key�(@RequestHeader("keepAlive") Boolean keepDbAlive�IF isPathVariableNotEMpty�, @PathVariable Long �pathVariableMatcher.group(1)��ENDIF��IF pathVariableMatcher.find()�, @PathVariable Long �pathVariableMatcher.group(1)��ENDIF�) throws Exception{
				if (!keepDbAlive) {
					setupBackground();
				}
				
				Expression expression1 = null;
				�FOR obj : objList�
				if (!keepDbAlive) {
					expression1 = spelParser.parseExpression("�objects.get(obj).get(0).replace("("+objects.get(obj).get(1)+")","("+associationTable.get(objects.get(obj).get(1)).get(count)+")")�", new TemplateParserContext());
				} else {
					expression1 = spelParser.parseExpression(String.format("�objects.get(obj).get(0).replace("("+objects.get(obj).get(1)+")","(new Long(%d))")�", �objects.get(obj).get(1)�), new TemplateParserContext());
				}
				�obj� _�obj.toFirstLower�1 = expression1.getValue(spelContext, �obj�.class);
				JsonNode �obj.toFirstLower�JsonNode = mapper.valueToTree(_�obj.toFirstLower�1);
				
				�var objMatcher = objectFromRepoVariablePattern.matcher(associationTable.get(objects.get(obj).get(2)).get(count))�
				�IF objMatcher.find�
				Expression expression�obj� = spelParser.parseExpression(String.format("�associationTable.get(objects.get(obj).get(2)).get(count).replace("\"","\\\"").replace("("+objMatcher.group(1)+")", "(new Long(%d))")�",�pathVariableMatcher.group(1)�), new TemplateParserContext());
				�ELSE�
				Expression expression�obj� = spelParser.parseExpression("�associationTable.get(objects.get(obj).get(2)).get(count).replace("\"","\\\"")�", new TemplateParserContext());
				�ENDIF�
				String expressionResult�obj� = expression�obj�.getValue(spelContext, String.class);
				JsonNode mergePatchResult�obj� = $patch(�obj.toFirstLower�JsonNode, expressionResult�obj�);
				�obj� _�obj.toFirstLower� = mapper.treeToValue(mergePatchResult�obj�, �obj�.class);
				
				�obj.toFirstLower�Repo.save(_�obj.toFirstLower�);
				�ENDFOR�
				
				HttpHeaders headers = new HttpHeaders();
				return new ResponseEntity<�object�>(_�object.toFirstLower�, headers, HttpStatus.valueOf(�status�));
			}
			
		'''				
						}
					}
					count = count + 1
				}
				methods.put(scOutline.title,methodStrMap)
			}
		}
	
		var completeBody = '''
		package com.example;
		
		import java.util.Iterator;
		
		import org.springframework.beans.factory.annotation.Autowired;
		import org.springframework.expression.*;
		import org.springframework.expression.common.TemplateParserContext;
		import org.springframework.expression.spel.standard.SpelExpressionParser;
		import org.springframework.expression.spel.support.StandardEvaluationContext;
		import org.springframework.hateoas.Link;
		import org.springframework.http.*;
		import org.springframework.web.bind.annotation.*;
		import org.springframework.stereotype.Component;
		import com.example.models.*;
		import com.fasterxml.jackson.databind.*;
		import com.fasterxml.jackson.databind.node.NullNode;
		import com.github.fge.jsonpatch.JsonPatch;
		import com.github.fge.jsonpatch.mergepatch.JsonMergePatch;
		
		@Component
		public class Skeleton {
			�FOR key : typeMap.keySet�
			@Autowired
			Mock�typeMap.get(key).value�Repository �key�;

			�ENDFOR�
			ObjectMapper mapper = new ObjectMapper().findAndRegisterModules();
			
			StandardEvaluationContext spelContext;
			ExpressionParser spelParser;
			
			class LocalSpelContext {
				�FOR key : typeMap.keySet�
				public Mock�typeMap.get(key).value�Repository $�typeMap.get(key).value�s = �key�;
				�ENDFOR�
				public String $toJson(Object o) throws Exception {
					return mapper.writeValueAsString(o);
				}		
			}
			
			public JsonNode $mergePatch(JsonNode obj, String json) throws Exception {
				JsonMergePatch mp = mapper.readValue(json, JsonMergePatch.class);
				return mp.apply(obj);
			}
		
			public JsonNode $patch(JsonNode obj, String json) throws Exception {
				JsonPatch mp = mapper.readValue(json, JsonPatch.class);
				return mp.apply(obj);
			}
			
		'''

		var fixtureString = ''''''
		for (key : typeMap.keySet) {
			fixtureString = fixtureString + 
			'''
			//
					for (String json: �typeMap.get(key).key�) {
						Expression expression = spelParser.parseExpression(json, new TemplateParserContext());
						String rewrittenJson = expression.getValue(spelContext, String.class);
						�key�.save(mapper.readValue(rewrittenJson, �typeMap.get(key).value�.class));
					}
			'''
		}
		
		completeBody + fixtures + '''
		//
			@RequestMapping("/initialize")
			public void setupBackground() throws Exception {		
				spelContext = new StandardEvaluationContext(new LocalSpelContext());
				spelParser = new SpelExpressionParser();
				spelContext.registerFunction("$toJson", LocalSpelContext.class.getDeclaredMethod("$toJson", new Class[] { Object.class }));
		''' + fixtureString + '''
		//
			}
			'''+ methodBody + '''
		}
	'''
	}
	
	def generateHelper (Feature feature) {
		var typeMap = new LinkedHashMap<String,Pair<String,String>>()
		var fixtures = new String
		for (step: feature.background.steps) {
			for (table : step.tables) {
				var type = step.description.substring(step.description.indexOf("following $") + "following $".length)
				type = type.substring(0,type.length-1)
				fixtures = fixtures + "\tString[] " + type.toFirstLower + "Fixtures = {\n"
				typeMap.put(type.toFirstLower+"Repo",new Pair(type.toFirstLower+"Fixtures",type.substring(0,type.length)))
				var titleRow = table.rows.get(0)
				var titleArray = titleRow.split("\\|", -1);
				titleArray = titleArray.subList(1,titleArray.size-1)
				for (valueRow : table.rows.subList(1, table.rows.size)) {
					var valueArray = valueRow.split("\\|", -1);
					valueArray = valueArray.subList(1,valueArray.size-1)
					var firstIteration = true
					fixtures = fixtures + "\t\t\"{"
					for (item : titleArray) {
						var value = valueArray.get(titleArray.indexOf(item)).trim
						var strFormat = "%s\"%s\\\":"+(if (value.startsWith("#{$")) "%s" else "\\\"%s\\\"")
						fixtures = fixtures + String.format(strFormat, (if (firstIteration) "\\" else ",\\"), 
							item.trim, value
						)
						firstIteration = false
					}
					fixtures = fixtures + "}\",\n"
				}
				fixtures = fixtures + "\t};\n"
			}
		}
		var Pattern phPattern = Pattern.compile("<(\\w+)>")
		var Pattern tableHeaderPattern = Pattern.compile("(\\w+)")
		var Pattern whenStepPattern = Pattern.compile(".+?\\bcalls?\\s+(\\'\\w+\\'|<\\w+>)\\s+using\\s+(\\'\\w+\\'|<\\w+>)\\s+on\\s+(\\'\\w+\\'|<\\w+>)(?:\\s+with\\s+(\\'\\w+\\'|<\\w+>))?")
		var Pattern objectFromRepoPattern = Pattern.compile("[.+]?<\\w+>\\sshould\\sbe\\s('.*')")
		var Pattern pathVariablePattern = Pattern.compile("/\\{(\\w+)\\}/?")
		
		var methodBody = ''''''
		for (scenario : feature.scenarios) {
			if (scenario instanceof ScenarioOutline) {
				
				var methodStructureMap = new LinkedHashMap<String, String>()
				var object = scenario.title.substring(scenario.title.lastIndexOf(" ")+1)
				var associationTable = LinkedListMultimap.create()
				var variables = new ArrayList<String>()
				var scOutline = scenario as ScenarioOutline
				
				var header = scOutline.examples.table.rows.get(0)
				var headerMatcher = tableHeaderPattern.matcher(header)
				
				while (headerMatcher.find)
					variables.add(headerMatcher.group(1))
								
				println(variables)
				var firstTime = true
				
				for (row : scOutline.examples.table.rows) {
					if (!firstTime) {
						var tokenizer = new StringTokenizer(row, "|")
						var pos = 0;
						while (tokenizer.hasMoreTokens) {
							if (pos < variables.size)
								associationTable.put(variables.get(pos++), tokenizer.nextToken.trim)
							else
								tokenizer.nextToken
						}
					}
					firstTime = false					
				}
				println(associationTable)
				for (step : scOutline.steps) {
					var phs = phPattern.matcher(step.description)
					while (phs.find) {
						println("PH: " + phs.group(1))
					}
				}
				var stepMatchingVariables = new LinkedList<String>()
				while(stepMatchingVariables.size<5) {
					stepMatchingVariables.add(null)
				}
				for (step : scOutline.steps) {
					var exampleRowSIze = scOutline.examples.table.rows.size - 1
					var whenStepMatcher = whenStepPattern.matcher(step.description)
					var objectFromRepoMatcher = objectFromRepoPattern.matcher(step.description)
					
					while (whenStepMatcher.find) {
						if(whenStepMatcher.group(1) != null && whenStepMatcher.group(2) != null && whenStepMatcher.group(3) != null) {
							stepMatchingVariables.set(0,whenStepMatcher.group(1).substring(1,whenStepMatcher.group(1).length-1))
							stepMatchingVariables.set(1,whenStepMatcher.group(2).substring(1,whenStepMatcher.group(2).length-1))
							stepMatchingVariables.set(2,whenStepMatcher.group(3).substring(1,whenStepMatcher.group(3).length-1))
							if(whenStepMatcher.group(4) != null)
								stepMatchingVariables.set(3,whenStepMatcher.group(4).substring(1,whenStepMatcher.group(4).length-1))
							
							if (!whenStepMatcher.group(1).startsWith("<") && !whenStepMatcher.group(2).startsWith("<")) {
								methodStructureMap.put(
									whenStepMatcher.group(1).substring(1,whenStepMatcher.group(1).length-1),
									whenStepMatcher.group(2).substring(1,whenStepMatcher.group(2).length-1))
							} else if (whenStepMatcher.group(1).startsWith("<") && !whenStepMatcher.group(2).startsWith("<")) {
								for (var count=0; count < exampleRowSIze; count++) {
									methodStructureMap.put(
										associationTable.get(whenStepMatcher.group(1).substring(1,whenStepMatcher.group(1).length-1)).get(count),
										whenStepMatcher.group(2).substring(1,whenStepMatcher.group(2).length-1))
								}
							} else if (!whenStepMatcher.group(1).startsWith("<") && whenStepMatcher.group(2).startsWith("<")) {
								for (var count=0; count < exampleRowSIze; count++) {
									methodStructureMap.put(
										whenStepMatcher.group(1).substring(1,whenStepMatcher.group(1).length-1),
										associationTable.get(whenStepMatcher.group(2).substring(1,whenStepMatcher.group(2).length-1)).get(count))
								}
							} else if (whenStepMatcher.group(1).startsWith("<") && whenStepMatcher.group(2).startsWith("<")) {
								for (var count=0; count < exampleRowSIze; count++) {
									methodStructureMap.put(
										associationTable.get(whenStepMatcher.group(1).substring(1,whenStepMatcher.group(1).length-1)).get(count),
										associationTable.get(whenStepMatcher.group(2).substring(1,whenStepMatcher.group(2).length-1)).get(count))
								}
							}
						}
					}
					if (objectFromRepoMatcher.find) {
						stepMatchingVariables.set(4,objectFromRepoMatcher.group(1))
					}
				}
				var count = 0
				for (key : methodStructureMap.keySet) {
				
					var requestBodyVariable = stepMatchingVariables.get(3)
					var isReqBodyNotEmpty = !requestBodyVariable.isNullOrEmpty
					var initialObjectValue = stepMatchingVariables.get(4)
					if (initialObjectValue != null) {
						initialObjectValue = initialObjectValue.substring(1,initialObjectValue.length-1)
					}
					var uri = associationTable.get(stepMatchingVariables.get(2)).get(count)
					var pathVariableMatcher = pathVariablePattern.matcher(uri)
					var isPathVariableNotEMpty = pathVariableMatcher.find
					var idVal = new String
					if(isPathVariableNotEMpty)
						idVal = associationTable.get(pathVariableMatcher.group(1)).get(count)
					if (!methodBody.contains("get"+key.toFirstUpper+"Data()")) {
						if(isReqBodyNotEmpty && initialObjectValue == null) {
							methodBody = methodBody + '''
				//
					public �object� get�key.toFirstUpper�Data() throws Exception{
						Expression expression = spelParser.parseExpression("�associationTable.get(requestBodyVariable).get(count).replace("\"","\\\"")�", new TemplateParserContext());
						String expressionResult = expression.getValue(spelContext, String.class);
						�object� _�object.toFirstLower� = mapper.readValue(expressionResult, �object�.class);
						return _�object.toFirstLower�;
					}
					
				'''
						} else if (!isReqBodyNotEmpty && initialObjectValue != null){
							methodBody = methodBody + '''
				//
					public �object� get�key.toFirstUpper�Data() throws Exception{
						Expression expression = spelParser.parseExpression("�initialObjectValue.replace("("+pathVariableMatcher.group(1)+")","("+idVal+")")�", new TemplateParserContext());
						�object� _�object.toFirstLower� = expression.getValue(spelContext, �object�.class);
						return _�object.toFirstLower�;
					}
					
				'''
						} else if (isReqBodyNotEmpty && initialObjectValue != null){
							methodBody = methodBody + '''
				//
					public �object� get�key.toFirstUpper�Data() throws Exception{
						Expression expression = spelParser.parseExpression("�associationTable.get(requestBodyVariable).get(count).replace("\"","\\\"")�", new TemplateParserContext());
						String expressionResult = expression.getValue(spelContext, String.class);
						�object� _�object.toFirstLower� = mapper.readValue(expressionResult, �object�.class);
						return _�object.toFirstLower�;
					}
					
					public �object� get�key.toFirstUpper�Data2() throws Exception{
						Expression expression = spelParser.parseExpression("�initialObjectValue.replace("("+pathVariableMatcher.group(1)+")","("+idVal+")")�", new TemplateParserContext());
						�object� _�object.toFirstLower� = expression.getValue(spelContext, �object�.class);
						return _�object.toFirstLower�;
					}
				'''
						}	
					}
					count = count + 1
				}
			}
		}
	
		var completeBody = '''
		package com.example;
		
		import org.springframework.beans.factory.annotation.Autowired;
		import org.springframework.expression.*;
		import org.springframework.expression.common.TemplateParserContext;
		import org.springframework.expression.spel.standard.SpelExpressionParser;
		import org.springframework.expression.spel.support.StandardEvaluationContext;
		import org.springframework.stereotype.Component;
		�FOR key : typeMap.keySet�
		import com.example.models.�typeMap.get(key).value�;
		import com.example.models.�typeMap.get(key).value�Repository;
		�ENDFOR�
		import com.fasterxml.jackson.databind.*;
		import com.github.fge.jsonpatch.JsonPatch;
		import com.github.fge.jsonpatch.mergepatch.JsonMergePatch;
		
		@Component
		public class SkeletonHelper {
			�FOR key : typeMap.keySet�
			@Autowired
			�typeMap.get(key).value�Repository �key�;

			�ENDFOR�
			ObjectMapper mapper = new ObjectMapper().findAndRegisterModules();
			
			StandardEvaluationContext spelContext;
			ExpressionParser spelParser;
			
			class LocalSpelContext {
				�FOR key : typeMap.keySet�
				public �typeMap.get(key).value�Repository $�typeMap.get(key).value�s = �key�;
				�ENDFOR�
				public String $toJson(Object o) throws Exception {
					return mapper.writeValueAsString(o);
				}		
			}
			
			public JsonNode $mergePatch(JsonNode obj, String json) throws Exception {
				JsonMergePatch mp = mapper.readValue(json, JsonMergePatch.class);
				return mp.apply(obj);
			}
		
			public JsonNode $patch(JsonNode obj, String json) throws Exception {
				JsonPatch mp = mapper.readValue(json, JsonPatch.class);
				return mp.apply(obj);
			}
			
		'''

		var fixtureString = ''''''
		for (key : typeMap.keySet) {
			fixtureString = fixtureString + 
			'''
			//
					for (String json: �typeMap.get(key).key�) {
						Expression expression = spelParser.parseExpression(json, new TemplateParserContext());
						String rewrittenJson = expression.getValue(spelContext, String.class);
						�key�.save(mapper.readValue(rewrittenJson, �typeMap.get(key).value�.class));
					}
			'''
		}
		
		completeBody + fixtures + '''
		//
			public void setupBackground() throws Exception {
				spelContext = new StandardEvaluationContext(new LocalSpelContext());
				spelParser = new SpelExpressionParser();
				spelContext.registerFunction("$toJson", LocalSpelContext.class.getDeclaredMethod("$toJson", new Class[] { Object.class }));
		''' + fixtureString + '''
		//
			}
			'''+ methodBody + '''
		}
	'''
	}
	
	def generateTest (Feature feature) {	
	
		var testBody = ''''''
		for (scenario : feature.scenarios) {
			if (scenario instanceof Scenario) {
				var Pattern scenarioSelectPattern = Pattern.compile("[.+]?scenario\\s\"(.+)\"\\swith\\s\\[([0-9]+)\\]")
				var Pattern pathVariablePattern = Pattern.compile("/\\{(\\w+)\\}/?")
				
				var testMethodHeader = "test"+getTestHeading(scenario.title)
				var object = scenario.title.substring(scenario.title.lastIndexOf(" ")+1)
				var newObject = new String
				var stepBody = ''''''
				var isSingleStepTest = scenario.steps.length == 1
				for (step : scenario.steps) {
					var scenarioSelectMatcher = scenarioSelectPattern.matcher(step.description)
					if(scenarioSelectMatcher.find) {
						var scenarioTitle = scenarioSelectMatcher.group(1)
						var exampleIndex = scenarioSelectMatcher.group(2)
						var methodStrList = methods.get(scenarioTitle).get(exampleIndex)
						
						var methodName = methodStrList.get(0)
						var uri = methodStrList.get(2)
						var verb = methodStrList.get(3)
						var status = methodStrList.get(4)
						var reqBody = methodStrList.get(5)
						var checkerVariable = methodStrList.get(6)
						var checkerVarValue = methodStrList.get(7)
						
						var pathVariableMatcher = pathVariablePattern.matcher(uri)
						var isPathVariableNotEMpty = pathVariableMatcher.find
						
						var stIndex = step.description.indexOf("\"")+1
						var endIndex = step.description.lastIndexOf("\"")
						var temp1 = step.description.substring(stIndex,endIndex)
						var currentObject = temp1.substring(temp1.lastIndexOf(" ")+1)
						if(currentObject.equals(object) || isSingleStepTest) {
							stepBody = stepBody + '''
				//
						�IF !reqBody.isNullOrEmpty || isSingleStepTest�
						�object.toFirstLower� = helper.get�methodName.toFirstUpper�Data();
						�ENDIF�
						�IF !reqBody.isNullOrEmpty && isSingleStepTest && isPathVariableNotEMpty && verb=="PUT"�
						_�object.toFirstLower� = helper.get�methodName.toFirstUpper�Data2();
						�ENDIF�
						result = mockMvc.perform(�verb.toLowerCase�("/generated�uri�"�IF (isPathVariableNotEMpty && !isSingleStepTest) || 
						(!reqBody.isNullOrEmpty && isSingleStepTest && isPathVariableNotEMpty && verb=="PUT")�,_�object.toFirstLower�.get_id()�ELSEIF 
						isSingleStepTest�,�object.toFirstLower�.get_id()�ENDIF��IF pathVariableMatcher.find�,_�newObject.toFirstLower�.get_id()�ENDIF�)
												.header("keepAlive", �IF isSingleStepTest�false�ELSE�true�ENDIF�)�IF reqBody.isNullOrEmpty && isPathVariableNotEMpty�)�ENDIF��IF !reqBody.isNullOrEmpty�
												.content(mapper.writeValueAsString(�object.toFirstLower�))
												.contentType(MediaType.APPLICATION_JSON)�IF !reqBody.isNullOrEmpty�)�ENDIF��ENDIF�
												.andExpect(status().is(�status�))
												.andReturn();
						
						_�object.toFirstLower� = mapper.readValue(result.getResponse().getContentAsString(), �object�.class);
						Assert.assertThat(_�currentObject.toFirstLower�.get�checkerVariable.toFirstUpper�().toString(), equalTo("�checkerVarValue�"));
						
						'''
						} else {
							newObject = currentObject
							if (!stepBody.contains(currentObject + " _" + currentObject.toFirstLower + " = null;")) {
								stepBody = stepBody + '''
						//		
								�currentObject� _�currentObject.toFirstLower� = null;
								�currentObject� �currentObject.toFirstLower� = null;
								'''
							}
							
							stepBody = stepBody + '''
				//
						�IF !reqBody.isNullOrEmpty �
						�currentObject.toFirstLower� = helper.get�methodName.toFirstUpper�Data();
						�ENDIF�
						result = mockMvc.perform(�verb.toLowerCase�("/generated�uri�"�IF isPathVariableNotEMpty�,_�object.toFirstLower�.get_id()�ENDIF�)
												.header("keepAlive", true)�IF reqBody.isNullOrEmpty && isPathVariableNotEMpty�)�ENDIF��IF !reqBody.isNullOrEmpty�
												.content(mapper.writeValueAsString(�currentObject.toFirstLower�))
												.contentType(MediaType.APPLICATION_JSON)�IF !reqBody.isNullOrEmpty�)�ENDIF��ENDIF�
												.andExpect(status().is(�status�))
												.andReturn();
						
						_�currentObject.toFirstLower� = mapper.readValue(result.getResponse().getContentAsString(), �currentObject�.class);
						Assert.assertThat(_�currentObject.toFirstLower�.get�checkerVariable.toFirstUpper�().toString(), equalTo("�checkerVarValue�"));
						
						'''
						}
					}
				}
				
		testBody = testBody + '''
		
			@Test
			public void �testMethodHeader�() throws Exception {
				MvcResult result = null;
				�object� _�object.toFirstLower� = null;
				�object� �object.toFirstLower� = null;
				''' + stepBody + '''
		//
		    }
		    
		'''
			}
		}
		
		'''
		package com.example;
		
		import org.junit.*;
		import org.junit.runner.RunWith;
		import org.springframework.beans.factory.annotation.*;
		import org.springframework.boot.test.SpringApplicationConfiguration;
		import org.springframework.http.MediaType;
		import org.springframework.test.annotation.DirtiesContext;
		import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
		import org.springframework.test.context.web.WebAppConfiguration;
		import org.springframework.test.web.servlet.*;
		import org.springframework.test.web.servlet.setup.MockMvcBuilders;
		import org.springframework.web.context.WebApplicationContext;
		
		import com.example.models.*;
		import com.fasterxml.jackson.databind.ObjectMapper;
		
		import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
		import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
		import static org.hamcrest.CoreMatchers.equalTo;
		
		@RunWith(SpringJUnit4ClassRunner.class)
		@SpringApplicationConfiguration(classes = DemoApplication.class)
		@WebAppConfiguration
		@DirtiesContext
		public class SkeletonTest {
			@Autowired
			SkeletonHelper helper;
						
			@Autowired
			private WebApplicationContext wac;
		
			@Autowired
			@Qualifier("_halObjectMapper")
			ObjectMapper mapper;
		
			private MockMvc mockMvc;
		
			@Before
			public void setup() throws Exception {
				this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
				helper.setupBackground();
				mockMvc.perform(post("/generated/initialize"));
			}

		''' + testBody + '''
		}
		'''
	}

	def String getTestHeading(String title) {
		var StringBuilder sb = new StringBuilder(title);
		var Pattern pattern = Pattern.compile("\\s\\w");
		var matcher = pattern.matcher(sb);
		var pos = 0;
		while (matcher.find(pos)) {
		    var String replacement = matcher.group().substring(1).toUpperCase();
		    pos = matcher.start();
		    sb.replace(pos, pos + 2, replacement);
		    pos += 1;
		}
		return sb.toString();
	}
}
