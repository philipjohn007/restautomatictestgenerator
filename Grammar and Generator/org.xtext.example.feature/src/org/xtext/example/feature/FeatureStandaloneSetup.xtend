/*
 * generated by Xtext 2.9.1
 */
package org.xtext.example.feature


/**
 * Initialization support for running Xtext languages without Equinox extension registry.
 */
class FeatureStandaloneSetup extends FeatureStandaloneSetupGenerated {

	def static void doSetup() {
		new FeatureStandaloneSetup().createInjectorAndDoEMFRegistration()
	}
}
