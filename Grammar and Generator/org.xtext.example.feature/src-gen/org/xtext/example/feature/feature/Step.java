/**
 * generated by Xtext 2.9.1
 */
package org.xtext.example.feature.feature;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Step</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.xtext.example.feature.feature.Step#getStepKeyword <em>Step Keyword</em>}</li>
 *   <li>{@link org.xtext.example.feature.feature.Step#getDescription <em>Description</em>}</li>
 *   <li>{@link org.xtext.example.feature.feature.Step#getTables <em>Tables</em>}</li>
 *   <li>{@link org.xtext.example.feature.feature.Step#getCode <em>Code</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.xtext.example.feature.feature.FeaturePackage#getStep()
 * @model
 * @generated
 */
public interface Step extends EObject
{
  /**
   * Returns the value of the '<em><b>Step Keyword</b></em>' attribute.
   * The literals are from the enumeration {@link org.xtext.example.feature.feature.StepKeyword}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Step Keyword</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Step Keyword</em>' attribute.
   * @see org.xtext.example.feature.feature.StepKeyword
   * @see #setStepKeyword(StepKeyword)
   * @see org.xtext.example.feature.feature.FeaturePackage#getStep_StepKeyword()
   * @model
   * @generated
   */
  StepKeyword getStepKeyword();

  /**
   * Sets the value of the '{@link org.xtext.example.feature.feature.Step#getStepKeyword <em>Step Keyword</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Step Keyword</em>' attribute.
   * @see org.xtext.example.feature.feature.StepKeyword
   * @see #getStepKeyword()
   * @generated
   */
  void setStepKeyword(StepKeyword value);

  /**
   * Returns the value of the '<em><b>Description</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Description</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Description</em>' attribute.
   * @see #setDescription(String)
   * @see org.xtext.example.feature.feature.FeaturePackage#getStep_Description()
   * @model
   * @generated
   */
  String getDescription();

  /**
   * Sets the value of the '{@link org.xtext.example.feature.feature.Step#getDescription <em>Description</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Description</em>' attribute.
   * @see #getDescription()
   * @generated
   */
  void setDescription(String value);

  /**
   * Returns the value of the '<em><b>Tables</b></em>' containment reference list.
   * The list contents are of type {@link org.xtext.example.feature.feature.Table}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Tables</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Tables</em>' containment reference list.
   * @see org.xtext.example.feature.feature.FeaturePackage#getStep_Tables()
   * @model containment="true"
   * @generated
   */
  EList<Table> getTables();

  /**
   * Returns the value of the '<em><b>Code</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Code</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Code</em>' containment reference.
   * @see #setCode(DocString)
   * @see org.xtext.example.feature.feature.FeaturePackage#getStep_Code()
   * @model containment="true"
   * @generated
   */
  DocString getCode();

  /**
   * Sets the value of the '{@link org.xtext.example.feature.feature.Step#getCode <em>Code</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Code</em>' containment reference.
   * @see #getCode()
   * @generated
   */
  void setCode(DocString value);

} // Step
