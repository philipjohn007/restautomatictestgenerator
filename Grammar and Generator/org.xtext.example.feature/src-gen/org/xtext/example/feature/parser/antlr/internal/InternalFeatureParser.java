package org.xtext.example.feature.parser.antlr.internal;

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.common.util.Enumerator;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import org.xtext.example.feature.services.FeatureGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalFeatureParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_EOL", "RULE_TABLE_ROW", "RULE_DOC_STRING", "RULE_WORD", "RULE_NUMBER", "RULE_STRING", "RULE_PLACEHOLDER", "RULE_TAGNAME", "RULE_NL", "RULE_SL_COMMENT", "RULE_WS", "RULE_ID", "RULE_INT", "RULE_ML_COMMENT", "RULE_ANY_OTHER", "'Feature:'", "'Background:'", "'Scenario:'", "'Scenario Outline:'", "'Examples:'", "'Given'", "'When'", "'Then'", "'And'", "'But'"
    };
    public static final int RULE_DOC_STRING=6;
    public static final int RULE_WORD=7;
    public static final int RULE_STRING=9;
    public static final int RULE_SL_COMMENT=13;
    public static final int T__19=19;
    public static final int EOF=-1;
    public static final int RULE_ID=15;
    public static final int RULE_EOL=4;
    public static final int RULE_WS=14;
    public static final int RULE_TAGNAME=11;
    public static final int RULE_ANY_OTHER=18;
    public static final int RULE_NUMBER=8;
    public static final int T__26=26;
    public static final int RULE_PLACEHOLDER=10;
    public static final int T__27=27;
    public static final int RULE_TABLE_ROW=5;
    public static final int T__28=28;
    public static final int RULE_INT=16;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=17;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int RULE_NL=12;
    public static final int T__20=20;
    public static final int T__21=21;

    // delegates
    // delegators


        public InternalFeatureParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalFeatureParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalFeatureParser.tokenNames; }
    public String getGrammarFileName() { return "InternalFeature.g"; }



     	private FeatureGrammarAccess grammarAccess;

        public InternalFeatureParser(TokenStream input, FeatureGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }

        @Override
        protected String getFirstRuleName() {
        	return "Feature";
       	}

       	@Override
       	protected FeatureGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}




    // $ANTLR start "entryRuleFeature"
    // InternalFeature.g:65:1: entryRuleFeature returns [EObject current=null] : iv_ruleFeature= ruleFeature EOF ;
    public final EObject entryRuleFeature() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleFeature = null;


        try {
            // InternalFeature.g:65:48: (iv_ruleFeature= ruleFeature EOF )
            // InternalFeature.g:66:2: iv_ruleFeature= ruleFeature EOF
            {
             newCompositeNode(grammarAccess.getFeatureRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleFeature=ruleFeature();

            state._fsp--;

             current =iv_ruleFeature; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleFeature"


    // $ANTLR start "ruleFeature"
    // InternalFeature.g:72:1: ruleFeature returns [EObject current=null] : ( ( (lv_tags_0_0= ruleTag ) )* otherlv_1= 'Feature:' ( (lv_title_2_0= ruleTitle ) ) (this_EOL_3= RULE_EOL )+ ( (lv_narrative_4_0= ruleNarrative ) )? ( (lv_background_5_0= ruleBackground ) )? ( ( (lv_scenarios_6_1= ruleScenario | lv_scenarios_6_2= ruleScenarioOutline ) ) )+ ) ;
    public final EObject ruleFeature() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token this_EOL_3=null;
        EObject lv_tags_0_0 = null;

        AntlrDatatypeRuleToken lv_title_2_0 = null;

        AntlrDatatypeRuleToken lv_narrative_4_0 = null;

        EObject lv_background_5_0 = null;

        EObject lv_scenarios_6_1 = null;

        EObject lv_scenarios_6_2 = null;



        	enterRule();

        try {
            // InternalFeature.g:78:2: ( ( ( (lv_tags_0_0= ruleTag ) )* otherlv_1= 'Feature:' ( (lv_title_2_0= ruleTitle ) ) (this_EOL_3= RULE_EOL )+ ( (lv_narrative_4_0= ruleNarrative ) )? ( (lv_background_5_0= ruleBackground ) )? ( ( (lv_scenarios_6_1= ruleScenario | lv_scenarios_6_2= ruleScenarioOutline ) ) )+ ) )
            // InternalFeature.g:79:2: ( ( (lv_tags_0_0= ruleTag ) )* otherlv_1= 'Feature:' ( (lv_title_2_0= ruleTitle ) ) (this_EOL_3= RULE_EOL )+ ( (lv_narrative_4_0= ruleNarrative ) )? ( (lv_background_5_0= ruleBackground ) )? ( ( (lv_scenarios_6_1= ruleScenario | lv_scenarios_6_2= ruleScenarioOutline ) ) )+ )
            {
            // InternalFeature.g:79:2: ( ( (lv_tags_0_0= ruleTag ) )* otherlv_1= 'Feature:' ( (lv_title_2_0= ruleTitle ) ) (this_EOL_3= RULE_EOL )+ ( (lv_narrative_4_0= ruleNarrative ) )? ( (lv_background_5_0= ruleBackground ) )? ( ( (lv_scenarios_6_1= ruleScenario | lv_scenarios_6_2= ruleScenarioOutline ) ) )+ )
            // InternalFeature.g:80:3: ( (lv_tags_0_0= ruleTag ) )* otherlv_1= 'Feature:' ( (lv_title_2_0= ruleTitle ) ) (this_EOL_3= RULE_EOL )+ ( (lv_narrative_4_0= ruleNarrative ) )? ( (lv_background_5_0= ruleBackground ) )? ( ( (lv_scenarios_6_1= ruleScenario | lv_scenarios_6_2= ruleScenarioOutline ) ) )+
            {
            // InternalFeature.g:80:3: ( (lv_tags_0_0= ruleTag ) )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( (LA1_0==RULE_TAGNAME) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // InternalFeature.g:81:4: (lv_tags_0_0= ruleTag )
            	    {
            	    // InternalFeature.g:81:4: (lv_tags_0_0= ruleTag )
            	    // InternalFeature.g:82:5: lv_tags_0_0= ruleTag
            	    {

            	    					newCompositeNode(grammarAccess.getFeatureAccess().getTagsTagParserRuleCall_0_0());
            	    				
            	    pushFollow(FOLLOW_3);
            	    lv_tags_0_0=ruleTag();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getFeatureRule());
            	    					}
            	    					add(
            	    						current,
            	    						"tags",
            	    						lv_tags_0_0,
            	    						"org.xtext.example.feature.Feature.Tag");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);

            otherlv_1=(Token)match(input,19,FOLLOW_4); 

            			newLeafNode(otherlv_1, grammarAccess.getFeatureAccess().getFeatureKeyword_1());
            		
            // InternalFeature.g:103:3: ( (lv_title_2_0= ruleTitle ) )
            // InternalFeature.g:104:4: (lv_title_2_0= ruleTitle )
            {
            // InternalFeature.g:104:4: (lv_title_2_0= ruleTitle )
            // InternalFeature.g:105:5: lv_title_2_0= ruleTitle
            {

            					newCompositeNode(grammarAccess.getFeatureAccess().getTitleTitleParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_5);
            lv_title_2_0=ruleTitle();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getFeatureRule());
            					}
            					set(
            						current,
            						"title",
            						lv_title_2_0,
            						"org.xtext.example.feature.Feature.Title");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalFeature.g:122:3: (this_EOL_3= RULE_EOL )+
            int cnt2=0;
            loop2:
            do {
                int alt2=2;
                int LA2_0 = input.LA(1);

                if ( (LA2_0==RULE_EOL) ) {
                    alt2=1;
                }


                switch (alt2) {
            	case 1 :
            	    // InternalFeature.g:123:4: this_EOL_3= RULE_EOL
            	    {
            	    this_EOL_3=(Token)match(input,RULE_EOL,FOLLOW_6); 

            	    				newLeafNode(this_EOL_3, grammarAccess.getFeatureAccess().getEOLTerminalRuleCall_3());
            	    			

            	    }
            	    break;

            	default :
            	    if ( cnt2 >= 1 ) break loop2;
                        EarlyExitException eee =
                            new EarlyExitException(2, input);
                        throw eee;
                }
                cnt2++;
            } while (true);

            // InternalFeature.g:128:3: ( (lv_narrative_4_0= ruleNarrative ) )?
            int alt3=2;
            int LA3_0 = input.LA(1);

            if ( ((LA3_0>=RULE_WORD && LA3_0<=RULE_PLACEHOLDER)) ) {
                alt3=1;
            }
            switch (alt3) {
                case 1 :
                    // InternalFeature.g:129:4: (lv_narrative_4_0= ruleNarrative )
                    {
                    // InternalFeature.g:129:4: (lv_narrative_4_0= ruleNarrative )
                    // InternalFeature.g:130:5: lv_narrative_4_0= ruleNarrative
                    {

                    					newCompositeNode(grammarAccess.getFeatureAccess().getNarrativeNarrativeParserRuleCall_4_0());
                    				
                    pushFollow(FOLLOW_7);
                    lv_narrative_4_0=ruleNarrative();

                    state._fsp--;


                    					if (current==null) {
                    						current = createModelElementForParent(grammarAccess.getFeatureRule());
                    					}
                    					set(
                    						current,
                    						"narrative",
                    						lv_narrative_4_0,
                    						"org.xtext.example.feature.Feature.Narrative");
                    					afterParserOrEnumRuleCall();
                    				

                    }


                    }
                    break;

            }

            // InternalFeature.g:147:3: ( (lv_background_5_0= ruleBackground ) )?
            int alt4=2;
            int LA4_0 = input.LA(1);

            if ( (LA4_0==20) ) {
                alt4=1;
            }
            switch (alt4) {
                case 1 :
                    // InternalFeature.g:148:4: (lv_background_5_0= ruleBackground )
                    {
                    // InternalFeature.g:148:4: (lv_background_5_0= ruleBackground )
                    // InternalFeature.g:149:5: lv_background_5_0= ruleBackground
                    {

                    					newCompositeNode(grammarAccess.getFeatureAccess().getBackgroundBackgroundParserRuleCall_5_0());
                    				
                    pushFollow(FOLLOW_7);
                    lv_background_5_0=ruleBackground();

                    state._fsp--;


                    					if (current==null) {
                    						current = createModelElementForParent(grammarAccess.getFeatureRule());
                    					}
                    					set(
                    						current,
                    						"background",
                    						lv_background_5_0,
                    						"org.xtext.example.feature.Feature.Background");
                    					afterParserOrEnumRuleCall();
                    				

                    }


                    }
                    break;

            }

            // InternalFeature.g:166:3: ( ( (lv_scenarios_6_1= ruleScenario | lv_scenarios_6_2= ruleScenarioOutline ) ) )+
            int cnt6=0;
            loop6:
            do {
                int alt6=2;
                int LA6_0 = input.LA(1);

                if ( (LA6_0==RULE_TAGNAME||(LA6_0>=21 && LA6_0<=22)) ) {
                    alt6=1;
                }


                switch (alt6) {
            	case 1 :
            	    // InternalFeature.g:167:4: ( (lv_scenarios_6_1= ruleScenario | lv_scenarios_6_2= ruleScenarioOutline ) )
            	    {
            	    // InternalFeature.g:167:4: ( (lv_scenarios_6_1= ruleScenario | lv_scenarios_6_2= ruleScenarioOutline ) )
            	    // InternalFeature.g:168:5: (lv_scenarios_6_1= ruleScenario | lv_scenarios_6_2= ruleScenarioOutline )
            	    {
            	    // InternalFeature.g:168:5: (lv_scenarios_6_1= ruleScenario | lv_scenarios_6_2= ruleScenarioOutline )
            	    int alt5=2;
            	    alt5 = dfa5.predict(input);
            	    switch (alt5) {
            	        case 1 :
            	            // InternalFeature.g:169:6: lv_scenarios_6_1= ruleScenario
            	            {

            	            						newCompositeNode(grammarAccess.getFeatureAccess().getScenariosScenarioParserRuleCall_6_0_0());
            	            					
            	            pushFollow(FOLLOW_8);
            	            lv_scenarios_6_1=ruleScenario();

            	            state._fsp--;


            	            						if (current==null) {
            	            							current = createModelElementForParent(grammarAccess.getFeatureRule());
            	            						}
            	            						add(
            	            							current,
            	            							"scenarios",
            	            							lv_scenarios_6_1,
            	            							"org.xtext.example.feature.Feature.Scenario");
            	            						afterParserOrEnumRuleCall();
            	            					

            	            }
            	            break;
            	        case 2 :
            	            // InternalFeature.g:185:6: lv_scenarios_6_2= ruleScenarioOutline
            	            {

            	            						newCompositeNode(grammarAccess.getFeatureAccess().getScenariosScenarioOutlineParserRuleCall_6_0_1());
            	            					
            	            pushFollow(FOLLOW_8);
            	            lv_scenarios_6_2=ruleScenarioOutline();

            	            state._fsp--;


            	            						if (current==null) {
            	            							current = createModelElementForParent(grammarAccess.getFeatureRule());
            	            						}
            	            						add(
            	            							current,
            	            							"scenarios",
            	            							lv_scenarios_6_2,
            	            							"org.xtext.example.feature.Feature.ScenarioOutline");
            	            						afterParserOrEnumRuleCall();
            	            					

            	            }
            	            break;

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt6 >= 1 ) break loop6;
                        EarlyExitException eee =
                            new EarlyExitException(6, input);
                        throw eee;
                }
                cnt6++;
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleFeature"


    // $ANTLR start "entryRuleBackground"
    // InternalFeature.g:207:1: entryRuleBackground returns [EObject current=null] : iv_ruleBackground= ruleBackground EOF ;
    public final EObject entryRuleBackground() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleBackground = null;


        try {
            // InternalFeature.g:207:51: (iv_ruleBackground= ruleBackground EOF )
            // InternalFeature.g:208:2: iv_ruleBackground= ruleBackground EOF
            {
             newCompositeNode(grammarAccess.getBackgroundRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleBackground=ruleBackground();

            state._fsp--;

             current =iv_ruleBackground; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleBackground"


    // $ANTLR start "ruleBackground"
    // InternalFeature.g:214:1: ruleBackground returns [EObject current=null] : (otherlv_0= 'Background:' ( (lv_title_1_0= ruleTitle ) )? (this_EOL_2= RULE_EOL )+ ( (lv_narrative_3_0= ruleNarrative ) )? ( (lv_steps_4_0= ruleStep ) )+ ) ;
    public final EObject ruleBackground() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token this_EOL_2=null;
        AntlrDatatypeRuleToken lv_title_1_0 = null;

        AntlrDatatypeRuleToken lv_narrative_3_0 = null;

        EObject lv_steps_4_0 = null;



        	enterRule();

        try {
            // InternalFeature.g:220:2: ( (otherlv_0= 'Background:' ( (lv_title_1_0= ruleTitle ) )? (this_EOL_2= RULE_EOL )+ ( (lv_narrative_3_0= ruleNarrative ) )? ( (lv_steps_4_0= ruleStep ) )+ ) )
            // InternalFeature.g:221:2: (otherlv_0= 'Background:' ( (lv_title_1_0= ruleTitle ) )? (this_EOL_2= RULE_EOL )+ ( (lv_narrative_3_0= ruleNarrative ) )? ( (lv_steps_4_0= ruleStep ) )+ )
            {
            // InternalFeature.g:221:2: (otherlv_0= 'Background:' ( (lv_title_1_0= ruleTitle ) )? (this_EOL_2= RULE_EOL )+ ( (lv_narrative_3_0= ruleNarrative ) )? ( (lv_steps_4_0= ruleStep ) )+ )
            // InternalFeature.g:222:3: otherlv_0= 'Background:' ( (lv_title_1_0= ruleTitle ) )? (this_EOL_2= RULE_EOL )+ ( (lv_narrative_3_0= ruleNarrative ) )? ( (lv_steps_4_0= ruleStep ) )+
            {
            otherlv_0=(Token)match(input,20,FOLLOW_9); 

            			newLeafNode(otherlv_0, grammarAccess.getBackgroundAccess().getBackgroundKeyword_0());
            		
            // InternalFeature.g:226:3: ( (lv_title_1_0= ruleTitle ) )?
            int alt7=2;
            int LA7_0 = input.LA(1);

            if ( ((LA7_0>=RULE_WORD && LA7_0<=RULE_PLACEHOLDER)) ) {
                alt7=1;
            }
            switch (alt7) {
                case 1 :
                    // InternalFeature.g:227:4: (lv_title_1_0= ruleTitle )
                    {
                    // InternalFeature.g:227:4: (lv_title_1_0= ruleTitle )
                    // InternalFeature.g:228:5: lv_title_1_0= ruleTitle
                    {

                    					newCompositeNode(grammarAccess.getBackgroundAccess().getTitleTitleParserRuleCall_1_0());
                    				
                    pushFollow(FOLLOW_5);
                    lv_title_1_0=ruleTitle();

                    state._fsp--;


                    					if (current==null) {
                    						current = createModelElementForParent(grammarAccess.getBackgroundRule());
                    					}
                    					set(
                    						current,
                    						"title",
                    						lv_title_1_0,
                    						"org.xtext.example.feature.Feature.Title");
                    					afterParserOrEnumRuleCall();
                    				

                    }


                    }
                    break;

            }

            // InternalFeature.g:245:3: (this_EOL_2= RULE_EOL )+
            int cnt8=0;
            loop8:
            do {
                int alt8=2;
                int LA8_0 = input.LA(1);

                if ( (LA8_0==RULE_EOL) ) {
                    alt8=1;
                }


                switch (alt8) {
            	case 1 :
            	    // InternalFeature.g:246:4: this_EOL_2= RULE_EOL
            	    {
            	    this_EOL_2=(Token)match(input,RULE_EOL,FOLLOW_10); 

            	    				newLeafNode(this_EOL_2, grammarAccess.getBackgroundAccess().getEOLTerminalRuleCall_2());
            	    			

            	    }
            	    break;

            	default :
            	    if ( cnt8 >= 1 ) break loop8;
                        EarlyExitException eee =
                            new EarlyExitException(8, input);
                        throw eee;
                }
                cnt8++;
            } while (true);

            // InternalFeature.g:251:3: ( (lv_narrative_3_0= ruleNarrative ) )?
            int alt9=2;
            int LA9_0 = input.LA(1);

            if ( ((LA9_0>=RULE_WORD && LA9_0<=RULE_PLACEHOLDER)) ) {
                alt9=1;
            }
            switch (alt9) {
                case 1 :
                    // InternalFeature.g:252:4: (lv_narrative_3_0= ruleNarrative )
                    {
                    // InternalFeature.g:252:4: (lv_narrative_3_0= ruleNarrative )
                    // InternalFeature.g:253:5: lv_narrative_3_0= ruleNarrative
                    {

                    					newCompositeNode(grammarAccess.getBackgroundAccess().getNarrativeNarrativeParserRuleCall_3_0());
                    				
                    pushFollow(FOLLOW_11);
                    lv_narrative_3_0=ruleNarrative();

                    state._fsp--;


                    					if (current==null) {
                    						current = createModelElementForParent(grammarAccess.getBackgroundRule());
                    					}
                    					set(
                    						current,
                    						"narrative",
                    						lv_narrative_3_0,
                    						"org.xtext.example.feature.Feature.Narrative");
                    					afterParserOrEnumRuleCall();
                    				

                    }


                    }
                    break;

            }

            // InternalFeature.g:270:3: ( (lv_steps_4_0= ruleStep ) )+
            int cnt10=0;
            loop10:
            do {
                int alt10=2;
                int LA10_0 = input.LA(1);

                if ( ((LA10_0>=24 && LA10_0<=28)) ) {
                    alt10=1;
                }


                switch (alt10) {
            	case 1 :
            	    // InternalFeature.g:271:4: (lv_steps_4_0= ruleStep )
            	    {
            	    // InternalFeature.g:271:4: (lv_steps_4_0= ruleStep )
            	    // InternalFeature.g:272:5: lv_steps_4_0= ruleStep
            	    {

            	    					newCompositeNode(grammarAccess.getBackgroundAccess().getStepsStepParserRuleCall_4_0());
            	    				
            	    pushFollow(FOLLOW_12);
            	    lv_steps_4_0=ruleStep();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getBackgroundRule());
            	    					}
            	    					add(
            	    						current,
            	    						"steps",
            	    						lv_steps_4_0,
            	    						"org.xtext.example.feature.Feature.Step");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt10 >= 1 ) break loop10;
                        EarlyExitException eee =
                            new EarlyExitException(10, input);
                        throw eee;
                }
                cnt10++;
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleBackground"


    // $ANTLR start "entryRuleScenario"
    // InternalFeature.g:293:1: entryRuleScenario returns [EObject current=null] : iv_ruleScenario= ruleScenario EOF ;
    public final EObject entryRuleScenario() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleScenario = null;


        try {
            // InternalFeature.g:293:49: (iv_ruleScenario= ruleScenario EOF )
            // InternalFeature.g:294:2: iv_ruleScenario= ruleScenario EOF
            {
             newCompositeNode(grammarAccess.getScenarioRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleScenario=ruleScenario();

            state._fsp--;

             current =iv_ruleScenario; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleScenario"


    // $ANTLR start "ruleScenario"
    // InternalFeature.g:300:1: ruleScenario returns [EObject current=null] : ( ( (lv_tags_0_0= ruleTag ) )* otherlv_1= 'Scenario:' ( (lv_title_2_0= ruleTitle ) ) (this_EOL_3= RULE_EOL )+ ( (lv_narrative_4_0= ruleNarrative ) )? ( (lv_steps_5_0= ruleStep ) )+ ) ;
    public final EObject ruleScenario() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token this_EOL_3=null;
        EObject lv_tags_0_0 = null;

        AntlrDatatypeRuleToken lv_title_2_0 = null;

        AntlrDatatypeRuleToken lv_narrative_4_0 = null;

        EObject lv_steps_5_0 = null;



        	enterRule();

        try {
            // InternalFeature.g:306:2: ( ( ( (lv_tags_0_0= ruleTag ) )* otherlv_1= 'Scenario:' ( (lv_title_2_0= ruleTitle ) ) (this_EOL_3= RULE_EOL )+ ( (lv_narrative_4_0= ruleNarrative ) )? ( (lv_steps_5_0= ruleStep ) )+ ) )
            // InternalFeature.g:307:2: ( ( (lv_tags_0_0= ruleTag ) )* otherlv_1= 'Scenario:' ( (lv_title_2_0= ruleTitle ) ) (this_EOL_3= RULE_EOL )+ ( (lv_narrative_4_0= ruleNarrative ) )? ( (lv_steps_5_0= ruleStep ) )+ )
            {
            // InternalFeature.g:307:2: ( ( (lv_tags_0_0= ruleTag ) )* otherlv_1= 'Scenario:' ( (lv_title_2_0= ruleTitle ) ) (this_EOL_3= RULE_EOL )+ ( (lv_narrative_4_0= ruleNarrative ) )? ( (lv_steps_5_0= ruleStep ) )+ )
            // InternalFeature.g:308:3: ( (lv_tags_0_0= ruleTag ) )* otherlv_1= 'Scenario:' ( (lv_title_2_0= ruleTitle ) ) (this_EOL_3= RULE_EOL )+ ( (lv_narrative_4_0= ruleNarrative ) )? ( (lv_steps_5_0= ruleStep ) )+
            {
            // InternalFeature.g:308:3: ( (lv_tags_0_0= ruleTag ) )*
            loop11:
            do {
                int alt11=2;
                int LA11_0 = input.LA(1);

                if ( (LA11_0==RULE_TAGNAME) ) {
                    alt11=1;
                }


                switch (alt11) {
            	case 1 :
            	    // InternalFeature.g:309:4: (lv_tags_0_0= ruleTag )
            	    {
            	    // InternalFeature.g:309:4: (lv_tags_0_0= ruleTag )
            	    // InternalFeature.g:310:5: lv_tags_0_0= ruleTag
            	    {

            	    					newCompositeNode(grammarAccess.getScenarioAccess().getTagsTagParserRuleCall_0_0());
            	    				
            	    pushFollow(FOLLOW_13);
            	    lv_tags_0_0=ruleTag();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getScenarioRule());
            	    					}
            	    					add(
            	    						current,
            	    						"tags",
            	    						lv_tags_0_0,
            	    						"org.xtext.example.feature.Feature.Tag");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop11;
                }
            } while (true);

            otherlv_1=(Token)match(input,21,FOLLOW_4); 

            			newLeafNode(otherlv_1, grammarAccess.getScenarioAccess().getScenarioKeyword_1());
            		
            // InternalFeature.g:331:3: ( (lv_title_2_0= ruleTitle ) )
            // InternalFeature.g:332:4: (lv_title_2_0= ruleTitle )
            {
            // InternalFeature.g:332:4: (lv_title_2_0= ruleTitle )
            // InternalFeature.g:333:5: lv_title_2_0= ruleTitle
            {

            					newCompositeNode(grammarAccess.getScenarioAccess().getTitleTitleParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_5);
            lv_title_2_0=ruleTitle();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getScenarioRule());
            					}
            					set(
            						current,
            						"title",
            						lv_title_2_0,
            						"org.xtext.example.feature.Feature.Title");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalFeature.g:350:3: (this_EOL_3= RULE_EOL )+
            int cnt12=0;
            loop12:
            do {
                int alt12=2;
                int LA12_0 = input.LA(1);

                if ( (LA12_0==RULE_EOL) ) {
                    alt12=1;
                }


                switch (alt12) {
            	case 1 :
            	    // InternalFeature.g:351:4: this_EOL_3= RULE_EOL
            	    {
            	    this_EOL_3=(Token)match(input,RULE_EOL,FOLLOW_10); 

            	    				newLeafNode(this_EOL_3, grammarAccess.getScenarioAccess().getEOLTerminalRuleCall_3());
            	    			

            	    }
            	    break;

            	default :
            	    if ( cnt12 >= 1 ) break loop12;
                        EarlyExitException eee =
                            new EarlyExitException(12, input);
                        throw eee;
                }
                cnt12++;
            } while (true);

            // InternalFeature.g:356:3: ( (lv_narrative_4_0= ruleNarrative ) )?
            int alt13=2;
            int LA13_0 = input.LA(1);

            if ( ((LA13_0>=RULE_WORD && LA13_0<=RULE_PLACEHOLDER)) ) {
                alt13=1;
            }
            switch (alt13) {
                case 1 :
                    // InternalFeature.g:357:4: (lv_narrative_4_0= ruleNarrative )
                    {
                    // InternalFeature.g:357:4: (lv_narrative_4_0= ruleNarrative )
                    // InternalFeature.g:358:5: lv_narrative_4_0= ruleNarrative
                    {

                    					newCompositeNode(grammarAccess.getScenarioAccess().getNarrativeNarrativeParserRuleCall_4_0());
                    				
                    pushFollow(FOLLOW_11);
                    lv_narrative_4_0=ruleNarrative();

                    state._fsp--;


                    					if (current==null) {
                    						current = createModelElementForParent(grammarAccess.getScenarioRule());
                    					}
                    					set(
                    						current,
                    						"narrative",
                    						lv_narrative_4_0,
                    						"org.xtext.example.feature.Feature.Narrative");
                    					afterParserOrEnumRuleCall();
                    				

                    }


                    }
                    break;

            }

            // InternalFeature.g:375:3: ( (lv_steps_5_0= ruleStep ) )+
            int cnt14=0;
            loop14:
            do {
                int alt14=2;
                int LA14_0 = input.LA(1);

                if ( ((LA14_0>=24 && LA14_0<=28)) ) {
                    alt14=1;
                }


                switch (alt14) {
            	case 1 :
            	    // InternalFeature.g:376:4: (lv_steps_5_0= ruleStep )
            	    {
            	    // InternalFeature.g:376:4: (lv_steps_5_0= ruleStep )
            	    // InternalFeature.g:377:5: lv_steps_5_0= ruleStep
            	    {

            	    					newCompositeNode(grammarAccess.getScenarioAccess().getStepsStepParserRuleCall_5_0());
            	    				
            	    pushFollow(FOLLOW_12);
            	    lv_steps_5_0=ruleStep();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getScenarioRule());
            	    					}
            	    					add(
            	    						current,
            	    						"steps",
            	    						lv_steps_5_0,
            	    						"org.xtext.example.feature.Feature.Step");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt14 >= 1 ) break loop14;
                        EarlyExitException eee =
                            new EarlyExitException(14, input);
                        throw eee;
                }
                cnt14++;
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleScenario"


    // $ANTLR start "entryRuleScenarioOutline"
    // InternalFeature.g:398:1: entryRuleScenarioOutline returns [EObject current=null] : iv_ruleScenarioOutline= ruleScenarioOutline EOF ;
    public final EObject entryRuleScenarioOutline() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleScenarioOutline = null;


        try {
            // InternalFeature.g:398:56: (iv_ruleScenarioOutline= ruleScenarioOutline EOF )
            // InternalFeature.g:399:2: iv_ruleScenarioOutline= ruleScenarioOutline EOF
            {
             newCompositeNode(grammarAccess.getScenarioOutlineRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleScenarioOutline=ruleScenarioOutline();

            state._fsp--;

             current =iv_ruleScenarioOutline; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleScenarioOutline"


    // $ANTLR start "ruleScenarioOutline"
    // InternalFeature.g:405:1: ruleScenarioOutline returns [EObject current=null] : ( ( (lv_tags_0_0= ruleTag ) )* otherlv_1= 'Scenario Outline:' ( (lv_title_2_0= ruleTitle ) ) (this_EOL_3= RULE_EOL )+ ( (lv_narrative_4_0= ruleNarrative ) )? ( (lv_steps_5_0= ruleStep ) )+ ( (lv_examples_6_0= ruleExamples ) ) ) ;
    public final EObject ruleScenarioOutline() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token this_EOL_3=null;
        EObject lv_tags_0_0 = null;

        AntlrDatatypeRuleToken lv_title_2_0 = null;

        AntlrDatatypeRuleToken lv_narrative_4_0 = null;

        EObject lv_steps_5_0 = null;

        EObject lv_examples_6_0 = null;



        	enterRule();

        try {
            // InternalFeature.g:411:2: ( ( ( (lv_tags_0_0= ruleTag ) )* otherlv_1= 'Scenario Outline:' ( (lv_title_2_0= ruleTitle ) ) (this_EOL_3= RULE_EOL )+ ( (lv_narrative_4_0= ruleNarrative ) )? ( (lv_steps_5_0= ruleStep ) )+ ( (lv_examples_6_0= ruleExamples ) ) ) )
            // InternalFeature.g:412:2: ( ( (lv_tags_0_0= ruleTag ) )* otherlv_1= 'Scenario Outline:' ( (lv_title_2_0= ruleTitle ) ) (this_EOL_3= RULE_EOL )+ ( (lv_narrative_4_0= ruleNarrative ) )? ( (lv_steps_5_0= ruleStep ) )+ ( (lv_examples_6_0= ruleExamples ) ) )
            {
            // InternalFeature.g:412:2: ( ( (lv_tags_0_0= ruleTag ) )* otherlv_1= 'Scenario Outline:' ( (lv_title_2_0= ruleTitle ) ) (this_EOL_3= RULE_EOL )+ ( (lv_narrative_4_0= ruleNarrative ) )? ( (lv_steps_5_0= ruleStep ) )+ ( (lv_examples_6_0= ruleExamples ) ) )
            // InternalFeature.g:413:3: ( (lv_tags_0_0= ruleTag ) )* otherlv_1= 'Scenario Outline:' ( (lv_title_2_0= ruleTitle ) ) (this_EOL_3= RULE_EOL )+ ( (lv_narrative_4_0= ruleNarrative ) )? ( (lv_steps_5_0= ruleStep ) )+ ( (lv_examples_6_0= ruleExamples ) )
            {
            // InternalFeature.g:413:3: ( (lv_tags_0_0= ruleTag ) )*
            loop15:
            do {
                int alt15=2;
                int LA15_0 = input.LA(1);

                if ( (LA15_0==RULE_TAGNAME) ) {
                    alt15=1;
                }


                switch (alt15) {
            	case 1 :
            	    // InternalFeature.g:414:4: (lv_tags_0_0= ruleTag )
            	    {
            	    // InternalFeature.g:414:4: (lv_tags_0_0= ruleTag )
            	    // InternalFeature.g:415:5: lv_tags_0_0= ruleTag
            	    {

            	    					newCompositeNode(grammarAccess.getScenarioOutlineAccess().getTagsTagParserRuleCall_0_0());
            	    				
            	    pushFollow(FOLLOW_14);
            	    lv_tags_0_0=ruleTag();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getScenarioOutlineRule());
            	    					}
            	    					add(
            	    						current,
            	    						"tags",
            	    						lv_tags_0_0,
            	    						"org.xtext.example.feature.Feature.Tag");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop15;
                }
            } while (true);

            otherlv_1=(Token)match(input,22,FOLLOW_4); 

            			newLeafNode(otherlv_1, grammarAccess.getScenarioOutlineAccess().getScenarioOutlineKeyword_1());
            		
            // InternalFeature.g:436:3: ( (lv_title_2_0= ruleTitle ) )
            // InternalFeature.g:437:4: (lv_title_2_0= ruleTitle )
            {
            // InternalFeature.g:437:4: (lv_title_2_0= ruleTitle )
            // InternalFeature.g:438:5: lv_title_2_0= ruleTitle
            {

            					newCompositeNode(grammarAccess.getScenarioOutlineAccess().getTitleTitleParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_5);
            lv_title_2_0=ruleTitle();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getScenarioOutlineRule());
            					}
            					set(
            						current,
            						"title",
            						lv_title_2_0,
            						"org.xtext.example.feature.Feature.Title");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalFeature.g:455:3: (this_EOL_3= RULE_EOL )+
            int cnt16=0;
            loop16:
            do {
                int alt16=2;
                int LA16_0 = input.LA(1);

                if ( (LA16_0==RULE_EOL) ) {
                    alt16=1;
                }


                switch (alt16) {
            	case 1 :
            	    // InternalFeature.g:456:4: this_EOL_3= RULE_EOL
            	    {
            	    this_EOL_3=(Token)match(input,RULE_EOL,FOLLOW_10); 

            	    				newLeafNode(this_EOL_3, grammarAccess.getScenarioOutlineAccess().getEOLTerminalRuleCall_3());
            	    			

            	    }
            	    break;

            	default :
            	    if ( cnt16 >= 1 ) break loop16;
                        EarlyExitException eee =
                            new EarlyExitException(16, input);
                        throw eee;
                }
                cnt16++;
            } while (true);

            // InternalFeature.g:461:3: ( (lv_narrative_4_0= ruleNarrative ) )?
            int alt17=2;
            int LA17_0 = input.LA(1);

            if ( ((LA17_0>=RULE_WORD && LA17_0<=RULE_PLACEHOLDER)) ) {
                alt17=1;
            }
            switch (alt17) {
                case 1 :
                    // InternalFeature.g:462:4: (lv_narrative_4_0= ruleNarrative )
                    {
                    // InternalFeature.g:462:4: (lv_narrative_4_0= ruleNarrative )
                    // InternalFeature.g:463:5: lv_narrative_4_0= ruleNarrative
                    {

                    					newCompositeNode(grammarAccess.getScenarioOutlineAccess().getNarrativeNarrativeParserRuleCall_4_0());
                    				
                    pushFollow(FOLLOW_11);
                    lv_narrative_4_0=ruleNarrative();

                    state._fsp--;


                    					if (current==null) {
                    						current = createModelElementForParent(grammarAccess.getScenarioOutlineRule());
                    					}
                    					set(
                    						current,
                    						"narrative",
                    						lv_narrative_4_0,
                    						"org.xtext.example.feature.Feature.Narrative");
                    					afterParserOrEnumRuleCall();
                    				

                    }


                    }
                    break;

            }

            // InternalFeature.g:480:3: ( (lv_steps_5_0= ruleStep ) )+
            int cnt18=0;
            loop18:
            do {
                int alt18=2;
                int LA18_0 = input.LA(1);

                if ( ((LA18_0>=24 && LA18_0<=28)) ) {
                    alt18=1;
                }


                switch (alt18) {
            	case 1 :
            	    // InternalFeature.g:481:4: (lv_steps_5_0= ruleStep )
            	    {
            	    // InternalFeature.g:481:4: (lv_steps_5_0= ruleStep )
            	    // InternalFeature.g:482:5: lv_steps_5_0= ruleStep
            	    {

            	    					newCompositeNode(grammarAccess.getScenarioOutlineAccess().getStepsStepParserRuleCall_5_0());
            	    				
            	    pushFollow(FOLLOW_15);
            	    lv_steps_5_0=ruleStep();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getScenarioOutlineRule());
            	    					}
            	    					add(
            	    						current,
            	    						"steps",
            	    						lv_steps_5_0,
            	    						"org.xtext.example.feature.Feature.Step");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt18 >= 1 ) break loop18;
                        EarlyExitException eee =
                            new EarlyExitException(18, input);
                        throw eee;
                }
                cnt18++;
            } while (true);

            // InternalFeature.g:499:3: ( (lv_examples_6_0= ruleExamples ) )
            // InternalFeature.g:500:4: (lv_examples_6_0= ruleExamples )
            {
            // InternalFeature.g:500:4: (lv_examples_6_0= ruleExamples )
            // InternalFeature.g:501:5: lv_examples_6_0= ruleExamples
            {

            					newCompositeNode(grammarAccess.getScenarioOutlineAccess().getExamplesExamplesParserRuleCall_6_0());
            				
            pushFollow(FOLLOW_2);
            lv_examples_6_0=ruleExamples();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getScenarioOutlineRule());
            					}
            					set(
            						current,
            						"examples",
            						lv_examples_6_0,
            						"org.xtext.example.feature.Feature.Examples");
            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleScenarioOutline"


    // $ANTLR start "entryRuleStep"
    // InternalFeature.g:522:1: entryRuleStep returns [EObject current=null] : iv_ruleStep= ruleStep EOF ;
    public final EObject entryRuleStep() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleStep = null;


        try {
            // InternalFeature.g:522:45: (iv_ruleStep= ruleStep EOF )
            // InternalFeature.g:523:2: iv_ruleStep= ruleStep EOF
            {
             newCompositeNode(grammarAccess.getStepRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleStep=ruleStep();

            state._fsp--;

             current =iv_ruleStep; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleStep"


    // $ANTLR start "ruleStep"
    // InternalFeature.g:529:1: ruleStep returns [EObject current=null] : ( ( (lv_stepKeyword_0_0= ruleStepKeyword ) ) ( (lv_description_1_0= ruleStepDescription ) ) (this_EOL_2= RULE_EOL )* ( (lv_tables_3_0= ruleTable ) )* ( (lv_code_4_0= ruleDocString ) )? ( (lv_tables_5_0= ruleTable ) )* ) ;
    public final EObject ruleStep() throws RecognitionException {
        EObject current = null;

        Token this_EOL_2=null;
        Enumerator lv_stepKeyword_0_0 = null;

        AntlrDatatypeRuleToken lv_description_1_0 = null;

        EObject lv_tables_3_0 = null;

        EObject lv_code_4_0 = null;

        EObject lv_tables_5_0 = null;



        	enterRule();

        try {
            // InternalFeature.g:535:2: ( ( ( (lv_stepKeyword_0_0= ruleStepKeyword ) ) ( (lv_description_1_0= ruleStepDescription ) ) (this_EOL_2= RULE_EOL )* ( (lv_tables_3_0= ruleTable ) )* ( (lv_code_4_0= ruleDocString ) )? ( (lv_tables_5_0= ruleTable ) )* ) )
            // InternalFeature.g:536:2: ( ( (lv_stepKeyword_0_0= ruleStepKeyword ) ) ( (lv_description_1_0= ruleStepDescription ) ) (this_EOL_2= RULE_EOL )* ( (lv_tables_3_0= ruleTable ) )* ( (lv_code_4_0= ruleDocString ) )? ( (lv_tables_5_0= ruleTable ) )* )
            {
            // InternalFeature.g:536:2: ( ( (lv_stepKeyword_0_0= ruleStepKeyword ) ) ( (lv_description_1_0= ruleStepDescription ) ) (this_EOL_2= RULE_EOL )* ( (lv_tables_3_0= ruleTable ) )* ( (lv_code_4_0= ruleDocString ) )? ( (lv_tables_5_0= ruleTable ) )* )
            // InternalFeature.g:537:3: ( (lv_stepKeyword_0_0= ruleStepKeyword ) ) ( (lv_description_1_0= ruleStepDescription ) ) (this_EOL_2= RULE_EOL )* ( (lv_tables_3_0= ruleTable ) )* ( (lv_code_4_0= ruleDocString ) )? ( (lv_tables_5_0= ruleTable ) )*
            {
            // InternalFeature.g:537:3: ( (lv_stepKeyword_0_0= ruleStepKeyword ) )
            // InternalFeature.g:538:4: (lv_stepKeyword_0_0= ruleStepKeyword )
            {
            // InternalFeature.g:538:4: (lv_stepKeyword_0_0= ruleStepKeyword )
            // InternalFeature.g:539:5: lv_stepKeyword_0_0= ruleStepKeyword
            {

            					newCompositeNode(grammarAccess.getStepAccess().getStepKeywordStepKeywordEnumRuleCall_0_0());
            				
            pushFollow(FOLLOW_16);
            lv_stepKeyword_0_0=ruleStepKeyword();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getStepRule());
            					}
            					set(
            						current,
            						"stepKeyword",
            						lv_stepKeyword_0_0,
            						"org.xtext.example.feature.Feature.StepKeyword");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalFeature.g:556:3: ( (lv_description_1_0= ruleStepDescription ) )
            // InternalFeature.g:557:4: (lv_description_1_0= ruleStepDescription )
            {
            // InternalFeature.g:557:4: (lv_description_1_0= ruleStepDescription )
            // InternalFeature.g:558:5: lv_description_1_0= ruleStepDescription
            {

            					newCompositeNode(grammarAccess.getStepAccess().getDescriptionStepDescriptionParserRuleCall_1_0());
            				
            pushFollow(FOLLOW_17);
            lv_description_1_0=ruleStepDescription();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getStepRule());
            					}
            					set(
            						current,
            						"description",
            						lv_description_1_0,
            						"org.xtext.example.feature.Feature.StepDescription");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalFeature.g:575:3: (this_EOL_2= RULE_EOL )*
            loop19:
            do {
                int alt19=2;
                int LA19_0 = input.LA(1);

                if ( (LA19_0==RULE_EOL) ) {
                    alt19=1;
                }


                switch (alt19) {
            	case 1 :
            	    // InternalFeature.g:576:4: this_EOL_2= RULE_EOL
            	    {
            	    this_EOL_2=(Token)match(input,RULE_EOL,FOLLOW_17); 

            	    				newLeafNode(this_EOL_2, grammarAccess.getStepAccess().getEOLTerminalRuleCall_2());
            	    			

            	    }
            	    break;

            	default :
            	    break loop19;
                }
            } while (true);

            // InternalFeature.g:581:3: ( (lv_tables_3_0= ruleTable ) )*
            loop20:
            do {
                int alt20=2;
                int LA20_0 = input.LA(1);

                if ( (LA20_0==RULE_TABLE_ROW) ) {
                    alt20=1;
                }


                switch (alt20) {
            	case 1 :
            	    // InternalFeature.g:582:4: (lv_tables_3_0= ruleTable )
            	    {
            	    // InternalFeature.g:582:4: (lv_tables_3_0= ruleTable )
            	    // InternalFeature.g:583:5: lv_tables_3_0= ruleTable
            	    {

            	    					newCompositeNode(grammarAccess.getStepAccess().getTablesTableParserRuleCall_3_0());
            	    				
            	    pushFollow(FOLLOW_18);
            	    lv_tables_3_0=ruleTable();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getStepRule());
            	    					}
            	    					add(
            	    						current,
            	    						"tables",
            	    						lv_tables_3_0,
            	    						"org.xtext.example.feature.Feature.Table");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop20;
                }
            } while (true);

            // InternalFeature.g:600:3: ( (lv_code_4_0= ruleDocString ) )?
            int alt21=2;
            int LA21_0 = input.LA(1);

            if ( (LA21_0==RULE_DOC_STRING) ) {
                alt21=1;
            }
            switch (alt21) {
                case 1 :
                    // InternalFeature.g:601:4: (lv_code_4_0= ruleDocString )
                    {
                    // InternalFeature.g:601:4: (lv_code_4_0= ruleDocString )
                    // InternalFeature.g:602:5: lv_code_4_0= ruleDocString
                    {

                    					newCompositeNode(grammarAccess.getStepAccess().getCodeDocStringParserRuleCall_4_0());
                    				
                    pushFollow(FOLLOW_19);
                    lv_code_4_0=ruleDocString();

                    state._fsp--;


                    					if (current==null) {
                    						current = createModelElementForParent(grammarAccess.getStepRule());
                    					}
                    					set(
                    						current,
                    						"code",
                    						lv_code_4_0,
                    						"org.xtext.example.feature.Feature.DocString");
                    					afterParserOrEnumRuleCall();
                    				

                    }


                    }
                    break;

            }

            // InternalFeature.g:619:3: ( (lv_tables_5_0= ruleTable ) )*
            loop22:
            do {
                int alt22=2;
                int LA22_0 = input.LA(1);

                if ( (LA22_0==RULE_TABLE_ROW) ) {
                    alt22=1;
                }


                switch (alt22) {
            	case 1 :
            	    // InternalFeature.g:620:4: (lv_tables_5_0= ruleTable )
            	    {
            	    // InternalFeature.g:620:4: (lv_tables_5_0= ruleTable )
            	    // InternalFeature.g:621:5: lv_tables_5_0= ruleTable
            	    {

            	    					newCompositeNode(grammarAccess.getStepAccess().getTablesTableParserRuleCall_5_0());
            	    				
            	    pushFollow(FOLLOW_19);
            	    lv_tables_5_0=ruleTable();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getStepRule());
            	    					}
            	    					add(
            	    						current,
            	    						"tables",
            	    						lv_tables_5_0,
            	    						"org.xtext.example.feature.Feature.Table");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop22;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleStep"


    // $ANTLR start "entryRuleExamples"
    // InternalFeature.g:642:1: entryRuleExamples returns [EObject current=null] : iv_ruleExamples= ruleExamples EOF ;
    public final EObject entryRuleExamples() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleExamples = null;


        try {
            // InternalFeature.g:642:49: (iv_ruleExamples= ruleExamples EOF )
            // InternalFeature.g:643:2: iv_ruleExamples= ruleExamples EOF
            {
             newCompositeNode(grammarAccess.getExamplesRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleExamples=ruleExamples();

            state._fsp--;

             current =iv_ruleExamples; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleExamples"


    // $ANTLR start "ruleExamples"
    // InternalFeature.g:649:1: ruleExamples returns [EObject current=null] : (otherlv_0= 'Examples:' ( (lv_title_1_0= ruleTitle ) )? (this_EOL_2= RULE_EOL )+ ( (lv_narrative_3_0= ruleNarrative ) )? ( (lv_table_4_0= ruleTable ) ) ) ;
    public final EObject ruleExamples() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token this_EOL_2=null;
        AntlrDatatypeRuleToken lv_title_1_0 = null;

        AntlrDatatypeRuleToken lv_narrative_3_0 = null;

        EObject lv_table_4_0 = null;



        	enterRule();

        try {
            // InternalFeature.g:655:2: ( (otherlv_0= 'Examples:' ( (lv_title_1_0= ruleTitle ) )? (this_EOL_2= RULE_EOL )+ ( (lv_narrative_3_0= ruleNarrative ) )? ( (lv_table_4_0= ruleTable ) ) ) )
            // InternalFeature.g:656:2: (otherlv_0= 'Examples:' ( (lv_title_1_0= ruleTitle ) )? (this_EOL_2= RULE_EOL )+ ( (lv_narrative_3_0= ruleNarrative ) )? ( (lv_table_4_0= ruleTable ) ) )
            {
            // InternalFeature.g:656:2: (otherlv_0= 'Examples:' ( (lv_title_1_0= ruleTitle ) )? (this_EOL_2= RULE_EOL )+ ( (lv_narrative_3_0= ruleNarrative ) )? ( (lv_table_4_0= ruleTable ) ) )
            // InternalFeature.g:657:3: otherlv_0= 'Examples:' ( (lv_title_1_0= ruleTitle ) )? (this_EOL_2= RULE_EOL )+ ( (lv_narrative_3_0= ruleNarrative ) )? ( (lv_table_4_0= ruleTable ) )
            {
            otherlv_0=(Token)match(input,23,FOLLOW_9); 

            			newLeafNode(otherlv_0, grammarAccess.getExamplesAccess().getExamplesKeyword_0());
            		
            // InternalFeature.g:661:3: ( (lv_title_1_0= ruleTitle ) )?
            int alt23=2;
            int LA23_0 = input.LA(1);

            if ( ((LA23_0>=RULE_WORD && LA23_0<=RULE_PLACEHOLDER)) ) {
                alt23=1;
            }
            switch (alt23) {
                case 1 :
                    // InternalFeature.g:662:4: (lv_title_1_0= ruleTitle )
                    {
                    // InternalFeature.g:662:4: (lv_title_1_0= ruleTitle )
                    // InternalFeature.g:663:5: lv_title_1_0= ruleTitle
                    {

                    					newCompositeNode(grammarAccess.getExamplesAccess().getTitleTitleParserRuleCall_1_0());
                    				
                    pushFollow(FOLLOW_5);
                    lv_title_1_0=ruleTitle();

                    state._fsp--;


                    					if (current==null) {
                    						current = createModelElementForParent(grammarAccess.getExamplesRule());
                    					}
                    					set(
                    						current,
                    						"title",
                    						lv_title_1_0,
                    						"org.xtext.example.feature.Feature.Title");
                    					afterParserOrEnumRuleCall();
                    				

                    }


                    }
                    break;

            }

            // InternalFeature.g:680:3: (this_EOL_2= RULE_EOL )+
            int cnt24=0;
            loop24:
            do {
                int alt24=2;
                int LA24_0 = input.LA(1);

                if ( (LA24_0==RULE_EOL) ) {
                    alt24=1;
                }


                switch (alt24) {
            	case 1 :
            	    // InternalFeature.g:681:4: this_EOL_2= RULE_EOL
            	    {
            	    this_EOL_2=(Token)match(input,RULE_EOL,FOLLOW_20); 

            	    				newLeafNode(this_EOL_2, grammarAccess.getExamplesAccess().getEOLTerminalRuleCall_2());
            	    			

            	    }
            	    break;

            	default :
            	    if ( cnt24 >= 1 ) break loop24;
                        EarlyExitException eee =
                            new EarlyExitException(24, input);
                        throw eee;
                }
                cnt24++;
            } while (true);

            // InternalFeature.g:686:3: ( (lv_narrative_3_0= ruleNarrative ) )?
            int alt25=2;
            int LA25_0 = input.LA(1);

            if ( ((LA25_0>=RULE_WORD && LA25_0<=RULE_PLACEHOLDER)) ) {
                alt25=1;
            }
            switch (alt25) {
                case 1 :
                    // InternalFeature.g:687:4: (lv_narrative_3_0= ruleNarrative )
                    {
                    // InternalFeature.g:687:4: (lv_narrative_3_0= ruleNarrative )
                    // InternalFeature.g:688:5: lv_narrative_3_0= ruleNarrative
                    {

                    					newCompositeNode(grammarAccess.getExamplesAccess().getNarrativeNarrativeParserRuleCall_3_0());
                    				
                    pushFollow(FOLLOW_21);
                    lv_narrative_3_0=ruleNarrative();

                    state._fsp--;


                    					if (current==null) {
                    						current = createModelElementForParent(grammarAccess.getExamplesRule());
                    					}
                    					set(
                    						current,
                    						"narrative",
                    						lv_narrative_3_0,
                    						"org.xtext.example.feature.Feature.Narrative");
                    					afterParserOrEnumRuleCall();
                    				

                    }


                    }
                    break;

            }

            // InternalFeature.g:705:3: ( (lv_table_4_0= ruleTable ) )
            // InternalFeature.g:706:4: (lv_table_4_0= ruleTable )
            {
            // InternalFeature.g:706:4: (lv_table_4_0= ruleTable )
            // InternalFeature.g:707:5: lv_table_4_0= ruleTable
            {

            					newCompositeNode(grammarAccess.getExamplesAccess().getTableTableParserRuleCall_4_0());
            				
            pushFollow(FOLLOW_2);
            lv_table_4_0=ruleTable();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getExamplesRule());
            					}
            					set(
            						current,
            						"table",
            						lv_table_4_0,
            						"org.xtext.example.feature.Feature.Table");
            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleExamples"


    // $ANTLR start "entryRuleTable"
    // InternalFeature.g:728:1: entryRuleTable returns [EObject current=null] : iv_ruleTable= ruleTable EOF ;
    public final EObject entryRuleTable() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleTable = null;


        try {
            // InternalFeature.g:728:46: (iv_ruleTable= ruleTable EOF )
            // InternalFeature.g:729:2: iv_ruleTable= ruleTable EOF
            {
             newCompositeNode(grammarAccess.getTableRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleTable=ruleTable();

            state._fsp--;

             current =iv_ruleTable; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleTable"


    // $ANTLR start "ruleTable"
    // InternalFeature.g:735:1: ruleTable returns [EObject current=null] : ( ( (lv_rows_0_0= RULE_TABLE_ROW ) )+ (this_EOL_1= RULE_EOL )* ) ;
    public final EObject ruleTable() throws RecognitionException {
        EObject current = null;

        Token lv_rows_0_0=null;
        Token this_EOL_1=null;


        	enterRule();

        try {
            // InternalFeature.g:741:2: ( ( ( (lv_rows_0_0= RULE_TABLE_ROW ) )+ (this_EOL_1= RULE_EOL )* ) )
            // InternalFeature.g:742:2: ( ( (lv_rows_0_0= RULE_TABLE_ROW ) )+ (this_EOL_1= RULE_EOL )* )
            {
            // InternalFeature.g:742:2: ( ( (lv_rows_0_0= RULE_TABLE_ROW ) )+ (this_EOL_1= RULE_EOL )* )
            // InternalFeature.g:743:3: ( (lv_rows_0_0= RULE_TABLE_ROW ) )+ (this_EOL_1= RULE_EOL )*
            {
            // InternalFeature.g:743:3: ( (lv_rows_0_0= RULE_TABLE_ROW ) )+
            int cnt26=0;
            loop26:
            do {
                int alt26=2;
                int LA26_0 = input.LA(1);

                if ( (LA26_0==RULE_TABLE_ROW) ) {
                    alt26=1;
                }


                switch (alt26) {
            	case 1 :
            	    // InternalFeature.g:744:4: (lv_rows_0_0= RULE_TABLE_ROW )
            	    {
            	    // InternalFeature.g:744:4: (lv_rows_0_0= RULE_TABLE_ROW )
            	    // InternalFeature.g:745:5: lv_rows_0_0= RULE_TABLE_ROW
            	    {
            	    lv_rows_0_0=(Token)match(input,RULE_TABLE_ROW,FOLLOW_22); 

            	    					newLeafNode(lv_rows_0_0, grammarAccess.getTableAccess().getRowsTABLE_ROWTerminalRuleCall_0_0());
            	    				

            	    					if (current==null) {
            	    						current = createModelElement(grammarAccess.getTableRule());
            	    					}
            	    					addWithLastConsumed(
            	    						current,
            	    						"rows",
            	    						lv_rows_0_0,
            	    						"org.xtext.example.feature.Feature.TABLE_ROW");
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt26 >= 1 ) break loop26;
                        EarlyExitException eee =
                            new EarlyExitException(26, input);
                        throw eee;
                }
                cnt26++;
            } while (true);

            // InternalFeature.g:761:3: (this_EOL_1= RULE_EOL )*
            loop27:
            do {
                int alt27=2;
                int LA27_0 = input.LA(1);

                if ( (LA27_0==RULE_EOL) ) {
                    alt27=1;
                }


                switch (alt27) {
            	case 1 :
            	    // InternalFeature.g:762:4: this_EOL_1= RULE_EOL
            	    {
            	    this_EOL_1=(Token)match(input,RULE_EOL,FOLLOW_23); 

            	    				newLeafNode(this_EOL_1, grammarAccess.getTableAccess().getEOLTerminalRuleCall_1());
            	    			

            	    }
            	    break;

            	default :
            	    break loop27;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleTable"


    // $ANTLR start "entryRuleDocString"
    // InternalFeature.g:771:1: entryRuleDocString returns [EObject current=null] : iv_ruleDocString= ruleDocString EOF ;
    public final EObject entryRuleDocString() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleDocString = null;


        try {
            // InternalFeature.g:771:50: (iv_ruleDocString= ruleDocString EOF )
            // InternalFeature.g:772:2: iv_ruleDocString= ruleDocString EOF
            {
             newCompositeNode(grammarAccess.getDocStringRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleDocString=ruleDocString();

            state._fsp--;

             current =iv_ruleDocString; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleDocString"


    // $ANTLR start "ruleDocString"
    // InternalFeature.g:778:1: ruleDocString returns [EObject current=null] : ( ( (lv_content_0_0= RULE_DOC_STRING ) ) (this_EOL_1= RULE_EOL )* ) ;
    public final EObject ruleDocString() throws RecognitionException {
        EObject current = null;

        Token lv_content_0_0=null;
        Token this_EOL_1=null;


        	enterRule();

        try {
            // InternalFeature.g:784:2: ( ( ( (lv_content_0_0= RULE_DOC_STRING ) ) (this_EOL_1= RULE_EOL )* ) )
            // InternalFeature.g:785:2: ( ( (lv_content_0_0= RULE_DOC_STRING ) ) (this_EOL_1= RULE_EOL )* )
            {
            // InternalFeature.g:785:2: ( ( (lv_content_0_0= RULE_DOC_STRING ) ) (this_EOL_1= RULE_EOL )* )
            // InternalFeature.g:786:3: ( (lv_content_0_0= RULE_DOC_STRING ) ) (this_EOL_1= RULE_EOL )*
            {
            // InternalFeature.g:786:3: ( (lv_content_0_0= RULE_DOC_STRING ) )
            // InternalFeature.g:787:4: (lv_content_0_0= RULE_DOC_STRING )
            {
            // InternalFeature.g:787:4: (lv_content_0_0= RULE_DOC_STRING )
            // InternalFeature.g:788:5: lv_content_0_0= RULE_DOC_STRING
            {
            lv_content_0_0=(Token)match(input,RULE_DOC_STRING,FOLLOW_23); 

            					newLeafNode(lv_content_0_0, grammarAccess.getDocStringAccess().getContentDOC_STRINGTerminalRuleCall_0_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getDocStringRule());
            					}
            					setWithLastConsumed(
            						current,
            						"content",
            						lv_content_0_0,
            						"org.xtext.example.feature.Feature.DOC_STRING");
            				

            }


            }

            // InternalFeature.g:804:3: (this_EOL_1= RULE_EOL )*
            loop28:
            do {
                int alt28=2;
                int LA28_0 = input.LA(1);

                if ( (LA28_0==RULE_EOL) ) {
                    alt28=1;
                }


                switch (alt28) {
            	case 1 :
            	    // InternalFeature.g:805:4: this_EOL_1= RULE_EOL
            	    {
            	    this_EOL_1=(Token)match(input,RULE_EOL,FOLLOW_23); 

            	    				newLeafNode(this_EOL_1, grammarAccess.getDocStringAccess().getEOLTerminalRuleCall_1());
            	    			

            	    }
            	    break;

            	default :
            	    break loop28;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDocString"


    // $ANTLR start "entryRuleTitle"
    // InternalFeature.g:814:1: entryRuleTitle returns [String current=null] : iv_ruleTitle= ruleTitle EOF ;
    public final String entryRuleTitle() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleTitle = null;


        try {
            // InternalFeature.g:814:45: (iv_ruleTitle= ruleTitle EOF )
            // InternalFeature.g:815:2: iv_ruleTitle= ruleTitle EOF
            {
             newCompositeNode(grammarAccess.getTitleRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleTitle=ruleTitle();

            state._fsp--;

             current =iv_ruleTitle.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleTitle"


    // $ANTLR start "ruleTitle"
    // InternalFeature.g:821:1: ruleTitle returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : ( (this_WORD_0= RULE_WORD | this_NUMBER_1= RULE_NUMBER | this_STRING_2= RULE_STRING | this_PLACEHOLDER_3= RULE_PLACEHOLDER ) (this_WORD_4= RULE_WORD | this_NUMBER_5= RULE_NUMBER | this_STRING_6= RULE_STRING | this_PLACEHOLDER_7= RULE_PLACEHOLDER | this_TAGNAME_8= RULE_TAGNAME )* ) ;
    public final AntlrDatatypeRuleToken ruleTitle() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_WORD_0=null;
        Token this_NUMBER_1=null;
        Token this_STRING_2=null;
        Token this_PLACEHOLDER_3=null;
        Token this_WORD_4=null;
        Token this_NUMBER_5=null;
        Token this_STRING_6=null;
        Token this_PLACEHOLDER_7=null;
        Token this_TAGNAME_8=null;


        	enterRule();

        try {
            // InternalFeature.g:827:2: ( ( (this_WORD_0= RULE_WORD | this_NUMBER_1= RULE_NUMBER | this_STRING_2= RULE_STRING | this_PLACEHOLDER_3= RULE_PLACEHOLDER ) (this_WORD_4= RULE_WORD | this_NUMBER_5= RULE_NUMBER | this_STRING_6= RULE_STRING | this_PLACEHOLDER_7= RULE_PLACEHOLDER | this_TAGNAME_8= RULE_TAGNAME )* ) )
            // InternalFeature.g:828:2: ( (this_WORD_0= RULE_WORD | this_NUMBER_1= RULE_NUMBER | this_STRING_2= RULE_STRING | this_PLACEHOLDER_3= RULE_PLACEHOLDER ) (this_WORD_4= RULE_WORD | this_NUMBER_5= RULE_NUMBER | this_STRING_6= RULE_STRING | this_PLACEHOLDER_7= RULE_PLACEHOLDER | this_TAGNAME_8= RULE_TAGNAME )* )
            {
            // InternalFeature.g:828:2: ( (this_WORD_0= RULE_WORD | this_NUMBER_1= RULE_NUMBER | this_STRING_2= RULE_STRING | this_PLACEHOLDER_3= RULE_PLACEHOLDER ) (this_WORD_4= RULE_WORD | this_NUMBER_5= RULE_NUMBER | this_STRING_6= RULE_STRING | this_PLACEHOLDER_7= RULE_PLACEHOLDER | this_TAGNAME_8= RULE_TAGNAME )* )
            // InternalFeature.g:829:3: (this_WORD_0= RULE_WORD | this_NUMBER_1= RULE_NUMBER | this_STRING_2= RULE_STRING | this_PLACEHOLDER_3= RULE_PLACEHOLDER ) (this_WORD_4= RULE_WORD | this_NUMBER_5= RULE_NUMBER | this_STRING_6= RULE_STRING | this_PLACEHOLDER_7= RULE_PLACEHOLDER | this_TAGNAME_8= RULE_TAGNAME )*
            {
            // InternalFeature.g:829:3: (this_WORD_0= RULE_WORD | this_NUMBER_1= RULE_NUMBER | this_STRING_2= RULE_STRING | this_PLACEHOLDER_3= RULE_PLACEHOLDER )
            int alt29=4;
            switch ( input.LA(1) ) {
            case RULE_WORD:
                {
                alt29=1;
                }
                break;
            case RULE_NUMBER:
                {
                alt29=2;
                }
                break;
            case RULE_STRING:
                {
                alt29=3;
                }
                break;
            case RULE_PLACEHOLDER:
                {
                alt29=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 29, 0, input);

                throw nvae;
            }

            switch (alt29) {
                case 1 :
                    // InternalFeature.g:830:4: this_WORD_0= RULE_WORD
                    {
                    this_WORD_0=(Token)match(input,RULE_WORD,FOLLOW_24); 

                    				current.merge(this_WORD_0);
                    			

                    				newLeafNode(this_WORD_0, grammarAccess.getTitleAccess().getWORDTerminalRuleCall_0_0());
                    			

                    }
                    break;
                case 2 :
                    // InternalFeature.g:838:4: this_NUMBER_1= RULE_NUMBER
                    {
                    this_NUMBER_1=(Token)match(input,RULE_NUMBER,FOLLOW_24); 

                    				current.merge(this_NUMBER_1);
                    			

                    				newLeafNode(this_NUMBER_1, grammarAccess.getTitleAccess().getNUMBERTerminalRuleCall_0_1());
                    			

                    }
                    break;
                case 3 :
                    // InternalFeature.g:846:4: this_STRING_2= RULE_STRING
                    {
                    this_STRING_2=(Token)match(input,RULE_STRING,FOLLOW_24); 

                    				current.merge(this_STRING_2);
                    			

                    				newLeafNode(this_STRING_2, grammarAccess.getTitleAccess().getSTRINGTerminalRuleCall_0_2());
                    			

                    }
                    break;
                case 4 :
                    // InternalFeature.g:854:4: this_PLACEHOLDER_3= RULE_PLACEHOLDER
                    {
                    this_PLACEHOLDER_3=(Token)match(input,RULE_PLACEHOLDER,FOLLOW_24); 

                    				current.merge(this_PLACEHOLDER_3);
                    			

                    				newLeafNode(this_PLACEHOLDER_3, grammarAccess.getTitleAccess().getPLACEHOLDERTerminalRuleCall_0_3());
                    			

                    }
                    break;

            }

            // InternalFeature.g:862:3: (this_WORD_4= RULE_WORD | this_NUMBER_5= RULE_NUMBER | this_STRING_6= RULE_STRING | this_PLACEHOLDER_7= RULE_PLACEHOLDER | this_TAGNAME_8= RULE_TAGNAME )*
            loop30:
            do {
                int alt30=6;
                switch ( input.LA(1) ) {
                case RULE_WORD:
                    {
                    alt30=1;
                    }
                    break;
                case RULE_NUMBER:
                    {
                    alt30=2;
                    }
                    break;
                case RULE_STRING:
                    {
                    alt30=3;
                    }
                    break;
                case RULE_PLACEHOLDER:
                    {
                    alt30=4;
                    }
                    break;
                case RULE_TAGNAME:
                    {
                    alt30=5;
                    }
                    break;

                }

                switch (alt30) {
            	case 1 :
            	    // InternalFeature.g:863:4: this_WORD_4= RULE_WORD
            	    {
            	    this_WORD_4=(Token)match(input,RULE_WORD,FOLLOW_24); 

            	    				current.merge(this_WORD_4);
            	    			

            	    				newLeafNode(this_WORD_4, grammarAccess.getTitleAccess().getWORDTerminalRuleCall_1_0());
            	    			

            	    }
            	    break;
            	case 2 :
            	    // InternalFeature.g:871:4: this_NUMBER_5= RULE_NUMBER
            	    {
            	    this_NUMBER_5=(Token)match(input,RULE_NUMBER,FOLLOW_24); 

            	    				current.merge(this_NUMBER_5);
            	    			

            	    				newLeafNode(this_NUMBER_5, grammarAccess.getTitleAccess().getNUMBERTerminalRuleCall_1_1());
            	    			

            	    }
            	    break;
            	case 3 :
            	    // InternalFeature.g:879:4: this_STRING_6= RULE_STRING
            	    {
            	    this_STRING_6=(Token)match(input,RULE_STRING,FOLLOW_24); 

            	    				current.merge(this_STRING_6);
            	    			

            	    				newLeafNode(this_STRING_6, grammarAccess.getTitleAccess().getSTRINGTerminalRuleCall_1_2());
            	    			

            	    }
            	    break;
            	case 4 :
            	    // InternalFeature.g:887:4: this_PLACEHOLDER_7= RULE_PLACEHOLDER
            	    {
            	    this_PLACEHOLDER_7=(Token)match(input,RULE_PLACEHOLDER,FOLLOW_24); 

            	    				current.merge(this_PLACEHOLDER_7);
            	    			

            	    				newLeafNode(this_PLACEHOLDER_7, grammarAccess.getTitleAccess().getPLACEHOLDERTerminalRuleCall_1_3());
            	    			

            	    }
            	    break;
            	case 5 :
            	    // InternalFeature.g:895:4: this_TAGNAME_8= RULE_TAGNAME
            	    {
            	    this_TAGNAME_8=(Token)match(input,RULE_TAGNAME,FOLLOW_24); 

            	    				current.merge(this_TAGNAME_8);
            	    			

            	    				newLeafNode(this_TAGNAME_8, grammarAccess.getTitleAccess().getTAGNAMETerminalRuleCall_1_4());
            	    			

            	    }
            	    break;

            	default :
            	    break loop30;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleTitle"


    // $ANTLR start "entryRuleNarrative"
    // InternalFeature.g:907:1: entryRuleNarrative returns [String current=null] : iv_ruleNarrative= ruleNarrative EOF ;
    public final String entryRuleNarrative() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleNarrative = null;


        try {
            // InternalFeature.g:907:49: (iv_ruleNarrative= ruleNarrative EOF )
            // InternalFeature.g:908:2: iv_ruleNarrative= ruleNarrative EOF
            {
             newCompositeNode(grammarAccess.getNarrativeRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleNarrative=ruleNarrative();

            state._fsp--;

             current =iv_ruleNarrative.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleNarrative"


    // $ANTLR start "ruleNarrative"
    // InternalFeature.g:914:1: ruleNarrative returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : ( (this_WORD_0= RULE_WORD | this_NUMBER_1= RULE_NUMBER | this_STRING_2= RULE_STRING | this_PLACEHOLDER_3= RULE_PLACEHOLDER ) (this_WORD_4= RULE_WORD | this_NUMBER_5= RULE_NUMBER | this_STRING_6= RULE_STRING | this_PLACEHOLDER_7= RULE_PLACEHOLDER | this_TAGNAME_8= RULE_TAGNAME )* (this_EOL_9= RULE_EOL )+ )+ ;
    public final AntlrDatatypeRuleToken ruleNarrative() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_WORD_0=null;
        Token this_NUMBER_1=null;
        Token this_STRING_2=null;
        Token this_PLACEHOLDER_3=null;
        Token this_WORD_4=null;
        Token this_NUMBER_5=null;
        Token this_STRING_6=null;
        Token this_PLACEHOLDER_7=null;
        Token this_TAGNAME_8=null;
        Token this_EOL_9=null;


        	enterRule();

        try {
            // InternalFeature.g:920:2: ( ( (this_WORD_0= RULE_WORD | this_NUMBER_1= RULE_NUMBER | this_STRING_2= RULE_STRING | this_PLACEHOLDER_3= RULE_PLACEHOLDER ) (this_WORD_4= RULE_WORD | this_NUMBER_5= RULE_NUMBER | this_STRING_6= RULE_STRING | this_PLACEHOLDER_7= RULE_PLACEHOLDER | this_TAGNAME_8= RULE_TAGNAME )* (this_EOL_9= RULE_EOL )+ )+ )
            // InternalFeature.g:921:2: ( (this_WORD_0= RULE_WORD | this_NUMBER_1= RULE_NUMBER | this_STRING_2= RULE_STRING | this_PLACEHOLDER_3= RULE_PLACEHOLDER ) (this_WORD_4= RULE_WORD | this_NUMBER_5= RULE_NUMBER | this_STRING_6= RULE_STRING | this_PLACEHOLDER_7= RULE_PLACEHOLDER | this_TAGNAME_8= RULE_TAGNAME )* (this_EOL_9= RULE_EOL )+ )+
            {
            // InternalFeature.g:921:2: ( (this_WORD_0= RULE_WORD | this_NUMBER_1= RULE_NUMBER | this_STRING_2= RULE_STRING | this_PLACEHOLDER_3= RULE_PLACEHOLDER ) (this_WORD_4= RULE_WORD | this_NUMBER_5= RULE_NUMBER | this_STRING_6= RULE_STRING | this_PLACEHOLDER_7= RULE_PLACEHOLDER | this_TAGNAME_8= RULE_TAGNAME )* (this_EOL_9= RULE_EOL )+ )+
            int cnt34=0;
            loop34:
            do {
                int alt34=2;
                int LA34_0 = input.LA(1);

                if ( ((LA34_0>=RULE_WORD && LA34_0<=RULE_PLACEHOLDER)) ) {
                    alt34=1;
                }


                switch (alt34) {
            	case 1 :
            	    // InternalFeature.g:922:3: (this_WORD_0= RULE_WORD | this_NUMBER_1= RULE_NUMBER | this_STRING_2= RULE_STRING | this_PLACEHOLDER_3= RULE_PLACEHOLDER ) (this_WORD_4= RULE_WORD | this_NUMBER_5= RULE_NUMBER | this_STRING_6= RULE_STRING | this_PLACEHOLDER_7= RULE_PLACEHOLDER | this_TAGNAME_8= RULE_TAGNAME )* (this_EOL_9= RULE_EOL )+
            	    {
            	    // InternalFeature.g:922:3: (this_WORD_0= RULE_WORD | this_NUMBER_1= RULE_NUMBER | this_STRING_2= RULE_STRING | this_PLACEHOLDER_3= RULE_PLACEHOLDER )
            	    int alt31=4;
            	    switch ( input.LA(1) ) {
            	    case RULE_WORD:
            	        {
            	        alt31=1;
            	        }
            	        break;
            	    case RULE_NUMBER:
            	        {
            	        alt31=2;
            	        }
            	        break;
            	    case RULE_STRING:
            	        {
            	        alt31=3;
            	        }
            	        break;
            	    case RULE_PLACEHOLDER:
            	        {
            	        alt31=4;
            	        }
            	        break;
            	    default:
            	        NoViableAltException nvae =
            	            new NoViableAltException("", 31, 0, input);

            	        throw nvae;
            	    }

            	    switch (alt31) {
            	        case 1 :
            	            // InternalFeature.g:923:4: this_WORD_0= RULE_WORD
            	            {
            	            this_WORD_0=(Token)match(input,RULE_WORD,FOLLOW_25); 

            	            				current.merge(this_WORD_0);
            	            			

            	            				newLeafNode(this_WORD_0, grammarAccess.getNarrativeAccess().getWORDTerminalRuleCall_0_0());
            	            			

            	            }
            	            break;
            	        case 2 :
            	            // InternalFeature.g:931:4: this_NUMBER_1= RULE_NUMBER
            	            {
            	            this_NUMBER_1=(Token)match(input,RULE_NUMBER,FOLLOW_25); 

            	            				current.merge(this_NUMBER_1);
            	            			

            	            				newLeafNode(this_NUMBER_1, grammarAccess.getNarrativeAccess().getNUMBERTerminalRuleCall_0_1());
            	            			

            	            }
            	            break;
            	        case 3 :
            	            // InternalFeature.g:939:4: this_STRING_2= RULE_STRING
            	            {
            	            this_STRING_2=(Token)match(input,RULE_STRING,FOLLOW_25); 

            	            				current.merge(this_STRING_2);
            	            			

            	            				newLeafNode(this_STRING_2, grammarAccess.getNarrativeAccess().getSTRINGTerminalRuleCall_0_2());
            	            			

            	            }
            	            break;
            	        case 4 :
            	            // InternalFeature.g:947:4: this_PLACEHOLDER_3= RULE_PLACEHOLDER
            	            {
            	            this_PLACEHOLDER_3=(Token)match(input,RULE_PLACEHOLDER,FOLLOW_25); 

            	            				current.merge(this_PLACEHOLDER_3);
            	            			

            	            				newLeafNode(this_PLACEHOLDER_3, grammarAccess.getNarrativeAccess().getPLACEHOLDERTerminalRuleCall_0_3());
            	            			

            	            }
            	            break;

            	    }

            	    // InternalFeature.g:955:3: (this_WORD_4= RULE_WORD | this_NUMBER_5= RULE_NUMBER | this_STRING_6= RULE_STRING | this_PLACEHOLDER_7= RULE_PLACEHOLDER | this_TAGNAME_8= RULE_TAGNAME )*
            	    loop32:
            	    do {
            	        int alt32=6;
            	        switch ( input.LA(1) ) {
            	        case RULE_WORD:
            	            {
            	            alt32=1;
            	            }
            	            break;
            	        case RULE_NUMBER:
            	            {
            	            alt32=2;
            	            }
            	            break;
            	        case RULE_STRING:
            	            {
            	            alt32=3;
            	            }
            	            break;
            	        case RULE_PLACEHOLDER:
            	            {
            	            alt32=4;
            	            }
            	            break;
            	        case RULE_TAGNAME:
            	            {
            	            alt32=5;
            	            }
            	            break;

            	        }

            	        switch (alt32) {
            	    	case 1 :
            	    	    // InternalFeature.g:956:4: this_WORD_4= RULE_WORD
            	    	    {
            	    	    this_WORD_4=(Token)match(input,RULE_WORD,FOLLOW_25); 

            	    	    				current.merge(this_WORD_4);
            	    	    			

            	    	    				newLeafNode(this_WORD_4, grammarAccess.getNarrativeAccess().getWORDTerminalRuleCall_1_0());
            	    	    			

            	    	    }
            	    	    break;
            	    	case 2 :
            	    	    // InternalFeature.g:964:4: this_NUMBER_5= RULE_NUMBER
            	    	    {
            	    	    this_NUMBER_5=(Token)match(input,RULE_NUMBER,FOLLOW_25); 

            	    	    				current.merge(this_NUMBER_5);
            	    	    			

            	    	    				newLeafNode(this_NUMBER_5, grammarAccess.getNarrativeAccess().getNUMBERTerminalRuleCall_1_1());
            	    	    			

            	    	    }
            	    	    break;
            	    	case 3 :
            	    	    // InternalFeature.g:972:4: this_STRING_6= RULE_STRING
            	    	    {
            	    	    this_STRING_6=(Token)match(input,RULE_STRING,FOLLOW_25); 

            	    	    				current.merge(this_STRING_6);
            	    	    			

            	    	    				newLeafNode(this_STRING_6, grammarAccess.getNarrativeAccess().getSTRINGTerminalRuleCall_1_2());
            	    	    			

            	    	    }
            	    	    break;
            	    	case 4 :
            	    	    // InternalFeature.g:980:4: this_PLACEHOLDER_7= RULE_PLACEHOLDER
            	    	    {
            	    	    this_PLACEHOLDER_7=(Token)match(input,RULE_PLACEHOLDER,FOLLOW_25); 

            	    	    				current.merge(this_PLACEHOLDER_7);
            	    	    			

            	    	    				newLeafNode(this_PLACEHOLDER_7, grammarAccess.getNarrativeAccess().getPLACEHOLDERTerminalRuleCall_1_3());
            	    	    			

            	    	    }
            	    	    break;
            	    	case 5 :
            	    	    // InternalFeature.g:988:4: this_TAGNAME_8= RULE_TAGNAME
            	    	    {
            	    	    this_TAGNAME_8=(Token)match(input,RULE_TAGNAME,FOLLOW_25); 

            	    	    				current.merge(this_TAGNAME_8);
            	    	    			

            	    	    				newLeafNode(this_TAGNAME_8, grammarAccess.getNarrativeAccess().getTAGNAMETerminalRuleCall_1_4());
            	    	    			

            	    	    }
            	    	    break;

            	    	default :
            	    	    break loop32;
            	        }
            	    } while (true);

            	    // InternalFeature.g:996:3: (this_EOL_9= RULE_EOL )+
            	    int cnt33=0;
            	    loop33:
            	    do {
            	        int alt33=2;
            	        int LA33_0 = input.LA(1);

            	        if ( (LA33_0==RULE_EOL) ) {
            	            alt33=1;
            	        }


            	        switch (alt33) {
            	    	case 1 :
            	    	    // InternalFeature.g:997:4: this_EOL_9= RULE_EOL
            	    	    {
            	    	    this_EOL_9=(Token)match(input,RULE_EOL,FOLLOW_26); 

            	    	    				current.merge(this_EOL_9);
            	    	    			

            	    	    				newLeafNode(this_EOL_9, grammarAccess.getNarrativeAccess().getEOLTerminalRuleCall_2());
            	    	    			

            	    	    }
            	    	    break;

            	    	default :
            	    	    if ( cnt33 >= 1 ) break loop33;
            	                EarlyExitException eee =
            	                    new EarlyExitException(33, input);
            	                throw eee;
            	        }
            	        cnt33++;
            	    } while (true);


            	    }
            	    break;

            	default :
            	    if ( cnt34 >= 1 ) break loop34;
                        EarlyExitException eee =
                            new EarlyExitException(34, input);
                        throw eee;
                }
                cnt34++;
            } while (true);


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleNarrative"


    // $ANTLR start "entryRuleStepDescription"
    // InternalFeature.g:1009:1: entryRuleStepDescription returns [String current=null] : iv_ruleStepDescription= ruleStepDescription EOF ;
    public final String entryRuleStepDescription() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleStepDescription = null;


        try {
            // InternalFeature.g:1009:55: (iv_ruleStepDescription= ruleStepDescription EOF )
            // InternalFeature.g:1010:2: iv_ruleStepDescription= ruleStepDescription EOF
            {
             newCompositeNode(grammarAccess.getStepDescriptionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleStepDescription=ruleStepDescription();

            state._fsp--;

             current =iv_ruleStepDescription.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleStepDescription"


    // $ANTLR start "ruleStepDescription"
    // InternalFeature.g:1016:1: ruleStepDescription returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_WORD_0= RULE_WORD | this_NUMBER_1= RULE_NUMBER | this_STRING_2= RULE_STRING | this_PLACEHOLDER_3= RULE_PLACEHOLDER | this_TAGNAME_4= RULE_TAGNAME )+ ;
    public final AntlrDatatypeRuleToken ruleStepDescription() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_WORD_0=null;
        Token this_NUMBER_1=null;
        Token this_STRING_2=null;
        Token this_PLACEHOLDER_3=null;
        Token this_TAGNAME_4=null;


        	enterRule();

        try {
            // InternalFeature.g:1022:2: ( (this_WORD_0= RULE_WORD | this_NUMBER_1= RULE_NUMBER | this_STRING_2= RULE_STRING | this_PLACEHOLDER_3= RULE_PLACEHOLDER | this_TAGNAME_4= RULE_TAGNAME )+ )
            // InternalFeature.g:1023:2: (this_WORD_0= RULE_WORD | this_NUMBER_1= RULE_NUMBER | this_STRING_2= RULE_STRING | this_PLACEHOLDER_3= RULE_PLACEHOLDER | this_TAGNAME_4= RULE_TAGNAME )+
            {
            // InternalFeature.g:1023:2: (this_WORD_0= RULE_WORD | this_NUMBER_1= RULE_NUMBER | this_STRING_2= RULE_STRING | this_PLACEHOLDER_3= RULE_PLACEHOLDER | this_TAGNAME_4= RULE_TAGNAME )+
            int cnt35=0;
            loop35:
            do {
                int alt35=6;
                switch ( input.LA(1) ) {
                case RULE_TAGNAME:
                    {
                    alt35=5;
                    }
                    break;
                case RULE_WORD:
                    {
                    alt35=1;
                    }
                    break;
                case RULE_NUMBER:
                    {
                    alt35=2;
                    }
                    break;
                case RULE_STRING:
                    {
                    alt35=3;
                    }
                    break;
                case RULE_PLACEHOLDER:
                    {
                    alt35=4;
                    }
                    break;

                }

                switch (alt35) {
            	case 1 :
            	    // InternalFeature.g:1024:3: this_WORD_0= RULE_WORD
            	    {
            	    this_WORD_0=(Token)match(input,RULE_WORD,FOLLOW_24); 

            	    			current.merge(this_WORD_0);
            	    		

            	    			newLeafNode(this_WORD_0, grammarAccess.getStepDescriptionAccess().getWORDTerminalRuleCall_0());
            	    		

            	    }
            	    break;
            	case 2 :
            	    // InternalFeature.g:1032:3: this_NUMBER_1= RULE_NUMBER
            	    {
            	    this_NUMBER_1=(Token)match(input,RULE_NUMBER,FOLLOW_24); 

            	    			current.merge(this_NUMBER_1);
            	    		

            	    			newLeafNode(this_NUMBER_1, grammarAccess.getStepDescriptionAccess().getNUMBERTerminalRuleCall_1());
            	    		

            	    }
            	    break;
            	case 3 :
            	    // InternalFeature.g:1040:3: this_STRING_2= RULE_STRING
            	    {
            	    this_STRING_2=(Token)match(input,RULE_STRING,FOLLOW_24); 

            	    			current.merge(this_STRING_2);
            	    		

            	    			newLeafNode(this_STRING_2, grammarAccess.getStepDescriptionAccess().getSTRINGTerminalRuleCall_2());
            	    		

            	    }
            	    break;
            	case 4 :
            	    // InternalFeature.g:1048:3: this_PLACEHOLDER_3= RULE_PLACEHOLDER
            	    {
            	    this_PLACEHOLDER_3=(Token)match(input,RULE_PLACEHOLDER,FOLLOW_24); 

            	    			current.merge(this_PLACEHOLDER_3);
            	    		

            	    			newLeafNode(this_PLACEHOLDER_3, grammarAccess.getStepDescriptionAccess().getPLACEHOLDERTerminalRuleCall_3());
            	    		

            	    }
            	    break;
            	case 5 :
            	    // InternalFeature.g:1056:3: this_TAGNAME_4= RULE_TAGNAME
            	    {
            	    this_TAGNAME_4=(Token)match(input,RULE_TAGNAME,FOLLOW_24); 

            	    			current.merge(this_TAGNAME_4);
            	    		

            	    			newLeafNode(this_TAGNAME_4, grammarAccess.getStepDescriptionAccess().getTAGNAMETerminalRuleCall_4());
            	    		

            	    }
            	    break;

            	default :
            	    if ( cnt35 >= 1 ) break loop35;
                        EarlyExitException eee =
                            new EarlyExitException(35, input);
                        throw eee;
                }
                cnt35++;
            } while (true);


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleStepDescription"


    // $ANTLR start "entryRuleTag"
    // InternalFeature.g:1067:1: entryRuleTag returns [EObject current=null] : iv_ruleTag= ruleTag EOF ;
    public final EObject entryRuleTag() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleTag = null;


        try {
            // InternalFeature.g:1067:44: (iv_ruleTag= ruleTag EOF )
            // InternalFeature.g:1068:2: iv_ruleTag= ruleTag EOF
            {
             newCompositeNode(grammarAccess.getTagRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleTag=ruleTag();

            state._fsp--;

             current =iv_ruleTag; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleTag"


    // $ANTLR start "ruleTag"
    // InternalFeature.g:1074:1: ruleTag returns [EObject current=null] : ( ( (lv_id_0_0= RULE_TAGNAME ) ) (this_EOL_1= RULE_EOL )? ) ;
    public final EObject ruleTag() throws RecognitionException {
        EObject current = null;

        Token lv_id_0_0=null;
        Token this_EOL_1=null;


        	enterRule();

        try {
            // InternalFeature.g:1080:2: ( ( ( (lv_id_0_0= RULE_TAGNAME ) ) (this_EOL_1= RULE_EOL )? ) )
            // InternalFeature.g:1081:2: ( ( (lv_id_0_0= RULE_TAGNAME ) ) (this_EOL_1= RULE_EOL )? )
            {
            // InternalFeature.g:1081:2: ( ( (lv_id_0_0= RULE_TAGNAME ) ) (this_EOL_1= RULE_EOL )? )
            // InternalFeature.g:1082:3: ( (lv_id_0_0= RULE_TAGNAME ) ) (this_EOL_1= RULE_EOL )?
            {
            // InternalFeature.g:1082:3: ( (lv_id_0_0= RULE_TAGNAME ) )
            // InternalFeature.g:1083:4: (lv_id_0_0= RULE_TAGNAME )
            {
            // InternalFeature.g:1083:4: (lv_id_0_0= RULE_TAGNAME )
            // InternalFeature.g:1084:5: lv_id_0_0= RULE_TAGNAME
            {
            lv_id_0_0=(Token)match(input,RULE_TAGNAME,FOLLOW_23); 

            					newLeafNode(lv_id_0_0, grammarAccess.getTagAccess().getIdTAGNAMETerminalRuleCall_0_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getTagRule());
            					}
            					setWithLastConsumed(
            						current,
            						"id",
            						lv_id_0_0,
            						"org.xtext.example.feature.Feature.TAGNAME");
            				

            }


            }

            // InternalFeature.g:1100:3: (this_EOL_1= RULE_EOL )?
            int alt36=2;
            int LA36_0 = input.LA(1);

            if ( (LA36_0==RULE_EOL) ) {
                alt36=1;
            }
            switch (alt36) {
                case 1 :
                    // InternalFeature.g:1101:4: this_EOL_1= RULE_EOL
                    {
                    this_EOL_1=(Token)match(input,RULE_EOL,FOLLOW_2); 

                    				newLeafNode(this_EOL_1, grammarAccess.getTagAccess().getEOLTerminalRuleCall_1());
                    			

                    }
                    break;

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleTag"


    // $ANTLR start "ruleStepKeyword"
    // InternalFeature.g:1110:1: ruleStepKeyword returns [Enumerator current=null] : ( (enumLiteral_0= 'Given' ) | (enumLiteral_1= 'When' ) | (enumLiteral_2= 'Then' ) | (enumLiteral_3= 'And' ) | (enumLiteral_4= 'But' ) ) ;
    public final Enumerator ruleStepKeyword() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;
        Token enumLiteral_1=null;
        Token enumLiteral_2=null;
        Token enumLiteral_3=null;
        Token enumLiteral_4=null;


        	enterRule();

        try {
            // InternalFeature.g:1116:2: ( ( (enumLiteral_0= 'Given' ) | (enumLiteral_1= 'When' ) | (enumLiteral_2= 'Then' ) | (enumLiteral_3= 'And' ) | (enumLiteral_4= 'But' ) ) )
            // InternalFeature.g:1117:2: ( (enumLiteral_0= 'Given' ) | (enumLiteral_1= 'When' ) | (enumLiteral_2= 'Then' ) | (enumLiteral_3= 'And' ) | (enumLiteral_4= 'But' ) )
            {
            // InternalFeature.g:1117:2: ( (enumLiteral_0= 'Given' ) | (enumLiteral_1= 'When' ) | (enumLiteral_2= 'Then' ) | (enumLiteral_3= 'And' ) | (enumLiteral_4= 'But' ) )
            int alt37=5;
            switch ( input.LA(1) ) {
            case 24:
                {
                alt37=1;
                }
                break;
            case 25:
                {
                alt37=2;
                }
                break;
            case 26:
                {
                alt37=3;
                }
                break;
            case 27:
                {
                alt37=4;
                }
                break;
            case 28:
                {
                alt37=5;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 37, 0, input);

                throw nvae;
            }

            switch (alt37) {
                case 1 :
                    // InternalFeature.g:1118:3: (enumLiteral_0= 'Given' )
                    {
                    // InternalFeature.g:1118:3: (enumLiteral_0= 'Given' )
                    // InternalFeature.g:1119:4: enumLiteral_0= 'Given'
                    {
                    enumLiteral_0=(Token)match(input,24,FOLLOW_2); 

                    				current = grammarAccess.getStepKeywordAccess().getGIVENEnumLiteralDeclaration_0().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_0, grammarAccess.getStepKeywordAccess().getGIVENEnumLiteralDeclaration_0());
                    			

                    }


                    }
                    break;
                case 2 :
                    // InternalFeature.g:1126:3: (enumLiteral_1= 'When' )
                    {
                    // InternalFeature.g:1126:3: (enumLiteral_1= 'When' )
                    // InternalFeature.g:1127:4: enumLiteral_1= 'When'
                    {
                    enumLiteral_1=(Token)match(input,25,FOLLOW_2); 

                    				current = grammarAccess.getStepKeywordAccess().getWHENEnumLiteralDeclaration_1().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_1, grammarAccess.getStepKeywordAccess().getWHENEnumLiteralDeclaration_1());
                    			

                    }


                    }
                    break;
                case 3 :
                    // InternalFeature.g:1134:3: (enumLiteral_2= 'Then' )
                    {
                    // InternalFeature.g:1134:3: (enumLiteral_2= 'Then' )
                    // InternalFeature.g:1135:4: enumLiteral_2= 'Then'
                    {
                    enumLiteral_2=(Token)match(input,26,FOLLOW_2); 

                    				current = grammarAccess.getStepKeywordAccess().getTHENEnumLiteralDeclaration_2().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_2, grammarAccess.getStepKeywordAccess().getTHENEnumLiteralDeclaration_2());
                    			

                    }


                    }
                    break;
                case 4 :
                    // InternalFeature.g:1142:3: (enumLiteral_3= 'And' )
                    {
                    // InternalFeature.g:1142:3: (enumLiteral_3= 'And' )
                    // InternalFeature.g:1143:4: enumLiteral_3= 'And'
                    {
                    enumLiteral_3=(Token)match(input,27,FOLLOW_2); 

                    				current = grammarAccess.getStepKeywordAccess().getANDEnumLiteralDeclaration_3().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_3, grammarAccess.getStepKeywordAccess().getANDEnumLiteralDeclaration_3());
                    			

                    }


                    }
                    break;
                case 5 :
                    // InternalFeature.g:1150:3: (enumLiteral_4= 'But' )
                    {
                    // InternalFeature.g:1150:3: (enumLiteral_4= 'But' )
                    // InternalFeature.g:1151:4: enumLiteral_4= 'But'
                    {
                    enumLiteral_4=(Token)match(input,28,FOLLOW_2); 

                    				current = grammarAccess.getStepKeywordAccess().getBUTEnumLiteralDeclaration_4().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_4, grammarAccess.getStepKeywordAccess().getBUTEnumLiteralDeclaration_4());
                    			

                    }


                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleStepKeyword"

    // Delegated rules


    protected DFA5 dfa5 = new DFA5(this);
    static final String dfa_1s = "\5\uffff";
    static final String dfa_2s = "\1\13\1\4\2\uffff\1\13";
    static final String dfa_3s = "\2\26\2\uffff\1\26";
    static final String dfa_4s = "\2\uffff\1\1\1\2\1\uffff";
    static final String dfa_5s = "\5\uffff}>";
    static final String[] dfa_6s = {
            "\1\1\11\uffff\1\2\1\3",
            "\1\4\6\uffff\1\1\11\uffff\1\2\1\3",
            "",
            "",
            "\1\1\11\uffff\1\2\1\3"
    };

    static final short[] dfa_1 = DFA.unpackEncodedString(dfa_1s);
    static final char[] dfa_2 = DFA.unpackEncodedStringToUnsignedChars(dfa_2s);
    static final char[] dfa_3 = DFA.unpackEncodedStringToUnsignedChars(dfa_3s);
    static final short[] dfa_4 = DFA.unpackEncodedString(dfa_4s);
    static final short[] dfa_5 = DFA.unpackEncodedString(dfa_5s);
    static final short[][] dfa_6 = unpackEncodedStringArray(dfa_6s);

    class DFA5 extends DFA {

        public DFA5(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 5;
            this.eot = dfa_1;
            this.eof = dfa_1;
            this.min = dfa_2;
            this.max = dfa_3;
            this.accept = dfa_4;
            this.special = dfa_5;
            this.transition = dfa_6;
        }
        public String getDescription() {
            return "168:5: (lv_scenarios_6_1= ruleScenario | lv_scenarios_6_2= ruleScenarioOutline )";
        }
    }
 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000000080800L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000000780L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000000780F90L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000000780F80L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000000000780F82L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000000000790L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x000000001F000790L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x000000001F000780L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x000000001F000782L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000000000280800L});
    public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x0000000000480800L});
    public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x000000001F800780L});
    public static final BitSet FOLLOW_16 = new BitSet(new long[]{0x0000000000000F80L});
    public static final BitSet FOLLOW_17 = new BitSet(new long[]{0x0000000000000072L});
    public static final BitSet FOLLOW_18 = new BitSet(new long[]{0x0000000000000062L});
    public static final BitSet FOLLOW_19 = new BitSet(new long[]{0x0000000000000022L});
    public static final BitSet FOLLOW_20 = new BitSet(new long[]{0x00000000000007B0L});
    public static final BitSet FOLLOW_21 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_22 = new BitSet(new long[]{0x0000000000000032L});
    public static final BitSet FOLLOW_23 = new BitSet(new long[]{0x0000000000000012L});
    public static final BitSet FOLLOW_24 = new BitSet(new long[]{0x0000000000000F82L});
    public static final BitSet FOLLOW_25 = new BitSet(new long[]{0x0000000000000F90L});
    public static final BitSet FOLLOW_26 = new BitSet(new long[]{0x0000000000000792L});

}