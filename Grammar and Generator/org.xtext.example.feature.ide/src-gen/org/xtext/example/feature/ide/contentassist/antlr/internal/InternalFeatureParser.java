package org.xtext.example.feature.ide.contentassist.antlr.internal;

import java.io.InputStream;
import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.AbstractInternalContentAssistParser;
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.DFA;
import org.xtext.example.feature.services.FeatureGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalFeatureParser extends AbstractInternalContentAssistParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_WORD", "RULE_NUMBER", "RULE_STRING", "RULE_PLACEHOLDER", "RULE_TAGNAME", "RULE_EOL", "RULE_TABLE_ROW", "RULE_DOC_STRING", "RULE_NL", "RULE_SL_COMMENT", "RULE_WS", "RULE_ID", "RULE_INT", "RULE_ML_COMMENT", "RULE_ANY_OTHER", "'Given'", "'When'", "'Then'", "'And'", "'But'", "'Feature:'", "'Background:'", "'Scenario:'", "'Scenario Outline:'", "'Examples:'"
    };
    public static final int RULE_DOC_STRING=11;
    public static final int RULE_WORD=4;
    public static final int RULE_STRING=6;
    public static final int RULE_SL_COMMENT=13;
    public static final int T__19=19;
    public static final int EOF=-1;
    public static final int RULE_ID=15;
    public static final int RULE_EOL=9;
    public static final int RULE_WS=14;
    public static final int RULE_TAGNAME=8;
    public static final int RULE_ANY_OTHER=18;
    public static final int RULE_NUMBER=5;
    public static final int T__26=26;
    public static final int RULE_PLACEHOLDER=7;
    public static final int T__27=27;
    public static final int RULE_TABLE_ROW=10;
    public static final int T__28=28;
    public static final int RULE_INT=16;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=17;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int RULE_NL=12;
    public static final int T__20=20;
    public static final int T__21=21;

    // delegates
    // delegators


        public InternalFeatureParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalFeatureParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalFeatureParser.tokenNames; }
    public String getGrammarFileName() { return "InternalFeature.g"; }


    	private FeatureGrammarAccess grammarAccess;

    	public void setGrammarAccess(FeatureGrammarAccess grammarAccess) {
    		this.grammarAccess = grammarAccess;
    	}

    	@Override
    	protected Grammar getGrammar() {
    		return grammarAccess.getGrammar();
    	}

    	@Override
    	protected String getValueForTokenName(String tokenName) {
    		return tokenName;
    	}



    // $ANTLR start "entryRuleFeature"
    // InternalFeature.g:53:1: entryRuleFeature : ruleFeature EOF ;
    public final void entryRuleFeature() throws RecognitionException {
        try {
            // InternalFeature.g:54:1: ( ruleFeature EOF )
            // InternalFeature.g:55:1: ruleFeature EOF
            {
             before(grammarAccess.getFeatureRule()); 
            pushFollow(FOLLOW_1);
            ruleFeature();

            state._fsp--;

             after(grammarAccess.getFeatureRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleFeature"


    // $ANTLR start "ruleFeature"
    // InternalFeature.g:62:1: ruleFeature : ( ( rule__Feature__Group__0 ) ) ;
    public final void ruleFeature() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFeature.g:66:2: ( ( ( rule__Feature__Group__0 ) ) )
            // InternalFeature.g:67:2: ( ( rule__Feature__Group__0 ) )
            {
            // InternalFeature.g:67:2: ( ( rule__Feature__Group__0 ) )
            // InternalFeature.g:68:3: ( rule__Feature__Group__0 )
            {
             before(grammarAccess.getFeatureAccess().getGroup()); 
            // InternalFeature.g:69:3: ( rule__Feature__Group__0 )
            // InternalFeature.g:69:4: rule__Feature__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Feature__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getFeatureAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleFeature"


    // $ANTLR start "entryRuleBackground"
    // InternalFeature.g:78:1: entryRuleBackground : ruleBackground EOF ;
    public final void entryRuleBackground() throws RecognitionException {
        try {
            // InternalFeature.g:79:1: ( ruleBackground EOF )
            // InternalFeature.g:80:1: ruleBackground EOF
            {
             before(grammarAccess.getBackgroundRule()); 
            pushFollow(FOLLOW_1);
            ruleBackground();

            state._fsp--;

             after(grammarAccess.getBackgroundRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleBackground"


    // $ANTLR start "ruleBackground"
    // InternalFeature.g:87:1: ruleBackground : ( ( rule__Background__Group__0 ) ) ;
    public final void ruleBackground() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFeature.g:91:2: ( ( ( rule__Background__Group__0 ) ) )
            // InternalFeature.g:92:2: ( ( rule__Background__Group__0 ) )
            {
            // InternalFeature.g:92:2: ( ( rule__Background__Group__0 ) )
            // InternalFeature.g:93:3: ( rule__Background__Group__0 )
            {
             before(grammarAccess.getBackgroundAccess().getGroup()); 
            // InternalFeature.g:94:3: ( rule__Background__Group__0 )
            // InternalFeature.g:94:4: rule__Background__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Background__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getBackgroundAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleBackground"


    // $ANTLR start "entryRuleScenario"
    // InternalFeature.g:103:1: entryRuleScenario : ruleScenario EOF ;
    public final void entryRuleScenario() throws RecognitionException {
        try {
            // InternalFeature.g:104:1: ( ruleScenario EOF )
            // InternalFeature.g:105:1: ruleScenario EOF
            {
             before(grammarAccess.getScenarioRule()); 
            pushFollow(FOLLOW_1);
            ruleScenario();

            state._fsp--;

             after(grammarAccess.getScenarioRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleScenario"


    // $ANTLR start "ruleScenario"
    // InternalFeature.g:112:1: ruleScenario : ( ( rule__Scenario__Group__0 ) ) ;
    public final void ruleScenario() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFeature.g:116:2: ( ( ( rule__Scenario__Group__0 ) ) )
            // InternalFeature.g:117:2: ( ( rule__Scenario__Group__0 ) )
            {
            // InternalFeature.g:117:2: ( ( rule__Scenario__Group__0 ) )
            // InternalFeature.g:118:3: ( rule__Scenario__Group__0 )
            {
             before(grammarAccess.getScenarioAccess().getGroup()); 
            // InternalFeature.g:119:3: ( rule__Scenario__Group__0 )
            // InternalFeature.g:119:4: rule__Scenario__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Scenario__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getScenarioAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleScenario"


    // $ANTLR start "entryRuleScenarioOutline"
    // InternalFeature.g:128:1: entryRuleScenarioOutline : ruleScenarioOutline EOF ;
    public final void entryRuleScenarioOutline() throws RecognitionException {
        try {
            // InternalFeature.g:129:1: ( ruleScenarioOutline EOF )
            // InternalFeature.g:130:1: ruleScenarioOutline EOF
            {
             before(grammarAccess.getScenarioOutlineRule()); 
            pushFollow(FOLLOW_1);
            ruleScenarioOutline();

            state._fsp--;

             after(grammarAccess.getScenarioOutlineRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleScenarioOutline"


    // $ANTLR start "ruleScenarioOutline"
    // InternalFeature.g:137:1: ruleScenarioOutline : ( ( rule__ScenarioOutline__Group__0 ) ) ;
    public final void ruleScenarioOutline() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFeature.g:141:2: ( ( ( rule__ScenarioOutline__Group__0 ) ) )
            // InternalFeature.g:142:2: ( ( rule__ScenarioOutline__Group__0 ) )
            {
            // InternalFeature.g:142:2: ( ( rule__ScenarioOutline__Group__0 ) )
            // InternalFeature.g:143:3: ( rule__ScenarioOutline__Group__0 )
            {
             before(grammarAccess.getScenarioOutlineAccess().getGroup()); 
            // InternalFeature.g:144:3: ( rule__ScenarioOutline__Group__0 )
            // InternalFeature.g:144:4: rule__ScenarioOutline__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__ScenarioOutline__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getScenarioOutlineAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleScenarioOutline"


    // $ANTLR start "entryRuleStep"
    // InternalFeature.g:153:1: entryRuleStep : ruleStep EOF ;
    public final void entryRuleStep() throws RecognitionException {
        try {
            // InternalFeature.g:154:1: ( ruleStep EOF )
            // InternalFeature.g:155:1: ruleStep EOF
            {
             before(grammarAccess.getStepRule()); 
            pushFollow(FOLLOW_1);
            ruleStep();

            state._fsp--;

             after(grammarAccess.getStepRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleStep"


    // $ANTLR start "ruleStep"
    // InternalFeature.g:162:1: ruleStep : ( ( rule__Step__Group__0 ) ) ;
    public final void ruleStep() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFeature.g:166:2: ( ( ( rule__Step__Group__0 ) ) )
            // InternalFeature.g:167:2: ( ( rule__Step__Group__0 ) )
            {
            // InternalFeature.g:167:2: ( ( rule__Step__Group__0 ) )
            // InternalFeature.g:168:3: ( rule__Step__Group__0 )
            {
             before(grammarAccess.getStepAccess().getGroup()); 
            // InternalFeature.g:169:3: ( rule__Step__Group__0 )
            // InternalFeature.g:169:4: rule__Step__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Step__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getStepAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleStep"


    // $ANTLR start "entryRuleExamples"
    // InternalFeature.g:178:1: entryRuleExamples : ruleExamples EOF ;
    public final void entryRuleExamples() throws RecognitionException {
        try {
            // InternalFeature.g:179:1: ( ruleExamples EOF )
            // InternalFeature.g:180:1: ruleExamples EOF
            {
             before(grammarAccess.getExamplesRule()); 
            pushFollow(FOLLOW_1);
            ruleExamples();

            state._fsp--;

             after(grammarAccess.getExamplesRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleExamples"


    // $ANTLR start "ruleExamples"
    // InternalFeature.g:187:1: ruleExamples : ( ( rule__Examples__Group__0 ) ) ;
    public final void ruleExamples() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFeature.g:191:2: ( ( ( rule__Examples__Group__0 ) ) )
            // InternalFeature.g:192:2: ( ( rule__Examples__Group__0 ) )
            {
            // InternalFeature.g:192:2: ( ( rule__Examples__Group__0 ) )
            // InternalFeature.g:193:3: ( rule__Examples__Group__0 )
            {
             before(grammarAccess.getExamplesAccess().getGroup()); 
            // InternalFeature.g:194:3: ( rule__Examples__Group__0 )
            // InternalFeature.g:194:4: rule__Examples__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Examples__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getExamplesAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleExamples"


    // $ANTLR start "entryRuleTable"
    // InternalFeature.g:203:1: entryRuleTable : ruleTable EOF ;
    public final void entryRuleTable() throws RecognitionException {
        try {
            // InternalFeature.g:204:1: ( ruleTable EOF )
            // InternalFeature.g:205:1: ruleTable EOF
            {
             before(grammarAccess.getTableRule()); 
            pushFollow(FOLLOW_1);
            ruleTable();

            state._fsp--;

             after(grammarAccess.getTableRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleTable"


    // $ANTLR start "ruleTable"
    // InternalFeature.g:212:1: ruleTable : ( ( rule__Table__Group__0 ) ) ;
    public final void ruleTable() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFeature.g:216:2: ( ( ( rule__Table__Group__0 ) ) )
            // InternalFeature.g:217:2: ( ( rule__Table__Group__0 ) )
            {
            // InternalFeature.g:217:2: ( ( rule__Table__Group__0 ) )
            // InternalFeature.g:218:3: ( rule__Table__Group__0 )
            {
             before(grammarAccess.getTableAccess().getGroup()); 
            // InternalFeature.g:219:3: ( rule__Table__Group__0 )
            // InternalFeature.g:219:4: rule__Table__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Table__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getTableAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleTable"


    // $ANTLR start "entryRuleDocString"
    // InternalFeature.g:228:1: entryRuleDocString : ruleDocString EOF ;
    public final void entryRuleDocString() throws RecognitionException {
        try {
            // InternalFeature.g:229:1: ( ruleDocString EOF )
            // InternalFeature.g:230:1: ruleDocString EOF
            {
             before(grammarAccess.getDocStringRule()); 
            pushFollow(FOLLOW_1);
            ruleDocString();

            state._fsp--;

             after(grammarAccess.getDocStringRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleDocString"


    // $ANTLR start "ruleDocString"
    // InternalFeature.g:237:1: ruleDocString : ( ( rule__DocString__Group__0 ) ) ;
    public final void ruleDocString() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFeature.g:241:2: ( ( ( rule__DocString__Group__0 ) ) )
            // InternalFeature.g:242:2: ( ( rule__DocString__Group__0 ) )
            {
            // InternalFeature.g:242:2: ( ( rule__DocString__Group__0 ) )
            // InternalFeature.g:243:3: ( rule__DocString__Group__0 )
            {
             before(grammarAccess.getDocStringAccess().getGroup()); 
            // InternalFeature.g:244:3: ( rule__DocString__Group__0 )
            // InternalFeature.g:244:4: rule__DocString__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__DocString__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getDocStringAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleDocString"


    // $ANTLR start "entryRuleTitle"
    // InternalFeature.g:253:1: entryRuleTitle : ruleTitle EOF ;
    public final void entryRuleTitle() throws RecognitionException {
        try {
            // InternalFeature.g:254:1: ( ruleTitle EOF )
            // InternalFeature.g:255:1: ruleTitle EOF
            {
             before(grammarAccess.getTitleRule()); 
            pushFollow(FOLLOW_1);
            ruleTitle();

            state._fsp--;

             after(grammarAccess.getTitleRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleTitle"


    // $ANTLR start "ruleTitle"
    // InternalFeature.g:262:1: ruleTitle : ( ( rule__Title__Group__0 ) ) ;
    public final void ruleTitle() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFeature.g:266:2: ( ( ( rule__Title__Group__0 ) ) )
            // InternalFeature.g:267:2: ( ( rule__Title__Group__0 ) )
            {
            // InternalFeature.g:267:2: ( ( rule__Title__Group__0 ) )
            // InternalFeature.g:268:3: ( rule__Title__Group__0 )
            {
             before(grammarAccess.getTitleAccess().getGroup()); 
            // InternalFeature.g:269:3: ( rule__Title__Group__0 )
            // InternalFeature.g:269:4: rule__Title__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Title__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getTitleAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleTitle"


    // $ANTLR start "entryRuleNarrative"
    // InternalFeature.g:278:1: entryRuleNarrative : ruleNarrative EOF ;
    public final void entryRuleNarrative() throws RecognitionException {
        try {
            // InternalFeature.g:279:1: ( ruleNarrative EOF )
            // InternalFeature.g:280:1: ruleNarrative EOF
            {
             before(grammarAccess.getNarrativeRule()); 
            pushFollow(FOLLOW_1);
            ruleNarrative();

            state._fsp--;

             after(grammarAccess.getNarrativeRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleNarrative"


    // $ANTLR start "ruleNarrative"
    // InternalFeature.g:287:1: ruleNarrative : ( ( ( rule__Narrative__Group__0 ) ) ( ( rule__Narrative__Group__0 )* ) ) ;
    public final void ruleNarrative() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFeature.g:291:2: ( ( ( ( rule__Narrative__Group__0 ) ) ( ( rule__Narrative__Group__0 )* ) ) )
            // InternalFeature.g:292:2: ( ( ( rule__Narrative__Group__0 ) ) ( ( rule__Narrative__Group__0 )* ) )
            {
            // InternalFeature.g:292:2: ( ( ( rule__Narrative__Group__0 ) ) ( ( rule__Narrative__Group__0 )* ) )
            // InternalFeature.g:293:3: ( ( rule__Narrative__Group__0 ) ) ( ( rule__Narrative__Group__0 )* )
            {
            // InternalFeature.g:293:3: ( ( rule__Narrative__Group__0 ) )
            // InternalFeature.g:294:4: ( rule__Narrative__Group__0 )
            {
             before(grammarAccess.getNarrativeAccess().getGroup()); 
            // InternalFeature.g:295:4: ( rule__Narrative__Group__0 )
            // InternalFeature.g:295:5: rule__Narrative__Group__0
            {
            pushFollow(FOLLOW_3);
            rule__Narrative__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getNarrativeAccess().getGroup()); 

            }

            // InternalFeature.g:298:3: ( ( rule__Narrative__Group__0 )* )
            // InternalFeature.g:299:4: ( rule__Narrative__Group__0 )*
            {
             before(grammarAccess.getNarrativeAccess().getGroup()); 
            // InternalFeature.g:300:4: ( rule__Narrative__Group__0 )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( ((LA1_0>=RULE_WORD && LA1_0<=RULE_PLACEHOLDER)) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // InternalFeature.g:300:5: rule__Narrative__Group__0
            	    {
            	    pushFollow(FOLLOW_3);
            	    rule__Narrative__Group__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);

             after(grammarAccess.getNarrativeAccess().getGroup()); 

            }


            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleNarrative"


    // $ANTLR start "entryRuleStepDescription"
    // InternalFeature.g:310:1: entryRuleStepDescription : ruleStepDescription EOF ;
    public final void entryRuleStepDescription() throws RecognitionException {
        try {
            // InternalFeature.g:311:1: ( ruleStepDescription EOF )
            // InternalFeature.g:312:1: ruleStepDescription EOF
            {
             before(grammarAccess.getStepDescriptionRule()); 
            pushFollow(FOLLOW_1);
            ruleStepDescription();

            state._fsp--;

             after(grammarAccess.getStepDescriptionRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleStepDescription"


    // $ANTLR start "ruleStepDescription"
    // InternalFeature.g:319:1: ruleStepDescription : ( ( ( rule__StepDescription__Alternatives ) ) ( ( rule__StepDescription__Alternatives )* ) ) ;
    public final void ruleStepDescription() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFeature.g:323:2: ( ( ( ( rule__StepDescription__Alternatives ) ) ( ( rule__StepDescription__Alternatives )* ) ) )
            // InternalFeature.g:324:2: ( ( ( rule__StepDescription__Alternatives ) ) ( ( rule__StepDescription__Alternatives )* ) )
            {
            // InternalFeature.g:324:2: ( ( ( rule__StepDescription__Alternatives ) ) ( ( rule__StepDescription__Alternatives )* ) )
            // InternalFeature.g:325:3: ( ( rule__StepDescription__Alternatives ) ) ( ( rule__StepDescription__Alternatives )* )
            {
            // InternalFeature.g:325:3: ( ( rule__StepDescription__Alternatives ) )
            // InternalFeature.g:326:4: ( rule__StepDescription__Alternatives )
            {
             before(grammarAccess.getStepDescriptionAccess().getAlternatives()); 
            // InternalFeature.g:327:4: ( rule__StepDescription__Alternatives )
            // InternalFeature.g:327:5: rule__StepDescription__Alternatives
            {
            pushFollow(FOLLOW_4);
            rule__StepDescription__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getStepDescriptionAccess().getAlternatives()); 

            }

            // InternalFeature.g:330:3: ( ( rule__StepDescription__Alternatives )* )
            // InternalFeature.g:331:4: ( rule__StepDescription__Alternatives )*
            {
             before(grammarAccess.getStepDescriptionAccess().getAlternatives()); 
            // InternalFeature.g:332:4: ( rule__StepDescription__Alternatives )*
            loop2:
            do {
                int alt2=2;
                int LA2_0 = input.LA(1);

                if ( ((LA2_0>=RULE_WORD && LA2_0<=RULE_TAGNAME)) ) {
                    alt2=1;
                }


                switch (alt2) {
            	case 1 :
            	    // InternalFeature.g:332:5: rule__StepDescription__Alternatives
            	    {
            	    pushFollow(FOLLOW_4);
            	    rule__StepDescription__Alternatives();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop2;
                }
            } while (true);

             after(grammarAccess.getStepDescriptionAccess().getAlternatives()); 

            }


            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleStepDescription"


    // $ANTLR start "entryRuleTag"
    // InternalFeature.g:342:1: entryRuleTag : ruleTag EOF ;
    public final void entryRuleTag() throws RecognitionException {
        try {
            // InternalFeature.g:343:1: ( ruleTag EOF )
            // InternalFeature.g:344:1: ruleTag EOF
            {
             before(grammarAccess.getTagRule()); 
            pushFollow(FOLLOW_1);
            ruleTag();

            state._fsp--;

             after(grammarAccess.getTagRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleTag"


    // $ANTLR start "ruleTag"
    // InternalFeature.g:351:1: ruleTag : ( ( rule__Tag__Group__0 ) ) ;
    public final void ruleTag() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFeature.g:355:2: ( ( ( rule__Tag__Group__0 ) ) )
            // InternalFeature.g:356:2: ( ( rule__Tag__Group__0 ) )
            {
            // InternalFeature.g:356:2: ( ( rule__Tag__Group__0 ) )
            // InternalFeature.g:357:3: ( rule__Tag__Group__0 )
            {
             before(grammarAccess.getTagAccess().getGroup()); 
            // InternalFeature.g:358:3: ( rule__Tag__Group__0 )
            // InternalFeature.g:358:4: rule__Tag__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Tag__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getTagAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleTag"


    // $ANTLR start "ruleStepKeyword"
    // InternalFeature.g:367:1: ruleStepKeyword : ( ( rule__StepKeyword__Alternatives ) ) ;
    public final void ruleStepKeyword() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFeature.g:371:1: ( ( ( rule__StepKeyword__Alternatives ) ) )
            // InternalFeature.g:372:2: ( ( rule__StepKeyword__Alternatives ) )
            {
            // InternalFeature.g:372:2: ( ( rule__StepKeyword__Alternatives ) )
            // InternalFeature.g:373:3: ( rule__StepKeyword__Alternatives )
            {
             before(grammarAccess.getStepKeywordAccess().getAlternatives()); 
            // InternalFeature.g:374:3: ( rule__StepKeyword__Alternatives )
            // InternalFeature.g:374:4: rule__StepKeyword__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__StepKeyword__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getStepKeywordAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleStepKeyword"


    // $ANTLR start "rule__Feature__ScenariosAlternatives_6_0"
    // InternalFeature.g:382:1: rule__Feature__ScenariosAlternatives_6_0 : ( ( ruleScenario ) | ( ruleScenarioOutline ) );
    public final void rule__Feature__ScenariosAlternatives_6_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFeature.g:386:1: ( ( ruleScenario ) | ( ruleScenarioOutline ) )
            int alt3=2;
            alt3 = dfa3.predict(input);
            switch (alt3) {
                case 1 :
                    // InternalFeature.g:387:2: ( ruleScenario )
                    {
                    // InternalFeature.g:387:2: ( ruleScenario )
                    // InternalFeature.g:388:3: ruleScenario
                    {
                     before(grammarAccess.getFeatureAccess().getScenariosScenarioParserRuleCall_6_0_0()); 
                    pushFollow(FOLLOW_2);
                    ruleScenario();

                    state._fsp--;

                     after(grammarAccess.getFeatureAccess().getScenariosScenarioParserRuleCall_6_0_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalFeature.g:393:2: ( ruleScenarioOutline )
                    {
                    // InternalFeature.g:393:2: ( ruleScenarioOutline )
                    // InternalFeature.g:394:3: ruleScenarioOutline
                    {
                     before(grammarAccess.getFeatureAccess().getScenariosScenarioOutlineParserRuleCall_6_0_1()); 
                    pushFollow(FOLLOW_2);
                    ruleScenarioOutline();

                    state._fsp--;

                     after(grammarAccess.getFeatureAccess().getScenariosScenarioOutlineParserRuleCall_6_0_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Feature__ScenariosAlternatives_6_0"


    // $ANTLR start "rule__Title__Alternatives_0"
    // InternalFeature.g:403:1: rule__Title__Alternatives_0 : ( ( RULE_WORD ) | ( RULE_NUMBER ) | ( RULE_STRING ) | ( RULE_PLACEHOLDER ) );
    public final void rule__Title__Alternatives_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFeature.g:407:1: ( ( RULE_WORD ) | ( RULE_NUMBER ) | ( RULE_STRING ) | ( RULE_PLACEHOLDER ) )
            int alt4=4;
            switch ( input.LA(1) ) {
            case RULE_WORD:
                {
                alt4=1;
                }
                break;
            case RULE_NUMBER:
                {
                alt4=2;
                }
                break;
            case RULE_STRING:
                {
                alt4=3;
                }
                break;
            case RULE_PLACEHOLDER:
                {
                alt4=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 4, 0, input);

                throw nvae;
            }

            switch (alt4) {
                case 1 :
                    // InternalFeature.g:408:2: ( RULE_WORD )
                    {
                    // InternalFeature.g:408:2: ( RULE_WORD )
                    // InternalFeature.g:409:3: RULE_WORD
                    {
                     before(grammarAccess.getTitleAccess().getWORDTerminalRuleCall_0_0()); 
                    match(input,RULE_WORD,FOLLOW_2); 
                     after(grammarAccess.getTitleAccess().getWORDTerminalRuleCall_0_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalFeature.g:414:2: ( RULE_NUMBER )
                    {
                    // InternalFeature.g:414:2: ( RULE_NUMBER )
                    // InternalFeature.g:415:3: RULE_NUMBER
                    {
                     before(grammarAccess.getTitleAccess().getNUMBERTerminalRuleCall_0_1()); 
                    match(input,RULE_NUMBER,FOLLOW_2); 
                     after(grammarAccess.getTitleAccess().getNUMBERTerminalRuleCall_0_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalFeature.g:420:2: ( RULE_STRING )
                    {
                    // InternalFeature.g:420:2: ( RULE_STRING )
                    // InternalFeature.g:421:3: RULE_STRING
                    {
                     before(grammarAccess.getTitleAccess().getSTRINGTerminalRuleCall_0_2()); 
                    match(input,RULE_STRING,FOLLOW_2); 
                     after(grammarAccess.getTitleAccess().getSTRINGTerminalRuleCall_0_2()); 

                    }


                    }
                    break;
                case 4 :
                    // InternalFeature.g:426:2: ( RULE_PLACEHOLDER )
                    {
                    // InternalFeature.g:426:2: ( RULE_PLACEHOLDER )
                    // InternalFeature.g:427:3: RULE_PLACEHOLDER
                    {
                     before(grammarAccess.getTitleAccess().getPLACEHOLDERTerminalRuleCall_0_3()); 
                    match(input,RULE_PLACEHOLDER,FOLLOW_2); 
                     after(grammarAccess.getTitleAccess().getPLACEHOLDERTerminalRuleCall_0_3()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Title__Alternatives_0"


    // $ANTLR start "rule__Title__Alternatives_1"
    // InternalFeature.g:436:1: rule__Title__Alternatives_1 : ( ( RULE_WORD ) | ( RULE_NUMBER ) | ( RULE_STRING ) | ( RULE_PLACEHOLDER ) | ( RULE_TAGNAME ) );
    public final void rule__Title__Alternatives_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFeature.g:440:1: ( ( RULE_WORD ) | ( RULE_NUMBER ) | ( RULE_STRING ) | ( RULE_PLACEHOLDER ) | ( RULE_TAGNAME ) )
            int alt5=5;
            switch ( input.LA(1) ) {
            case RULE_WORD:
                {
                alt5=1;
                }
                break;
            case RULE_NUMBER:
                {
                alt5=2;
                }
                break;
            case RULE_STRING:
                {
                alt5=3;
                }
                break;
            case RULE_PLACEHOLDER:
                {
                alt5=4;
                }
                break;
            case RULE_TAGNAME:
                {
                alt5=5;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 5, 0, input);

                throw nvae;
            }

            switch (alt5) {
                case 1 :
                    // InternalFeature.g:441:2: ( RULE_WORD )
                    {
                    // InternalFeature.g:441:2: ( RULE_WORD )
                    // InternalFeature.g:442:3: RULE_WORD
                    {
                     before(grammarAccess.getTitleAccess().getWORDTerminalRuleCall_1_0()); 
                    match(input,RULE_WORD,FOLLOW_2); 
                     after(grammarAccess.getTitleAccess().getWORDTerminalRuleCall_1_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalFeature.g:447:2: ( RULE_NUMBER )
                    {
                    // InternalFeature.g:447:2: ( RULE_NUMBER )
                    // InternalFeature.g:448:3: RULE_NUMBER
                    {
                     before(grammarAccess.getTitleAccess().getNUMBERTerminalRuleCall_1_1()); 
                    match(input,RULE_NUMBER,FOLLOW_2); 
                     after(grammarAccess.getTitleAccess().getNUMBERTerminalRuleCall_1_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalFeature.g:453:2: ( RULE_STRING )
                    {
                    // InternalFeature.g:453:2: ( RULE_STRING )
                    // InternalFeature.g:454:3: RULE_STRING
                    {
                     before(grammarAccess.getTitleAccess().getSTRINGTerminalRuleCall_1_2()); 
                    match(input,RULE_STRING,FOLLOW_2); 
                     after(grammarAccess.getTitleAccess().getSTRINGTerminalRuleCall_1_2()); 

                    }


                    }
                    break;
                case 4 :
                    // InternalFeature.g:459:2: ( RULE_PLACEHOLDER )
                    {
                    // InternalFeature.g:459:2: ( RULE_PLACEHOLDER )
                    // InternalFeature.g:460:3: RULE_PLACEHOLDER
                    {
                     before(grammarAccess.getTitleAccess().getPLACEHOLDERTerminalRuleCall_1_3()); 
                    match(input,RULE_PLACEHOLDER,FOLLOW_2); 
                     after(grammarAccess.getTitleAccess().getPLACEHOLDERTerminalRuleCall_1_3()); 

                    }


                    }
                    break;
                case 5 :
                    // InternalFeature.g:465:2: ( RULE_TAGNAME )
                    {
                    // InternalFeature.g:465:2: ( RULE_TAGNAME )
                    // InternalFeature.g:466:3: RULE_TAGNAME
                    {
                     before(grammarAccess.getTitleAccess().getTAGNAMETerminalRuleCall_1_4()); 
                    match(input,RULE_TAGNAME,FOLLOW_2); 
                     after(grammarAccess.getTitleAccess().getTAGNAMETerminalRuleCall_1_4()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Title__Alternatives_1"


    // $ANTLR start "rule__Narrative__Alternatives_0"
    // InternalFeature.g:475:1: rule__Narrative__Alternatives_0 : ( ( RULE_WORD ) | ( RULE_NUMBER ) | ( RULE_STRING ) | ( RULE_PLACEHOLDER ) );
    public final void rule__Narrative__Alternatives_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFeature.g:479:1: ( ( RULE_WORD ) | ( RULE_NUMBER ) | ( RULE_STRING ) | ( RULE_PLACEHOLDER ) )
            int alt6=4;
            switch ( input.LA(1) ) {
            case RULE_WORD:
                {
                alt6=1;
                }
                break;
            case RULE_NUMBER:
                {
                alt6=2;
                }
                break;
            case RULE_STRING:
                {
                alt6=3;
                }
                break;
            case RULE_PLACEHOLDER:
                {
                alt6=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 6, 0, input);

                throw nvae;
            }

            switch (alt6) {
                case 1 :
                    // InternalFeature.g:480:2: ( RULE_WORD )
                    {
                    // InternalFeature.g:480:2: ( RULE_WORD )
                    // InternalFeature.g:481:3: RULE_WORD
                    {
                     before(grammarAccess.getNarrativeAccess().getWORDTerminalRuleCall_0_0()); 
                    match(input,RULE_WORD,FOLLOW_2); 
                     after(grammarAccess.getNarrativeAccess().getWORDTerminalRuleCall_0_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalFeature.g:486:2: ( RULE_NUMBER )
                    {
                    // InternalFeature.g:486:2: ( RULE_NUMBER )
                    // InternalFeature.g:487:3: RULE_NUMBER
                    {
                     before(grammarAccess.getNarrativeAccess().getNUMBERTerminalRuleCall_0_1()); 
                    match(input,RULE_NUMBER,FOLLOW_2); 
                     after(grammarAccess.getNarrativeAccess().getNUMBERTerminalRuleCall_0_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalFeature.g:492:2: ( RULE_STRING )
                    {
                    // InternalFeature.g:492:2: ( RULE_STRING )
                    // InternalFeature.g:493:3: RULE_STRING
                    {
                     before(grammarAccess.getNarrativeAccess().getSTRINGTerminalRuleCall_0_2()); 
                    match(input,RULE_STRING,FOLLOW_2); 
                     after(grammarAccess.getNarrativeAccess().getSTRINGTerminalRuleCall_0_2()); 

                    }


                    }
                    break;
                case 4 :
                    // InternalFeature.g:498:2: ( RULE_PLACEHOLDER )
                    {
                    // InternalFeature.g:498:2: ( RULE_PLACEHOLDER )
                    // InternalFeature.g:499:3: RULE_PLACEHOLDER
                    {
                     before(grammarAccess.getNarrativeAccess().getPLACEHOLDERTerminalRuleCall_0_3()); 
                    match(input,RULE_PLACEHOLDER,FOLLOW_2); 
                     after(grammarAccess.getNarrativeAccess().getPLACEHOLDERTerminalRuleCall_0_3()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Narrative__Alternatives_0"


    // $ANTLR start "rule__Narrative__Alternatives_1"
    // InternalFeature.g:508:1: rule__Narrative__Alternatives_1 : ( ( RULE_WORD ) | ( RULE_NUMBER ) | ( RULE_STRING ) | ( RULE_PLACEHOLDER ) | ( RULE_TAGNAME ) );
    public final void rule__Narrative__Alternatives_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFeature.g:512:1: ( ( RULE_WORD ) | ( RULE_NUMBER ) | ( RULE_STRING ) | ( RULE_PLACEHOLDER ) | ( RULE_TAGNAME ) )
            int alt7=5;
            switch ( input.LA(1) ) {
            case RULE_WORD:
                {
                alt7=1;
                }
                break;
            case RULE_NUMBER:
                {
                alt7=2;
                }
                break;
            case RULE_STRING:
                {
                alt7=3;
                }
                break;
            case RULE_PLACEHOLDER:
                {
                alt7=4;
                }
                break;
            case RULE_TAGNAME:
                {
                alt7=5;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 7, 0, input);

                throw nvae;
            }

            switch (alt7) {
                case 1 :
                    // InternalFeature.g:513:2: ( RULE_WORD )
                    {
                    // InternalFeature.g:513:2: ( RULE_WORD )
                    // InternalFeature.g:514:3: RULE_WORD
                    {
                     before(grammarAccess.getNarrativeAccess().getWORDTerminalRuleCall_1_0()); 
                    match(input,RULE_WORD,FOLLOW_2); 
                     after(grammarAccess.getNarrativeAccess().getWORDTerminalRuleCall_1_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalFeature.g:519:2: ( RULE_NUMBER )
                    {
                    // InternalFeature.g:519:2: ( RULE_NUMBER )
                    // InternalFeature.g:520:3: RULE_NUMBER
                    {
                     before(grammarAccess.getNarrativeAccess().getNUMBERTerminalRuleCall_1_1()); 
                    match(input,RULE_NUMBER,FOLLOW_2); 
                     after(grammarAccess.getNarrativeAccess().getNUMBERTerminalRuleCall_1_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalFeature.g:525:2: ( RULE_STRING )
                    {
                    // InternalFeature.g:525:2: ( RULE_STRING )
                    // InternalFeature.g:526:3: RULE_STRING
                    {
                     before(grammarAccess.getNarrativeAccess().getSTRINGTerminalRuleCall_1_2()); 
                    match(input,RULE_STRING,FOLLOW_2); 
                     after(grammarAccess.getNarrativeAccess().getSTRINGTerminalRuleCall_1_2()); 

                    }


                    }
                    break;
                case 4 :
                    // InternalFeature.g:531:2: ( RULE_PLACEHOLDER )
                    {
                    // InternalFeature.g:531:2: ( RULE_PLACEHOLDER )
                    // InternalFeature.g:532:3: RULE_PLACEHOLDER
                    {
                     before(grammarAccess.getNarrativeAccess().getPLACEHOLDERTerminalRuleCall_1_3()); 
                    match(input,RULE_PLACEHOLDER,FOLLOW_2); 
                     after(grammarAccess.getNarrativeAccess().getPLACEHOLDERTerminalRuleCall_1_3()); 

                    }


                    }
                    break;
                case 5 :
                    // InternalFeature.g:537:2: ( RULE_TAGNAME )
                    {
                    // InternalFeature.g:537:2: ( RULE_TAGNAME )
                    // InternalFeature.g:538:3: RULE_TAGNAME
                    {
                     before(grammarAccess.getNarrativeAccess().getTAGNAMETerminalRuleCall_1_4()); 
                    match(input,RULE_TAGNAME,FOLLOW_2); 
                     after(grammarAccess.getNarrativeAccess().getTAGNAMETerminalRuleCall_1_4()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Narrative__Alternatives_1"


    // $ANTLR start "rule__StepDescription__Alternatives"
    // InternalFeature.g:547:1: rule__StepDescription__Alternatives : ( ( RULE_WORD ) | ( RULE_NUMBER ) | ( RULE_STRING ) | ( RULE_PLACEHOLDER ) | ( RULE_TAGNAME ) );
    public final void rule__StepDescription__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFeature.g:551:1: ( ( RULE_WORD ) | ( RULE_NUMBER ) | ( RULE_STRING ) | ( RULE_PLACEHOLDER ) | ( RULE_TAGNAME ) )
            int alt8=5;
            switch ( input.LA(1) ) {
            case RULE_WORD:
                {
                alt8=1;
                }
                break;
            case RULE_NUMBER:
                {
                alt8=2;
                }
                break;
            case RULE_STRING:
                {
                alt8=3;
                }
                break;
            case RULE_PLACEHOLDER:
                {
                alt8=4;
                }
                break;
            case RULE_TAGNAME:
                {
                alt8=5;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 8, 0, input);

                throw nvae;
            }

            switch (alt8) {
                case 1 :
                    // InternalFeature.g:552:2: ( RULE_WORD )
                    {
                    // InternalFeature.g:552:2: ( RULE_WORD )
                    // InternalFeature.g:553:3: RULE_WORD
                    {
                     before(grammarAccess.getStepDescriptionAccess().getWORDTerminalRuleCall_0()); 
                    match(input,RULE_WORD,FOLLOW_2); 
                     after(grammarAccess.getStepDescriptionAccess().getWORDTerminalRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalFeature.g:558:2: ( RULE_NUMBER )
                    {
                    // InternalFeature.g:558:2: ( RULE_NUMBER )
                    // InternalFeature.g:559:3: RULE_NUMBER
                    {
                     before(grammarAccess.getStepDescriptionAccess().getNUMBERTerminalRuleCall_1()); 
                    match(input,RULE_NUMBER,FOLLOW_2); 
                     after(grammarAccess.getStepDescriptionAccess().getNUMBERTerminalRuleCall_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalFeature.g:564:2: ( RULE_STRING )
                    {
                    // InternalFeature.g:564:2: ( RULE_STRING )
                    // InternalFeature.g:565:3: RULE_STRING
                    {
                     before(grammarAccess.getStepDescriptionAccess().getSTRINGTerminalRuleCall_2()); 
                    match(input,RULE_STRING,FOLLOW_2); 
                     after(grammarAccess.getStepDescriptionAccess().getSTRINGTerminalRuleCall_2()); 

                    }


                    }
                    break;
                case 4 :
                    // InternalFeature.g:570:2: ( RULE_PLACEHOLDER )
                    {
                    // InternalFeature.g:570:2: ( RULE_PLACEHOLDER )
                    // InternalFeature.g:571:3: RULE_PLACEHOLDER
                    {
                     before(grammarAccess.getStepDescriptionAccess().getPLACEHOLDERTerminalRuleCall_3()); 
                    match(input,RULE_PLACEHOLDER,FOLLOW_2); 
                     after(grammarAccess.getStepDescriptionAccess().getPLACEHOLDERTerminalRuleCall_3()); 

                    }


                    }
                    break;
                case 5 :
                    // InternalFeature.g:576:2: ( RULE_TAGNAME )
                    {
                    // InternalFeature.g:576:2: ( RULE_TAGNAME )
                    // InternalFeature.g:577:3: RULE_TAGNAME
                    {
                     before(grammarAccess.getStepDescriptionAccess().getTAGNAMETerminalRuleCall_4()); 
                    match(input,RULE_TAGNAME,FOLLOW_2); 
                     after(grammarAccess.getStepDescriptionAccess().getTAGNAMETerminalRuleCall_4()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StepDescription__Alternatives"


    // $ANTLR start "rule__StepKeyword__Alternatives"
    // InternalFeature.g:586:1: rule__StepKeyword__Alternatives : ( ( ( 'Given' ) ) | ( ( 'When' ) ) | ( ( 'Then' ) ) | ( ( 'And' ) ) | ( ( 'But' ) ) );
    public final void rule__StepKeyword__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFeature.g:590:1: ( ( ( 'Given' ) ) | ( ( 'When' ) ) | ( ( 'Then' ) ) | ( ( 'And' ) ) | ( ( 'But' ) ) )
            int alt9=5;
            switch ( input.LA(1) ) {
            case 19:
                {
                alt9=1;
                }
                break;
            case 20:
                {
                alt9=2;
                }
                break;
            case 21:
                {
                alt9=3;
                }
                break;
            case 22:
                {
                alt9=4;
                }
                break;
            case 23:
                {
                alt9=5;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 9, 0, input);

                throw nvae;
            }

            switch (alt9) {
                case 1 :
                    // InternalFeature.g:591:2: ( ( 'Given' ) )
                    {
                    // InternalFeature.g:591:2: ( ( 'Given' ) )
                    // InternalFeature.g:592:3: ( 'Given' )
                    {
                     before(grammarAccess.getStepKeywordAccess().getGIVENEnumLiteralDeclaration_0()); 
                    // InternalFeature.g:593:3: ( 'Given' )
                    // InternalFeature.g:593:4: 'Given'
                    {
                    match(input,19,FOLLOW_2); 

                    }

                     after(grammarAccess.getStepKeywordAccess().getGIVENEnumLiteralDeclaration_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalFeature.g:597:2: ( ( 'When' ) )
                    {
                    // InternalFeature.g:597:2: ( ( 'When' ) )
                    // InternalFeature.g:598:3: ( 'When' )
                    {
                     before(grammarAccess.getStepKeywordAccess().getWHENEnumLiteralDeclaration_1()); 
                    // InternalFeature.g:599:3: ( 'When' )
                    // InternalFeature.g:599:4: 'When'
                    {
                    match(input,20,FOLLOW_2); 

                    }

                     after(grammarAccess.getStepKeywordAccess().getWHENEnumLiteralDeclaration_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalFeature.g:603:2: ( ( 'Then' ) )
                    {
                    // InternalFeature.g:603:2: ( ( 'Then' ) )
                    // InternalFeature.g:604:3: ( 'Then' )
                    {
                     before(grammarAccess.getStepKeywordAccess().getTHENEnumLiteralDeclaration_2()); 
                    // InternalFeature.g:605:3: ( 'Then' )
                    // InternalFeature.g:605:4: 'Then'
                    {
                    match(input,21,FOLLOW_2); 

                    }

                     after(grammarAccess.getStepKeywordAccess().getTHENEnumLiteralDeclaration_2()); 

                    }


                    }
                    break;
                case 4 :
                    // InternalFeature.g:609:2: ( ( 'And' ) )
                    {
                    // InternalFeature.g:609:2: ( ( 'And' ) )
                    // InternalFeature.g:610:3: ( 'And' )
                    {
                     before(grammarAccess.getStepKeywordAccess().getANDEnumLiteralDeclaration_3()); 
                    // InternalFeature.g:611:3: ( 'And' )
                    // InternalFeature.g:611:4: 'And'
                    {
                    match(input,22,FOLLOW_2); 

                    }

                     after(grammarAccess.getStepKeywordAccess().getANDEnumLiteralDeclaration_3()); 

                    }


                    }
                    break;
                case 5 :
                    // InternalFeature.g:615:2: ( ( 'But' ) )
                    {
                    // InternalFeature.g:615:2: ( ( 'But' ) )
                    // InternalFeature.g:616:3: ( 'But' )
                    {
                     before(grammarAccess.getStepKeywordAccess().getBUTEnumLiteralDeclaration_4()); 
                    // InternalFeature.g:617:3: ( 'But' )
                    // InternalFeature.g:617:4: 'But'
                    {
                    match(input,23,FOLLOW_2); 

                    }

                     after(grammarAccess.getStepKeywordAccess().getBUTEnumLiteralDeclaration_4()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StepKeyword__Alternatives"


    // $ANTLR start "rule__Feature__Group__0"
    // InternalFeature.g:625:1: rule__Feature__Group__0 : rule__Feature__Group__0__Impl rule__Feature__Group__1 ;
    public final void rule__Feature__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFeature.g:629:1: ( rule__Feature__Group__0__Impl rule__Feature__Group__1 )
            // InternalFeature.g:630:2: rule__Feature__Group__0__Impl rule__Feature__Group__1
            {
            pushFollow(FOLLOW_5);
            rule__Feature__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Feature__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Feature__Group__0"


    // $ANTLR start "rule__Feature__Group__0__Impl"
    // InternalFeature.g:637:1: rule__Feature__Group__0__Impl : ( ( rule__Feature__TagsAssignment_0 )* ) ;
    public final void rule__Feature__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFeature.g:641:1: ( ( ( rule__Feature__TagsAssignment_0 )* ) )
            // InternalFeature.g:642:1: ( ( rule__Feature__TagsAssignment_0 )* )
            {
            // InternalFeature.g:642:1: ( ( rule__Feature__TagsAssignment_0 )* )
            // InternalFeature.g:643:2: ( rule__Feature__TagsAssignment_0 )*
            {
             before(grammarAccess.getFeatureAccess().getTagsAssignment_0()); 
            // InternalFeature.g:644:2: ( rule__Feature__TagsAssignment_0 )*
            loop10:
            do {
                int alt10=2;
                int LA10_0 = input.LA(1);

                if ( (LA10_0==RULE_TAGNAME) ) {
                    alt10=1;
                }


                switch (alt10) {
            	case 1 :
            	    // InternalFeature.g:644:3: rule__Feature__TagsAssignment_0
            	    {
            	    pushFollow(FOLLOW_6);
            	    rule__Feature__TagsAssignment_0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop10;
                }
            } while (true);

             after(grammarAccess.getFeatureAccess().getTagsAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Feature__Group__0__Impl"


    // $ANTLR start "rule__Feature__Group__1"
    // InternalFeature.g:652:1: rule__Feature__Group__1 : rule__Feature__Group__1__Impl rule__Feature__Group__2 ;
    public final void rule__Feature__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFeature.g:656:1: ( rule__Feature__Group__1__Impl rule__Feature__Group__2 )
            // InternalFeature.g:657:2: rule__Feature__Group__1__Impl rule__Feature__Group__2
            {
            pushFollow(FOLLOW_7);
            rule__Feature__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Feature__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Feature__Group__1"


    // $ANTLR start "rule__Feature__Group__1__Impl"
    // InternalFeature.g:664:1: rule__Feature__Group__1__Impl : ( 'Feature:' ) ;
    public final void rule__Feature__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFeature.g:668:1: ( ( 'Feature:' ) )
            // InternalFeature.g:669:1: ( 'Feature:' )
            {
            // InternalFeature.g:669:1: ( 'Feature:' )
            // InternalFeature.g:670:2: 'Feature:'
            {
             before(grammarAccess.getFeatureAccess().getFeatureKeyword_1()); 
            match(input,24,FOLLOW_2); 
             after(grammarAccess.getFeatureAccess().getFeatureKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Feature__Group__1__Impl"


    // $ANTLR start "rule__Feature__Group__2"
    // InternalFeature.g:679:1: rule__Feature__Group__2 : rule__Feature__Group__2__Impl rule__Feature__Group__3 ;
    public final void rule__Feature__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFeature.g:683:1: ( rule__Feature__Group__2__Impl rule__Feature__Group__3 )
            // InternalFeature.g:684:2: rule__Feature__Group__2__Impl rule__Feature__Group__3
            {
            pushFollow(FOLLOW_8);
            rule__Feature__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Feature__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Feature__Group__2"


    // $ANTLR start "rule__Feature__Group__2__Impl"
    // InternalFeature.g:691:1: rule__Feature__Group__2__Impl : ( ( rule__Feature__TitleAssignment_2 ) ) ;
    public final void rule__Feature__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFeature.g:695:1: ( ( ( rule__Feature__TitleAssignment_2 ) ) )
            // InternalFeature.g:696:1: ( ( rule__Feature__TitleAssignment_2 ) )
            {
            // InternalFeature.g:696:1: ( ( rule__Feature__TitleAssignment_2 ) )
            // InternalFeature.g:697:2: ( rule__Feature__TitleAssignment_2 )
            {
             before(grammarAccess.getFeatureAccess().getTitleAssignment_2()); 
            // InternalFeature.g:698:2: ( rule__Feature__TitleAssignment_2 )
            // InternalFeature.g:698:3: rule__Feature__TitleAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__Feature__TitleAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getFeatureAccess().getTitleAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Feature__Group__2__Impl"


    // $ANTLR start "rule__Feature__Group__3"
    // InternalFeature.g:706:1: rule__Feature__Group__3 : rule__Feature__Group__3__Impl rule__Feature__Group__4 ;
    public final void rule__Feature__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFeature.g:710:1: ( rule__Feature__Group__3__Impl rule__Feature__Group__4 )
            // InternalFeature.g:711:2: rule__Feature__Group__3__Impl rule__Feature__Group__4
            {
            pushFollow(FOLLOW_9);
            rule__Feature__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Feature__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Feature__Group__3"


    // $ANTLR start "rule__Feature__Group__3__Impl"
    // InternalFeature.g:718:1: rule__Feature__Group__3__Impl : ( ( ( RULE_EOL ) ) ( ( RULE_EOL )* ) ) ;
    public final void rule__Feature__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFeature.g:722:1: ( ( ( ( RULE_EOL ) ) ( ( RULE_EOL )* ) ) )
            // InternalFeature.g:723:1: ( ( ( RULE_EOL ) ) ( ( RULE_EOL )* ) )
            {
            // InternalFeature.g:723:1: ( ( ( RULE_EOL ) ) ( ( RULE_EOL )* ) )
            // InternalFeature.g:724:2: ( ( RULE_EOL ) ) ( ( RULE_EOL )* )
            {
            // InternalFeature.g:724:2: ( ( RULE_EOL ) )
            // InternalFeature.g:725:3: ( RULE_EOL )
            {
             before(grammarAccess.getFeatureAccess().getEOLTerminalRuleCall_3()); 
            // InternalFeature.g:726:3: ( RULE_EOL )
            // InternalFeature.g:726:4: RULE_EOL
            {
            match(input,RULE_EOL,FOLLOW_10); 

            }

             after(grammarAccess.getFeatureAccess().getEOLTerminalRuleCall_3()); 

            }

            // InternalFeature.g:729:2: ( ( RULE_EOL )* )
            // InternalFeature.g:730:3: ( RULE_EOL )*
            {
             before(grammarAccess.getFeatureAccess().getEOLTerminalRuleCall_3()); 
            // InternalFeature.g:731:3: ( RULE_EOL )*
            loop11:
            do {
                int alt11=2;
                int LA11_0 = input.LA(1);

                if ( (LA11_0==RULE_EOL) ) {
                    alt11=1;
                }


                switch (alt11) {
            	case 1 :
            	    // InternalFeature.g:731:4: RULE_EOL
            	    {
            	    match(input,RULE_EOL,FOLLOW_10); 

            	    }
            	    break;

            	default :
            	    break loop11;
                }
            } while (true);

             after(grammarAccess.getFeatureAccess().getEOLTerminalRuleCall_3()); 

            }


            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Feature__Group__3__Impl"


    // $ANTLR start "rule__Feature__Group__4"
    // InternalFeature.g:740:1: rule__Feature__Group__4 : rule__Feature__Group__4__Impl rule__Feature__Group__5 ;
    public final void rule__Feature__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFeature.g:744:1: ( rule__Feature__Group__4__Impl rule__Feature__Group__5 )
            // InternalFeature.g:745:2: rule__Feature__Group__4__Impl rule__Feature__Group__5
            {
            pushFollow(FOLLOW_9);
            rule__Feature__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Feature__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Feature__Group__4"


    // $ANTLR start "rule__Feature__Group__4__Impl"
    // InternalFeature.g:752:1: rule__Feature__Group__4__Impl : ( ( rule__Feature__NarrativeAssignment_4 )? ) ;
    public final void rule__Feature__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFeature.g:756:1: ( ( ( rule__Feature__NarrativeAssignment_4 )? ) )
            // InternalFeature.g:757:1: ( ( rule__Feature__NarrativeAssignment_4 )? )
            {
            // InternalFeature.g:757:1: ( ( rule__Feature__NarrativeAssignment_4 )? )
            // InternalFeature.g:758:2: ( rule__Feature__NarrativeAssignment_4 )?
            {
             before(grammarAccess.getFeatureAccess().getNarrativeAssignment_4()); 
            // InternalFeature.g:759:2: ( rule__Feature__NarrativeAssignment_4 )?
            int alt12=2;
            int LA12_0 = input.LA(1);

            if ( ((LA12_0>=RULE_WORD && LA12_0<=RULE_PLACEHOLDER)) ) {
                alt12=1;
            }
            switch (alt12) {
                case 1 :
                    // InternalFeature.g:759:3: rule__Feature__NarrativeAssignment_4
                    {
                    pushFollow(FOLLOW_2);
                    rule__Feature__NarrativeAssignment_4();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getFeatureAccess().getNarrativeAssignment_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Feature__Group__4__Impl"


    // $ANTLR start "rule__Feature__Group__5"
    // InternalFeature.g:767:1: rule__Feature__Group__5 : rule__Feature__Group__5__Impl rule__Feature__Group__6 ;
    public final void rule__Feature__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFeature.g:771:1: ( rule__Feature__Group__5__Impl rule__Feature__Group__6 )
            // InternalFeature.g:772:2: rule__Feature__Group__5__Impl rule__Feature__Group__6
            {
            pushFollow(FOLLOW_9);
            rule__Feature__Group__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Feature__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Feature__Group__5"


    // $ANTLR start "rule__Feature__Group__5__Impl"
    // InternalFeature.g:779:1: rule__Feature__Group__5__Impl : ( ( rule__Feature__BackgroundAssignment_5 )? ) ;
    public final void rule__Feature__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFeature.g:783:1: ( ( ( rule__Feature__BackgroundAssignment_5 )? ) )
            // InternalFeature.g:784:1: ( ( rule__Feature__BackgroundAssignment_5 )? )
            {
            // InternalFeature.g:784:1: ( ( rule__Feature__BackgroundAssignment_5 )? )
            // InternalFeature.g:785:2: ( rule__Feature__BackgroundAssignment_5 )?
            {
             before(grammarAccess.getFeatureAccess().getBackgroundAssignment_5()); 
            // InternalFeature.g:786:2: ( rule__Feature__BackgroundAssignment_5 )?
            int alt13=2;
            int LA13_0 = input.LA(1);

            if ( (LA13_0==25) ) {
                alt13=1;
            }
            switch (alt13) {
                case 1 :
                    // InternalFeature.g:786:3: rule__Feature__BackgroundAssignment_5
                    {
                    pushFollow(FOLLOW_2);
                    rule__Feature__BackgroundAssignment_5();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getFeatureAccess().getBackgroundAssignment_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Feature__Group__5__Impl"


    // $ANTLR start "rule__Feature__Group__6"
    // InternalFeature.g:794:1: rule__Feature__Group__6 : rule__Feature__Group__6__Impl ;
    public final void rule__Feature__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFeature.g:798:1: ( rule__Feature__Group__6__Impl )
            // InternalFeature.g:799:2: rule__Feature__Group__6__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Feature__Group__6__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Feature__Group__6"


    // $ANTLR start "rule__Feature__Group__6__Impl"
    // InternalFeature.g:805:1: rule__Feature__Group__6__Impl : ( ( ( rule__Feature__ScenariosAssignment_6 ) ) ( ( rule__Feature__ScenariosAssignment_6 )* ) ) ;
    public final void rule__Feature__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFeature.g:809:1: ( ( ( ( rule__Feature__ScenariosAssignment_6 ) ) ( ( rule__Feature__ScenariosAssignment_6 )* ) ) )
            // InternalFeature.g:810:1: ( ( ( rule__Feature__ScenariosAssignment_6 ) ) ( ( rule__Feature__ScenariosAssignment_6 )* ) )
            {
            // InternalFeature.g:810:1: ( ( ( rule__Feature__ScenariosAssignment_6 ) ) ( ( rule__Feature__ScenariosAssignment_6 )* ) )
            // InternalFeature.g:811:2: ( ( rule__Feature__ScenariosAssignment_6 ) ) ( ( rule__Feature__ScenariosAssignment_6 )* )
            {
            // InternalFeature.g:811:2: ( ( rule__Feature__ScenariosAssignment_6 ) )
            // InternalFeature.g:812:3: ( rule__Feature__ScenariosAssignment_6 )
            {
             before(grammarAccess.getFeatureAccess().getScenariosAssignment_6()); 
            // InternalFeature.g:813:3: ( rule__Feature__ScenariosAssignment_6 )
            // InternalFeature.g:813:4: rule__Feature__ScenariosAssignment_6
            {
            pushFollow(FOLLOW_11);
            rule__Feature__ScenariosAssignment_6();

            state._fsp--;


            }

             after(grammarAccess.getFeatureAccess().getScenariosAssignment_6()); 

            }

            // InternalFeature.g:816:2: ( ( rule__Feature__ScenariosAssignment_6 )* )
            // InternalFeature.g:817:3: ( rule__Feature__ScenariosAssignment_6 )*
            {
             before(grammarAccess.getFeatureAccess().getScenariosAssignment_6()); 
            // InternalFeature.g:818:3: ( rule__Feature__ScenariosAssignment_6 )*
            loop14:
            do {
                int alt14=2;
                int LA14_0 = input.LA(1);

                if ( (LA14_0==RULE_TAGNAME||(LA14_0>=26 && LA14_0<=27)) ) {
                    alt14=1;
                }


                switch (alt14) {
            	case 1 :
            	    // InternalFeature.g:818:4: rule__Feature__ScenariosAssignment_6
            	    {
            	    pushFollow(FOLLOW_11);
            	    rule__Feature__ScenariosAssignment_6();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop14;
                }
            } while (true);

             after(grammarAccess.getFeatureAccess().getScenariosAssignment_6()); 

            }


            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Feature__Group__6__Impl"


    // $ANTLR start "rule__Background__Group__0"
    // InternalFeature.g:828:1: rule__Background__Group__0 : rule__Background__Group__0__Impl rule__Background__Group__1 ;
    public final void rule__Background__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFeature.g:832:1: ( rule__Background__Group__0__Impl rule__Background__Group__1 )
            // InternalFeature.g:833:2: rule__Background__Group__0__Impl rule__Background__Group__1
            {
            pushFollow(FOLLOW_12);
            rule__Background__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Background__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Background__Group__0"


    // $ANTLR start "rule__Background__Group__0__Impl"
    // InternalFeature.g:840:1: rule__Background__Group__0__Impl : ( 'Background:' ) ;
    public final void rule__Background__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFeature.g:844:1: ( ( 'Background:' ) )
            // InternalFeature.g:845:1: ( 'Background:' )
            {
            // InternalFeature.g:845:1: ( 'Background:' )
            // InternalFeature.g:846:2: 'Background:'
            {
             before(grammarAccess.getBackgroundAccess().getBackgroundKeyword_0()); 
            match(input,25,FOLLOW_2); 
             after(grammarAccess.getBackgroundAccess().getBackgroundKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Background__Group__0__Impl"


    // $ANTLR start "rule__Background__Group__1"
    // InternalFeature.g:855:1: rule__Background__Group__1 : rule__Background__Group__1__Impl rule__Background__Group__2 ;
    public final void rule__Background__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFeature.g:859:1: ( rule__Background__Group__1__Impl rule__Background__Group__2 )
            // InternalFeature.g:860:2: rule__Background__Group__1__Impl rule__Background__Group__2
            {
            pushFollow(FOLLOW_12);
            rule__Background__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Background__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Background__Group__1"


    // $ANTLR start "rule__Background__Group__1__Impl"
    // InternalFeature.g:867:1: rule__Background__Group__1__Impl : ( ( rule__Background__TitleAssignment_1 )? ) ;
    public final void rule__Background__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFeature.g:871:1: ( ( ( rule__Background__TitleAssignment_1 )? ) )
            // InternalFeature.g:872:1: ( ( rule__Background__TitleAssignment_1 )? )
            {
            // InternalFeature.g:872:1: ( ( rule__Background__TitleAssignment_1 )? )
            // InternalFeature.g:873:2: ( rule__Background__TitleAssignment_1 )?
            {
             before(grammarAccess.getBackgroundAccess().getTitleAssignment_1()); 
            // InternalFeature.g:874:2: ( rule__Background__TitleAssignment_1 )?
            int alt15=2;
            int LA15_0 = input.LA(1);

            if ( ((LA15_0>=RULE_WORD && LA15_0<=RULE_PLACEHOLDER)) ) {
                alt15=1;
            }
            switch (alt15) {
                case 1 :
                    // InternalFeature.g:874:3: rule__Background__TitleAssignment_1
                    {
                    pushFollow(FOLLOW_2);
                    rule__Background__TitleAssignment_1();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getBackgroundAccess().getTitleAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Background__Group__1__Impl"


    // $ANTLR start "rule__Background__Group__2"
    // InternalFeature.g:882:1: rule__Background__Group__2 : rule__Background__Group__2__Impl rule__Background__Group__3 ;
    public final void rule__Background__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFeature.g:886:1: ( rule__Background__Group__2__Impl rule__Background__Group__3 )
            // InternalFeature.g:887:2: rule__Background__Group__2__Impl rule__Background__Group__3
            {
            pushFollow(FOLLOW_13);
            rule__Background__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Background__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Background__Group__2"


    // $ANTLR start "rule__Background__Group__2__Impl"
    // InternalFeature.g:894:1: rule__Background__Group__2__Impl : ( ( ( RULE_EOL ) ) ( ( RULE_EOL )* ) ) ;
    public final void rule__Background__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFeature.g:898:1: ( ( ( ( RULE_EOL ) ) ( ( RULE_EOL )* ) ) )
            // InternalFeature.g:899:1: ( ( ( RULE_EOL ) ) ( ( RULE_EOL )* ) )
            {
            // InternalFeature.g:899:1: ( ( ( RULE_EOL ) ) ( ( RULE_EOL )* ) )
            // InternalFeature.g:900:2: ( ( RULE_EOL ) ) ( ( RULE_EOL )* )
            {
            // InternalFeature.g:900:2: ( ( RULE_EOL ) )
            // InternalFeature.g:901:3: ( RULE_EOL )
            {
             before(grammarAccess.getBackgroundAccess().getEOLTerminalRuleCall_2()); 
            // InternalFeature.g:902:3: ( RULE_EOL )
            // InternalFeature.g:902:4: RULE_EOL
            {
            match(input,RULE_EOL,FOLLOW_10); 

            }

             after(grammarAccess.getBackgroundAccess().getEOLTerminalRuleCall_2()); 

            }

            // InternalFeature.g:905:2: ( ( RULE_EOL )* )
            // InternalFeature.g:906:3: ( RULE_EOL )*
            {
             before(grammarAccess.getBackgroundAccess().getEOLTerminalRuleCall_2()); 
            // InternalFeature.g:907:3: ( RULE_EOL )*
            loop16:
            do {
                int alt16=2;
                int LA16_0 = input.LA(1);

                if ( (LA16_0==RULE_EOL) ) {
                    alt16=1;
                }


                switch (alt16) {
            	case 1 :
            	    // InternalFeature.g:907:4: RULE_EOL
            	    {
            	    match(input,RULE_EOL,FOLLOW_10); 

            	    }
            	    break;

            	default :
            	    break loop16;
                }
            } while (true);

             after(grammarAccess.getBackgroundAccess().getEOLTerminalRuleCall_2()); 

            }


            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Background__Group__2__Impl"


    // $ANTLR start "rule__Background__Group__3"
    // InternalFeature.g:916:1: rule__Background__Group__3 : rule__Background__Group__3__Impl rule__Background__Group__4 ;
    public final void rule__Background__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFeature.g:920:1: ( rule__Background__Group__3__Impl rule__Background__Group__4 )
            // InternalFeature.g:921:2: rule__Background__Group__3__Impl rule__Background__Group__4
            {
            pushFollow(FOLLOW_13);
            rule__Background__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Background__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Background__Group__3"


    // $ANTLR start "rule__Background__Group__3__Impl"
    // InternalFeature.g:928:1: rule__Background__Group__3__Impl : ( ( rule__Background__NarrativeAssignment_3 )? ) ;
    public final void rule__Background__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFeature.g:932:1: ( ( ( rule__Background__NarrativeAssignment_3 )? ) )
            // InternalFeature.g:933:1: ( ( rule__Background__NarrativeAssignment_3 )? )
            {
            // InternalFeature.g:933:1: ( ( rule__Background__NarrativeAssignment_3 )? )
            // InternalFeature.g:934:2: ( rule__Background__NarrativeAssignment_3 )?
            {
             before(grammarAccess.getBackgroundAccess().getNarrativeAssignment_3()); 
            // InternalFeature.g:935:2: ( rule__Background__NarrativeAssignment_3 )?
            int alt17=2;
            int LA17_0 = input.LA(1);

            if ( ((LA17_0>=RULE_WORD && LA17_0<=RULE_PLACEHOLDER)) ) {
                alt17=1;
            }
            switch (alt17) {
                case 1 :
                    // InternalFeature.g:935:3: rule__Background__NarrativeAssignment_3
                    {
                    pushFollow(FOLLOW_2);
                    rule__Background__NarrativeAssignment_3();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getBackgroundAccess().getNarrativeAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Background__Group__3__Impl"


    // $ANTLR start "rule__Background__Group__4"
    // InternalFeature.g:943:1: rule__Background__Group__4 : rule__Background__Group__4__Impl ;
    public final void rule__Background__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFeature.g:947:1: ( rule__Background__Group__4__Impl )
            // InternalFeature.g:948:2: rule__Background__Group__4__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Background__Group__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Background__Group__4"


    // $ANTLR start "rule__Background__Group__4__Impl"
    // InternalFeature.g:954:1: rule__Background__Group__4__Impl : ( ( ( rule__Background__StepsAssignment_4 ) ) ( ( rule__Background__StepsAssignment_4 )* ) ) ;
    public final void rule__Background__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFeature.g:958:1: ( ( ( ( rule__Background__StepsAssignment_4 ) ) ( ( rule__Background__StepsAssignment_4 )* ) ) )
            // InternalFeature.g:959:1: ( ( ( rule__Background__StepsAssignment_4 ) ) ( ( rule__Background__StepsAssignment_4 )* ) )
            {
            // InternalFeature.g:959:1: ( ( ( rule__Background__StepsAssignment_4 ) ) ( ( rule__Background__StepsAssignment_4 )* ) )
            // InternalFeature.g:960:2: ( ( rule__Background__StepsAssignment_4 ) ) ( ( rule__Background__StepsAssignment_4 )* )
            {
            // InternalFeature.g:960:2: ( ( rule__Background__StepsAssignment_4 ) )
            // InternalFeature.g:961:3: ( rule__Background__StepsAssignment_4 )
            {
             before(grammarAccess.getBackgroundAccess().getStepsAssignment_4()); 
            // InternalFeature.g:962:3: ( rule__Background__StepsAssignment_4 )
            // InternalFeature.g:962:4: rule__Background__StepsAssignment_4
            {
            pushFollow(FOLLOW_14);
            rule__Background__StepsAssignment_4();

            state._fsp--;


            }

             after(grammarAccess.getBackgroundAccess().getStepsAssignment_4()); 

            }

            // InternalFeature.g:965:2: ( ( rule__Background__StepsAssignment_4 )* )
            // InternalFeature.g:966:3: ( rule__Background__StepsAssignment_4 )*
            {
             before(grammarAccess.getBackgroundAccess().getStepsAssignment_4()); 
            // InternalFeature.g:967:3: ( rule__Background__StepsAssignment_4 )*
            loop18:
            do {
                int alt18=2;
                int LA18_0 = input.LA(1);

                if ( ((LA18_0>=19 && LA18_0<=23)) ) {
                    alt18=1;
                }


                switch (alt18) {
            	case 1 :
            	    // InternalFeature.g:967:4: rule__Background__StepsAssignment_4
            	    {
            	    pushFollow(FOLLOW_14);
            	    rule__Background__StepsAssignment_4();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop18;
                }
            } while (true);

             after(grammarAccess.getBackgroundAccess().getStepsAssignment_4()); 

            }


            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Background__Group__4__Impl"


    // $ANTLR start "rule__Scenario__Group__0"
    // InternalFeature.g:977:1: rule__Scenario__Group__0 : rule__Scenario__Group__0__Impl rule__Scenario__Group__1 ;
    public final void rule__Scenario__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFeature.g:981:1: ( rule__Scenario__Group__0__Impl rule__Scenario__Group__1 )
            // InternalFeature.g:982:2: rule__Scenario__Group__0__Impl rule__Scenario__Group__1
            {
            pushFollow(FOLLOW_15);
            rule__Scenario__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Scenario__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Scenario__Group__0"


    // $ANTLR start "rule__Scenario__Group__0__Impl"
    // InternalFeature.g:989:1: rule__Scenario__Group__0__Impl : ( ( rule__Scenario__TagsAssignment_0 )* ) ;
    public final void rule__Scenario__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFeature.g:993:1: ( ( ( rule__Scenario__TagsAssignment_0 )* ) )
            // InternalFeature.g:994:1: ( ( rule__Scenario__TagsAssignment_0 )* )
            {
            // InternalFeature.g:994:1: ( ( rule__Scenario__TagsAssignment_0 )* )
            // InternalFeature.g:995:2: ( rule__Scenario__TagsAssignment_0 )*
            {
             before(grammarAccess.getScenarioAccess().getTagsAssignment_0()); 
            // InternalFeature.g:996:2: ( rule__Scenario__TagsAssignment_0 )*
            loop19:
            do {
                int alt19=2;
                int LA19_0 = input.LA(1);

                if ( (LA19_0==RULE_TAGNAME) ) {
                    alt19=1;
                }


                switch (alt19) {
            	case 1 :
            	    // InternalFeature.g:996:3: rule__Scenario__TagsAssignment_0
            	    {
            	    pushFollow(FOLLOW_6);
            	    rule__Scenario__TagsAssignment_0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop19;
                }
            } while (true);

             after(grammarAccess.getScenarioAccess().getTagsAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Scenario__Group__0__Impl"


    // $ANTLR start "rule__Scenario__Group__1"
    // InternalFeature.g:1004:1: rule__Scenario__Group__1 : rule__Scenario__Group__1__Impl rule__Scenario__Group__2 ;
    public final void rule__Scenario__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFeature.g:1008:1: ( rule__Scenario__Group__1__Impl rule__Scenario__Group__2 )
            // InternalFeature.g:1009:2: rule__Scenario__Group__1__Impl rule__Scenario__Group__2
            {
            pushFollow(FOLLOW_7);
            rule__Scenario__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Scenario__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Scenario__Group__1"


    // $ANTLR start "rule__Scenario__Group__1__Impl"
    // InternalFeature.g:1016:1: rule__Scenario__Group__1__Impl : ( 'Scenario:' ) ;
    public final void rule__Scenario__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFeature.g:1020:1: ( ( 'Scenario:' ) )
            // InternalFeature.g:1021:1: ( 'Scenario:' )
            {
            // InternalFeature.g:1021:1: ( 'Scenario:' )
            // InternalFeature.g:1022:2: 'Scenario:'
            {
             before(grammarAccess.getScenarioAccess().getScenarioKeyword_1()); 
            match(input,26,FOLLOW_2); 
             after(grammarAccess.getScenarioAccess().getScenarioKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Scenario__Group__1__Impl"


    // $ANTLR start "rule__Scenario__Group__2"
    // InternalFeature.g:1031:1: rule__Scenario__Group__2 : rule__Scenario__Group__2__Impl rule__Scenario__Group__3 ;
    public final void rule__Scenario__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFeature.g:1035:1: ( rule__Scenario__Group__2__Impl rule__Scenario__Group__3 )
            // InternalFeature.g:1036:2: rule__Scenario__Group__2__Impl rule__Scenario__Group__3
            {
            pushFollow(FOLLOW_8);
            rule__Scenario__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Scenario__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Scenario__Group__2"


    // $ANTLR start "rule__Scenario__Group__2__Impl"
    // InternalFeature.g:1043:1: rule__Scenario__Group__2__Impl : ( ( rule__Scenario__TitleAssignment_2 ) ) ;
    public final void rule__Scenario__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFeature.g:1047:1: ( ( ( rule__Scenario__TitleAssignment_2 ) ) )
            // InternalFeature.g:1048:1: ( ( rule__Scenario__TitleAssignment_2 ) )
            {
            // InternalFeature.g:1048:1: ( ( rule__Scenario__TitleAssignment_2 ) )
            // InternalFeature.g:1049:2: ( rule__Scenario__TitleAssignment_2 )
            {
             before(grammarAccess.getScenarioAccess().getTitleAssignment_2()); 
            // InternalFeature.g:1050:2: ( rule__Scenario__TitleAssignment_2 )
            // InternalFeature.g:1050:3: rule__Scenario__TitleAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__Scenario__TitleAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getScenarioAccess().getTitleAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Scenario__Group__2__Impl"


    // $ANTLR start "rule__Scenario__Group__3"
    // InternalFeature.g:1058:1: rule__Scenario__Group__3 : rule__Scenario__Group__3__Impl rule__Scenario__Group__4 ;
    public final void rule__Scenario__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFeature.g:1062:1: ( rule__Scenario__Group__3__Impl rule__Scenario__Group__4 )
            // InternalFeature.g:1063:2: rule__Scenario__Group__3__Impl rule__Scenario__Group__4
            {
            pushFollow(FOLLOW_13);
            rule__Scenario__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Scenario__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Scenario__Group__3"


    // $ANTLR start "rule__Scenario__Group__3__Impl"
    // InternalFeature.g:1070:1: rule__Scenario__Group__3__Impl : ( ( ( RULE_EOL ) ) ( ( RULE_EOL )* ) ) ;
    public final void rule__Scenario__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFeature.g:1074:1: ( ( ( ( RULE_EOL ) ) ( ( RULE_EOL )* ) ) )
            // InternalFeature.g:1075:1: ( ( ( RULE_EOL ) ) ( ( RULE_EOL )* ) )
            {
            // InternalFeature.g:1075:1: ( ( ( RULE_EOL ) ) ( ( RULE_EOL )* ) )
            // InternalFeature.g:1076:2: ( ( RULE_EOL ) ) ( ( RULE_EOL )* )
            {
            // InternalFeature.g:1076:2: ( ( RULE_EOL ) )
            // InternalFeature.g:1077:3: ( RULE_EOL )
            {
             before(grammarAccess.getScenarioAccess().getEOLTerminalRuleCall_3()); 
            // InternalFeature.g:1078:3: ( RULE_EOL )
            // InternalFeature.g:1078:4: RULE_EOL
            {
            match(input,RULE_EOL,FOLLOW_10); 

            }

             after(grammarAccess.getScenarioAccess().getEOLTerminalRuleCall_3()); 

            }

            // InternalFeature.g:1081:2: ( ( RULE_EOL )* )
            // InternalFeature.g:1082:3: ( RULE_EOL )*
            {
             before(grammarAccess.getScenarioAccess().getEOLTerminalRuleCall_3()); 
            // InternalFeature.g:1083:3: ( RULE_EOL )*
            loop20:
            do {
                int alt20=2;
                int LA20_0 = input.LA(1);

                if ( (LA20_0==RULE_EOL) ) {
                    alt20=1;
                }


                switch (alt20) {
            	case 1 :
            	    // InternalFeature.g:1083:4: RULE_EOL
            	    {
            	    match(input,RULE_EOL,FOLLOW_10); 

            	    }
            	    break;

            	default :
            	    break loop20;
                }
            } while (true);

             after(grammarAccess.getScenarioAccess().getEOLTerminalRuleCall_3()); 

            }


            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Scenario__Group__3__Impl"


    // $ANTLR start "rule__Scenario__Group__4"
    // InternalFeature.g:1092:1: rule__Scenario__Group__4 : rule__Scenario__Group__4__Impl rule__Scenario__Group__5 ;
    public final void rule__Scenario__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFeature.g:1096:1: ( rule__Scenario__Group__4__Impl rule__Scenario__Group__5 )
            // InternalFeature.g:1097:2: rule__Scenario__Group__4__Impl rule__Scenario__Group__5
            {
            pushFollow(FOLLOW_13);
            rule__Scenario__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Scenario__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Scenario__Group__4"


    // $ANTLR start "rule__Scenario__Group__4__Impl"
    // InternalFeature.g:1104:1: rule__Scenario__Group__4__Impl : ( ( rule__Scenario__NarrativeAssignment_4 )? ) ;
    public final void rule__Scenario__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFeature.g:1108:1: ( ( ( rule__Scenario__NarrativeAssignment_4 )? ) )
            // InternalFeature.g:1109:1: ( ( rule__Scenario__NarrativeAssignment_4 )? )
            {
            // InternalFeature.g:1109:1: ( ( rule__Scenario__NarrativeAssignment_4 )? )
            // InternalFeature.g:1110:2: ( rule__Scenario__NarrativeAssignment_4 )?
            {
             before(grammarAccess.getScenarioAccess().getNarrativeAssignment_4()); 
            // InternalFeature.g:1111:2: ( rule__Scenario__NarrativeAssignment_4 )?
            int alt21=2;
            int LA21_0 = input.LA(1);

            if ( ((LA21_0>=RULE_WORD && LA21_0<=RULE_PLACEHOLDER)) ) {
                alt21=1;
            }
            switch (alt21) {
                case 1 :
                    // InternalFeature.g:1111:3: rule__Scenario__NarrativeAssignment_4
                    {
                    pushFollow(FOLLOW_2);
                    rule__Scenario__NarrativeAssignment_4();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getScenarioAccess().getNarrativeAssignment_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Scenario__Group__4__Impl"


    // $ANTLR start "rule__Scenario__Group__5"
    // InternalFeature.g:1119:1: rule__Scenario__Group__5 : rule__Scenario__Group__5__Impl ;
    public final void rule__Scenario__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFeature.g:1123:1: ( rule__Scenario__Group__5__Impl )
            // InternalFeature.g:1124:2: rule__Scenario__Group__5__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Scenario__Group__5__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Scenario__Group__5"


    // $ANTLR start "rule__Scenario__Group__5__Impl"
    // InternalFeature.g:1130:1: rule__Scenario__Group__5__Impl : ( ( ( rule__Scenario__StepsAssignment_5 ) ) ( ( rule__Scenario__StepsAssignment_5 )* ) ) ;
    public final void rule__Scenario__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFeature.g:1134:1: ( ( ( ( rule__Scenario__StepsAssignment_5 ) ) ( ( rule__Scenario__StepsAssignment_5 )* ) ) )
            // InternalFeature.g:1135:1: ( ( ( rule__Scenario__StepsAssignment_5 ) ) ( ( rule__Scenario__StepsAssignment_5 )* ) )
            {
            // InternalFeature.g:1135:1: ( ( ( rule__Scenario__StepsAssignment_5 ) ) ( ( rule__Scenario__StepsAssignment_5 )* ) )
            // InternalFeature.g:1136:2: ( ( rule__Scenario__StepsAssignment_5 ) ) ( ( rule__Scenario__StepsAssignment_5 )* )
            {
            // InternalFeature.g:1136:2: ( ( rule__Scenario__StepsAssignment_5 ) )
            // InternalFeature.g:1137:3: ( rule__Scenario__StepsAssignment_5 )
            {
             before(grammarAccess.getScenarioAccess().getStepsAssignment_5()); 
            // InternalFeature.g:1138:3: ( rule__Scenario__StepsAssignment_5 )
            // InternalFeature.g:1138:4: rule__Scenario__StepsAssignment_5
            {
            pushFollow(FOLLOW_14);
            rule__Scenario__StepsAssignment_5();

            state._fsp--;


            }

             after(grammarAccess.getScenarioAccess().getStepsAssignment_5()); 

            }

            // InternalFeature.g:1141:2: ( ( rule__Scenario__StepsAssignment_5 )* )
            // InternalFeature.g:1142:3: ( rule__Scenario__StepsAssignment_5 )*
            {
             before(grammarAccess.getScenarioAccess().getStepsAssignment_5()); 
            // InternalFeature.g:1143:3: ( rule__Scenario__StepsAssignment_5 )*
            loop22:
            do {
                int alt22=2;
                int LA22_0 = input.LA(1);

                if ( ((LA22_0>=19 && LA22_0<=23)) ) {
                    alt22=1;
                }


                switch (alt22) {
            	case 1 :
            	    // InternalFeature.g:1143:4: rule__Scenario__StepsAssignment_5
            	    {
            	    pushFollow(FOLLOW_14);
            	    rule__Scenario__StepsAssignment_5();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop22;
                }
            } while (true);

             after(grammarAccess.getScenarioAccess().getStepsAssignment_5()); 

            }


            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Scenario__Group__5__Impl"


    // $ANTLR start "rule__ScenarioOutline__Group__0"
    // InternalFeature.g:1153:1: rule__ScenarioOutline__Group__0 : rule__ScenarioOutline__Group__0__Impl rule__ScenarioOutline__Group__1 ;
    public final void rule__ScenarioOutline__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFeature.g:1157:1: ( rule__ScenarioOutline__Group__0__Impl rule__ScenarioOutline__Group__1 )
            // InternalFeature.g:1158:2: rule__ScenarioOutline__Group__0__Impl rule__ScenarioOutline__Group__1
            {
            pushFollow(FOLLOW_9);
            rule__ScenarioOutline__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ScenarioOutline__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ScenarioOutline__Group__0"


    // $ANTLR start "rule__ScenarioOutline__Group__0__Impl"
    // InternalFeature.g:1165:1: rule__ScenarioOutline__Group__0__Impl : ( ( rule__ScenarioOutline__TagsAssignment_0 )* ) ;
    public final void rule__ScenarioOutline__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFeature.g:1169:1: ( ( ( rule__ScenarioOutline__TagsAssignment_0 )* ) )
            // InternalFeature.g:1170:1: ( ( rule__ScenarioOutline__TagsAssignment_0 )* )
            {
            // InternalFeature.g:1170:1: ( ( rule__ScenarioOutline__TagsAssignment_0 )* )
            // InternalFeature.g:1171:2: ( rule__ScenarioOutline__TagsAssignment_0 )*
            {
             before(grammarAccess.getScenarioOutlineAccess().getTagsAssignment_0()); 
            // InternalFeature.g:1172:2: ( rule__ScenarioOutline__TagsAssignment_0 )*
            loop23:
            do {
                int alt23=2;
                int LA23_0 = input.LA(1);

                if ( (LA23_0==RULE_TAGNAME) ) {
                    alt23=1;
                }


                switch (alt23) {
            	case 1 :
            	    // InternalFeature.g:1172:3: rule__ScenarioOutline__TagsAssignment_0
            	    {
            	    pushFollow(FOLLOW_6);
            	    rule__ScenarioOutline__TagsAssignment_0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop23;
                }
            } while (true);

             after(grammarAccess.getScenarioOutlineAccess().getTagsAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ScenarioOutline__Group__0__Impl"


    // $ANTLR start "rule__ScenarioOutline__Group__1"
    // InternalFeature.g:1180:1: rule__ScenarioOutline__Group__1 : rule__ScenarioOutline__Group__1__Impl rule__ScenarioOutline__Group__2 ;
    public final void rule__ScenarioOutline__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFeature.g:1184:1: ( rule__ScenarioOutline__Group__1__Impl rule__ScenarioOutline__Group__2 )
            // InternalFeature.g:1185:2: rule__ScenarioOutline__Group__1__Impl rule__ScenarioOutline__Group__2
            {
            pushFollow(FOLLOW_7);
            rule__ScenarioOutline__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ScenarioOutline__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ScenarioOutline__Group__1"


    // $ANTLR start "rule__ScenarioOutline__Group__1__Impl"
    // InternalFeature.g:1192:1: rule__ScenarioOutline__Group__1__Impl : ( 'Scenario Outline:' ) ;
    public final void rule__ScenarioOutline__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFeature.g:1196:1: ( ( 'Scenario Outline:' ) )
            // InternalFeature.g:1197:1: ( 'Scenario Outline:' )
            {
            // InternalFeature.g:1197:1: ( 'Scenario Outline:' )
            // InternalFeature.g:1198:2: 'Scenario Outline:'
            {
             before(grammarAccess.getScenarioOutlineAccess().getScenarioOutlineKeyword_1()); 
            match(input,27,FOLLOW_2); 
             after(grammarAccess.getScenarioOutlineAccess().getScenarioOutlineKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ScenarioOutline__Group__1__Impl"


    // $ANTLR start "rule__ScenarioOutline__Group__2"
    // InternalFeature.g:1207:1: rule__ScenarioOutline__Group__2 : rule__ScenarioOutline__Group__2__Impl rule__ScenarioOutline__Group__3 ;
    public final void rule__ScenarioOutline__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFeature.g:1211:1: ( rule__ScenarioOutline__Group__2__Impl rule__ScenarioOutline__Group__3 )
            // InternalFeature.g:1212:2: rule__ScenarioOutline__Group__2__Impl rule__ScenarioOutline__Group__3
            {
            pushFollow(FOLLOW_8);
            rule__ScenarioOutline__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ScenarioOutline__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ScenarioOutline__Group__2"


    // $ANTLR start "rule__ScenarioOutline__Group__2__Impl"
    // InternalFeature.g:1219:1: rule__ScenarioOutline__Group__2__Impl : ( ( rule__ScenarioOutline__TitleAssignment_2 ) ) ;
    public final void rule__ScenarioOutline__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFeature.g:1223:1: ( ( ( rule__ScenarioOutline__TitleAssignment_2 ) ) )
            // InternalFeature.g:1224:1: ( ( rule__ScenarioOutline__TitleAssignment_2 ) )
            {
            // InternalFeature.g:1224:1: ( ( rule__ScenarioOutline__TitleAssignment_2 ) )
            // InternalFeature.g:1225:2: ( rule__ScenarioOutline__TitleAssignment_2 )
            {
             before(grammarAccess.getScenarioOutlineAccess().getTitleAssignment_2()); 
            // InternalFeature.g:1226:2: ( rule__ScenarioOutline__TitleAssignment_2 )
            // InternalFeature.g:1226:3: rule__ScenarioOutline__TitleAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__ScenarioOutline__TitleAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getScenarioOutlineAccess().getTitleAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ScenarioOutline__Group__2__Impl"


    // $ANTLR start "rule__ScenarioOutline__Group__3"
    // InternalFeature.g:1234:1: rule__ScenarioOutline__Group__3 : rule__ScenarioOutline__Group__3__Impl rule__ScenarioOutline__Group__4 ;
    public final void rule__ScenarioOutline__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFeature.g:1238:1: ( rule__ScenarioOutline__Group__3__Impl rule__ScenarioOutline__Group__4 )
            // InternalFeature.g:1239:2: rule__ScenarioOutline__Group__3__Impl rule__ScenarioOutline__Group__4
            {
            pushFollow(FOLLOW_13);
            rule__ScenarioOutline__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ScenarioOutline__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ScenarioOutline__Group__3"


    // $ANTLR start "rule__ScenarioOutline__Group__3__Impl"
    // InternalFeature.g:1246:1: rule__ScenarioOutline__Group__3__Impl : ( ( ( RULE_EOL ) ) ( ( RULE_EOL )* ) ) ;
    public final void rule__ScenarioOutline__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFeature.g:1250:1: ( ( ( ( RULE_EOL ) ) ( ( RULE_EOL )* ) ) )
            // InternalFeature.g:1251:1: ( ( ( RULE_EOL ) ) ( ( RULE_EOL )* ) )
            {
            // InternalFeature.g:1251:1: ( ( ( RULE_EOL ) ) ( ( RULE_EOL )* ) )
            // InternalFeature.g:1252:2: ( ( RULE_EOL ) ) ( ( RULE_EOL )* )
            {
            // InternalFeature.g:1252:2: ( ( RULE_EOL ) )
            // InternalFeature.g:1253:3: ( RULE_EOL )
            {
             before(grammarAccess.getScenarioOutlineAccess().getEOLTerminalRuleCall_3()); 
            // InternalFeature.g:1254:3: ( RULE_EOL )
            // InternalFeature.g:1254:4: RULE_EOL
            {
            match(input,RULE_EOL,FOLLOW_10); 

            }

             after(grammarAccess.getScenarioOutlineAccess().getEOLTerminalRuleCall_3()); 

            }

            // InternalFeature.g:1257:2: ( ( RULE_EOL )* )
            // InternalFeature.g:1258:3: ( RULE_EOL )*
            {
             before(grammarAccess.getScenarioOutlineAccess().getEOLTerminalRuleCall_3()); 
            // InternalFeature.g:1259:3: ( RULE_EOL )*
            loop24:
            do {
                int alt24=2;
                int LA24_0 = input.LA(1);

                if ( (LA24_0==RULE_EOL) ) {
                    alt24=1;
                }


                switch (alt24) {
            	case 1 :
            	    // InternalFeature.g:1259:4: RULE_EOL
            	    {
            	    match(input,RULE_EOL,FOLLOW_10); 

            	    }
            	    break;

            	default :
            	    break loop24;
                }
            } while (true);

             after(grammarAccess.getScenarioOutlineAccess().getEOLTerminalRuleCall_3()); 

            }


            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ScenarioOutline__Group__3__Impl"


    // $ANTLR start "rule__ScenarioOutline__Group__4"
    // InternalFeature.g:1268:1: rule__ScenarioOutline__Group__4 : rule__ScenarioOutline__Group__4__Impl rule__ScenarioOutline__Group__5 ;
    public final void rule__ScenarioOutline__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFeature.g:1272:1: ( rule__ScenarioOutline__Group__4__Impl rule__ScenarioOutline__Group__5 )
            // InternalFeature.g:1273:2: rule__ScenarioOutline__Group__4__Impl rule__ScenarioOutline__Group__5
            {
            pushFollow(FOLLOW_13);
            rule__ScenarioOutline__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ScenarioOutline__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ScenarioOutline__Group__4"


    // $ANTLR start "rule__ScenarioOutline__Group__4__Impl"
    // InternalFeature.g:1280:1: rule__ScenarioOutline__Group__4__Impl : ( ( rule__ScenarioOutline__NarrativeAssignment_4 )? ) ;
    public final void rule__ScenarioOutline__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFeature.g:1284:1: ( ( ( rule__ScenarioOutline__NarrativeAssignment_4 )? ) )
            // InternalFeature.g:1285:1: ( ( rule__ScenarioOutline__NarrativeAssignment_4 )? )
            {
            // InternalFeature.g:1285:1: ( ( rule__ScenarioOutline__NarrativeAssignment_4 )? )
            // InternalFeature.g:1286:2: ( rule__ScenarioOutline__NarrativeAssignment_4 )?
            {
             before(grammarAccess.getScenarioOutlineAccess().getNarrativeAssignment_4()); 
            // InternalFeature.g:1287:2: ( rule__ScenarioOutline__NarrativeAssignment_4 )?
            int alt25=2;
            int LA25_0 = input.LA(1);

            if ( ((LA25_0>=RULE_WORD && LA25_0<=RULE_PLACEHOLDER)) ) {
                alt25=1;
            }
            switch (alt25) {
                case 1 :
                    // InternalFeature.g:1287:3: rule__ScenarioOutline__NarrativeAssignment_4
                    {
                    pushFollow(FOLLOW_2);
                    rule__ScenarioOutline__NarrativeAssignment_4();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getScenarioOutlineAccess().getNarrativeAssignment_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ScenarioOutline__Group__4__Impl"


    // $ANTLR start "rule__ScenarioOutline__Group__5"
    // InternalFeature.g:1295:1: rule__ScenarioOutline__Group__5 : rule__ScenarioOutline__Group__5__Impl rule__ScenarioOutline__Group__6 ;
    public final void rule__ScenarioOutline__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFeature.g:1299:1: ( rule__ScenarioOutline__Group__5__Impl rule__ScenarioOutline__Group__6 )
            // InternalFeature.g:1300:2: rule__ScenarioOutline__Group__5__Impl rule__ScenarioOutline__Group__6
            {
            pushFollow(FOLLOW_16);
            rule__ScenarioOutline__Group__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ScenarioOutline__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ScenarioOutline__Group__5"


    // $ANTLR start "rule__ScenarioOutline__Group__5__Impl"
    // InternalFeature.g:1307:1: rule__ScenarioOutline__Group__5__Impl : ( ( ( rule__ScenarioOutline__StepsAssignment_5 ) ) ( ( rule__ScenarioOutline__StepsAssignment_5 )* ) ) ;
    public final void rule__ScenarioOutline__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFeature.g:1311:1: ( ( ( ( rule__ScenarioOutline__StepsAssignment_5 ) ) ( ( rule__ScenarioOutline__StepsAssignment_5 )* ) ) )
            // InternalFeature.g:1312:1: ( ( ( rule__ScenarioOutline__StepsAssignment_5 ) ) ( ( rule__ScenarioOutline__StepsAssignment_5 )* ) )
            {
            // InternalFeature.g:1312:1: ( ( ( rule__ScenarioOutline__StepsAssignment_5 ) ) ( ( rule__ScenarioOutline__StepsAssignment_5 )* ) )
            // InternalFeature.g:1313:2: ( ( rule__ScenarioOutline__StepsAssignment_5 ) ) ( ( rule__ScenarioOutline__StepsAssignment_5 )* )
            {
            // InternalFeature.g:1313:2: ( ( rule__ScenarioOutline__StepsAssignment_5 ) )
            // InternalFeature.g:1314:3: ( rule__ScenarioOutline__StepsAssignment_5 )
            {
             before(grammarAccess.getScenarioOutlineAccess().getStepsAssignment_5()); 
            // InternalFeature.g:1315:3: ( rule__ScenarioOutline__StepsAssignment_5 )
            // InternalFeature.g:1315:4: rule__ScenarioOutline__StepsAssignment_5
            {
            pushFollow(FOLLOW_14);
            rule__ScenarioOutline__StepsAssignment_5();

            state._fsp--;


            }

             after(grammarAccess.getScenarioOutlineAccess().getStepsAssignment_5()); 

            }

            // InternalFeature.g:1318:2: ( ( rule__ScenarioOutline__StepsAssignment_5 )* )
            // InternalFeature.g:1319:3: ( rule__ScenarioOutline__StepsAssignment_5 )*
            {
             before(grammarAccess.getScenarioOutlineAccess().getStepsAssignment_5()); 
            // InternalFeature.g:1320:3: ( rule__ScenarioOutline__StepsAssignment_5 )*
            loop26:
            do {
                int alt26=2;
                int LA26_0 = input.LA(1);

                if ( ((LA26_0>=19 && LA26_0<=23)) ) {
                    alt26=1;
                }


                switch (alt26) {
            	case 1 :
            	    // InternalFeature.g:1320:4: rule__ScenarioOutline__StepsAssignment_5
            	    {
            	    pushFollow(FOLLOW_14);
            	    rule__ScenarioOutline__StepsAssignment_5();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop26;
                }
            } while (true);

             after(grammarAccess.getScenarioOutlineAccess().getStepsAssignment_5()); 

            }


            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ScenarioOutline__Group__5__Impl"


    // $ANTLR start "rule__ScenarioOutline__Group__6"
    // InternalFeature.g:1329:1: rule__ScenarioOutline__Group__6 : rule__ScenarioOutline__Group__6__Impl ;
    public final void rule__ScenarioOutline__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFeature.g:1333:1: ( rule__ScenarioOutline__Group__6__Impl )
            // InternalFeature.g:1334:2: rule__ScenarioOutline__Group__6__Impl
            {
            pushFollow(FOLLOW_2);
            rule__ScenarioOutline__Group__6__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ScenarioOutline__Group__6"


    // $ANTLR start "rule__ScenarioOutline__Group__6__Impl"
    // InternalFeature.g:1340:1: rule__ScenarioOutline__Group__6__Impl : ( ( rule__ScenarioOutline__ExamplesAssignment_6 ) ) ;
    public final void rule__ScenarioOutline__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFeature.g:1344:1: ( ( ( rule__ScenarioOutline__ExamplesAssignment_6 ) ) )
            // InternalFeature.g:1345:1: ( ( rule__ScenarioOutline__ExamplesAssignment_6 ) )
            {
            // InternalFeature.g:1345:1: ( ( rule__ScenarioOutline__ExamplesAssignment_6 ) )
            // InternalFeature.g:1346:2: ( rule__ScenarioOutline__ExamplesAssignment_6 )
            {
             before(grammarAccess.getScenarioOutlineAccess().getExamplesAssignment_6()); 
            // InternalFeature.g:1347:2: ( rule__ScenarioOutline__ExamplesAssignment_6 )
            // InternalFeature.g:1347:3: rule__ScenarioOutline__ExamplesAssignment_6
            {
            pushFollow(FOLLOW_2);
            rule__ScenarioOutline__ExamplesAssignment_6();

            state._fsp--;


            }

             after(grammarAccess.getScenarioOutlineAccess().getExamplesAssignment_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ScenarioOutline__Group__6__Impl"


    // $ANTLR start "rule__Step__Group__0"
    // InternalFeature.g:1356:1: rule__Step__Group__0 : rule__Step__Group__0__Impl rule__Step__Group__1 ;
    public final void rule__Step__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFeature.g:1360:1: ( rule__Step__Group__0__Impl rule__Step__Group__1 )
            // InternalFeature.g:1361:2: rule__Step__Group__0__Impl rule__Step__Group__1
            {
            pushFollow(FOLLOW_17);
            rule__Step__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Step__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Step__Group__0"


    // $ANTLR start "rule__Step__Group__0__Impl"
    // InternalFeature.g:1368:1: rule__Step__Group__0__Impl : ( ( rule__Step__StepKeywordAssignment_0 ) ) ;
    public final void rule__Step__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFeature.g:1372:1: ( ( ( rule__Step__StepKeywordAssignment_0 ) ) )
            // InternalFeature.g:1373:1: ( ( rule__Step__StepKeywordAssignment_0 ) )
            {
            // InternalFeature.g:1373:1: ( ( rule__Step__StepKeywordAssignment_0 ) )
            // InternalFeature.g:1374:2: ( rule__Step__StepKeywordAssignment_0 )
            {
             before(grammarAccess.getStepAccess().getStepKeywordAssignment_0()); 
            // InternalFeature.g:1375:2: ( rule__Step__StepKeywordAssignment_0 )
            // InternalFeature.g:1375:3: rule__Step__StepKeywordAssignment_0
            {
            pushFollow(FOLLOW_2);
            rule__Step__StepKeywordAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getStepAccess().getStepKeywordAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Step__Group__0__Impl"


    // $ANTLR start "rule__Step__Group__1"
    // InternalFeature.g:1383:1: rule__Step__Group__1 : rule__Step__Group__1__Impl rule__Step__Group__2 ;
    public final void rule__Step__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFeature.g:1387:1: ( rule__Step__Group__1__Impl rule__Step__Group__2 )
            // InternalFeature.g:1388:2: rule__Step__Group__1__Impl rule__Step__Group__2
            {
            pushFollow(FOLLOW_18);
            rule__Step__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Step__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Step__Group__1"


    // $ANTLR start "rule__Step__Group__1__Impl"
    // InternalFeature.g:1395:1: rule__Step__Group__1__Impl : ( ( rule__Step__DescriptionAssignment_1 ) ) ;
    public final void rule__Step__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFeature.g:1399:1: ( ( ( rule__Step__DescriptionAssignment_1 ) ) )
            // InternalFeature.g:1400:1: ( ( rule__Step__DescriptionAssignment_1 ) )
            {
            // InternalFeature.g:1400:1: ( ( rule__Step__DescriptionAssignment_1 ) )
            // InternalFeature.g:1401:2: ( rule__Step__DescriptionAssignment_1 )
            {
             before(grammarAccess.getStepAccess().getDescriptionAssignment_1()); 
            // InternalFeature.g:1402:2: ( rule__Step__DescriptionAssignment_1 )
            // InternalFeature.g:1402:3: rule__Step__DescriptionAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__Step__DescriptionAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getStepAccess().getDescriptionAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Step__Group__1__Impl"


    // $ANTLR start "rule__Step__Group__2"
    // InternalFeature.g:1410:1: rule__Step__Group__2 : rule__Step__Group__2__Impl rule__Step__Group__3 ;
    public final void rule__Step__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFeature.g:1414:1: ( rule__Step__Group__2__Impl rule__Step__Group__3 )
            // InternalFeature.g:1415:2: rule__Step__Group__2__Impl rule__Step__Group__3
            {
            pushFollow(FOLLOW_18);
            rule__Step__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Step__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Step__Group__2"


    // $ANTLR start "rule__Step__Group__2__Impl"
    // InternalFeature.g:1422:1: rule__Step__Group__2__Impl : ( ( RULE_EOL )* ) ;
    public final void rule__Step__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFeature.g:1426:1: ( ( ( RULE_EOL )* ) )
            // InternalFeature.g:1427:1: ( ( RULE_EOL )* )
            {
            // InternalFeature.g:1427:1: ( ( RULE_EOL )* )
            // InternalFeature.g:1428:2: ( RULE_EOL )*
            {
             before(grammarAccess.getStepAccess().getEOLTerminalRuleCall_2()); 
            // InternalFeature.g:1429:2: ( RULE_EOL )*
            loop27:
            do {
                int alt27=2;
                int LA27_0 = input.LA(1);

                if ( (LA27_0==RULE_EOL) ) {
                    alt27=1;
                }


                switch (alt27) {
            	case 1 :
            	    // InternalFeature.g:1429:3: RULE_EOL
            	    {
            	    match(input,RULE_EOL,FOLLOW_10); 

            	    }
            	    break;

            	default :
            	    break loop27;
                }
            } while (true);

             after(grammarAccess.getStepAccess().getEOLTerminalRuleCall_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Step__Group__2__Impl"


    // $ANTLR start "rule__Step__Group__3"
    // InternalFeature.g:1437:1: rule__Step__Group__3 : rule__Step__Group__3__Impl rule__Step__Group__4 ;
    public final void rule__Step__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFeature.g:1441:1: ( rule__Step__Group__3__Impl rule__Step__Group__4 )
            // InternalFeature.g:1442:2: rule__Step__Group__3__Impl rule__Step__Group__4
            {
            pushFollow(FOLLOW_18);
            rule__Step__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Step__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Step__Group__3"


    // $ANTLR start "rule__Step__Group__3__Impl"
    // InternalFeature.g:1449:1: rule__Step__Group__3__Impl : ( ( rule__Step__TablesAssignment_3 )* ) ;
    public final void rule__Step__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFeature.g:1453:1: ( ( ( rule__Step__TablesAssignment_3 )* ) )
            // InternalFeature.g:1454:1: ( ( rule__Step__TablesAssignment_3 )* )
            {
            // InternalFeature.g:1454:1: ( ( rule__Step__TablesAssignment_3 )* )
            // InternalFeature.g:1455:2: ( rule__Step__TablesAssignment_3 )*
            {
             before(grammarAccess.getStepAccess().getTablesAssignment_3()); 
            // InternalFeature.g:1456:2: ( rule__Step__TablesAssignment_3 )*
            loop28:
            do {
                int alt28=2;
                int LA28_0 = input.LA(1);

                if ( (LA28_0==RULE_TABLE_ROW) ) {
                    alt28=1;
                }


                switch (alt28) {
            	case 1 :
            	    // InternalFeature.g:1456:3: rule__Step__TablesAssignment_3
            	    {
            	    pushFollow(FOLLOW_19);
            	    rule__Step__TablesAssignment_3();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop28;
                }
            } while (true);

             after(grammarAccess.getStepAccess().getTablesAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Step__Group__3__Impl"


    // $ANTLR start "rule__Step__Group__4"
    // InternalFeature.g:1464:1: rule__Step__Group__4 : rule__Step__Group__4__Impl rule__Step__Group__5 ;
    public final void rule__Step__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFeature.g:1468:1: ( rule__Step__Group__4__Impl rule__Step__Group__5 )
            // InternalFeature.g:1469:2: rule__Step__Group__4__Impl rule__Step__Group__5
            {
            pushFollow(FOLLOW_18);
            rule__Step__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Step__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Step__Group__4"


    // $ANTLR start "rule__Step__Group__4__Impl"
    // InternalFeature.g:1476:1: rule__Step__Group__4__Impl : ( ( rule__Step__CodeAssignment_4 )? ) ;
    public final void rule__Step__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFeature.g:1480:1: ( ( ( rule__Step__CodeAssignment_4 )? ) )
            // InternalFeature.g:1481:1: ( ( rule__Step__CodeAssignment_4 )? )
            {
            // InternalFeature.g:1481:1: ( ( rule__Step__CodeAssignment_4 )? )
            // InternalFeature.g:1482:2: ( rule__Step__CodeAssignment_4 )?
            {
             before(grammarAccess.getStepAccess().getCodeAssignment_4()); 
            // InternalFeature.g:1483:2: ( rule__Step__CodeAssignment_4 )?
            int alt29=2;
            int LA29_0 = input.LA(1);

            if ( (LA29_0==RULE_DOC_STRING) ) {
                alt29=1;
            }
            switch (alt29) {
                case 1 :
                    // InternalFeature.g:1483:3: rule__Step__CodeAssignment_4
                    {
                    pushFollow(FOLLOW_2);
                    rule__Step__CodeAssignment_4();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getStepAccess().getCodeAssignment_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Step__Group__4__Impl"


    // $ANTLR start "rule__Step__Group__5"
    // InternalFeature.g:1491:1: rule__Step__Group__5 : rule__Step__Group__5__Impl ;
    public final void rule__Step__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFeature.g:1495:1: ( rule__Step__Group__5__Impl )
            // InternalFeature.g:1496:2: rule__Step__Group__5__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Step__Group__5__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Step__Group__5"


    // $ANTLR start "rule__Step__Group__5__Impl"
    // InternalFeature.g:1502:1: rule__Step__Group__5__Impl : ( ( rule__Step__TablesAssignment_5 )* ) ;
    public final void rule__Step__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFeature.g:1506:1: ( ( ( rule__Step__TablesAssignment_5 )* ) )
            // InternalFeature.g:1507:1: ( ( rule__Step__TablesAssignment_5 )* )
            {
            // InternalFeature.g:1507:1: ( ( rule__Step__TablesAssignment_5 )* )
            // InternalFeature.g:1508:2: ( rule__Step__TablesAssignment_5 )*
            {
             before(grammarAccess.getStepAccess().getTablesAssignment_5()); 
            // InternalFeature.g:1509:2: ( rule__Step__TablesAssignment_5 )*
            loop30:
            do {
                int alt30=2;
                int LA30_0 = input.LA(1);

                if ( (LA30_0==RULE_TABLE_ROW) ) {
                    alt30=1;
                }


                switch (alt30) {
            	case 1 :
            	    // InternalFeature.g:1509:3: rule__Step__TablesAssignment_5
            	    {
            	    pushFollow(FOLLOW_19);
            	    rule__Step__TablesAssignment_5();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop30;
                }
            } while (true);

             after(grammarAccess.getStepAccess().getTablesAssignment_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Step__Group__5__Impl"


    // $ANTLR start "rule__Examples__Group__0"
    // InternalFeature.g:1518:1: rule__Examples__Group__0 : rule__Examples__Group__0__Impl rule__Examples__Group__1 ;
    public final void rule__Examples__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFeature.g:1522:1: ( rule__Examples__Group__0__Impl rule__Examples__Group__1 )
            // InternalFeature.g:1523:2: rule__Examples__Group__0__Impl rule__Examples__Group__1
            {
            pushFollow(FOLLOW_12);
            rule__Examples__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Examples__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Examples__Group__0"


    // $ANTLR start "rule__Examples__Group__0__Impl"
    // InternalFeature.g:1530:1: rule__Examples__Group__0__Impl : ( 'Examples:' ) ;
    public final void rule__Examples__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFeature.g:1534:1: ( ( 'Examples:' ) )
            // InternalFeature.g:1535:1: ( 'Examples:' )
            {
            // InternalFeature.g:1535:1: ( 'Examples:' )
            // InternalFeature.g:1536:2: 'Examples:'
            {
             before(grammarAccess.getExamplesAccess().getExamplesKeyword_0()); 
            match(input,28,FOLLOW_2); 
             after(grammarAccess.getExamplesAccess().getExamplesKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Examples__Group__0__Impl"


    // $ANTLR start "rule__Examples__Group__1"
    // InternalFeature.g:1545:1: rule__Examples__Group__1 : rule__Examples__Group__1__Impl rule__Examples__Group__2 ;
    public final void rule__Examples__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFeature.g:1549:1: ( rule__Examples__Group__1__Impl rule__Examples__Group__2 )
            // InternalFeature.g:1550:2: rule__Examples__Group__1__Impl rule__Examples__Group__2
            {
            pushFollow(FOLLOW_12);
            rule__Examples__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Examples__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Examples__Group__1"


    // $ANTLR start "rule__Examples__Group__1__Impl"
    // InternalFeature.g:1557:1: rule__Examples__Group__1__Impl : ( ( rule__Examples__TitleAssignment_1 )? ) ;
    public final void rule__Examples__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFeature.g:1561:1: ( ( ( rule__Examples__TitleAssignment_1 )? ) )
            // InternalFeature.g:1562:1: ( ( rule__Examples__TitleAssignment_1 )? )
            {
            // InternalFeature.g:1562:1: ( ( rule__Examples__TitleAssignment_1 )? )
            // InternalFeature.g:1563:2: ( rule__Examples__TitleAssignment_1 )?
            {
             before(grammarAccess.getExamplesAccess().getTitleAssignment_1()); 
            // InternalFeature.g:1564:2: ( rule__Examples__TitleAssignment_1 )?
            int alt31=2;
            int LA31_0 = input.LA(1);

            if ( ((LA31_0>=RULE_WORD && LA31_0<=RULE_PLACEHOLDER)) ) {
                alt31=1;
            }
            switch (alt31) {
                case 1 :
                    // InternalFeature.g:1564:3: rule__Examples__TitleAssignment_1
                    {
                    pushFollow(FOLLOW_2);
                    rule__Examples__TitleAssignment_1();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getExamplesAccess().getTitleAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Examples__Group__1__Impl"


    // $ANTLR start "rule__Examples__Group__2"
    // InternalFeature.g:1572:1: rule__Examples__Group__2 : rule__Examples__Group__2__Impl rule__Examples__Group__3 ;
    public final void rule__Examples__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFeature.g:1576:1: ( rule__Examples__Group__2__Impl rule__Examples__Group__3 )
            // InternalFeature.g:1577:2: rule__Examples__Group__2__Impl rule__Examples__Group__3
            {
            pushFollow(FOLLOW_20);
            rule__Examples__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Examples__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Examples__Group__2"


    // $ANTLR start "rule__Examples__Group__2__Impl"
    // InternalFeature.g:1584:1: rule__Examples__Group__2__Impl : ( ( ( RULE_EOL ) ) ( ( RULE_EOL )* ) ) ;
    public final void rule__Examples__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFeature.g:1588:1: ( ( ( ( RULE_EOL ) ) ( ( RULE_EOL )* ) ) )
            // InternalFeature.g:1589:1: ( ( ( RULE_EOL ) ) ( ( RULE_EOL )* ) )
            {
            // InternalFeature.g:1589:1: ( ( ( RULE_EOL ) ) ( ( RULE_EOL )* ) )
            // InternalFeature.g:1590:2: ( ( RULE_EOL ) ) ( ( RULE_EOL )* )
            {
            // InternalFeature.g:1590:2: ( ( RULE_EOL ) )
            // InternalFeature.g:1591:3: ( RULE_EOL )
            {
             before(grammarAccess.getExamplesAccess().getEOLTerminalRuleCall_2()); 
            // InternalFeature.g:1592:3: ( RULE_EOL )
            // InternalFeature.g:1592:4: RULE_EOL
            {
            match(input,RULE_EOL,FOLLOW_10); 

            }

             after(grammarAccess.getExamplesAccess().getEOLTerminalRuleCall_2()); 

            }

            // InternalFeature.g:1595:2: ( ( RULE_EOL )* )
            // InternalFeature.g:1596:3: ( RULE_EOL )*
            {
             before(grammarAccess.getExamplesAccess().getEOLTerminalRuleCall_2()); 
            // InternalFeature.g:1597:3: ( RULE_EOL )*
            loop32:
            do {
                int alt32=2;
                int LA32_0 = input.LA(1);

                if ( (LA32_0==RULE_EOL) ) {
                    alt32=1;
                }


                switch (alt32) {
            	case 1 :
            	    // InternalFeature.g:1597:4: RULE_EOL
            	    {
            	    match(input,RULE_EOL,FOLLOW_10); 

            	    }
            	    break;

            	default :
            	    break loop32;
                }
            } while (true);

             after(grammarAccess.getExamplesAccess().getEOLTerminalRuleCall_2()); 

            }


            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Examples__Group__2__Impl"


    // $ANTLR start "rule__Examples__Group__3"
    // InternalFeature.g:1606:1: rule__Examples__Group__3 : rule__Examples__Group__3__Impl rule__Examples__Group__4 ;
    public final void rule__Examples__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFeature.g:1610:1: ( rule__Examples__Group__3__Impl rule__Examples__Group__4 )
            // InternalFeature.g:1611:2: rule__Examples__Group__3__Impl rule__Examples__Group__4
            {
            pushFollow(FOLLOW_20);
            rule__Examples__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Examples__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Examples__Group__3"


    // $ANTLR start "rule__Examples__Group__3__Impl"
    // InternalFeature.g:1618:1: rule__Examples__Group__3__Impl : ( ( rule__Examples__NarrativeAssignment_3 )? ) ;
    public final void rule__Examples__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFeature.g:1622:1: ( ( ( rule__Examples__NarrativeAssignment_3 )? ) )
            // InternalFeature.g:1623:1: ( ( rule__Examples__NarrativeAssignment_3 )? )
            {
            // InternalFeature.g:1623:1: ( ( rule__Examples__NarrativeAssignment_3 )? )
            // InternalFeature.g:1624:2: ( rule__Examples__NarrativeAssignment_3 )?
            {
             before(grammarAccess.getExamplesAccess().getNarrativeAssignment_3()); 
            // InternalFeature.g:1625:2: ( rule__Examples__NarrativeAssignment_3 )?
            int alt33=2;
            int LA33_0 = input.LA(1);

            if ( ((LA33_0>=RULE_WORD && LA33_0<=RULE_PLACEHOLDER)) ) {
                alt33=1;
            }
            switch (alt33) {
                case 1 :
                    // InternalFeature.g:1625:3: rule__Examples__NarrativeAssignment_3
                    {
                    pushFollow(FOLLOW_2);
                    rule__Examples__NarrativeAssignment_3();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getExamplesAccess().getNarrativeAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Examples__Group__3__Impl"


    // $ANTLR start "rule__Examples__Group__4"
    // InternalFeature.g:1633:1: rule__Examples__Group__4 : rule__Examples__Group__4__Impl ;
    public final void rule__Examples__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFeature.g:1637:1: ( rule__Examples__Group__4__Impl )
            // InternalFeature.g:1638:2: rule__Examples__Group__4__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Examples__Group__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Examples__Group__4"


    // $ANTLR start "rule__Examples__Group__4__Impl"
    // InternalFeature.g:1644:1: rule__Examples__Group__4__Impl : ( ( rule__Examples__TableAssignment_4 ) ) ;
    public final void rule__Examples__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFeature.g:1648:1: ( ( ( rule__Examples__TableAssignment_4 ) ) )
            // InternalFeature.g:1649:1: ( ( rule__Examples__TableAssignment_4 ) )
            {
            // InternalFeature.g:1649:1: ( ( rule__Examples__TableAssignment_4 ) )
            // InternalFeature.g:1650:2: ( rule__Examples__TableAssignment_4 )
            {
             before(grammarAccess.getExamplesAccess().getTableAssignment_4()); 
            // InternalFeature.g:1651:2: ( rule__Examples__TableAssignment_4 )
            // InternalFeature.g:1651:3: rule__Examples__TableAssignment_4
            {
            pushFollow(FOLLOW_2);
            rule__Examples__TableAssignment_4();

            state._fsp--;


            }

             after(grammarAccess.getExamplesAccess().getTableAssignment_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Examples__Group__4__Impl"


    // $ANTLR start "rule__Table__Group__0"
    // InternalFeature.g:1660:1: rule__Table__Group__0 : rule__Table__Group__0__Impl rule__Table__Group__1 ;
    public final void rule__Table__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFeature.g:1664:1: ( rule__Table__Group__0__Impl rule__Table__Group__1 )
            // InternalFeature.g:1665:2: rule__Table__Group__0__Impl rule__Table__Group__1
            {
            pushFollow(FOLLOW_8);
            rule__Table__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Table__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Table__Group__0"


    // $ANTLR start "rule__Table__Group__0__Impl"
    // InternalFeature.g:1672:1: rule__Table__Group__0__Impl : ( ( ( rule__Table__RowsAssignment_0 ) ) ( ( rule__Table__RowsAssignment_0 )* ) ) ;
    public final void rule__Table__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFeature.g:1676:1: ( ( ( ( rule__Table__RowsAssignment_0 ) ) ( ( rule__Table__RowsAssignment_0 )* ) ) )
            // InternalFeature.g:1677:1: ( ( ( rule__Table__RowsAssignment_0 ) ) ( ( rule__Table__RowsAssignment_0 )* ) )
            {
            // InternalFeature.g:1677:1: ( ( ( rule__Table__RowsAssignment_0 ) ) ( ( rule__Table__RowsAssignment_0 )* ) )
            // InternalFeature.g:1678:2: ( ( rule__Table__RowsAssignment_0 ) ) ( ( rule__Table__RowsAssignment_0 )* )
            {
            // InternalFeature.g:1678:2: ( ( rule__Table__RowsAssignment_0 ) )
            // InternalFeature.g:1679:3: ( rule__Table__RowsAssignment_0 )
            {
             before(grammarAccess.getTableAccess().getRowsAssignment_0()); 
            // InternalFeature.g:1680:3: ( rule__Table__RowsAssignment_0 )
            // InternalFeature.g:1680:4: rule__Table__RowsAssignment_0
            {
            pushFollow(FOLLOW_19);
            rule__Table__RowsAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getTableAccess().getRowsAssignment_0()); 

            }

            // InternalFeature.g:1683:2: ( ( rule__Table__RowsAssignment_0 )* )
            // InternalFeature.g:1684:3: ( rule__Table__RowsAssignment_0 )*
            {
             before(grammarAccess.getTableAccess().getRowsAssignment_0()); 
            // InternalFeature.g:1685:3: ( rule__Table__RowsAssignment_0 )*
            loop34:
            do {
                int alt34=2;
                int LA34_0 = input.LA(1);

                if ( (LA34_0==RULE_TABLE_ROW) ) {
                    alt34=1;
                }


                switch (alt34) {
            	case 1 :
            	    // InternalFeature.g:1685:4: rule__Table__RowsAssignment_0
            	    {
            	    pushFollow(FOLLOW_19);
            	    rule__Table__RowsAssignment_0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop34;
                }
            } while (true);

             after(grammarAccess.getTableAccess().getRowsAssignment_0()); 

            }


            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Table__Group__0__Impl"


    // $ANTLR start "rule__Table__Group__1"
    // InternalFeature.g:1694:1: rule__Table__Group__1 : rule__Table__Group__1__Impl ;
    public final void rule__Table__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFeature.g:1698:1: ( rule__Table__Group__1__Impl )
            // InternalFeature.g:1699:2: rule__Table__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Table__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Table__Group__1"


    // $ANTLR start "rule__Table__Group__1__Impl"
    // InternalFeature.g:1705:1: rule__Table__Group__1__Impl : ( ( RULE_EOL )* ) ;
    public final void rule__Table__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFeature.g:1709:1: ( ( ( RULE_EOL )* ) )
            // InternalFeature.g:1710:1: ( ( RULE_EOL )* )
            {
            // InternalFeature.g:1710:1: ( ( RULE_EOL )* )
            // InternalFeature.g:1711:2: ( RULE_EOL )*
            {
             before(grammarAccess.getTableAccess().getEOLTerminalRuleCall_1()); 
            // InternalFeature.g:1712:2: ( RULE_EOL )*
            loop35:
            do {
                int alt35=2;
                int LA35_0 = input.LA(1);

                if ( (LA35_0==RULE_EOL) ) {
                    alt35=1;
                }


                switch (alt35) {
            	case 1 :
            	    // InternalFeature.g:1712:3: RULE_EOL
            	    {
            	    match(input,RULE_EOL,FOLLOW_10); 

            	    }
            	    break;

            	default :
            	    break loop35;
                }
            } while (true);

             after(grammarAccess.getTableAccess().getEOLTerminalRuleCall_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Table__Group__1__Impl"


    // $ANTLR start "rule__DocString__Group__0"
    // InternalFeature.g:1721:1: rule__DocString__Group__0 : rule__DocString__Group__0__Impl rule__DocString__Group__1 ;
    public final void rule__DocString__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFeature.g:1725:1: ( rule__DocString__Group__0__Impl rule__DocString__Group__1 )
            // InternalFeature.g:1726:2: rule__DocString__Group__0__Impl rule__DocString__Group__1
            {
            pushFollow(FOLLOW_8);
            rule__DocString__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__DocString__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DocString__Group__0"


    // $ANTLR start "rule__DocString__Group__0__Impl"
    // InternalFeature.g:1733:1: rule__DocString__Group__0__Impl : ( ( rule__DocString__ContentAssignment_0 ) ) ;
    public final void rule__DocString__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFeature.g:1737:1: ( ( ( rule__DocString__ContentAssignment_0 ) ) )
            // InternalFeature.g:1738:1: ( ( rule__DocString__ContentAssignment_0 ) )
            {
            // InternalFeature.g:1738:1: ( ( rule__DocString__ContentAssignment_0 ) )
            // InternalFeature.g:1739:2: ( rule__DocString__ContentAssignment_0 )
            {
             before(grammarAccess.getDocStringAccess().getContentAssignment_0()); 
            // InternalFeature.g:1740:2: ( rule__DocString__ContentAssignment_0 )
            // InternalFeature.g:1740:3: rule__DocString__ContentAssignment_0
            {
            pushFollow(FOLLOW_2);
            rule__DocString__ContentAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getDocStringAccess().getContentAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DocString__Group__0__Impl"


    // $ANTLR start "rule__DocString__Group__1"
    // InternalFeature.g:1748:1: rule__DocString__Group__1 : rule__DocString__Group__1__Impl ;
    public final void rule__DocString__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFeature.g:1752:1: ( rule__DocString__Group__1__Impl )
            // InternalFeature.g:1753:2: rule__DocString__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__DocString__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DocString__Group__1"


    // $ANTLR start "rule__DocString__Group__1__Impl"
    // InternalFeature.g:1759:1: rule__DocString__Group__1__Impl : ( ( RULE_EOL )* ) ;
    public final void rule__DocString__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFeature.g:1763:1: ( ( ( RULE_EOL )* ) )
            // InternalFeature.g:1764:1: ( ( RULE_EOL )* )
            {
            // InternalFeature.g:1764:1: ( ( RULE_EOL )* )
            // InternalFeature.g:1765:2: ( RULE_EOL )*
            {
             before(grammarAccess.getDocStringAccess().getEOLTerminalRuleCall_1()); 
            // InternalFeature.g:1766:2: ( RULE_EOL )*
            loop36:
            do {
                int alt36=2;
                int LA36_0 = input.LA(1);

                if ( (LA36_0==RULE_EOL) ) {
                    alt36=1;
                }


                switch (alt36) {
            	case 1 :
            	    // InternalFeature.g:1766:3: RULE_EOL
            	    {
            	    match(input,RULE_EOL,FOLLOW_10); 

            	    }
            	    break;

            	default :
            	    break loop36;
                }
            } while (true);

             after(grammarAccess.getDocStringAccess().getEOLTerminalRuleCall_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DocString__Group__1__Impl"


    // $ANTLR start "rule__Title__Group__0"
    // InternalFeature.g:1775:1: rule__Title__Group__0 : rule__Title__Group__0__Impl rule__Title__Group__1 ;
    public final void rule__Title__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFeature.g:1779:1: ( rule__Title__Group__0__Impl rule__Title__Group__1 )
            // InternalFeature.g:1780:2: rule__Title__Group__0__Impl rule__Title__Group__1
            {
            pushFollow(FOLLOW_17);
            rule__Title__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Title__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Title__Group__0"


    // $ANTLR start "rule__Title__Group__0__Impl"
    // InternalFeature.g:1787:1: rule__Title__Group__0__Impl : ( ( rule__Title__Alternatives_0 ) ) ;
    public final void rule__Title__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFeature.g:1791:1: ( ( ( rule__Title__Alternatives_0 ) ) )
            // InternalFeature.g:1792:1: ( ( rule__Title__Alternatives_0 ) )
            {
            // InternalFeature.g:1792:1: ( ( rule__Title__Alternatives_0 ) )
            // InternalFeature.g:1793:2: ( rule__Title__Alternatives_0 )
            {
             before(grammarAccess.getTitleAccess().getAlternatives_0()); 
            // InternalFeature.g:1794:2: ( rule__Title__Alternatives_0 )
            // InternalFeature.g:1794:3: rule__Title__Alternatives_0
            {
            pushFollow(FOLLOW_2);
            rule__Title__Alternatives_0();

            state._fsp--;


            }

             after(grammarAccess.getTitleAccess().getAlternatives_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Title__Group__0__Impl"


    // $ANTLR start "rule__Title__Group__1"
    // InternalFeature.g:1802:1: rule__Title__Group__1 : rule__Title__Group__1__Impl ;
    public final void rule__Title__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFeature.g:1806:1: ( rule__Title__Group__1__Impl )
            // InternalFeature.g:1807:2: rule__Title__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Title__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Title__Group__1"


    // $ANTLR start "rule__Title__Group__1__Impl"
    // InternalFeature.g:1813:1: rule__Title__Group__1__Impl : ( ( rule__Title__Alternatives_1 )* ) ;
    public final void rule__Title__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFeature.g:1817:1: ( ( ( rule__Title__Alternatives_1 )* ) )
            // InternalFeature.g:1818:1: ( ( rule__Title__Alternatives_1 )* )
            {
            // InternalFeature.g:1818:1: ( ( rule__Title__Alternatives_1 )* )
            // InternalFeature.g:1819:2: ( rule__Title__Alternatives_1 )*
            {
             before(grammarAccess.getTitleAccess().getAlternatives_1()); 
            // InternalFeature.g:1820:2: ( rule__Title__Alternatives_1 )*
            loop37:
            do {
                int alt37=2;
                int LA37_0 = input.LA(1);

                if ( ((LA37_0>=RULE_WORD && LA37_0<=RULE_TAGNAME)) ) {
                    alt37=1;
                }


                switch (alt37) {
            	case 1 :
            	    // InternalFeature.g:1820:3: rule__Title__Alternatives_1
            	    {
            	    pushFollow(FOLLOW_4);
            	    rule__Title__Alternatives_1();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop37;
                }
            } while (true);

             after(grammarAccess.getTitleAccess().getAlternatives_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Title__Group__1__Impl"


    // $ANTLR start "rule__Narrative__Group__0"
    // InternalFeature.g:1829:1: rule__Narrative__Group__0 : rule__Narrative__Group__0__Impl rule__Narrative__Group__1 ;
    public final void rule__Narrative__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFeature.g:1833:1: ( rule__Narrative__Group__0__Impl rule__Narrative__Group__1 )
            // InternalFeature.g:1834:2: rule__Narrative__Group__0__Impl rule__Narrative__Group__1
            {
            pushFollow(FOLLOW_21);
            rule__Narrative__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Narrative__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Narrative__Group__0"


    // $ANTLR start "rule__Narrative__Group__0__Impl"
    // InternalFeature.g:1841:1: rule__Narrative__Group__0__Impl : ( ( rule__Narrative__Alternatives_0 ) ) ;
    public final void rule__Narrative__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFeature.g:1845:1: ( ( ( rule__Narrative__Alternatives_0 ) ) )
            // InternalFeature.g:1846:1: ( ( rule__Narrative__Alternatives_0 ) )
            {
            // InternalFeature.g:1846:1: ( ( rule__Narrative__Alternatives_0 ) )
            // InternalFeature.g:1847:2: ( rule__Narrative__Alternatives_0 )
            {
             before(grammarAccess.getNarrativeAccess().getAlternatives_0()); 
            // InternalFeature.g:1848:2: ( rule__Narrative__Alternatives_0 )
            // InternalFeature.g:1848:3: rule__Narrative__Alternatives_0
            {
            pushFollow(FOLLOW_2);
            rule__Narrative__Alternatives_0();

            state._fsp--;


            }

             after(grammarAccess.getNarrativeAccess().getAlternatives_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Narrative__Group__0__Impl"


    // $ANTLR start "rule__Narrative__Group__1"
    // InternalFeature.g:1856:1: rule__Narrative__Group__1 : rule__Narrative__Group__1__Impl rule__Narrative__Group__2 ;
    public final void rule__Narrative__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFeature.g:1860:1: ( rule__Narrative__Group__1__Impl rule__Narrative__Group__2 )
            // InternalFeature.g:1861:2: rule__Narrative__Group__1__Impl rule__Narrative__Group__2
            {
            pushFollow(FOLLOW_21);
            rule__Narrative__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Narrative__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Narrative__Group__1"


    // $ANTLR start "rule__Narrative__Group__1__Impl"
    // InternalFeature.g:1868:1: rule__Narrative__Group__1__Impl : ( ( rule__Narrative__Alternatives_1 )* ) ;
    public final void rule__Narrative__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFeature.g:1872:1: ( ( ( rule__Narrative__Alternatives_1 )* ) )
            // InternalFeature.g:1873:1: ( ( rule__Narrative__Alternatives_1 )* )
            {
            // InternalFeature.g:1873:1: ( ( rule__Narrative__Alternatives_1 )* )
            // InternalFeature.g:1874:2: ( rule__Narrative__Alternatives_1 )*
            {
             before(grammarAccess.getNarrativeAccess().getAlternatives_1()); 
            // InternalFeature.g:1875:2: ( rule__Narrative__Alternatives_1 )*
            loop38:
            do {
                int alt38=2;
                int LA38_0 = input.LA(1);

                if ( ((LA38_0>=RULE_WORD && LA38_0<=RULE_TAGNAME)) ) {
                    alt38=1;
                }


                switch (alt38) {
            	case 1 :
            	    // InternalFeature.g:1875:3: rule__Narrative__Alternatives_1
            	    {
            	    pushFollow(FOLLOW_4);
            	    rule__Narrative__Alternatives_1();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop38;
                }
            } while (true);

             after(grammarAccess.getNarrativeAccess().getAlternatives_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Narrative__Group__1__Impl"


    // $ANTLR start "rule__Narrative__Group__2"
    // InternalFeature.g:1883:1: rule__Narrative__Group__2 : rule__Narrative__Group__2__Impl ;
    public final void rule__Narrative__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFeature.g:1887:1: ( rule__Narrative__Group__2__Impl )
            // InternalFeature.g:1888:2: rule__Narrative__Group__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Narrative__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Narrative__Group__2"


    // $ANTLR start "rule__Narrative__Group__2__Impl"
    // InternalFeature.g:1894:1: rule__Narrative__Group__2__Impl : ( ( ( RULE_EOL ) ) ( ( RULE_EOL )* ) ) ;
    public final void rule__Narrative__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFeature.g:1898:1: ( ( ( ( RULE_EOL ) ) ( ( RULE_EOL )* ) ) )
            // InternalFeature.g:1899:1: ( ( ( RULE_EOL ) ) ( ( RULE_EOL )* ) )
            {
            // InternalFeature.g:1899:1: ( ( ( RULE_EOL ) ) ( ( RULE_EOL )* ) )
            // InternalFeature.g:1900:2: ( ( RULE_EOL ) ) ( ( RULE_EOL )* )
            {
            // InternalFeature.g:1900:2: ( ( RULE_EOL ) )
            // InternalFeature.g:1901:3: ( RULE_EOL )
            {
             before(grammarAccess.getNarrativeAccess().getEOLTerminalRuleCall_2()); 
            // InternalFeature.g:1902:3: ( RULE_EOL )
            // InternalFeature.g:1902:4: RULE_EOL
            {
            match(input,RULE_EOL,FOLLOW_10); 

            }

             after(grammarAccess.getNarrativeAccess().getEOLTerminalRuleCall_2()); 

            }

            // InternalFeature.g:1905:2: ( ( RULE_EOL )* )
            // InternalFeature.g:1906:3: ( RULE_EOL )*
            {
             before(grammarAccess.getNarrativeAccess().getEOLTerminalRuleCall_2()); 
            // InternalFeature.g:1907:3: ( RULE_EOL )*
            loop39:
            do {
                int alt39=2;
                int LA39_0 = input.LA(1);

                if ( (LA39_0==RULE_EOL) ) {
                    alt39=1;
                }


                switch (alt39) {
            	case 1 :
            	    // InternalFeature.g:1907:4: RULE_EOL
            	    {
            	    match(input,RULE_EOL,FOLLOW_10); 

            	    }
            	    break;

            	default :
            	    break loop39;
                }
            } while (true);

             after(grammarAccess.getNarrativeAccess().getEOLTerminalRuleCall_2()); 

            }


            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Narrative__Group__2__Impl"


    // $ANTLR start "rule__Tag__Group__0"
    // InternalFeature.g:1917:1: rule__Tag__Group__0 : rule__Tag__Group__0__Impl rule__Tag__Group__1 ;
    public final void rule__Tag__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFeature.g:1921:1: ( rule__Tag__Group__0__Impl rule__Tag__Group__1 )
            // InternalFeature.g:1922:2: rule__Tag__Group__0__Impl rule__Tag__Group__1
            {
            pushFollow(FOLLOW_8);
            rule__Tag__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Tag__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Tag__Group__0"


    // $ANTLR start "rule__Tag__Group__0__Impl"
    // InternalFeature.g:1929:1: rule__Tag__Group__0__Impl : ( ( rule__Tag__IdAssignment_0 ) ) ;
    public final void rule__Tag__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFeature.g:1933:1: ( ( ( rule__Tag__IdAssignment_0 ) ) )
            // InternalFeature.g:1934:1: ( ( rule__Tag__IdAssignment_0 ) )
            {
            // InternalFeature.g:1934:1: ( ( rule__Tag__IdAssignment_0 ) )
            // InternalFeature.g:1935:2: ( rule__Tag__IdAssignment_0 )
            {
             before(grammarAccess.getTagAccess().getIdAssignment_0()); 
            // InternalFeature.g:1936:2: ( rule__Tag__IdAssignment_0 )
            // InternalFeature.g:1936:3: rule__Tag__IdAssignment_0
            {
            pushFollow(FOLLOW_2);
            rule__Tag__IdAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getTagAccess().getIdAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Tag__Group__0__Impl"


    // $ANTLR start "rule__Tag__Group__1"
    // InternalFeature.g:1944:1: rule__Tag__Group__1 : rule__Tag__Group__1__Impl ;
    public final void rule__Tag__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFeature.g:1948:1: ( rule__Tag__Group__1__Impl )
            // InternalFeature.g:1949:2: rule__Tag__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Tag__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Tag__Group__1"


    // $ANTLR start "rule__Tag__Group__1__Impl"
    // InternalFeature.g:1955:1: rule__Tag__Group__1__Impl : ( ( RULE_EOL )? ) ;
    public final void rule__Tag__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFeature.g:1959:1: ( ( ( RULE_EOL )? ) )
            // InternalFeature.g:1960:1: ( ( RULE_EOL )? )
            {
            // InternalFeature.g:1960:1: ( ( RULE_EOL )? )
            // InternalFeature.g:1961:2: ( RULE_EOL )?
            {
             before(grammarAccess.getTagAccess().getEOLTerminalRuleCall_1()); 
            // InternalFeature.g:1962:2: ( RULE_EOL )?
            int alt40=2;
            int LA40_0 = input.LA(1);

            if ( (LA40_0==RULE_EOL) ) {
                alt40=1;
            }
            switch (alt40) {
                case 1 :
                    // InternalFeature.g:1962:3: RULE_EOL
                    {
                    match(input,RULE_EOL,FOLLOW_2); 

                    }
                    break;

            }

             after(grammarAccess.getTagAccess().getEOLTerminalRuleCall_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Tag__Group__1__Impl"


    // $ANTLR start "rule__Feature__TagsAssignment_0"
    // InternalFeature.g:1971:1: rule__Feature__TagsAssignment_0 : ( ruleTag ) ;
    public final void rule__Feature__TagsAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFeature.g:1975:1: ( ( ruleTag ) )
            // InternalFeature.g:1976:2: ( ruleTag )
            {
            // InternalFeature.g:1976:2: ( ruleTag )
            // InternalFeature.g:1977:3: ruleTag
            {
             before(grammarAccess.getFeatureAccess().getTagsTagParserRuleCall_0_0()); 
            pushFollow(FOLLOW_2);
            ruleTag();

            state._fsp--;

             after(grammarAccess.getFeatureAccess().getTagsTagParserRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Feature__TagsAssignment_0"


    // $ANTLR start "rule__Feature__TitleAssignment_2"
    // InternalFeature.g:1986:1: rule__Feature__TitleAssignment_2 : ( ruleTitle ) ;
    public final void rule__Feature__TitleAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFeature.g:1990:1: ( ( ruleTitle ) )
            // InternalFeature.g:1991:2: ( ruleTitle )
            {
            // InternalFeature.g:1991:2: ( ruleTitle )
            // InternalFeature.g:1992:3: ruleTitle
            {
             before(grammarAccess.getFeatureAccess().getTitleTitleParserRuleCall_2_0()); 
            pushFollow(FOLLOW_2);
            ruleTitle();

            state._fsp--;

             after(grammarAccess.getFeatureAccess().getTitleTitleParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Feature__TitleAssignment_2"


    // $ANTLR start "rule__Feature__NarrativeAssignment_4"
    // InternalFeature.g:2001:1: rule__Feature__NarrativeAssignment_4 : ( ruleNarrative ) ;
    public final void rule__Feature__NarrativeAssignment_4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFeature.g:2005:1: ( ( ruleNarrative ) )
            // InternalFeature.g:2006:2: ( ruleNarrative )
            {
            // InternalFeature.g:2006:2: ( ruleNarrative )
            // InternalFeature.g:2007:3: ruleNarrative
            {
             before(grammarAccess.getFeatureAccess().getNarrativeNarrativeParserRuleCall_4_0()); 
            pushFollow(FOLLOW_2);
            ruleNarrative();

            state._fsp--;

             after(grammarAccess.getFeatureAccess().getNarrativeNarrativeParserRuleCall_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Feature__NarrativeAssignment_4"


    // $ANTLR start "rule__Feature__BackgroundAssignment_5"
    // InternalFeature.g:2016:1: rule__Feature__BackgroundAssignment_5 : ( ruleBackground ) ;
    public final void rule__Feature__BackgroundAssignment_5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFeature.g:2020:1: ( ( ruleBackground ) )
            // InternalFeature.g:2021:2: ( ruleBackground )
            {
            // InternalFeature.g:2021:2: ( ruleBackground )
            // InternalFeature.g:2022:3: ruleBackground
            {
             before(grammarAccess.getFeatureAccess().getBackgroundBackgroundParserRuleCall_5_0()); 
            pushFollow(FOLLOW_2);
            ruleBackground();

            state._fsp--;

             after(grammarAccess.getFeatureAccess().getBackgroundBackgroundParserRuleCall_5_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Feature__BackgroundAssignment_5"


    // $ANTLR start "rule__Feature__ScenariosAssignment_6"
    // InternalFeature.g:2031:1: rule__Feature__ScenariosAssignment_6 : ( ( rule__Feature__ScenariosAlternatives_6_0 ) ) ;
    public final void rule__Feature__ScenariosAssignment_6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFeature.g:2035:1: ( ( ( rule__Feature__ScenariosAlternatives_6_0 ) ) )
            // InternalFeature.g:2036:2: ( ( rule__Feature__ScenariosAlternatives_6_0 ) )
            {
            // InternalFeature.g:2036:2: ( ( rule__Feature__ScenariosAlternatives_6_0 ) )
            // InternalFeature.g:2037:3: ( rule__Feature__ScenariosAlternatives_6_0 )
            {
             before(grammarAccess.getFeatureAccess().getScenariosAlternatives_6_0()); 
            // InternalFeature.g:2038:3: ( rule__Feature__ScenariosAlternatives_6_0 )
            // InternalFeature.g:2038:4: rule__Feature__ScenariosAlternatives_6_0
            {
            pushFollow(FOLLOW_2);
            rule__Feature__ScenariosAlternatives_6_0();

            state._fsp--;


            }

             after(grammarAccess.getFeatureAccess().getScenariosAlternatives_6_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Feature__ScenariosAssignment_6"


    // $ANTLR start "rule__Background__TitleAssignment_1"
    // InternalFeature.g:2046:1: rule__Background__TitleAssignment_1 : ( ruleTitle ) ;
    public final void rule__Background__TitleAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFeature.g:2050:1: ( ( ruleTitle ) )
            // InternalFeature.g:2051:2: ( ruleTitle )
            {
            // InternalFeature.g:2051:2: ( ruleTitle )
            // InternalFeature.g:2052:3: ruleTitle
            {
             before(grammarAccess.getBackgroundAccess().getTitleTitleParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleTitle();

            state._fsp--;

             after(grammarAccess.getBackgroundAccess().getTitleTitleParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Background__TitleAssignment_1"


    // $ANTLR start "rule__Background__NarrativeAssignment_3"
    // InternalFeature.g:2061:1: rule__Background__NarrativeAssignment_3 : ( ruleNarrative ) ;
    public final void rule__Background__NarrativeAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFeature.g:2065:1: ( ( ruleNarrative ) )
            // InternalFeature.g:2066:2: ( ruleNarrative )
            {
            // InternalFeature.g:2066:2: ( ruleNarrative )
            // InternalFeature.g:2067:3: ruleNarrative
            {
             before(grammarAccess.getBackgroundAccess().getNarrativeNarrativeParserRuleCall_3_0()); 
            pushFollow(FOLLOW_2);
            ruleNarrative();

            state._fsp--;

             after(grammarAccess.getBackgroundAccess().getNarrativeNarrativeParserRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Background__NarrativeAssignment_3"


    // $ANTLR start "rule__Background__StepsAssignment_4"
    // InternalFeature.g:2076:1: rule__Background__StepsAssignment_4 : ( ruleStep ) ;
    public final void rule__Background__StepsAssignment_4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFeature.g:2080:1: ( ( ruleStep ) )
            // InternalFeature.g:2081:2: ( ruleStep )
            {
            // InternalFeature.g:2081:2: ( ruleStep )
            // InternalFeature.g:2082:3: ruleStep
            {
             before(grammarAccess.getBackgroundAccess().getStepsStepParserRuleCall_4_0()); 
            pushFollow(FOLLOW_2);
            ruleStep();

            state._fsp--;

             after(grammarAccess.getBackgroundAccess().getStepsStepParserRuleCall_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Background__StepsAssignment_4"


    // $ANTLR start "rule__Scenario__TagsAssignment_0"
    // InternalFeature.g:2091:1: rule__Scenario__TagsAssignment_0 : ( ruleTag ) ;
    public final void rule__Scenario__TagsAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFeature.g:2095:1: ( ( ruleTag ) )
            // InternalFeature.g:2096:2: ( ruleTag )
            {
            // InternalFeature.g:2096:2: ( ruleTag )
            // InternalFeature.g:2097:3: ruleTag
            {
             before(grammarAccess.getScenarioAccess().getTagsTagParserRuleCall_0_0()); 
            pushFollow(FOLLOW_2);
            ruleTag();

            state._fsp--;

             after(grammarAccess.getScenarioAccess().getTagsTagParserRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Scenario__TagsAssignment_0"


    // $ANTLR start "rule__Scenario__TitleAssignment_2"
    // InternalFeature.g:2106:1: rule__Scenario__TitleAssignment_2 : ( ruleTitle ) ;
    public final void rule__Scenario__TitleAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFeature.g:2110:1: ( ( ruleTitle ) )
            // InternalFeature.g:2111:2: ( ruleTitle )
            {
            // InternalFeature.g:2111:2: ( ruleTitle )
            // InternalFeature.g:2112:3: ruleTitle
            {
             before(grammarAccess.getScenarioAccess().getTitleTitleParserRuleCall_2_0()); 
            pushFollow(FOLLOW_2);
            ruleTitle();

            state._fsp--;

             after(grammarAccess.getScenarioAccess().getTitleTitleParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Scenario__TitleAssignment_2"


    // $ANTLR start "rule__Scenario__NarrativeAssignment_4"
    // InternalFeature.g:2121:1: rule__Scenario__NarrativeAssignment_4 : ( ruleNarrative ) ;
    public final void rule__Scenario__NarrativeAssignment_4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFeature.g:2125:1: ( ( ruleNarrative ) )
            // InternalFeature.g:2126:2: ( ruleNarrative )
            {
            // InternalFeature.g:2126:2: ( ruleNarrative )
            // InternalFeature.g:2127:3: ruleNarrative
            {
             before(grammarAccess.getScenarioAccess().getNarrativeNarrativeParserRuleCall_4_0()); 
            pushFollow(FOLLOW_2);
            ruleNarrative();

            state._fsp--;

             after(grammarAccess.getScenarioAccess().getNarrativeNarrativeParserRuleCall_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Scenario__NarrativeAssignment_4"


    // $ANTLR start "rule__Scenario__StepsAssignment_5"
    // InternalFeature.g:2136:1: rule__Scenario__StepsAssignment_5 : ( ruleStep ) ;
    public final void rule__Scenario__StepsAssignment_5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFeature.g:2140:1: ( ( ruleStep ) )
            // InternalFeature.g:2141:2: ( ruleStep )
            {
            // InternalFeature.g:2141:2: ( ruleStep )
            // InternalFeature.g:2142:3: ruleStep
            {
             before(grammarAccess.getScenarioAccess().getStepsStepParserRuleCall_5_0()); 
            pushFollow(FOLLOW_2);
            ruleStep();

            state._fsp--;

             after(grammarAccess.getScenarioAccess().getStepsStepParserRuleCall_5_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Scenario__StepsAssignment_5"


    // $ANTLR start "rule__ScenarioOutline__TagsAssignment_0"
    // InternalFeature.g:2151:1: rule__ScenarioOutline__TagsAssignment_0 : ( ruleTag ) ;
    public final void rule__ScenarioOutline__TagsAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFeature.g:2155:1: ( ( ruleTag ) )
            // InternalFeature.g:2156:2: ( ruleTag )
            {
            // InternalFeature.g:2156:2: ( ruleTag )
            // InternalFeature.g:2157:3: ruleTag
            {
             before(grammarAccess.getScenarioOutlineAccess().getTagsTagParserRuleCall_0_0()); 
            pushFollow(FOLLOW_2);
            ruleTag();

            state._fsp--;

             after(grammarAccess.getScenarioOutlineAccess().getTagsTagParserRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ScenarioOutline__TagsAssignment_0"


    // $ANTLR start "rule__ScenarioOutline__TitleAssignment_2"
    // InternalFeature.g:2166:1: rule__ScenarioOutline__TitleAssignment_2 : ( ruleTitle ) ;
    public final void rule__ScenarioOutline__TitleAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFeature.g:2170:1: ( ( ruleTitle ) )
            // InternalFeature.g:2171:2: ( ruleTitle )
            {
            // InternalFeature.g:2171:2: ( ruleTitle )
            // InternalFeature.g:2172:3: ruleTitle
            {
             before(grammarAccess.getScenarioOutlineAccess().getTitleTitleParserRuleCall_2_0()); 
            pushFollow(FOLLOW_2);
            ruleTitle();

            state._fsp--;

             after(grammarAccess.getScenarioOutlineAccess().getTitleTitleParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ScenarioOutline__TitleAssignment_2"


    // $ANTLR start "rule__ScenarioOutline__NarrativeAssignment_4"
    // InternalFeature.g:2181:1: rule__ScenarioOutline__NarrativeAssignment_4 : ( ruleNarrative ) ;
    public final void rule__ScenarioOutline__NarrativeAssignment_4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFeature.g:2185:1: ( ( ruleNarrative ) )
            // InternalFeature.g:2186:2: ( ruleNarrative )
            {
            // InternalFeature.g:2186:2: ( ruleNarrative )
            // InternalFeature.g:2187:3: ruleNarrative
            {
             before(grammarAccess.getScenarioOutlineAccess().getNarrativeNarrativeParserRuleCall_4_0()); 
            pushFollow(FOLLOW_2);
            ruleNarrative();

            state._fsp--;

             after(grammarAccess.getScenarioOutlineAccess().getNarrativeNarrativeParserRuleCall_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ScenarioOutline__NarrativeAssignment_4"


    // $ANTLR start "rule__ScenarioOutline__StepsAssignment_5"
    // InternalFeature.g:2196:1: rule__ScenarioOutline__StepsAssignment_5 : ( ruleStep ) ;
    public final void rule__ScenarioOutline__StepsAssignment_5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFeature.g:2200:1: ( ( ruleStep ) )
            // InternalFeature.g:2201:2: ( ruleStep )
            {
            // InternalFeature.g:2201:2: ( ruleStep )
            // InternalFeature.g:2202:3: ruleStep
            {
             before(grammarAccess.getScenarioOutlineAccess().getStepsStepParserRuleCall_5_0()); 
            pushFollow(FOLLOW_2);
            ruleStep();

            state._fsp--;

             after(grammarAccess.getScenarioOutlineAccess().getStepsStepParserRuleCall_5_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ScenarioOutline__StepsAssignment_5"


    // $ANTLR start "rule__ScenarioOutline__ExamplesAssignment_6"
    // InternalFeature.g:2211:1: rule__ScenarioOutline__ExamplesAssignment_6 : ( ruleExamples ) ;
    public final void rule__ScenarioOutline__ExamplesAssignment_6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFeature.g:2215:1: ( ( ruleExamples ) )
            // InternalFeature.g:2216:2: ( ruleExamples )
            {
            // InternalFeature.g:2216:2: ( ruleExamples )
            // InternalFeature.g:2217:3: ruleExamples
            {
             before(grammarAccess.getScenarioOutlineAccess().getExamplesExamplesParserRuleCall_6_0()); 
            pushFollow(FOLLOW_2);
            ruleExamples();

            state._fsp--;

             after(grammarAccess.getScenarioOutlineAccess().getExamplesExamplesParserRuleCall_6_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ScenarioOutline__ExamplesAssignment_6"


    // $ANTLR start "rule__Step__StepKeywordAssignment_0"
    // InternalFeature.g:2226:1: rule__Step__StepKeywordAssignment_0 : ( ruleStepKeyword ) ;
    public final void rule__Step__StepKeywordAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFeature.g:2230:1: ( ( ruleStepKeyword ) )
            // InternalFeature.g:2231:2: ( ruleStepKeyword )
            {
            // InternalFeature.g:2231:2: ( ruleStepKeyword )
            // InternalFeature.g:2232:3: ruleStepKeyword
            {
             before(grammarAccess.getStepAccess().getStepKeywordStepKeywordEnumRuleCall_0_0()); 
            pushFollow(FOLLOW_2);
            ruleStepKeyword();

            state._fsp--;

             after(grammarAccess.getStepAccess().getStepKeywordStepKeywordEnumRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Step__StepKeywordAssignment_0"


    // $ANTLR start "rule__Step__DescriptionAssignment_1"
    // InternalFeature.g:2241:1: rule__Step__DescriptionAssignment_1 : ( ruleStepDescription ) ;
    public final void rule__Step__DescriptionAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFeature.g:2245:1: ( ( ruleStepDescription ) )
            // InternalFeature.g:2246:2: ( ruleStepDescription )
            {
            // InternalFeature.g:2246:2: ( ruleStepDescription )
            // InternalFeature.g:2247:3: ruleStepDescription
            {
             before(grammarAccess.getStepAccess().getDescriptionStepDescriptionParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleStepDescription();

            state._fsp--;

             after(grammarAccess.getStepAccess().getDescriptionStepDescriptionParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Step__DescriptionAssignment_1"


    // $ANTLR start "rule__Step__TablesAssignment_3"
    // InternalFeature.g:2256:1: rule__Step__TablesAssignment_3 : ( ruleTable ) ;
    public final void rule__Step__TablesAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFeature.g:2260:1: ( ( ruleTable ) )
            // InternalFeature.g:2261:2: ( ruleTable )
            {
            // InternalFeature.g:2261:2: ( ruleTable )
            // InternalFeature.g:2262:3: ruleTable
            {
             before(grammarAccess.getStepAccess().getTablesTableParserRuleCall_3_0()); 
            pushFollow(FOLLOW_2);
            ruleTable();

            state._fsp--;

             after(grammarAccess.getStepAccess().getTablesTableParserRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Step__TablesAssignment_3"


    // $ANTLR start "rule__Step__CodeAssignment_4"
    // InternalFeature.g:2271:1: rule__Step__CodeAssignment_4 : ( ruleDocString ) ;
    public final void rule__Step__CodeAssignment_4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFeature.g:2275:1: ( ( ruleDocString ) )
            // InternalFeature.g:2276:2: ( ruleDocString )
            {
            // InternalFeature.g:2276:2: ( ruleDocString )
            // InternalFeature.g:2277:3: ruleDocString
            {
             before(grammarAccess.getStepAccess().getCodeDocStringParserRuleCall_4_0()); 
            pushFollow(FOLLOW_2);
            ruleDocString();

            state._fsp--;

             after(grammarAccess.getStepAccess().getCodeDocStringParserRuleCall_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Step__CodeAssignment_4"


    // $ANTLR start "rule__Step__TablesAssignment_5"
    // InternalFeature.g:2286:1: rule__Step__TablesAssignment_5 : ( ruleTable ) ;
    public final void rule__Step__TablesAssignment_5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFeature.g:2290:1: ( ( ruleTable ) )
            // InternalFeature.g:2291:2: ( ruleTable )
            {
            // InternalFeature.g:2291:2: ( ruleTable )
            // InternalFeature.g:2292:3: ruleTable
            {
             before(grammarAccess.getStepAccess().getTablesTableParserRuleCall_5_0()); 
            pushFollow(FOLLOW_2);
            ruleTable();

            state._fsp--;

             after(grammarAccess.getStepAccess().getTablesTableParserRuleCall_5_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Step__TablesAssignment_5"


    // $ANTLR start "rule__Examples__TitleAssignment_1"
    // InternalFeature.g:2301:1: rule__Examples__TitleAssignment_1 : ( ruleTitle ) ;
    public final void rule__Examples__TitleAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFeature.g:2305:1: ( ( ruleTitle ) )
            // InternalFeature.g:2306:2: ( ruleTitle )
            {
            // InternalFeature.g:2306:2: ( ruleTitle )
            // InternalFeature.g:2307:3: ruleTitle
            {
             before(grammarAccess.getExamplesAccess().getTitleTitleParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleTitle();

            state._fsp--;

             after(grammarAccess.getExamplesAccess().getTitleTitleParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Examples__TitleAssignment_1"


    // $ANTLR start "rule__Examples__NarrativeAssignment_3"
    // InternalFeature.g:2316:1: rule__Examples__NarrativeAssignment_3 : ( ruleNarrative ) ;
    public final void rule__Examples__NarrativeAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFeature.g:2320:1: ( ( ruleNarrative ) )
            // InternalFeature.g:2321:2: ( ruleNarrative )
            {
            // InternalFeature.g:2321:2: ( ruleNarrative )
            // InternalFeature.g:2322:3: ruleNarrative
            {
             before(grammarAccess.getExamplesAccess().getNarrativeNarrativeParserRuleCall_3_0()); 
            pushFollow(FOLLOW_2);
            ruleNarrative();

            state._fsp--;

             after(grammarAccess.getExamplesAccess().getNarrativeNarrativeParserRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Examples__NarrativeAssignment_3"


    // $ANTLR start "rule__Examples__TableAssignment_4"
    // InternalFeature.g:2331:1: rule__Examples__TableAssignment_4 : ( ruleTable ) ;
    public final void rule__Examples__TableAssignment_4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFeature.g:2335:1: ( ( ruleTable ) )
            // InternalFeature.g:2336:2: ( ruleTable )
            {
            // InternalFeature.g:2336:2: ( ruleTable )
            // InternalFeature.g:2337:3: ruleTable
            {
             before(grammarAccess.getExamplesAccess().getTableTableParserRuleCall_4_0()); 
            pushFollow(FOLLOW_2);
            ruleTable();

            state._fsp--;

             after(grammarAccess.getExamplesAccess().getTableTableParserRuleCall_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Examples__TableAssignment_4"


    // $ANTLR start "rule__Table__RowsAssignment_0"
    // InternalFeature.g:2346:1: rule__Table__RowsAssignment_0 : ( RULE_TABLE_ROW ) ;
    public final void rule__Table__RowsAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFeature.g:2350:1: ( ( RULE_TABLE_ROW ) )
            // InternalFeature.g:2351:2: ( RULE_TABLE_ROW )
            {
            // InternalFeature.g:2351:2: ( RULE_TABLE_ROW )
            // InternalFeature.g:2352:3: RULE_TABLE_ROW
            {
             before(grammarAccess.getTableAccess().getRowsTABLE_ROWTerminalRuleCall_0_0()); 
            match(input,RULE_TABLE_ROW,FOLLOW_2); 
             after(grammarAccess.getTableAccess().getRowsTABLE_ROWTerminalRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Table__RowsAssignment_0"


    // $ANTLR start "rule__DocString__ContentAssignment_0"
    // InternalFeature.g:2361:1: rule__DocString__ContentAssignment_0 : ( RULE_DOC_STRING ) ;
    public final void rule__DocString__ContentAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFeature.g:2365:1: ( ( RULE_DOC_STRING ) )
            // InternalFeature.g:2366:2: ( RULE_DOC_STRING )
            {
            // InternalFeature.g:2366:2: ( RULE_DOC_STRING )
            // InternalFeature.g:2367:3: RULE_DOC_STRING
            {
             before(grammarAccess.getDocStringAccess().getContentDOC_STRINGTerminalRuleCall_0_0()); 
            match(input,RULE_DOC_STRING,FOLLOW_2); 
             after(grammarAccess.getDocStringAccess().getContentDOC_STRINGTerminalRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DocString__ContentAssignment_0"


    // $ANTLR start "rule__Tag__IdAssignment_0"
    // InternalFeature.g:2376:1: rule__Tag__IdAssignment_0 : ( RULE_TAGNAME ) ;
    public final void rule__Tag__IdAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalFeature.g:2380:1: ( ( RULE_TAGNAME ) )
            // InternalFeature.g:2381:2: ( RULE_TAGNAME )
            {
            // InternalFeature.g:2381:2: ( RULE_TAGNAME )
            // InternalFeature.g:2382:3: RULE_TAGNAME
            {
             before(grammarAccess.getTagAccess().getIdTAGNAMETerminalRuleCall_0_0()); 
            match(input,RULE_TAGNAME,FOLLOW_2); 
             after(grammarAccess.getTagAccess().getIdTAGNAMETerminalRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Tag__IdAssignment_0"

    // Delegated rules


    protected DFA3 dfa3 = new DFA3(this);
    static final String dfa_1s = "\5\uffff";
    static final String dfa_2s = "\2\10\2\uffff\1\10";
    static final String dfa_3s = "\2\33\2\uffff\1\33";
    static final String dfa_4s = "\2\uffff\1\1\1\2\1\uffff";
    static final String dfa_5s = "\5\uffff}>";
    static final String[] dfa_6s = {
            "\1\1\21\uffff\1\2\1\3",
            "\1\1\1\4\20\uffff\1\2\1\3",
            "",
            "",
            "\1\1\21\uffff\1\2\1\3"
    };

    static final short[] dfa_1 = DFA.unpackEncodedString(dfa_1s);
    static final char[] dfa_2 = DFA.unpackEncodedStringToUnsignedChars(dfa_2s);
    static final char[] dfa_3 = DFA.unpackEncodedStringToUnsignedChars(dfa_3s);
    static final short[] dfa_4 = DFA.unpackEncodedString(dfa_4s);
    static final short[] dfa_5 = DFA.unpackEncodedString(dfa_5s);
    static final short[][] dfa_6 = unpackEncodedStringArray(dfa_6s);

    class DFA3 extends DFA {

        public DFA3(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 3;
            this.eot = dfa_1;
            this.eof = dfa_1;
            this.min = dfa_2;
            this.max = dfa_3;
            this.accept = dfa_4;
            this.special = dfa_5;
            this.transition = dfa_6;
        }
        public String getDescription() {
            return "382:1: rule__Feature__ScenariosAlternatives_6_0 : ( ( ruleScenario ) | ( ruleScenarioOutline ) );";
        }
    }
 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x00000000000000F2L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x00000000000001F2L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000001000000L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000000000102L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x00000000000000F0L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000000000000200L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x000000000E0001F0L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x0000000000000202L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x000000000E0001F2L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x00000000000002F0L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000000000F800F0L});
    public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x0000000000F800F2L});
    public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x0000000004000100L});
    public static final BitSet FOLLOW_16 = new BitSet(new long[]{0x0000000010000000L});
    public static final BitSet FOLLOW_17 = new BitSet(new long[]{0x00000000000001F0L});
    public static final BitSet FOLLOW_18 = new BitSet(new long[]{0x0000000000000E00L});
    public static final BitSet FOLLOW_19 = new BitSet(new long[]{0x0000000000000402L});
    public static final BitSet FOLLOW_20 = new BitSet(new long[]{0x00000000000004F0L});
    public static final BitSet FOLLOW_21 = new BitSet(new long[]{0x00000000000003F0L});

}