package org.xtext.example.feature.ide.contentassist.antlr.internal;

// Hack: Use our own Lexer superclass by means of import. 
// Currently there is no other way to specify the superclass for the lexer.
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.Lexer;


import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalFeatureLexer extends Lexer {
    public static final int RULE_DOC_STRING=11;
    public static final int RULE_WORD=4;
    public static final int RULE_STRING=6;
    public static final int RULE_SL_COMMENT=13;
    public static final int T__19=19;
    public static final int EOF=-1;
    public static final int RULE_ID=15;
    public static final int RULE_EOL=9;
    public static final int RULE_WS=14;
    public static final int RULE_TAGNAME=8;
    public static final int RULE_ANY_OTHER=18;
    public static final int RULE_NUMBER=5;
    public static final int T__26=26;
    public static final int RULE_PLACEHOLDER=7;
    public static final int T__27=27;
    public static final int RULE_TABLE_ROW=10;
    public static final int T__28=28;
    public static final int RULE_INT=16;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=17;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int RULE_NL=12;
    public static final int T__20=20;
    public static final int T__21=21;

    // delegates
    // delegators

    public InternalFeatureLexer() {;} 
    public InternalFeatureLexer(CharStream input) {
        this(input, new RecognizerSharedState());
    }
    public InternalFeatureLexer(CharStream input, RecognizerSharedState state) {
        super(input,state);

    }
    public String getGrammarFileName() { return "InternalFeature.g"; }

    // $ANTLR start "T__19"
    public final void mT__19() throws RecognitionException {
        try {
            int _type = T__19;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalFeature.g:11:7: ( 'Given' )
            // InternalFeature.g:11:9: 'Given'
            {
            match("Given"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__19"

    // $ANTLR start "T__20"
    public final void mT__20() throws RecognitionException {
        try {
            int _type = T__20;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalFeature.g:12:7: ( 'When' )
            // InternalFeature.g:12:9: 'When'
            {
            match("When"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__20"

    // $ANTLR start "T__21"
    public final void mT__21() throws RecognitionException {
        try {
            int _type = T__21;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalFeature.g:13:7: ( 'Then' )
            // InternalFeature.g:13:9: 'Then'
            {
            match("Then"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__21"

    // $ANTLR start "T__22"
    public final void mT__22() throws RecognitionException {
        try {
            int _type = T__22;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalFeature.g:14:7: ( 'And' )
            // InternalFeature.g:14:9: 'And'
            {
            match("And"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__22"

    // $ANTLR start "T__23"
    public final void mT__23() throws RecognitionException {
        try {
            int _type = T__23;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalFeature.g:15:7: ( 'But' )
            // InternalFeature.g:15:9: 'But'
            {
            match("But"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__23"

    // $ANTLR start "T__24"
    public final void mT__24() throws RecognitionException {
        try {
            int _type = T__24;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalFeature.g:16:7: ( 'Feature:' )
            // InternalFeature.g:16:9: 'Feature:'
            {
            match("Feature:"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__24"

    // $ANTLR start "T__25"
    public final void mT__25() throws RecognitionException {
        try {
            int _type = T__25;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalFeature.g:17:7: ( 'Background:' )
            // InternalFeature.g:17:9: 'Background:'
            {
            match("Background:"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__25"

    // $ANTLR start "T__26"
    public final void mT__26() throws RecognitionException {
        try {
            int _type = T__26;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalFeature.g:18:7: ( 'Scenario:' )
            // InternalFeature.g:18:9: 'Scenario:'
            {
            match("Scenario:"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__26"

    // $ANTLR start "T__27"
    public final void mT__27() throws RecognitionException {
        try {
            int _type = T__27;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalFeature.g:19:7: ( 'Scenario Outline:' )
            // InternalFeature.g:19:9: 'Scenario Outline:'
            {
            match("Scenario Outline:"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__27"

    // $ANTLR start "T__28"
    public final void mT__28() throws RecognitionException {
        try {
            int _type = T__28;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalFeature.g:20:7: ( 'Examples:' )
            // InternalFeature.g:20:9: 'Examples:'
            {
            match("Examples:"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__28"

    // $ANTLR start "RULE_NUMBER"
    public final void mRULE_NUMBER() throws RecognitionException {
        try {
            int _type = RULE_NUMBER;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalFeature.g:2391:13: ( ( '-' )? ( '0' .. '9' )+ ( '.' ( '0' .. '9' )+ )? )
            // InternalFeature.g:2391:15: ( '-' )? ( '0' .. '9' )+ ( '.' ( '0' .. '9' )+ )?
            {
            // InternalFeature.g:2391:15: ( '-' )?
            int alt1=2;
            int LA1_0 = input.LA(1);

            if ( (LA1_0=='-') ) {
                alt1=1;
            }
            switch (alt1) {
                case 1 :
                    // InternalFeature.g:2391:15: '-'
                    {
                    match('-'); 

                    }
                    break;

            }

            // InternalFeature.g:2391:20: ( '0' .. '9' )+
            int cnt2=0;
            loop2:
            do {
                int alt2=2;
                int LA2_0 = input.LA(1);

                if ( ((LA2_0>='0' && LA2_0<='9')) ) {
                    alt2=1;
                }


                switch (alt2) {
            	case 1 :
            	    // InternalFeature.g:2391:21: '0' .. '9'
            	    {
            	    matchRange('0','9'); 

            	    }
            	    break;

            	default :
            	    if ( cnt2 >= 1 ) break loop2;
                        EarlyExitException eee =
                            new EarlyExitException(2, input);
                        throw eee;
                }
                cnt2++;
            } while (true);

            // InternalFeature.g:2391:32: ( '.' ( '0' .. '9' )+ )?
            int alt4=2;
            int LA4_0 = input.LA(1);

            if ( (LA4_0=='.') ) {
                alt4=1;
            }
            switch (alt4) {
                case 1 :
                    // InternalFeature.g:2391:33: '.' ( '0' .. '9' )+
                    {
                    match('.'); 
                    // InternalFeature.g:2391:37: ( '0' .. '9' )+
                    int cnt3=0;
                    loop3:
                    do {
                        int alt3=2;
                        int LA3_0 = input.LA(1);

                        if ( ((LA3_0>='0' && LA3_0<='9')) ) {
                            alt3=1;
                        }


                        switch (alt3) {
                    	case 1 :
                    	    // InternalFeature.g:2391:38: '0' .. '9'
                    	    {
                    	    matchRange('0','9'); 

                    	    }
                    	    break;

                    	default :
                    	    if ( cnt3 >= 1 ) break loop3;
                                EarlyExitException eee =
                                    new EarlyExitException(3, input);
                                throw eee;
                        }
                        cnt3++;
                    } while (true);


                    }
                    break;

            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_NUMBER"

    // $ANTLR start "RULE_PLACEHOLDER"
    public final void mRULE_PLACEHOLDER() throws RecognitionException {
        try {
            int _type = RULE_PLACEHOLDER;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalFeature.g:2393:18: ( '<' (~ ( ( '>' | ' ' | '\\t' | '\\n' | '\\r' ) ) )+ '>' )
            // InternalFeature.g:2393:20: '<' (~ ( ( '>' | ' ' | '\\t' | '\\n' | '\\r' ) ) )+ '>'
            {
            match('<'); 
            // InternalFeature.g:2393:24: (~ ( ( '>' | ' ' | '\\t' | '\\n' | '\\r' ) ) )+
            int cnt5=0;
            loop5:
            do {
                int alt5=2;
                int LA5_0 = input.LA(1);

                if ( ((LA5_0>='\u0000' && LA5_0<='\b')||(LA5_0>='\u000B' && LA5_0<='\f')||(LA5_0>='\u000E' && LA5_0<='\u001F')||(LA5_0>='!' && LA5_0<='=')||(LA5_0>='?' && LA5_0<='\uFFFF')) ) {
                    alt5=1;
                }


                switch (alt5) {
            	case 1 :
            	    // InternalFeature.g:2393:24: ~ ( ( '>' | ' ' | '\\t' | '\\n' | '\\r' ) )
            	    {
            	    if ( (input.LA(1)>='\u0000' && input.LA(1)<='\b')||(input.LA(1)>='\u000B' && input.LA(1)<='\f')||(input.LA(1)>='\u000E' && input.LA(1)<='\u001F')||(input.LA(1)>='!' && input.LA(1)<='=')||(input.LA(1)>='?' && input.LA(1)<='\uFFFF') ) {
            	        input.consume();

            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;}


            	    }
            	    break;

            	default :
            	    if ( cnt5 >= 1 ) break loop5;
                        EarlyExitException eee =
                            new EarlyExitException(5, input);
                        throw eee;
                }
                cnt5++;
            } while (true);

            match('>'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_PLACEHOLDER"

    // $ANTLR start "RULE_TABLE_ROW"
    public final void mRULE_TABLE_ROW() throws RecognitionException {
        try {
            int _type = RULE_TABLE_ROW;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalFeature.g:2395:16: ( '|' ( (~ ( ( '|' | '\\n' | '\\r' ) ) )* '|' )+ ( ' ' | '\\t' )* RULE_NL )
            // InternalFeature.g:2395:18: '|' ( (~ ( ( '|' | '\\n' | '\\r' ) ) )* '|' )+ ( ' ' | '\\t' )* RULE_NL
            {
            match('|'); 
            // InternalFeature.g:2395:22: ( (~ ( ( '|' | '\\n' | '\\r' ) ) )* '|' )+
            int cnt7=0;
            loop7:
            do {
                int alt7=2;
                alt7 = dfa7.predict(input);
                switch (alt7) {
            	case 1 :
            	    // InternalFeature.g:2395:23: (~ ( ( '|' | '\\n' | '\\r' ) ) )* '|'
            	    {
            	    // InternalFeature.g:2395:23: (~ ( ( '|' | '\\n' | '\\r' ) ) )*
            	    loop6:
            	    do {
            	        int alt6=2;
            	        int LA6_0 = input.LA(1);

            	        if ( ((LA6_0>='\u0000' && LA6_0<='\t')||(LA6_0>='\u000B' && LA6_0<='\f')||(LA6_0>='\u000E' && LA6_0<='{')||(LA6_0>='}' && LA6_0<='\uFFFF')) ) {
            	            alt6=1;
            	        }


            	        switch (alt6) {
            	    	case 1 :
            	    	    // InternalFeature.g:2395:23: ~ ( ( '|' | '\\n' | '\\r' ) )
            	    	    {
            	    	    if ( (input.LA(1)>='\u0000' && input.LA(1)<='\t')||(input.LA(1)>='\u000B' && input.LA(1)<='\f')||(input.LA(1)>='\u000E' && input.LA(1)<='{')||(input.LA(1)>='}' && input.LA(1)<='\uFFFF') ) {
            	    	        input.consume();

            	    	    }
            	    	    else {
            	    	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	    	        recover(mse);
            	    	        throw mse;}


            	    	    }
            	    	    break;

            	    	default :
            	    	    break loop6;
            	        }
            	    } while (true);

            	    match('|'); 

            	    }
            	    break;

            	default :
            	    if ( cnt7 >= 1 ) break loop7;
                        EarlyExitException eee =
                            new EarlyExitException(7, input);
                        throw eee;
                }
                cnt7++;
            } while (true);

            // InternalFeature.g:2395:49: ( ' ' | '\\t' )*
            loop8:
            do {
                int alt8=2;
                int LA8_0 = input.LA(1);

                if ( (LA8_0=='\t'||LA8_0==' ') ) {
                    alt8=1;
                }


                switch (alt8) {
            	case 1 :
            	    // InternalFeature.g:
            	    {
            	    if ( input.LA(1)=='\t'||input.LA(1)==' ' ) {
            	        input.consume();

            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;}


            	    }
            	    break;

            	default :
            	    break loop8;
                }
            } while (true);

            mRULE_NL(); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_TABLE_ROW"

    // $ANTLR start "RULE_DOC_STRING"
    public final void mRULE_DOC_STRING() throws RecognitionException {
        try {
            int _type = RULE_DOC_STRING;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalFeature.g:2397:17: ( ( '\"\"\"' ( options {greedy=false; } : . )* '\"\"\"' | '\\'\\'\\'' ( options {greedy=false; } : . )* '\\'\\'\\'' ) RULE_NL )
            // InternalFeature.g:2397:19: ( '\"\"\"' ( options {greedy=false; } : . )* '\"\"\"' | '\\'\\'\\'' ( options {greedy=false; } : . )* '\\'\\'\\'' ) RULE_NL
            {
            // InternalFeature.g:2397:19: ( '\"\"\"' ( options {greedy=false; } : . )* '\"\"\"' | '\\'\\'\\'' ( options {greedy=false; } : . )* '\\'\\'\\'' )
            int alt11=2;
            int LA11_0 = input.LA(1);

            if ( (LA11_0=='\"') ) {
                alt11=1;
            }
            else if ( (LA11_0=='\'') ) {
                alt11=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 11, 0, input);

                throw nvae;
            }
            switch (alt11) {
                case 1 :
                    // InternalFeature.g:2397:20: '\"\"\"' ( options {greedy=false; } : . )* '\"\"\"'
                    {
                    match("\"\"\""); 

                    // InternalFeature.g:2397:26: ( options {greedy=false; } : . )*
                    loop9:
                    do {
                        int alt9=2;
                        int LA9_0 = input.LA(1);

                        if ( (LA9_0=='\"') ) {
                            int LA9_1 = input.LA(2);

                            if ( (LA9_1=='\"') ) {
                                int LA9_3 = input.LA(3);

                                if ( (LA9_3=='\"') ) {
                                    alt9=2;
                                }
                                else if ( ((LA9_3>='\u0000' && LA9_3<='!')||(LA9_3>='#' && LA9_3<='\uFFFF')) ) {
                                    alt9=1;
                                }


                            }
                            else if ( ((LA9_1>='\u0000' && LA9_1<='!')||(LA9_1>='#' && LA9_1<='\uFFFF')) ) {
                                alt9=1;
                            }


                        }
                        else if ( ((LA9_0>='\u0000' && LA9_0<='!')||(LA9_0>='#' && LA9_0<='\uFFFF')) ) {
                            alt9=1;
                        }


                        switch (alt9) {
                    	case 1 :
                    	    // InternalFeature.g:2397:54: .
                    	    {
                    	    matchAny(); 

                    	    }
                    	    break;

                    	default :
                    	    break loop9;
                        }
                    } while (true);

                    match("\"\"\""); 


                    }
                    break;
                case 2 :
                    // InternalFeature.g:2397:64: '\\'\\'\\'' ( options {greedy=false; } : . )* '\\'\\'\\''
                    {
                    match("'''"); 

                    // InternalFeature.g:2397:73: ( options {greedy=false; } : . )*
                    loop10:
                    do {
                        int alt10=2;
                        int LA10_0 = input.LA(1);

                        if ( (LA10_0=='\'') ) {
                            int LA10_1 = input.LA(2);

                            if ( (LA10_1=='\'') ) {
                                int LA10_3 = input.LA(3);

                                if ( (LA10_3=='\'') ) {
                                    alt10=2;
                                }
                                else if ( ((LA10_3>='\u0000' && LA10_3<='&')||(LA10_3>='(' && LA10_3<='\uFFFF')) ) {
                                    alt10=1;
                                }


                            }
                            else if ( ((LA10_1>='\u0000' && LA10_1<='&')||(LA10_1>='(' && LA10_1<='\uFFFF')) ) {
                                alt10=1;
                            }


                        }
                        else if ( ((LA10_0>='\u0000' && LA10_0<='&')||(LA10_0>='(' && LA10_0<='\uFFFF')) ) {
                            alt10=1;
                        }


                        switch (alt10) {
                    	case 1 :
                    	    // InternalFeature.g:2397:101: .
                    	    {
                    	    matchAny(); 

                    	    }
                    	    break;

                    	default :
                    	    break loop10;
                        }
                    } while (true);

                    match("'''"); 


                    }
                    break;

            }

            mRULE_NL(); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_DOC_STRING"

    // $ANTLR start "RULE_STRING"
    public final void mRULE_STRING() throws RecognitionException {
        try {
            int _type = RULE_STRING;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalFeature.g:2399:13: ( ( '\"' ( '\\\\' ( 'b' | 't' | 'n' | 'f' | 'r' | 'u' | '\"' | '\\'' | '\\\\' ) | ~ ( ( '\\\\' | '\"' | '\\r' | '\\n' ) ) )* '\"' | '\\'' ( '\\\\' ( 'b' | 't' | 'n' | 'f' | 'r' | 'u' | '\"' | '\\'' | '\\\\' ) | ~ ( ( '\\\\' | '\\'' | '\\r' | '\\n' ) ) )* '\\'' ) )
            // InternalFeature.g:2399:15: ( '\"' ( '\\\\' ( 'b' | 't' | 'n' | 'f' | 'r' | 'u' | '\"' | '\\'' | '\\\\' ) | ~ ( ( '\\\\' | '\"' | '\\r' | '\\n' ) ) )* '\"' | '\\'' ( '\\\\' ( 'b' | 't' | 'n' | 'f' | 'r' | 'u' | '\"' | '\\'' | '\\\\' ) | ~ ( ( '\\\\' | '\\'' | '\\r' | '\\n' ) ) )* '\\'' )
            {
            // InternalFeature.g:2399:15: ( '\"' ( '\\\\' ( 'b' | 't' | 'n' | 'f' | 'r' | 'u' | '\"' | '\\'' | '\\\\' ) | ~ ( ( '\\\\' | '\"' | '\\r' | '\\n' ) ) )* '\"' | '\\'' ( '\\\\' ( 'b' | 't' | 'n' | 'f' | 'r' | 'u' | '\"' | '\\'' | '\\\\' ) | ~ ( ( '\\\\' | '\\'' | '\\r' | '\\n' ) ) )* '\\'' )
            int alt14=2;
            int LA14_0 = input.LA(1);

            if ( (LA14_0=='\"') ) {
                alt14=1;
            }
            else if ( (LA14_0=='\'') ) {
                alt14=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 14, 0, input);

                throw nvae;
            }
            switch (alt14) {
                case 1 :
                    // InternalFeature.g:2399:16: '\"' ( '\\\\' ( 'b' | 't' | 'n' | 'f' | 'r' | 'u' | '\"' | '\\'' | '\\\\' ) | ~ ( ( '\\\\' | '\"' | '\\r' | '\\n' ) ) )* '\"'
                    {
                    match('\"'); 
                    // InternalFeature.g:2399:20: ( '\\\\' ( 'b' | 't' | 'n' | 'f' | 'r' | 'u' | '\"' | '\\'' | '\\\\' ) | ~ ( ( '\\\\' | '\"' | '\\r' | '\\n' ) ) )*
                    loop12:
                    do {
                        int alt12=3;
                        int LA12_0 = input.LA(1);

                        if ( (LA12_0=='\\') ) {
                            alt12=1;
                        }
                        else if ( ((LA12_0>='\u0000' && LA12_0<='\t')||(LA12_0>='\u000B' && LA12_0<='\f')||(LA12_0>='\u000E' && LA12_0<='!')||(LA12_0>='#' && LA12_0<='[')||(LA12_0>=']' && LA12_0<='\uFFFF')) ) {
                            alt12=2;
                        }


                        switch (alt12) {
                    	case 1 :
                    	    // InternalFeature.g:2399:21: '\\\\' ( 'b' | 't' | 'n' | 'f' | 'r' | 'u' | '\"' | '\\'' | '\\\\' )
                    	    {
                    	    match('\\'); 
                    	    if ( input.LA(1)=='\"'||input.LA(1)=='\''||input.LA(1)=='\\'||input.LA(1)=='b'||input.LA(1)=='f'||input.LA(1)=='n'||input.LA(1)=='r'||(input.LA(1)>='t' && input.LA(1)<='u') ) {
                    	        input.consume();

                    	    }
                    	    else {
                    	        MismatchedSetException mse = new MismatchedSetException(null,input);
                    	        recover(mse);
                    	        throw mse;}


                    	    }
                    	    break;
                    	case 2 :
                    	    // InternalFeature.g:2399:66: ~ ( ( '\\\\' | '\"' | '\\r' | '\\n' ) )
                    	    {
                    	    if ( (input.LA(1)>='\u0000' && input.LA(1)<='\t')||(input.LA(1)>='\u000B' && input.LA(1)<='\f')||(input.LA(1)>='\u000E' && input.LA(1)<='!')||(input.LA(1)>='#' && input.LA(1)<='[')||(input.LA(1)>=']' && input.LA(1)<='\uFFFF') ) {
                    	        input.consume();

                    	    }
                    	    else {
                    	        MismatchedSetException mse = new MismatchedSetException(null,input);
                    	        recover(mse);
                    	        throw mse;}


                    	    }
                    	    break;

                    	default :
                    	    break loop12;
                        }
                    } while (true);

                    match('\"'); 

                    }
                    break;
                case 2 :
                    // InternalFeature.g:2399:96: '\\'' ( '\\\\' ( 'b' | 't' | 'n' | 'f' | 'r' | 'u' | '\"' | '\\'' | '\\\\' ) | ~ ( ( '\\\\' | '\\'' | '\\r' | '\\n' ) ) )* '\\''
                    {
                    match('\''); 
                    // InternalFeature.g:2399:101: ( '\\\\' ( 'b' | 't' | 'n' | 'f' | 'r' | 'u' | '\"' | '\\'' | '\\\\' ) | ~ ( ( '\\\\' | '\\'' | '\\r' | '\\n' ) ) )*
                    loop13:
                    do {
                        int alt13=3;
                        int LA13_0 = input.LA(1);

                        if ( (LA13_0=='\\') ) {
                            alt13=1;
                        }
                        else if ( ((LA13_0>='\u0000' && LA13_0<='\t')||(LA13_0>='\u000B' && LA13_0<='\f')||(LA13_0>='\u000E' && LA13_0<='&')||(LA13_0>='(' && LA13_0<='[')||(LA13_0>=']' && LA13_0<='\uFFFF')) ) {
                            alt13=2;
                        }


                        switch (alt13) {
                    	case 1 :
                    	    // InternalFeature.g:2399:102: '\\\\' ( 'b' | 't' | 'n' | 'f' | 'r' | 'u' | '\"' | '\\'' | '\\\\' )
                    	    {
                    	    match('\\'); 
                    	    if ( input.LA(1)=='\"'||input.LA(1)=='\''||input.LA(1)=='\\'||input.LA(1)=='b'||input.LA(1)=='f'||input.LA(1)=='n'||input.LA(1)=='r'||(input.LA(1)>='t' && input.LA(1)<='u') ) {
                    	        input.consume();

                    	    }
                    	    else {
                    	        MismatchedSetException mse = new MismatchedSetException(null,input);
                    	        recover(mse);
                    	        throw mse;}


                    	    }
                    	    break;
                    	case 2 :
                    	    // InternalFeature.g:2399:147: ~ ( ( '\\\\' | '\\'' | '\\r' | '\\n' ) )
                    	    {
                    	    if ( (input.LA(1)>='\u0000' && input.LA(1)<='\t')||(input.LA(1)>='\u000B' && input.LA(1)<='\f')||(input.LA(1)>='\u000E' && input.LA(1)<='&')||(input.LA(1)>='(' && input.LA(1)<='[')||(input.LA(1)>=']' && input.LA(1)<='\uFFFF') ) {
                    	        input.consume();

                    	    }
                    	    else {
                    	        MismatchedSetException mse = new MismatchedSetException(null,input);
                    	        recover(mse);
                    	        throw mse;}


                    	    }
                    	    break;

                    	default :
                    	    break loop13;
                        }
                    } while (true);

                    match('\''); 

                    }
                    break;

            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_STRING"

    // $ANTLR start "RULE_SL_COMMENT"
    public final void mRULE_SL_COMMENT() throws RecognitionException {
        try {
            int _type = RULE_SL_COMMENT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalFeature.g:2401:17: ( '#' (~ ( ( '\\n' | '\\r' ) ) )* RULE_NL )
            // InternalFeature.g:2401:19: '#' (~ ( ( '\\n' | '\\r' ) ) )* RULE_NL
            {
            match('#'); 
            // InternalFeature.g:2401:23: (~ ( ( '\\n' | '\\r' ) ) )*
            loop15:
            do {
                int alt15=2;
                int LA15_0 = input.LA(1);

                if ( ((LA15_0>='\u0000' && LA15_0<='\t')||(LA15_0>='\u000B' && LA15_0<='\f')||(LA15_0>='\u000E' && LA15_0<='\uFFFF')) ) {
                    alt15=1;
                }


                switch (alt15) {
            	case 1 :
            	    // InternalFeature.g:2401:23: ~ ( ( '\\n' | '\\r' ) )
            	    {
            	    if ( (input.LA(1)>='\u0000' && input.LA(1)<='\t')||(input.LA(1)>='\u000B' && input.LA(1)<='\f')||(input.LA(1)>='\u000E' && input.LA(1)<='\uFFFF') ) {
            	        input.consume();

            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;}


            	    }
            	    break;

            	default :
            	    break loop15;
                }
            } while (true);

            mRULE_NL(); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_SL_COMMENT"

    // $ANTLR start "RULE_TAGNAME"
    public final void mRULE_TAGNAME() throws RecognitionException {
        try {
            int _type = RULE_TAGNAME;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalFeature.g:2403:14: ( '@' (~ ( ( ' ' | '\\t' | '\\n' | '\\r' ) ) )+ )
            // InternalFeature.g:2403:16: '@' (~ ( ( ' ' | '\\t' | '\\n' | '\\r' ) ) )+
            {
            match('@'); 
            // InternalFeature.g:2403:20: (~ ( ( ' ' | '\\t' | '\\n' | '\\r' ) ) )+
            int cnt16=0;
            loop16:
            do {
                int alt16=2;
                int LA16_0 = input.LA(1);

                if ( ((LA16_0>='\u0000' && LA16_0<='\b')||(LA16_0>='\u000B' && LA16_0<='\f')||(LA16_0>='\u000E' && LA16_0<='\u001F')||(LA16_0>='!' && LA16_0<='\uFFFF')) ) {
                    alt16=1;
                }


                switch (alt16) {
            	case 1 :
            	    // InternalFeature.g:2403:20: ~ ( ( ' ' | '\\t' | '\\n' | '\\r' ) )
            	    {
            	    if ( (input.LA(1)>='\u0000' && input.LA(1)<='\b')||(input.LA(1)>='\u000B' && input.LA(1)<='\f')||(input.LA(1)>='\u000E' && input.LA(1)<='\u001F')||(input.LA(1)>='!' && input.LA(1)<='\uFFFF') ) {
            	        input.consume();

            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;}


            	    }
            	    break;

            	default :
            	    if ( cnt16 >= 1 ) break loop16;
                        EarlyExitException eee =
                            new EarlyExitException(16, input);
                        throw eee;
                }
                cnt16++;
            } while (true);


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_TAGNAME"

    // $ANTLR start "RULE_WORD"
    public final void mRULE_WORD() throws RecognitionException {
        try {
            int _type = RULE_WORD;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalFeature.g:2405:11: (~ ( ( '@' | '|' | ' ' | '\\t' | '\\n' | '\\r' ) ) (~ ( ( ' ' | '\\t' | '\\n' | '\\r' ) ) )* )
            // InternalFeature.g:2405:13: ~ ( ( '@' | '|' | ' ' | '\\t' | '\\n' | '\\r' ) ) (~ ( ( ' ' | '\\t' | '\\n' | '\\r' ) ) )*
            {
            if ( (input.LA(1)>='\u0000' && input.LA(1)<='\b')||(input.LA(1)>='\u000B' && input.LA(1)<='\f')||(input.LA(1)>='\u000E' && input.LA(1)<='\u001F')||(input.LA(1)>='!' && input.LA(1)<='?')||(input.LA(1)>='A' && input.LA(1)<='{')||(input.LA(1)>='}' && input.LA(1)<='\uFFFF') ) {
                input.consume();

            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;}

            // InternalFeature.g:2405:45: (~ ( ( ' ' | '\\t' | '\\n' | '\\r' ) ) )*
            loop17:
            do {
                int alt17=2;
                int LA17_0 = input.LA(1);

                if ( ((LA17_0>='\u0000' && LA17_0<='\b')||(LA17_0>='\u000B' && LA17_0<='\f')||(LA17_0>='\u000E' && LA17_0<='\u001F')||(LA17_0>='!' && LA17_0<='\uFFFF')) ) {
                    alt17=1;
                }


                switch (alt17) {
            	case 1 :
            	    // InternalFeature.g:2405:45: ~ ( ( ' ' | '\\t' | '\\n' | '\\r' ) )
            	    {
            	    if ( (input.LA(1)>='\u0000' && input.LA(1)<='\b')||(input.LA(1)>='\u000B' && input.LA(1)<='\f')||(input.LA(1)>='\u000E' && input.LA(1)<='\u001F')||(input.LA(1)>='!' && input.LA(1)<='\uFFFF') ) {
            	        input.consume();

            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;}


            	    }
            	    break;

            	default :
            	    break loop17;
                }
            } while (true);


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_WORD"

    // $ANTLR start "RULE_WS"
    public final void mRULE_WS() throws RecognitionException {
        try {
            int _type = RULE_WS;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalFeature.g:2407:9: ( ( ' ' | '\\t' ) )
            // InternalFeature.g:2407:11: ( ' ' | '\\t' )
            {
            if ( input.LA(1)=='\t'||input.LA(1)==' ' ) {
                input.consume();

            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;}


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_WS"

    // $ANTLR start "RULE_EOL"
    public final void mRULE_EOL() throws RecognitionException {
        try {
            int _type = RULE_EOL;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalFeature.g:2409:10: ( RULE_NL )
            // InternalFeature.g:2409:12: RULE_NL
            {
            mRULE_NL(); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_EOL"

    // $ANTLR start "RULE_NL"
    public final void mRULE_NL() throws RecognitionException {
        try {
            // InternalFeature.g:2411:18: ( ( '\\r' )? ( '\\n' )? )
            // InternalFeature.g:2411:20: ( '\\r' )? ( '\\n' )?
            {
            // InternalFeature.g:2411:20: ( '\\r' )?
            int alt18=2;
            int LA18_0 = input.LA(1);

            if ( (LA18_0=='\r') ) {
                alt18=1;
            }
            switch (alt18) {
                case 1 :
                    // InternalFeature.g:2411:20: '\\r'
                    {
                    match('\r'); 

                    }
                    break;

            }

            // InternalFeature.g:2411:26: ( '\\n' )?
            int alt19=2;
            int LA19_0 = input.LA(1);

            if ( (LA19_0=='\n') ) {
                alt19=1;
            }
            switch (alt19) {
                case 1 :
                    // InternalFeature.g:2411:26: '\\n'
                    {
                    match('\n'); 

                    }
                    break;

            }


            }

        }
        finally {
        }
    }
    // $ANTLR end "RULE_NL"

    // $ANTLR start "RULE_ID"
    public final void mRULE_ID() throws RecognitionException {
        try {
            int _type = RULE_ID;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalFeature.g:2413:9: ( ( '^' )? ( 'a' .. 'z' | 'A' .. 'Z' | '_' ) ( 'a' .. 'z' | 'A' .. 'Z' | '_' | '0' .. '9' )* )
            // InternalFeature.g:2413:11: ( '^' )? ( 'a' .. 'z' | 'A' .. 'Z' | '_' ) ( 'a' .. 'z' | 'A' .. 'Z' | '_' | '0' .. '9' )*
            {
            // InternalFeature.g:2413:11: ( '^' )?
            int alt20=2;
            int LA20_0 = input.LA(1);

            if ( (LA20_0=='^') ) {
                alt20=1;
            }
            switch (alt20) {
                case 1 :
                    // InternalFeature.g:2413:11: '^'
                    {
                    match('^'); 

                    }
                    break;

            }

            if ( (input.LA(1)>='A' && input.LA(1)<='Z')||input.LA(1)=='_'||(input.LA(1)>='a' && input.LA(1)<='z') ) {
                input.consume();

            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;}

            // InternalFeature.g:2413:40: ( 'a' .. 'z' | 'A' .. 'Z' | '_' | '0' .. '9' )*
            loop21:
            do {
                int alt21=2;
                int LA21_0 = input.LA(1);

                if ( ((LA21_0>='0' && LA21_0<='9')||(LA21_0>='A' && LA21_0<='Z')||LA21_0=='_'||(LA21_0>='a' && LA21_0<='z')) ) {
                    alt21=1;
                }


                switch (alt21) {
            	case 1 :
            	    // InternalFeature.g:
            	    {
            	    if ( (input.LA(1)>='0' && input.LA(1)<='9')||(input.LA(1)>='A' && input.LA(1)<='Z')||input.LA(1)=='_'||(input.LA(1)>='a' && input.LA(1)<='z') ) {
            	        input.consume();

            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;}


            	    }
            	    break;

            	default :
            	    break loop21;
                }
            } while (true);


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_ID"

    // $ANTLR start "RULE_INT"
    public final void mRULE_INT() throws RecognitionException {
        try {
            int _type = RULE_INT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalFeature.g:2415:10: ( ( '0' .. '9' )+ )
            // InternalFeature.g:2415:12: ( '0' .. '9' )+
            {
            // InternalFeature.g:2415:12: ( '0' .. '9' )+
            int cnt22=0;
            loop22:
            do {
                int alt22=2;
                int LA22_0 = input.LA(1);

                if ( ((LA22_0>='0' && LA22_0<='9')) ) {
                    alt22=1;
                }


                switch (alt22) {
            	case 1 :
            	    // InternalFeature.g:2415:13: '0' .. '9'
            	    {
            	    matchRange('0','9'); 

            	    }
            	    break;

            	default :
            	    if ( cnt22 >= 1 ) break loop22;
                        EarlyExitException eee =
                            new EarlyExitException(22, input);
                        throw eee;
                }
                cnt22++;
            } while (true);


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_INT"

    // $ANTLR start "RULE_ML_COMMENT"
    public final void mRULE_ML_COMMENT() throws RecognitionException {
        try {
            int _type = RULE_ML_COMMENT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalFeature.g:2417:17: ( '/*' ( options {greedy=false; } : . )* '*/' )
            // InternalFeature.g:2417:19: '/*' ( options {greedy=false; } : . )* '*/'
            {
            match("/*"); 

            // InternalFeature.g:2417:24: ( options {greedy=false; } : . )*
            loop23:
            do {
                int alt23=2;
                int LA23_0 = input.LA(1);

                if ( (LA23_0=='*') ) {
                    int LA23_1 = input.LA(2);

                    if ( (LA23_1=='/') ) {
                        alt23=2;
                    }
                    else if ( ((LA23_1>='\u0000' && LA23_1<='.')||(LA23_1>='0' && LA23_1<='\uFFFF')) ) {
                        alt23=1;
                    }


                }
                else if ( ((LA23_0>='\u0000' && LA23_0<=')')||(LA23_0>='+' && LA23_0<='\uFFFF')) ) {
                    alt23=1;
                }


                switch (alt23) {
            	case 1 :
            	    // InternalFeature.g:2417:52: .
            	    {
            	    matchAny(); 

            	    }
            	    break;

            	default :
            	    break loop23;
                }
            } while (true);

            match("*/"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_ML_COMMENT"

    // $ANTLR start "RULE_ANY_OTHER"
    public final void mRULE_ANY_OTHER() throws RecognitionException {
        try {
            int _type = RULE_ANY_OTHER;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalFeature.g:2419:16: ( . )
            // InternalFeature.g:2419:18: .
            {
            matchAny(); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_ANY_OTHER"

    public void mTokens() throws RecognitionException {
        // InternalFeature.g:1:8: ( T__19 | T__20 | T__21 | T__22 | T__23 | T__24 | T__25 | T__26 | T__27 | T__28 | RULE_NUMBER | RULE_PLACEHOLDER | RULE_TABLE_ROW | RULE_DOC_STRING | RULE_STRING | RULE_SL_COMMENT | RULE_TAGNAME | RULE_WORD | RULE_WS | RULE_EOL | RULE_ID | RULE_INT | RULE_ML_COMMENT | RULE_ANY_OTHER )
        int alt24=24;
        alt24 = dfa24.predict(input);
        switch (alt24) {
            case 1 :
                // InternalFeature.g:1:10: T__19
                {
                mT__19(); 

                }
                break;
            case 2 :
                // InternalFeature.g:1:16: T__20
                {
                mT__20(); 

                }
                break;
            case 3 :
                // InternalFeature.g:1:22: T__21
                {
                mT__21(); 

                }
                break;
            case 4 :
                // InternalFeature.g:1:28: T__22
                {
                mT__22(); 

                }
                break;
            case 5 :
                // InternalFeature.g:1:34: T__23
                {
                mT__23(); 

                }
                break;
            case 6 :
                // InternalFeature.g:1:40: T__24
                {
                mT__24(); 

                }
                break;
            case 7 :
                // InternalFeature.g:1:46: T__25
                {
                mT__25(); 

                }
                break;
            case 8 :
                // InternalFeature.g:1:52: T__26
                {
                mT__26(); 

                }
                break;
            case 9 :
                // InternalFeature.g:1:58: T__27
                {
                mT__27(); 

                }
                break;
            case 10 :
                // InternalFeature.g:1:64: T__28
                {
                mT__28(); 

                }
                break;
            case 11 :
                // InternalFeature.g:1:70: RULE_NUMBER
                {
                mRULE_NUMBER(); 

                }
                break;
            case 12 :
                // InternalFeature.g:1:82: RULE_PLACEHOLDER
                {
                mRULE_PLACEHOLDER(); 

                }
                break;
            case 13 :
                // InternalFeature.g:1:99: RULE_TABLE_ROW
                {
                mRULE_TABLE_ROW(); 

                }
                break;
            case 14 :
                // InternalFeature.g:1:114: RULE_DOC_STRING
                {
                mRULE_DOC_STRING(); 

                }
                break;
            case 15 :
                // InternalFeature.g:1:130: RULE_STRING
                {
                mRULE_STRING(); 

                }
                break;
            case 16 :
                // InternalFeature.g:1:142: RULE_SL_COMMENT
                {
                mRULE_SL_COMMENT(); 

                }
                break;
            case 17 :
                // InternalFeature.g:1:158: RULE_TAGNAME
                {
                mRULE_TAGNAME(); 

                }
                break;
            case 18 :
                // InternalFeature.g:1:171: RULE_WORD
                {
                mRULE_WORD(); 

                }
                break;
            case 19 :
                // InternalFeature.g:1:181: RULE_WS
                {
                mRULE_WS(); 

                }
                break;
            case 20 :
                // InternalFeature.g:1:189: RULE_EOL
                {
                mRULE_EOL(); 

                }
                break;
            case 21 :
                // InternalFeature.g:1:198: RULE_ID
                {
                mRULE_ID(); 

                }
                break;
            case 22 :
                // InternalFeature.g:1:206: RULE_INT
                {
                mRULE_INT(); 

                }
                break;
            case 23 :
                // InternalFeature.g:1:215: RULE_ML_COMMENT
                {
                mRULE_ML_COMMENT(); 

                }
                break;
            case 24 :
                // InternalFeature.g:1:231: RULE_ANY_OTHER
                {
                mRULE_ANY_OTHER(); 

                }
                break;

        }

    }


    protected DFA7 dfa7 = new DFA7(this);
    protected DFA24 dfa24 = new DFA24(this);
    static final String DFA7_eotS =
        "\2\2\2\uffff";
    static final String DFA7_eofS =
        "\4\uffff";
    static final String DFA7_minS =
        "\2\0\2\uffff";
    static final String DFA7_maxS =
        "\2\uffff\2\uffff";
    static final String DFA7_acceptS =
        "\2\uffff\1\2\1\1";
    static final String DFA7_specialS =
        "\1\1\1\0\2\uffff}>";
    static final String[] DFA7_transitionS = {
            "\11\3\1\1\1\uffff\2\3\1\uffff\22\3\1\1\uffdf\3",
            "\11\3\1\1\1\uffff\2\3\1\uffff\22\3\1\1\uffdf\3",
            "",
            ""
    };

    static final short[] DFA7_eot = DFA.unpackEncodedString(DFA7_eotS);
    static final short[] DFA7_eof = DFA.unpackEncodedString(DFA7_eofS);
    static final char[] DFA7_min = DFA.unpackEncodedStringToUnsignedChars(DFA7_minS);
    static final char[] DFA7_max = DFA.unpackEncodedStringToUnsignedChars(DFA7_maxS);
    static final short[] DFA7_accept = DFA.unpackEncodedString(DFA7_acceptS);
    static final short[] DFA7_special = DFA.unpackEncodedString(DFA7_specialS);
    static final short[][] DFA7_transition;

    static {
        int numStates = DFA7_transitionS.length;
        DFA7_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA7_transition[i] = DFA.unpackEncodedString(DFA7_transitionS[i]);
        }
    }

    class DFA7 extends DFA {

        public DFA7(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 7;
            this.eot = DFA7_eot;
            this.eof = DFA7_eof;
            this.min = DFA7_min;
            this.max = DFA7_max;
            this.accept = DFA7_accept;
            this.special = DFA7_special;
            this.transition = DFA7_transition;
        }
        public String getDescription() {
            return "()+ loopback of 2395:22: ( (~ ( ( '|' | '\\n' | '\\r' ) ) )* '|' )+";
        }
        public int specialStateTransition(int s, IntStream _input) throws NoViableAltException {
            IntStream input = _input;
        	int _s = s;
            switch ( s ) {
                    case 0 : 
                        int LA7_1 = input.LA(1);

                        s = -1;
                        if ( (LA7_1=='\t'||LA7_1==' ') ) {s = 1;}

                        else if ( ((LA7_1>='\u0000' && LA7_1<='\b')||(LA7_1>='\u000B' && LA7_1<='\f')||(LA7_1>='\u000E' && LA7_1<='\u001F')||(LA7_1>='!' && LA7_1<='\uFFFF')) ) {s = 3;}

                        else s = 2;

                        if ( s>=0 ) return s;
                        break;
                    case 1 : 
                        int LA7_0 = input.LA(1);

                        s = -1;
                        if ( (LA7_0=='\t'||LA7_0==' ') ) {s = 1;}

                        else if ( ((LA7_0>='\u0000' && LA7_0<='\b')||(LA7_0>='\u000B' && LA7_0<='\f')||(LA7_0>='\u000E' && LA7_0<='\u001F')||(LA7_0>='!' && LA7_0<='\uFFFF')) ) {s = 3;}

                        else s = 2;

                        if ( s>=0 ) return s;
                        break;
            }
            NoViableAltException nvae =
                new NoViableAltException(getDescription(), 7, _s, input);
            error(nvae);
            throw nvae;
        }
    }
    static final String DFA24_eotS =
        "\1\25\11\33\1\46\1\33\1\52\2\33\1\63\1\52\1\33\4\uffff\2\33\1\uffff\2\33\1\uffff\10\33\1\46\1\33\1\uffff\1\46\1\33\2\uffff\1\56\2\33\1\uffff\1\56\2\33\1\63\2\uffff\1\33\1\uffff\4\33\1\117\1\120\4\33\1\46\1\125\2\33\1\56\2\33\1\56\1\33\1\uffff\2\33\1\135\1\136\2\uffff\4\33\1\uffff\1\33\1\uffff\4\33\1\145\2\uffff\6\33\1\uffff\4\33\2\127\5\33\1\165\3\33\1\uffff\1\172\1\uffff\1\173\1\33\2\uffff\1\175\1\uffff";
    static final String DFA24_eofS =
        "\176\uffff";
    static final String DFA24_minS =
        "\1\0\11\60\7\0\1\101\4\uffff\1\60\1\52\1\uffff\2\60\1\uffff\10\60\1\0\1\60\1\uffff\2\0\2\uffff\1\0\1\42\1\0\1\uffff\1\0\1\42\2\0\2\uffff\1\60\1\uffff\1\0\3\60\2\0\4\60\11\0\1\uffff\1\0\1\60\2\0\2\uffff\4\60\1\uffff\1\0\1\uffff\5\0\2\uffff\4\60\2\0\1\uffff\4\60\2\0\5\60\1\0\1\40\2\60\1\uffff\1\0\1\uffff\1\0\1\60\2\uffff\1\0\1\uffff";
    static final String DFA24_maxS =
        "\1\uffff\10\172\1\71\7\uffff\1\172\4\uffff\1\172\1\52\1\uffff\2\172\1\uffff\10\172\1\uffff\1\71\1\uffff\2\uffff\2\uffff\1\uffff\1\165\1\uffff\1\uffff\1\uffff\1\165\2\uffff\2\uffff\1\172\1\uffff\1\uffff\3\172\2\uffff\4\172\11\uffff\1\uffff\1\uffff\1\172\2\uffff\2\uffff\4\172\1\uffff\1\uffff\1\uffff\5\uffff\2\uffff\4\172\2\uffff\1\uffff\4\172\2\uffff\5\172\1\uffff\3\172\1\uffff\1\uffff\1\uffff\1\uffff\1\172\2\uffff\1\uffff\1\uffff";
    static final String DFA24_acceptS =
        "\22\uffff\1\23\3\24\2\uffff\1\22\2\uffff\1\22\12\uffff\1\13\2\uffff\1\15\1\30\3\uffff\1\17\4\uffff\1\20\1\21\1\uffff\1\23\23\uffff\1\27\4\uffff\1\4\1\5\4\uffff\1\14\1\uffff\1\16\5\uffff\1\2\1\3\6\uffff\1\1\17\uffff\1\6\1\uffff\1\11\2\uffff\1\10\1\12\1\uffff\1\7";
    static final String DFA24_specialS =
        "\1\24\11\uffff\1\32\1\37\1\11\1\41\1\22\1\44\1\23\23\uffff\1\10\2\uffff\1\12\1\5\2\uffff\1\6\1\uffff\1\31\1\uffff\1\15\1\uffff\1\3\1\2\4\uffff\1\25\3\uffff\1\47\1\53\4\uffff\1\14\1\36\1\33\1\27\1\16\1\46\1\1\1\20\1\51\1\uffff\1\21\1\uffff\1\43\1\45\7\uffff\1\35\1\uffff\1\42\1\52\1\54\1\0\1\40\6\uffff\1\34\1\50\5\uffff\1\4\1\26\5\uffff\1\7\4\uffff\1\17\1\uffff\1\30\3\uffff\1\13\1\uffff}>";
    static final String[] DFA24_transitionS = {
            "\11\30\1\22\1\24\2\30\1\23\22\30\1\22\1\30\1\15\1\17\3\30\1\16\5\30\1\11\1\30\1\27\12\12\2\30\1\13\3\30\1\20\1\4\1\5\2\26\1\10\1\6\1\1\13\26\1\7\1\3\2\26\1\2\3\26\3\30\1\21\1\26\1\30\32\26\1\30\1\14\uff83\30",
            "\12\32\7\uffff\32\32\4\uffff\1\32\1\uffff\10\32\1\31\21\32",
            "\12\32\7\uffff\32\32\4\uffff\1\32\1\uffff\7\32\1\34\22\32",
            "\12\32\7\uffff\32\32\4\uffff\1\32\1\uffff\7\32\1\35\22\32",
            "\12\32\7\uffff\32\32\4\uffff\1\32\1\uffff\15\32\1\36\14\32",
            "\12\32\7\uffff\32\32\4\uffff\1\32\1\uffff\1\40\23\32\1\37\5\32",
            "\12\32\7\uffff\32\32\4\uffff\1\32\1\uffff\4\32\1\41\25\32",
            "\12\32\7\uffff\32\32\4\uffff\1\32\1\uffff\2\32\1\42\27\32",
            "\12\32\7\uffff\32\32\4\uffff\1\32\1\uffff\27\32\1\43\2\32",
            "\12\44",
            "\11\33\2\uffff\2\33\1\uffff\22\33\1\uffff\15\33\1\45\1\33\12\47\uffc6\33",
            "\11\50\2\uffff\2\50\1\uffff\22\50\1\uffff\35\50\1\uffff\uffc1\50",
            "\12\51\1\uffff\2\51\1\uffff\ufff2\51",
            "\11\55\1\56\1\uffff\2\55\1\uffff\22\55\1\56\1\55\1\53\71\55\1\54\uffa3\55",
            "\11\61\1\56\1\uffff\2\61\1\uffff\22\61\1\56\6\61\1\57\64\61\1\60\uffa3\61",
            "\11\62\2\uffff\2\62\1\uffff\22\62\1\uffff\uffdf\62",
            "\11\64\2\uffff\2\64\1\uffff\22\64\1\uffff\uffdf\64",
            "\32\65\4\uffff\1\65\1\uffff\32\65",
            "",
            "",
            "",
            "",
            "\12\32\7\uffff\32\32\4\uffff\1\32\1\uffff\32\32",
            "\1\67",
            "",
            "\12\32\7\uffff\32\32\4\uffff\1\32\1\uffff\25\32\1\70\4\32",
            "\12\32\7\uffff\32\32\4\uffff\1\32\1\uffff\32\32",
            "",
            "\12\32\7\uffff\32\32\4\uffff\1\32\1\uffff\4\32\1\71\25\32",
            "\12\32\7\uffff\32\32\4\uffff\1\32\1\uffff\4\32\1\72\25\32",
            "\12\32\7\uffff\32\32\4\uffff\1\32\1\uffff\3\32\1\73\26\32",
            "\12\32\7\uffff\32\32\4\uffff\1\32\1\uffff\23\32\1\74\6\32",
            "\12\32\7\uffff\32\32\4\uffff\1\32\1\uffff\2\32\1\75\27\32",
            "\12\32\7\uffff\32\32\4\uffff\1\32\1\uffff\1\76\31\32",
            "\12\32\7\uffff\32\32\4\uffff\1\32\1\uffff\4\32\1\77\25\32",
            "\12\32\7\uffff\32\32\4\uffff\1\32\1\uffff\1\100\31\32",
            "\11\33\2\uffff\2\33\1\uffff\22\33\1\uffff\15\33\1\45\1\33\12\44\uffc6\33",
            "\12\101",
            "",
            "\11\33\2\uffff\2\33\1\uffff\22\33\1\uffff\15\33\1\45\1\33\12\47\uffc6\33",
            "\11\50\2\uffff\2\50\1\uffff\22\50\1\uffff\35\50\1\102\uffc1\50",
            "",
            "",
            "\11\33\2\uffff\2\33\1\uffff\22\33\1\uffff\1\33\1\103\uffdd\33",
            "\1\104\4\uffff\1\104\64\uffff\1\104\5\uffff\1\104\3\uffff\1\104\7\uffff\1\104\3\uffff\1\104\1\uffff\2\104",
            "\11\55\1\56\1\uffff\2\55\1\uffff\22\55\1\56\1\55\1\105\71\55\1\54\uffa3\55",
            "",
            "\11\33\2\uffff\2\33\1\uffff\22\33\1\uffff\6\33\1\106\uffd8\33",
            "\1\107\4\uffff\1\107\64\uffff\1\107\5\uffff\1\107\3\uffff\1\107\7\uffff\1\107\3\uffff\1\107\1\uffff\2\107",
            "\11\61\1\56\1\uffff\2\61\1\uffff\22\61\1\56\6\61\1\110\64\61\1\60\uffa3\61",
            "\11\62\2\uffff\2\62\1\uffff\22\62\1\uffff\uffdf\62",
            "",
            "",
            "\12\32\7\uffff\32\32\4\uffff\1\32\1\uffff\32\32",
            "",
            "\11\113\2\112\2\113\1\112\22\113\1\112\11\113\1\111\uffd5\113",
            "\12\32\7\uffff\32\32\4\uffff\1\32\1\uffff\4\32\1\114\25\32",
            "\12\32\7\uffff\32\32\4\uffff\1\32\1\uffff\15\32\1\115\14\32",
            "\12\32\7\uffff\32\32\4\uffff\1\32\1\uffff\15\32\1\116\14\32",
            "\11\33\2\uffff\2\33\1\uffff\22\33\1\uffff\17\33\12\32\7\33\32\32\4\33\1\32\1\33\32\32\uff85\33",
            "\11\33\2\uffff\2\33\1\uffff\22\33\1\uffff\17\33\12\32\7\33\32\32\4\33\1\32\1\33\32\32\uff85\33",
            "\12\32\7\uffff\32\32\4\uffff\1\32\1\uffff\12\32\1\121\17\32",
            "\12\32\7\uffff\32\32\4\uffff\1\32\1\uffff\23\32\1\122\6\32",
            "\12\32\7\uffff\32\32\4\uffff\1\32\1\uffff\15\32\1\123\14\32",
            "\12\32\7\uffff\32\32\4\uffff\1\32\1\uffff\14\32\1\124\15\32",
            "\11\33\2\uffff\2\33\1\uffff\22\33\1\uffff\17\33\12\101\uffc6\33",
            "\11\33\2\uffff\2\33\1\uffff\22\33\1\uffff\uffdf\33",
            "\11\130\2\127\2\130\1\127\22\130\1\127\1\130\1\126\uffdd\130",
            "\11\55\1\56\1\uffff\2\55\1\uffff\22\55\1\56\1\55\1\105\71\55\1\54\uffa3\55",
            "\11\33\2\uffff\2\33\1\uffff\22\33\1\uffff\uffdf\33",
            "\11\132\2\127\2\132\1\127\22\132\1\127\6\132\1\131\uffd8\132",
            "\11\61\1\56\1\uffff\2\61\1\uffff\22\61\1\56\6\61\1\110\64\61\1\60\uffa3\61",
            "\11\33\2\uffff\2\33\1\uffff\22\33\1\uffff\uffdf\33",
            "\11\113\2\112\2\113\1\112\22\113\1\112\11\113\1\111\4\113\1\133\uffd0\113",
            "",
            "\11\113\2\112\2\113\1\112\22\113\1\112\11\113\1\111\uffd5\113",
            "\12\32\7\uffff\32\32\4\uffff\1\32\1\uffff\15\32\1\134\14\32",
            "\11\33\2\uffff\2\33\1\uffff\22\33\1\uffff\17\33\12\32\7\33\32\32\4\33\1\32\1\33\32\32\uff85\33",
            "\11\33\2\uffff\2\33\1\uffff\22\33\1\uffff\17\33\12\32\7\33\32\32\4\33\1\32\1\33\32\32\uff85\33",
            "",
            "",
            "\12\32\7\uffff\32\32\4\uffff\1\32\1\uffff\6\32\1\137\23\32",
            "\12\32\7\uffff\32\32\4\uffff\1\32\1\uffff\24\32\1\140\5\32",
            "\12\32\7\uffff\32\32\4\uffff\1\32\1\uffff\1\141\31\32",
            "\12\32\7\uffff\32\32\4\uffff\1\32\1\uffff\17\32\1\142\12\32",
            "",
            "\11\130\2\127\2\130\1\127\22\130\1\127\1\130\1\143\uffdd\130",
            "",
            "\11\130\2\127\2\130\1\127\22\130\1\127\1\130\1\126\uffdd\130",
            "\11\132\2\127\2\132\1\127\22\132\1\127\6\132\1\144\uffd8\132",
            "\11\132\2\127\2\132\1\127\22\132\1\127\6\132\1\131\uffd8\132",
            "\11\113\2\112\2\113\1\112\22\113\1\112\11\113\1\111\uffd5\113",
            "\11\33\2\uffff\2\33\1\uffff\22\33\1\uffff\17\33\12\32\7\33\32\32\4\33\1\32\1\33\32\32\uff85\33",
            "",
            "",
            "\12\32\7\uffff\32\32\4\uffff\1\32\1\uffff\21\32\1\146\10\32",
            "\12\32\7\uffff\32\32\4\uffff\1\32\1\uffff\21\32\1\147\10\32",
            "\12\32\7\uffff\32\32\4\uffff\1\32\1\uffff\21\32\1\150\10\32",
            "\12\32\7\uffff\32\32\4\uffff\1\32\1\uffff\13\32\1\151\16\32",
            "\11\130\2\127\2\130\1\127\22\130\1\127\1\130\1\152\uffdd\130",
            "\11\132\2\127\2\132\1\127\22\132\1\127\6\132\1\153\uffd8\132",
            "",
            "\12\32\7\uffff\32\32\4\uffff\1\32\1\uffff\16\32\1\154\13\32",
            "\12\32\7\uffff\32\32\4\uffff\1\32\1\uffff\4\32\1\155\25\32",
            "\12\32\7\uffff\32\32\4\uffff\1\32\1\uffff\10\32\1\156\21\32",
            "\12\32\7\uffff\32\32\4\uffff\1\32\1\uffff\4\32\1\157\25\32",
            "\11\130\2\uffff\2\130\1\uffff\22\130\1\uffff\1\130\1\152\uffdd\130",
            "\11\132\2\uffff\2\132\1\uffff\22\132\1\uffff\6\132\1\153\uffd8\132",
            "\12\32\7\uffff\32\32\4\uffff\1\32\1\uffff\24\32\1\160\5\32",
            "\12\32\1\161\6\uffff\32\32\4\uffff\1\32\1\uffff\32\32",
            "\12\32\7\uffff\32\32\4\uffff\1\32\1\uffff\16\32\1\162\13\32",
            "\12\32\7\uffff\32\32\4\uffff\1\32\1\uffff\22\32\1\163\7\32",
            "\12\32\7\uffff\32\32\4\uffff\1\32\1\uffff\15\32\1\164\14\32",
            "\11\33\2\uffff\2\33\1\uffff\22\33\1\uffff\uffdf\33",
            "\1\167\17\uffff\12\32\1\166\6\uffff\32\32\4\uffff\1\32\1\uffff\32\32",
            "\12\32\1\170\6\uffff\32\32\4\uffff\1\32\1\uffff\32\32",
            "\12\32\7\uffff\32\32\4\uffff\1\32\1\uffff\3\32\1\171\26\32",
            "",
            "\11\33\2\uffff\2\33\1\uffff\22\33\1\uffff\uffdf\33",
            "",
            "\11\33\2\uffff\2\33\1\uffff\22\33\1\uffff\uffdf\33",
            "\12\32\1\174\6\uffff\32\32\4\uffff\1\32\1\uffff\32\32",
            "",
            "",
            "\11\33\2\uffff\2\33\1\uffff\22\33\1\uffff\uffdf\33",
            ""
    };

    static final short[] DFA24_eot = DFA.unpackEncodedString(DFA24_eotS);
    static final short[] DFA24_eof = DFA.unpackEncodedString(DFA24_eofS);
    static final char[] DFA24_min = DFA.unpackEncodedStringToUnsignedChars(DFA24_minS);
    static final char[] DFA24_max = DFA.unpackEncodedStringToUnsignedChars(DFA24_maxS);
    static final short[] DFA24_accept = DFA.unpackEncodedString(DFA24_acceptS);
    static final short[] DFA24_special = DFA.unpackEncodedString(DFA24_specialS);
    static final short[][] DFA24_transition;

    static {
        int numStates = DFA24_transitionS.length;
        DFA24_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA24_transition[i] = DFA.unpackEncodedString(DFA24_transitionS[i]);
        }
    }

    class DFA24 extends DFA {

        public DFA24(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 24;
            this.eot = DFA24_eot;
            this.eof = DFA24_eof;
            this.min = DFA24_min;
            this.max = DFA24_max;
            this.accept = DFA24_accept;
            this.special = DFA24_special;
            this.transition = DFA24_transition;
        }
        public String getDescription() {
            return "1:1: Tokens : ( T__19 | T__20 | T__21 | T__22 | T__23 | T__24 | T__25 | T__26 | T__27 | T__28 | RULE_NUMBER | RULE_PLACEHOLDER | RULE_TABLE_ROW | RULE_DOC_STRING | RULE_STRING | RULE_SL_COMMENT | RULE_TAGNAME | RULE_WORD | RULE_WS | RULE_EOL | RULE_ID | RULE_INT | RULE_ML_COMMENT | RULE_ANY_OTHER );";
        }
        public int specialStateTransition(int s, IntStream _input) throws NoViableAltException {
            IntStream input = _input;
        	int _s = s;
            switch ( s ) {
                    case 0 : 
                        int LA24_91 = input.LA(1);

                        s = -1;
                        if ( (LA24_91=='*') ) {s = 73;}

                        else if ( ((LA24_91>='\u0000' && LA24_91<='\b')||(LA24_91>='\u000B' && LA24_91<='\f')||(LA24_91>='\u000E' && LA24_91<='\u001F')||(LA24_91>='!' && LA24_91<=')')||(LA24_91>='+' && LA24_91<='\uFFFF')) ) {s = 75;}

                        else if ( ((LA24_91>='\t' && LA24_91<='\n')||LA24_91=='\r'||LA24_91==' ') ) {s = 74;}

                        else s = 27;

                        if ( s>=0 ) return s;
                        break;
                    case 1 : 
                        int LA24_71 = input.LA(1);

                        s = -1;
                        if ( (LA24_71=='\'') ) {s = 72;}

                        else if ( (LA24_71=='\\') ) {s = 48;}

                        else if ( ((LA24_71>='\u0000' && LA24_71<='\b')||(LA24_71>='\u000B' && LA24_71<='\f')||(LA24_71>='\u000E' && LA24_71<='\u001F')||(LA24_71>='!' && LA24_71<='&')||(LA24_71>='(' && LA24_71<='[')||(LA24_71>=']' && LA24_71<='\uFFFF')) ) {s = 49;}

                        else if ( (LA24_71=='\t'||LA24_71==' ') ) {s = 46;}

                        else s = 27;

                        if ( s>=0 ) return s;
                        break;
                    case 2 : 
                        int LA24_50 = input.LA(1);

                        s = -1;
                        if ( ((LA24_50>='\u0000' && LA24_50<='\b')||(LA24_50>='\u000B' && LA24_50<='\f')||(LA24_50>='\u000E' && LA24_50<='\u001F')||(LA24_50>='!' && LA24_50<='\uFFFF')) ) {s = 50;}

                        else s = 51;

                        if ( s>=0 ) return s;
                        break;
                    case 3 : 
                        int LA24_49 = input.LA(1);

                        s = -1;
                        if ( (LA24_49=='\'') ) {s = 72;}

                        else if ( (LA24_49=='\\') ) {s = 48;}

                        else if ( ((LA24_49>='\u0000' && LA24_49<='\b')||(LA24_49>='\u000B' && LA24_49<='\f')||(LA24_49>='\u000E' && LA24_49<='\u001F')||(LA24_49>='!' && LA24_49<='&')||(LA24_49>='(' && LA24_49<='[')||(LA24_49>=']' && LA24_49<='\uFFFF')) ) {s = 49;}

                        else if ( (LA24_49=='\t'||LA24_49==' ') ) {s = 46;}

                        else s = 27;

                        if ( s>=0 ) return s;
                        break;
                    case 4 : 
                        int LA24_106 = input.LA(1);

                        s = -1;
                        if ( (LA24_106=='\"') ) {s = 106;}

                        else if ( ((LA24_106>='\u0000' && LA24_106<='\b')||(LA24_106>='\u000B' && LA24_106<='\f')||(LA24_106>='\u000E' && LA24_106<='\u001F')||LA24_106=='!'||(LA24_106>='#' && LA24_106<='\uFFFF')) ) {s = 88;}

                        else s = 87;

                        if ( s>=0 ) return s;
                        break;
                    case 5 : 
                        int LA24_40 = input.LA(1);

                        s = -1;
                        if ( (LA24_40=='>') ) {s = 66;}

                        else if ( ((LA24_40>='\u0000' && LA24_40<='\b')||(LA24_40>='\u000B' && LA24_40<='\f')||(LA24_40>='\u000E' && LA24_40<='\u001F')||(LA24_40>='!' && LA24_40<='=')||(LA24_40>='?' && LA24_40<='\uFFFF')) ) {s = 40;}

                        else s = 27;

                        if ( s>=0 ) return s;
                        break;
                    case 6 : 
                        int LA24_43 = input.LA(1);

                        s = -1;
                        if ( (LA24_43=='\"') ) {s = 67;}

                        else if ( ((LA24_43>='\u0000' && LA24_43<='\b')||(LA24_43>='\u000B' && LA24_43<='\f')||(LA24_43>='\u000E' && LA24_43<='\u001F')||LA24_43=='!'||(LA24_43>='#' && LA24_43<='\uFFFF')) ) {s = 27;}

                        else s = 46;

                        if ( s>=0 ) return s;
                        break;
                    case 7 : 
                        int LA24_113 = input.LA(1);

                        s = -1;
                        if ( ((LA24_113>='\u0000' && LA24_113<='\b')||(LA24_113>='\u000B' && LA24_113<='\f')||(LA24_113>='\u000E' && LA24_113<='\u001F')||(LA24_113>='!' && LA24_113<='\uFFFF')) ) {s = 27;}

                        else s = 117;

                        if ( s>=0 ) return s;
                        break;
                    case 8 : 
                        int LA24_36 = input.LA(1);

                        s = -1;
                        if ( (LA24_36=='.') ) {s = 37;}

                        else if ( ((LA24_36>='0' && LA24_36<='9')) ) {s = 36;}

                        else if ( ((LA24_36>='\u0000' && LA24_36<='\b')||(LA24_36>='\u000B' && LA24_36<='\f')||(LA24_36>='\u000E' && LA24_36<='\u001F')||(LA24_36>='!' && LA24_36<='-')||LA24_36=='/'||(LA24_36>=':' && LA24_36<='\uFFFF')) ) {s = 27;}

                        else s = 38;

                        if ( s>=0 ) return s;
                        break;
                    case 9 : 
                        int LA24_12 = input.LA(1);

                        s = -1;
                        if ( ((LA24_12>='\u0000' && LA24_12<='\t')||(LA24_12>='\u000B' && LA24_12<='\f')||(LA24_12>='\u000E' && LA24_12<='\uFFFF')) ) {s = 41;}

                        else s = 42;

                        if ( s>=0 ) return s;
                        break;
                    case 10 : 
                        int LA24_39 = input.LA(1);

                        s = -1;
                        if ( (LA24_39=='.') ) {s = 37;}

                        else if ( ((LA24_39>='0' && LA24_39<='9')) ) {s = 39;}

                        else if ( ((LA24_39>='\u0000' && LA24_39<='\b')||(LA24_39>='\u000B' && LA24_39<='\f')||(LA24_39>='\u000E' && LA24_39<='\u001F')||(LA24_39>='!' && LA24_39<='-')||LA24_39=='/'||(LA24_39>=':' && LA24_39<='\uFFFF')) ) {s = 27;}

                        else s = 38;

                        if ( s>=0 ) return s;
                        break;
                    case 11 : 
                        int LA24_124 = input.LA(1);

                        s = -1;
                        if ( ((LA24_124>='\u0000' && LA24_124<='\b')||(LA24_124>='\u000B' && LA24_124<='\f')||(LA24_124>='\u000E' && LA24_124<='\u001F')||(LA24_124>='!' && LA24_124<='\uFFFF')) ) {s = 27;}

                        else s = 125;

                        if ( s>=0 ) return s;
                        break;
                    case 12 : 
                        int LA24_65 = input.LA(1);

                        s = -1;
                        if ( ((LA24_65>='0' && LA24_65<='9')) ) {s = 65;}

                        else if ( ((LA24_65>='\u0000' && LA24_65<='\b')||(LA24_65>='\u000B' && LA24_65<='\f')||(LA24_65>='\u000E' && LA24_65<='\u001F')||(LA24_65>='!' && LA24_65<='/')||(LA24_65>=':' && LA24_65<='\uFFFF')) ) {s = 27;}

                        else s = 38;

                        if ( s>=0 ) return s;
                        break;
                    case 13 : 
                        int LA24_47 = input.LA(1);

                        s = -1;
                        if ( (LA24_47=='\'') ) {s = 70;}

                        else if ( ((LA24_47>='\u0000' && LA24_47<='\b')||(LA24_47>='\u000B' && LA24_47<='\f')||(LA24_47>='\u000E' && LA24_47<='\u001F')||(LA24_47>='!' && LA24_47<='&')||(LA24_47>='(' && LA24_47<='\uFFFF')) ) {s = 27;}

                        else s = 46;

                        if ( s>=0 ) return s;
                        break;
                    case 14 : 
                        int LA24_69 = input.LA(1);

                        s = -1;
                        if ( ((LA24_69>='\u0000' && LA24_69<='\b')||(LA24_69>='\u000B' && LA24_69<='\f')||(LA24_69>='\u000E' && LA24_69<='\u001F')||(LA24_69>='!' && LA24_69<='\uFFFF')) ) {s = 27;}

                        else s = 46;

                        if ( s>=0 ) return s;
                        break;
                    case 15 : 
                        int LA24_118 = input.LA(1);

                        s = -1;
                        if ( ((LA24_118>='\u0000' && LA24_118<='\b')||(LA24_118>='\u000B' && LA24_118<='\f')||(LA24_118>='\u000E' && LA24_118<='\u001F')||(LA24_118>='!' && LA24_118<='\uFFFF')) ) {s = 27;}

                        else s = 122;

                        if ( s>=0 ) return s;
                        break;
                    case 16 : 
                        int LA24_72 = input.LA(1);

                        s = -1;
                        if ( ((LA24_72>='\u0000' && LA24_72<='\b')||(LA24_72>='\u000B' && LA24_72<='\f')||(LA24_72>='\u000E' && LA24_72<='\u001F')||(LA24_72>='!' && LA24_72<='\uFFFF')) ) {s = 27;}

                        else s = 46;

                        if ( s>=0 ) return s;
                        break;
                    case 17 : 
                        int LA24_75 = input.LA(1);

                        s = -1;
                        if ( (LA24_75=='*') ) {s = 73;}

                        else if ( ((LA24_75>='\u0000' && LA24_75<='\b')||(LA24_75>='\u000B' && LA24_75<='\f')||(LA24_75>='\u000E' && LA24_75<='\u001F')||(LA24_75>='!' && LA24_75<=')')||(LA24_75>='+' && LA24_75<='\uFFFF')) ) {s = 75;}

                        else if ( ((LA24_75>='\t' && LA24_75<='\n')||LA24_75=='\r'||LA24_75==' ') ) {s = 74;}

                        else s = 27;

                        if ( s>=0 ) return s;
                        break;
                    case 18 : 
                        int LA24_14 = input.LA(1);

                        s = -1;
                        if ( (LA24_14=='\'') ) {s = 47;}

                        else if ( (LA24_14=='\\') ) {s = 48;}

                        else if ( ((LA24_14>='\u0000' && LA24_14<='\b')||(LA24_14>='\u000B' && LA24_14<='\f')||(LA24_14>='\u000E' && LA24_14<='\u001F')||(LA24_14>='!' && LA24_14<='&')||(LA24_14>='(' && LA24_14<='[')||(LA24_14>=']' && LA24_14<='\uFFFF')) ) {s = 49;}

                        else if ( (LA24_14=='\t'||LA24_14==' ') ) {s = 46;}

                        else s = 27;

                        if ( s>=0 ) return s;
                        break;
                    case 19 : 
                        int LA24_16 = input.LA(1);

                        s = -1;
                        if ( ((LA24_16>='\u0000' && LA24_16<='\b')||(LA24_16>='\u000B' && LA24_16<='\f')||(LA24_16>='\u000E' && LA24_16<='\u001F')||(LA24_16>='!' && LA24_16<='\uFFFF')) ) {s = 52;}

                        else s = 42;

                        if ( s>=0 ) return s;
                        break;
                    case 20 : 
                        int LA24_0 = input.LA(1);

                        s = -1;
                        if ( (LA24_0=='G') ) {s = 1;}

                        else if ( (LA24_0=='W') ) {s = 2;}

                        else if ( (LA24_0=='T') ) {s = 3;}

                        else if ( (LA24_0=='A') ) {s = 4;}

                        else if ( (LA24_0=='B') ) {s = 5;}

                        else if ( (LA24_0=='F') ) {s = 6;}

                        else if ( (LA24_0=='S') ) {s = 7;}

                        else if ( (LA24_0=='E') ) {s = 8;}

                        else if ( (LA24_0=='-') ) {s = 9;}

                        else if ( ((LA24_0>='0' && LA24_0<='9')) ) {s = 10;}

                        else if ( (LA24_0=='<') ) {s = 11;}

                        else if ( (LA24_0=='|') ) {s = 12;}

                        else if ( (LA24_0=='\"') ) {s = 13;}

                        else if ( (LA24_0=='\'') ) {s = 14;}

                        else if ( (LA24_0=='#') ) {s = 15;}

                        else if ( (LA24_0=='@') ) {s = 16;}

                        else if ( (LA24_0=='^') ) {s = 17;}

                        else if ( (LA24_0=='\t'||LA24_0==' ') ) {s = 18;}

                        else if ( (LA24_0=='\r') ) {s = 19;}

                        else if ( (LA24_0=='\n') ) {s = 20;}

                        else if ( ((LA24_0>='C' && LA24_0<='D')||(LA24_0>='H' && LA24_0<='R')||(LA24_0>='U' && LA24_0<='V')||(LA24_0>='X' && LA24_0<='Z')||LA24_0=='_'||(LA24_0>='a' && LA24_0<='z')) ) {s = 22;}

                        else if ( (LA24_0=='/') ) {s = 23;}

                        else if ( ((LA24_0>='\u0000' && LA24_0<='\b')||(LA24_0>='\u000B' && LA24_0<='\f')||(LA24_0>='\u000E' && LA24_0<='\u001F')||LA24_0=='!'||(LA24_0>='$' && LA24_0<='&')||(LA24_0>='(' && LA24_0<=',')||LA24_0=='.'||(LA24_0>=':' && LA24_0<=';')||(LA24_0>='=' && LA24_0<='?')||(LA24_0>='[' && LA24_0<=']')||LA24_0=='`'||LA24_0=='{'||(LA24_0>='}' && LA24_0<='\uFFFF')) ) {s = 24;}

                        else s = 21;

                        if ( s>=0 ) return s;
                        break;
                    case 21 : 
                        int LA24_55 = input.LA(1);

                        s = -1;
                        if ( (LA24_55=='*') ) {s = 73;}

                        else if ( ((LA24_55>='\t' && LA24_55<='\n')||LA24_55=='\r'||LA24_55==' ') ) {s = 74;}

                        else if ( ((LA24_55>='\u0000' && LA24_55<='\b')||(LA24_55>='\u000B' && LA24_55<='\f')||(LA24_55>='\u000E' && LA24_55<='\u001F')||(LA24_55>='!' && LA24_55<=')')||(LA24_55>='+' && LA24_55<='\uFFFF')) ) {s = 75;}

                        else s = 27;

                        if ( s>=0 ) return s;
                        break;
                    case 22 : 
                        int LA24_107 = input.LA(1);

                        s = -1;
                        if ( (LA24_107=='\'') ) {s = 107;}

                        else if ( ((LA24_107>='\u0000' && LA24_107<='\b')||(LA24_107>='\u000B' && LA24_107<='\f')||(LA24_107>='\u000E' && LA24_107<='\u001F')||(LA24_107>='!' && LA24_107<='&')||(LA24_107>='(' && LA24_107<='\uFFFF')) ) {s = 90;}

                        else s = 87;

                        if ( s>=0 ) return s;
                        break;
                    case 23 : 
                        int LA24_68 = input.LA(1);

                        s = -1;
                        if ( (LA24_68=='\"') ) {s = 69;}

                        else if ( (LA24_68=='\\') ) {s = 44;}

                        else if ( ((LA24_68>='\u0000' && LA24_68<='\b')||(LA24_68>='\u000B' && LA24_68<='\f')||(LA24_68>='\u000E' && LA24_68<='\u001F')||LA24_68=='!'||(LA24_68>='#' && LA24_68<='[')||(LA24_68>=']' && LA24_68<='\uFFFF')) ) {s = 45;}

                        else if ( (LA24_68=='\t'||LA24_68==' ') ) {s = 46;}

                        else s = 27;

                        if ( s>=0 ) return s;
                        break;
                    case 24 : 
                        int LA24_120 = input.LA(1);

                        s = -1;
                        if ( ((LA24_120>='\u0000' && LA24_120<='\b')||(LA24_120>='\u000B' && LA24_120<='\f')||(LA24_120>='\u000E' && LA24_120<='\u001F')||(LA24_120>='!' && LA24_120<='\uFFFF')) ) {s = 27;}

                        else s = 123;

                        if ( s>=0 ) return s;
                        break;
                    case 25 : 
                        int LA24_45 = input.LA(1);

                        s = -1;
                        if ( (LA24_45=='\"') ) {s = 69;}

                        else if ( (LA24_45=='\\') ) {s = 44;}

                        else if ( ((LA24_45>='\u0000' && LA24_45<='\b')||(LA24_45>='\u000B' && LA24_45<='\f')||(LA24_45>='\u000E' && LA24_45<='\u001F')||LA24_45=='!'||(LA24_45>='#' && LA24_45<='[')||(LA24_45>=']' && LA24_45<='\uFFFF')) ) {s = 45;}

                        else if ( (LA24_45=='\t'||LA24_45==' ') ) {s = 46;}

                        else s = 27;

                        if ( s>=0 ) return s;
                        break;
                    case 26 : 
                        int LA24_10 = input.LA(1);

                        s = -1;
                        if ( (LA24_10=='.') ) {s = 37;}

                        else if ( ((LA24_10>='0' && LA24_10<='9')) ) {s = 39;}

                        else if ( ((LA24_10>='\u0000' && LA24_10<='\b')||(LA24_10>='\u000B' && LA24_10<='\f')||(LA24_10>='\u000E' && LA24_10<='\u001F')||(LA24_10>='!' && LA24_10<='-')||LA24_10=='/'||(LA24_10>=':' && LA24_10<='\uFFFF')) ) {s = 27;}

                        else s = 38;

                        if ( s>=0 ) return s;
                        break;
                    case 27 : 
                        int LA24_67 = input.LA(1);

                        s = -1;
                        if ( (LA24_67=='\"') ) {s = 86;}

                        else if ( ((LA24_67>='\t' && LA24_67<='\n')||LA24_67=='\r'||LA24_67==' ') ) {s = 87;}

                        else if ( ((LA24_67>='\u0000' && LA24_67<='\b')||(LA24_67>='\u000B' && LA24_67<='\f')||(LA24_67>='\u000E' && LA24_67<='\u001F')||LA24_67=='!'||(LA24_67>='#' && LA24_67<='\uFFFF')) ) {s = 88;}

                        else s = 27;

                        if ( s>=0 ) return s;
                        break;
                    case 28 : 
                        int LA24_99 = input.LA(1);

                        s = -1;
                        if ( (LA24_99=='\"') ) {s = 106;}

                        else if ( ((LA24_99>='\u0000' && LA24_99<='\b')||(LA24_99>='\u000B' && LA24_99<='\f')||(LA24_99>='\u000E' && LA24_99<='\u001F')||LA24_99=='!'||(LA24_99>='#' && LA24_99<='\uFFFF')) ) {s = 88;}

                        else if ( ((LA24_99>='\t' && LA24_99<='\n')||LA24_99=='\r'||LA24_99==' ') ) {s = 87;}

                        else s = 27;

                        if ( s>=0 ) return s;
                        break;
                    case 29 : 
                        int LA24_86 = input.LA(1);

                        s = -1;
                        if ( (LA24_86=='\"') ) {s = 99;}

                        else if ( ((LA24_86>='\u0000' && LA24_86<='\b')||(LA24_86>='\u000B' && LA24_86<='\f')||(LA24_86>='\u000E' && LA24_86<='\u001F')||LA24_86=='!'||(LA24_86>='#' && LA24_86<='\uFFFF')) ) {s = 88;}

                        else if ( ((LA24_86>='\t' && LA24_86<='\n')||LA24_86=='\r'||LA24_86==' ') ) {s = 87;}

                        else s = 27;

                        if ( s>=0 ) return s;
                        break;
                    case 30 : 
                        int LA24_66 = input.LA(1);

                        s = -1;
                        if ( ((LA24_66>='\u0000' && LA24_66<='\b')||(LA24_66>='\u000B' && LA24_66<='\f')||(LA24_66>='\u000E' && LA24_66<='\u001F')||(LA24_66>='!' && LA24_66<='\uFFFF')) ) {s = 27;}

                        else s = 85;

                        if ( s>=0 ) return s;
                        break;
                    case 31 : 
                        int LA24_11 = input.LA(1);

                        s = -1;
                        if ( ((LA24_11>='\u0000' && LA24_11<='\b')||(LA24_11>='\u000B' && LA24_11<='\f')||(LA24_11>='\u000E' && LA24_11<='\u001F')||(LA24_11>='!' && LA24_11<='=')||(LA24_11>='?' && LA24_11<='\uFFFF')) ) {s = 40;}

                        else s = 27;

                        if ( s>=0 ) return s;
                        break;
                    case 32 : 
                        int LA24_92 = input.LA(1);

                        s = -1;
                        if ( ((LA24_92>='0' && LA24_92<='9')||(LA24_92>='A' && LA24_92<='Z')||LA24_92=='_'||(LA24_92>='a' && LA24_92<='z')) ) {s = 26;}

                        else if ( ((LA24_92>='\u0000' && LA24_92<='\b')||(LA24_92>='\u000B' && LA24_92<='\f')||(LA24_92>='\u000E' && LA24_92<='\u001F')||(LA24_92>='!' && LA24_92<='/')||(LA24_92>=':' && LA24_92<='@')||(LA24_92>='[' && LA24_92<='^')||LA24_92=='`'||(LA24_92>='{' && LA24_92<='\uFFFF')) ) {s = 27;}

                        else s = 101;

                        if ( s>=0 ) return s;
                        break;
                    case 33 : 
                        int LA24_13 = input.LA(1);

                        s = -1;
                        if ( (LA24_13=='\"') ) {s = 43;}

                        else if ( (LA24_13=='\\') ) {s = 44;}

                        else if ( ((LA24_13>='\u0000' && LA24_13<='\b')||(LA24_13>='\u000B' && LA24_13<='\f')||(LA24_13>='\u000E' && LA24_13<='\u001F')||LA24_13=='!'||(LA24_13>='#' && LA24_13<='[')||(LA24_13>=']' && LA24_13<='\uFFFF')) ) {s = 45;}

                        else if ( (LA24_13=='\t'||LA24_13==' ') ) {s = 46;}

                        else s = 27;

                        if ( s>=0 ) return s;
                        break;
                    case 34 : 
                        int LA24_88 = input.LA(1);

                        s = -1;
                        if ( (LA24_88=='\"') ) {s = 86;}

                        else if ( ((LA24_88>='\u0000' && LA24_88<='\b')||(LA24_88>='\u000B' && LA24_88<='\f')||(LA24_88>='\u000E' && LA24_88<='\u001F')||LA24_88=='!'||(LA24_88>='#' && LA24_88<='\uFFFF')) ) {s = 88;}

                        else if ( ((LA24_88>='\t' && LA24_88<='\n')||LA24_88=='\r'||LA24_88==' ') ) {s = 87;}

                        else s = 27;

                        if ( s>=0 ) return s;
                        break;
                    case 35 : 
                        int LA24_77 = input.LA(1);

                        s = -1;
                        if ( ((LA24_77>='0' && LA24_77<='9')||(LA24_77>='A' && LA24_77<='Z')||LA24_77=='_'||(LA24_77>='a' && LA24_77<='z')) ) {s = 26;}

                        else if ( ((LA24_77>='\u0000' && LA24_77<='\b')||(LA24_77>='\u000B' && LA24_77<='\f')||(LA24_77>='\u000E' && LA24_77<='\u001F')||(LA24_77>='!' && LA24_77<='/')||(LA24_77>=':' && LA24_77<='@')||(LA24_77>='[' && LA24_77<='^')||LA24_77=='`'||(LA24_77>='{' && LA24_77<='\uFFFF')) ) {s = 27;}

                        else s = 93;

                        if ( s>=0 ) return s;
                        break;
                    case 36 : 
                        int LA24_15 = input.LA(1);

                        s = -1;
                        if ( ((LA24_15>='\u0000' && LA24_15<='\b')||(LA24_15>='\u000B' && LA24_15<='\f')||(LA24_15>='\u000E' && LA24_15<='\u001F')||(LA24_15>='!' && LA24_15<='\uFFFF')) ) {s = 50;}

                        else s = 51;

                        if ( s>=0 ) return s;
                        break;
                    case 37 : 
                        int LA24_78 = input.LA(1);

                        s = -1;
                        if ( ((LA24_78>='0' && LA24_78<='9')||(LA24_78>='A' && LA24_78<='Z')||LA24_78=='_'||(LA24_78>='a' && LA24_78<='z')) ) {s = 26;}

                        else if ( ((LA24_78>='\u0000' && LA24_78<='\b')||(LA24_78>='\u000B' && LA24_78<='\f')||(LA24_78>='\u000E' && LA24_78<='\u001F')||(LA24_78>='!' && LA24_78<='/')||(LA24_78>=':' && LA24_78<='@')||(LA24_78>='[' && LA24_78<='^')||LA24_78=='`'||(LA24_78>='{' && LA24_78<='\uFFFF')) ) {s = 27;}

                        else s = 94;

                        if ( s>=0 ) return s;
                        break;
                    case 38 : 
                        int LA24_70 = input.LA(1);

                        s = -1;
                        if ( (LA24_70=='\'') ) {s = 89;}

                        else if ( ((LA24_70>='\t' && LA24_70<='\n')||LA24_70=='\r'||LA24_70==' ') ) {s = 87;}

                        else if ( ((LA24_70>='\u0000' && LA24_70<='\b')||(LA24_70>='\u000B' && LA24_70<='\f')||(LA24_70>='\u000E' && LA24_70<='\u001F')||(LA24_70>='!' && LA24_70<='&')||(LA24_70>='(' && LA24_70<='\uFFFF')) ) {s = 90;}

                        else s = 27;

                        if ( s>=0 ) return s;
                        break;
                    case 39 : 
                        int LA24_59 = input.LA(1);

                        s = -1;
                        if ( ((LA24_59>='0' && LA24_59<='9')||(LA24_59>='A' && LA24_59<='Z')||LA24_59=='_'||(LA24_59>='a' && LA24_59<='z')) ) {s = 26;}

                        else if ( ((LA24_59>='\u0000' && LA24_59<='\b')||(LA24_59>='\u000B' && LA24_59<='\f')||(LA24_59>='\u000E' && LA24_59<='\u001F')||(LA24_59>='!' && LA24_59<='/')||(LA24_59>=':' && LA24_59<='@')||(LA24_59>='[' && LA24_59<='^')||LA24_59=='`'||(LA24_59>='{' && LA24_59<='\uFFFF')) ) {s = 27;}

                        else s = 79;

                        if ( s>=0 ) return s;
                        break;
                    case 40 : 
                        int LA24_100 = input.LA(1);

                        s = -1;
                        if ( (LA24_100=='\'') ) {s = 107;}

                        else if ( ((LA24_100>='\u0000' && LA24_100<='\b')||(LA24_100>='\u000B' && LA24_100<='\f')||(LA24_100>='\u000E' && LA24_100<='\u001F')||(LA24_100>='!' && LA24_100<='&')||(LA24_100>='(' && LA24_100<='\uFFFF')) ) {s = 90;}

                        else if ( ((LA24_100>='\t' && LA24_100<='\n')||LA24_100=='\r'||LA24_100==' ') ) {s = 87;}

                        else s = 27;

                        if ( s>=0 ) return s;
                        break;
                    case 41 : 
                        int LA24_73 = input.LA(1);

                        s = -1;
                        if ( (LA24_73=='/') ) {s = 91;}

                        else if ( (LA24_73=='*') ) {s = 73;}

                        else if ( ((LA24_73>='\u0000' && LA24_73<='\b')||(LA24_73>='\u000B' && LA24_73<='\f')||(LA24_73>='\u000E' && LA24_73<='\u001F')||(LA24_73>='!' && LA24_73<=')')||(LA24_73>='+' && LA24_73<='.')||(LA24_73>='0' && LA24_73<='\uFFFF')) ) {s = 75;}

                        else if ( ((LA24_73>='\t' && LA24_73<='\n')||LA24_73=='\r'||LA24_73==' ') ) {s = 74;}

                        else s = 27;

                        if ( s>=0 ) return s;
                        break;
                    case 42 : 
                        int LA24_89 = input.LA(1);

                        s = -1;
                        if ( (LA24_89=='\'') ) {s = 100;}

                        else if ( ((LA24_89>='\u0000' && LA24_89<='\b')||(LA24_89>='\u000B' && LA24_89<='\f')||(LA24_89>='\u000E' && LA24_89<='\u001F')||(LA24_89>='!' && LA24_89<='&')||(LA24_89>='(' && LA24_89<='\uFFFF')) ) {s = 90;}

                        else if ( ((LA24_89>='\t' && LA24_89<='\n')||LA24_89=='\r'||LA24_89==' ') ) {s = 87;}

                        else s = 27;

                        if ( s>=0 ) return s;
                        break;
                    case 43 : 
                        int LA24_60 = input.LA(1);

                        s = -1;
                        if ( ((LA24_60>='0' && LA24_60<='9')||(LA24_60>='A' && LA24_60<='Z')||LA24_60=='_'||(LA24_60>='a' && LA24_60<='z')) ) {s = 26;}

                        else if ( ((LA24_60>='\u0000' && LA24_60<='\b')||(LA24_60>='\u000B' && LA24_60<='\f')||(LA24_60>='\u000E' && LA24_60<='\u001F')||(LA24_60>='!' && LA24_60<='/')||(LA24_60>=':' && LA24_60<='@')||(LA24_60>='[' && LA24_60<='^')||LA24_60=='`'||(LA24_60>='{' && LA24_60<='\uFFFF')) ) {s = 27;}

                        else s = 80;

                        if ( s>=0 ) return s;
                        break;
                    case 44 : 
                        int LA24_90 = input.LA(1);

                        s = -1;
                        if ( (LA24_90=='\'') ) {s = 89;}

                        else if ( ((LA24_90>='\u0000' && LA24_90<='\b')||(LA24_90>='\u000B' && LA24_90<='\f')||(LA24_90>='\u000E' && LA24_90<='\u001F')||(LA24_90>='!' && LA24_90<='&')||(LA24_90>='(' && LA24_90<='\uFFFF')) ) {s = 90;}

                        else if ( ((LA24_90>='\t' && LA24_90<='\n')||LA24_90=='\r'||LA24_90==' ') ) {s = 87;}

                        else s = 27;

                        if ( s>=0 ) return s;
                        break;
            }
            NoViableAltException nvae =
                new NoViableAltException(getDescription(), 24, _s, input);
            error(nvae);
            throw nvae;
        }
    }
 

}